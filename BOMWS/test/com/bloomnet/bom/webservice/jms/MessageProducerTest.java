package com.bloomnet.bom.webservice.jms;


import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.mvc.jms.MessageProducer;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testWebServices.xml"})  
public class MessageProducerTest {
	
	@Autowired MessageProducer messageProducer;

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testProduceMessage(){
		String message="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><foreignSystemInterface><security><username>M177</username><password>qa</password><shopCode>M1770000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>M1770000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>1001</externalShopOrderNumber><externalShopMessageNumber>1001</externalShopMessageNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20111114022517</messageCreateTimestamp><orderDetails><orderNumber>3419776</orderNumber><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription><productCode>2962</productCode></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType></orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>12/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Danil</recipientFirstName><recipientLastName>Svirchtchev</recipientLastName><recipientAddress1>139 paramus Road</recipientAddress1><recipientAddress2></recipientAddress2><recipientCity>Rutherford</recipientCity><recipientState>NJ</recipientState><recipientZipCode>64109</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>5551234678</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><shippingDate xsi:nil=\"true\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"/></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";
		//String message="<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>D734</username><password>qa</password><shopCode>D7340000</shopCode></security><errors/><messagesOnOrder><messageCount>1</messageCount><messageInqr><messageType>1</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>M1770000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419776</bmtOrderNumber><bmtSeqNumberOfOrder></bmtSeqNumberOfOrder><bmtSeqNumberOfMessage/><externalShopOrderNumber/><externalShopMessageNumber>10027</externalShopMessageNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20060204050607</messageCreateTimestamp><messageText>my INQR message</messageText></messageInqr></messagesOnOrder></foreignSystemInterface>";
		//String message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>D734</username><password>qa</password><shopCode>D7340000</shopCode></security><errors/><messagesOnOrder><messageCount>1</messageCount><messageResp><messageType>2</messageType><sendingShopCode>D7340000</sendingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419776</bmtOrderNumber><bmtSeqNumberOfOrder>28129275</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage/><externalShopOrderNumber/><externalShopMessageNumber>10002</externalShopMessageNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20060204050607</messageCreateTimestamp><messageText>my RESP message</messageText></messageResp></messagesOnOrder></foreignSystemInterface>";
		//String message = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>D734</username><password>qa</password><shopCode>D7340000</shopCode></security><errors/><messagesOnOrder><messageCount>1</messageCount><messageCanc><messageType>3</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>M1770000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419776</bmtOrderNumber><bmtSeqNumberOfOrder/><bmtSeqNumberOfMessage/><externalShopOrderNumber>10000</externalShopOrderNumber><externalShopMessageNumber>10005</externalShopMessageNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20060604050607</messageCreateTimestamp><messageText>I have to cancel this order</messageText></messageCanc></messagesOnOrder></foreignSystemInterface>";
		String orderNumber=new String();
		String destination="";
		
		orderNumber="123";
		destination="BMTFSI";
		String skillset = "2";
		String sendingShopCode = "Z999";
		//String orderType = Byte.toString(BOMConstants.ORDER_TYPE_BMT);
//public void produceMessage(String destination, String message, final String orderNumber,final String sendingShopCode,  final String deliveryDate,
		//final String city,final String zip, final String occasionCode,final String skillset, final String messageType, final String orderType) {
			
		messageProducer.produceMessage(destination, message, orderNumber,sendingShopCode, "ORDER", skillset);
	}
	
	@Test
	public void testBrowseQueue(){
		List<String> tloOrders  = new ArrayList<String>();
		tloOrders = messageProducer.BrowseQueue();
		
		System.out.println(tloOrders.size());
	}

}
