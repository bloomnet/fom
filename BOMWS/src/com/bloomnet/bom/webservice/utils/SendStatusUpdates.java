package com.bloomnet.bom.webservice.utils;

import java.util.Date;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;

public class SendStatusUpdates {
	
	@Autowired private Properties fsiProperties;
	@Autowired private ShopDAO shopDAO;
	@Autowired private OrderDAO orderDAO;
	@Autowired private TransformService transformService;	

	
	public SendStatusUpdates(){
		//default
	}
	
	private MessageInfo setStatusUpdateFramework(Bomorder bomOrder){
		
		Shop receivingShop = bomOrder.getShopBySendingShopId();

		
		Bomorder originalBMTOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
		String xml = originalBMTOrder.getOrderXml();
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		
		Identifiers identifiers = fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers();
			
		MessageInfo message = new MessageInfo();
		message.setFulfillingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		message.setSendingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		message.setReceivingShopCode(shopDAO.getBloomNetShopnetwork(receivingShop.getShopId()).getShopCode());
		message.setIdentifiers(identifiers);
		message.setMessageCreateTimestamp(DateUtil.toString(new Date()));
		message.setMessageType(BOMConstants.INFO_MESSAGE_DESC);
		message.setSystemType(BOMConstants.SYSTEM_TYPE);
		
		return message;
	}
	
	public MessageInfo sendSentToShop(Bomorder bomOrder, Shop fulfillingShop){
		
		MessageInfo message = setStatusUpdateFramework(bomOrder);
		message.setMessageText("Commitment to Coverage has sent your order to a shop for fulfillment. The shop's name is: "+fulfillingShop.getShopName()+". Their phone number is: "+fulfillingShop.getShopPhone()+" " +
				"All further communications will be automatically routed between you and the fulfilling shop. Thank you for sending your outgoing orders through BloomNet!");
		return message;
	}
	
	public MessageInfo sendRejectedByShop(Bomorder bomOrder){

		MessageInfo message = setStatusUpdateFramework(bomOrder);
		message.setMessageText("Your order was rejected by the shop we last sent it to. " +
				"Commitment to Coverage will attempt to find another shop to send it to and update you accordingly.");
		return message;
	}
	
	public MessageInfo sendSentToTLO(Bomorder bomOrder){
		
		MessageInfo message = setStatusUpdateFramework(bomOrder);
		message.setMessageText("Commitment to Coverage's automated system was not able to locate a shop to send your order to. " +
				"The order is now being passed along to the BloomNet team. All further communications will be forwarded to them.");
		return message;
	}

}
