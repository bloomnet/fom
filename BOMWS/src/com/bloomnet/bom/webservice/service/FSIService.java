package com.bloomnet.bom.webservice.service;

public interface FSIService {

	/**
	 * place message chunk on queue then consume it Convert to fsi object
	 * then traverse and process messages
	 * @param msg
	 * @throws Exception 
	 */
	public abstract void processMessage(String msg) throws Exception;

}