package com.bloomnet.bom.webservice.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.mdi.Shop;

public interface FSIAgentService {

	/**
	 * Handles order received from message listener
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public abstract void handleOrder(String orderXML, String OrderNumber,
			String orderType) throws Exception;

	
	/**
	 * Searches for a shop to fulfill order
	 */

	public abstract String searchForShops(MessageOrder order, int count)
			throws Exception;

	/**
	 * Send order to queue
	 * 
	 * @param message
	 * @param orderNumber
	 * @param sendingShopCode
	 * @param messageType
	 * @param skillset
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public abstract void sendOrderToQueue(String message, String orderNumber,
			String sendingShopCode, String messageType, String skillset,
			String deliveryDate, String city, String zip, String occasionCode,
			String orderType, String timeZone);

}