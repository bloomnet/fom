package com.bloomnet.bom.webservice.service;

import javax.jms.JMSException;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.Bomorder;

public interface TFSIService {

	public abstract String sendOrder(MessageOnOrderBean moob);

	public abstract void resendOrder(String orderNumber);

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void sendToTFMessagesQueue(String xml, String orderNumber,
			String sendingShopCode, String messageType, String bundleId) throws JMSException;


	void getMessages();

	String[] getOriginalVars(Bomorder bomOrder);

	void sendMessageToBloomlink(String messageXml, String messageType)
			throws JMSException;

	void sendInqr(MessageOnOrderBean moob);

	void sendInqrResponse(MessageOnOrderBean moob);

	void sendRjt(MessageOnOrderBean moob);

	void sendCanc(MessageOnOrderBean moob);

	void sendPchg(MessageOnOrderBean moob);

	void processMessage(String xml, String bundleId, String messageType,
			String sendingShopCode, String orderNumber) throws JMSException;
	
	Integer getLastSeqNumber();

}