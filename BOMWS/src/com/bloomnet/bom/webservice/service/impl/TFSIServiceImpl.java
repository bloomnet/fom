package com.bloomnet.bom.webservice.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.WebserviceMonitor;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.Error;
import com.bloomnet.bom.common.jaxb.fsi.Errors;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckf;
import com.bloomnet.bom.common.jaxb.fsi.MessageConf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDeni;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.tfsi.TFSIMessage;
import com.bloomnet.bom.common.tfsi.TFSIUtil;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.client.TelefloraWebServiceClient;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.webservice.service.TFSIService;
import com.bloomnet.bom.webservice.utils.SendStatusUpdates;

@Service("tfsiService")
@Transactional(propagation = Propagation.SUPPORTS)
public class TFSIServiceImpl implements TFSIService {
	
	@Autowired private TelefloraWebServiceClient twsc;
	
	@Autowired private MessageProducer messageProducer;
	
	@Autowired private OrderDAO orderDAO;
	@Autowired private ShopDAO shopDAO;
	@Autowired private MessageDAO messageDAO;
	@Autowired private UserDAO userDAO;
	
	@Autowired private Properties fsiProperties;
	@Autowired private Properties tfsiProperties;
	@Autowired private Properties bomProperties;
	
	@Autowired private FSIClient fsiClient;
	
	@Autowired private SendStatusUpdates sendStatusUpdates;
	@Autowired private TransformService transformService;	


	
	static Logger logger = Logger.getLogger( TFSIServiceImpl.class );
	
	TFSIUtil tfsiUtil = new TFSIUtil();
	
	public TFSIServiceImpl(){}
	
	public void setCommonVars(){
		twsc.setMessageSequenceNumber();
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIService#sendOrder()
	 */
	@Override
	public String sendOrder(MessageOnOrderBean moob){
		setCommonVars();
		String tempOrderNumber = "TFN"+new Date().getTime();
		twsc.sendOrderTransaction(tempOrderNumber, moob);
		
		return tempOrderNumber;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIService#resendOrder(java.lang.String)
	 */
	@Override
	public void resendOrder(String orderNumber){
			
		Bomorder bomOrder = orderDAO.getOrderByOrderNumber(orderNumber);
		
		String tfsiXML = bomOrder.getOrderXml();
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" + tfsiXML.split("<TFETransactionHeader>")[1].split("</TFETransactionHeader>")[0] + "</TFETransactionHeader>";
		String body = tfsiXML.split("</TFETransactionHeader>")[1];
		
		long originalSequenceNumber = Long.valueOf(bomOrder.getBmtOrderSequenceNumber());
		
		twsc.resendOrderTransaction(header, body, originalSequenceNumber);
	}
	
	@Override
	public void sendInqr(MessageOnOrderBean moob){
		setCommonVars();
		twsc.inquiryTransaction(moob);
	}
	
	@Override
	public void sendInqrResponse(MessageOnOrderBean moob){
		setCommonVars();
		twsc.inquiryResponseTransaction(moob);
	}
	
	@Override
	public void sendRjt(MessageOnOrderBean moob){
		setCommonVars();
		twsc.refusalTransaction(moob);
	}
	
	@Override
	public void sendCanc(MessageOnOrderBean moob){
		setCommonVars();
		twsc.cancelTransaction(moob);
	}
	
	@Override
	public void sendPchg(MessageOnOrderBean moob){
		setCommonVars();
		twsc.priceChangeTransaction(moob);
	}
	
	private Shop getOriginalSendingShop(TFSIMessage tfsiMessage, Bomorder bomOrder){
		
		Bomorder originalBMTOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
		
		Shop originalSendingShop = originalBMTOrder.getShopBySendingShopId();
		
		return originalSendingShop;
	}
	
	@Override
	public String[] getOriginalVars(Bomorder bomOrder){
		
		Transform.PROPERTIES = fsiProperties;

		
		Bomorder originalBMTOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
		
		String xml = originalBMTOrder.getOrderXml();
		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		
		String[] vars = {fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getExternalShopMessageNumber(),
						 fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber(),
						 fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getInwireSequenceNo(),
						 fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(),
						 fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder(),
						 fsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber()};
		
		for(int ii=0; ii<vars.length; ++ii){
			logger.debug("General Identifier "+ii+": "+ vars[ii]);
		}
		
		return vars;
	}
	
	private String convertToFSIXML(Object obj, Bomorder bomOrder, Shop originalSendingShop, 
			String originalSendingShopCode, String messageText, String messageType, TFSIMessage tfsiMessage) throws JMSException{
		
		Transform.PROPERTIES = fsiProperties;

		
		MessageOnOrderBean moob = new MessageOnOrderBean();
		
		logger.debug("Original Sending Shop Code: "+originalSendingShopCode);
		logger.debug("Original Sending Shop Name From Object: "+originalSendingShop.getShopName());
		
		String[] originalGeneralIdentifiers = getOriginalVars(bomOrder);
		
		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();
		
		generalIdentifiers.setExternalShopMessageNumber(originalGeneralIdentifiers[0]);
		generalIdentifiers.setExternalShopOrderNumber(originalGeneralIdentifiers[1]);
		generalIdentifiers.setInwireSequenceNo(originalGeneralIdentifiers[2]);
		generalIdentifiers.setBmtSeqNumberOfMessage(originalGeneralIdentifiers[3]);
		generalIdentifiers.setBmtSeqNumberOfOrder(originalGeneralIdentifiers[4]);
		generalIdentifiers.setBmtOrderNumber(originalGeneralIdentifiers[5]);
		
		identifiers.setGeneralIdentifiers(generalIdentifiers);
		
		String fsiShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
		
		moob.setSendingShop(bomOrder.getShopByReceivingShopId());
		moob.setReceivingShop(shopDAO.getShopByCode(fsiShopCode));
		moob.setStatus(Integer.valueOf(bomProperties.getProperty(BOMConstants.MESSAGE_SENT_TO_SHOP)));
		moob.setOrderNumber(originalGeneralIdentifiers[5]);
		moob.setCommMethod(BOMConstants.COMMMETHOD_API_ID);
		
		byte status = 5;
		int messageNumber = 0;
		
		try{
			tfsiUtil.saveMessage(
					messageDAO, 
					userDAO, 
					orderDAO, 
					moob, 
					messageType, 
					messageNumber, 
					tfsiMessage.getXmlMessage(), 
					messageText,
					status);
		}catch(Exception ee){
			throw new JMSException("Convert To FSIXML exception: " + ee.getMessage());
		}
		
		if(messageType.equals("ACKF")){
			((BloomNetMessage) obj).setFulfillingShopCode(fsiProperties.getProperty("fsi.shopcode"));
			((BloomNetMessage) obj).setSendingShopCode(originalSendingShopCode);
			((BloomNetMessage) obj).setReceivingShopCode(fsiProperties.getProperty("fsi.shopcode"));
		}else{
			((BloomNetMessage) obj).setFulfillingShopCode(fsiProperties.getProperty("fsi.shopcode"));
			((BloomNetMessage) obj).setSendingShopCode(fsiProperties.getProperty("fsi.shopcode"));
			((BloomNetMessage) obj).setReceivingShopCode(originalSendingShopCode);
		}
		((BloomNetMessage) obj).setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		((BloomNetMessage) obj).setMessageStatus(bomProperties.getProperty("towork"));
		((BloomNetMessage) obj).setMessageType(messageType);
		((BloomNetMessage) obj).setSystemType(BOMConstants.SYSTEM_TYPE);
		((BloomNetMessage) obj).setIdentifiers(identifiers);
		
		logger.debug("Final Fulfilling Shop Code: "+ ((BloomNetMessage) obj).getFulfillingShopCode());
		logger.debug("Final Sending Shop Code: "+ ((BloomNetMessage) obj).getSendingShopCode());
		logger.debug("Final Receiving Shop Code: "+ ((BloomNetMessage) obj).getReceivingShopCode());
		
		try {
			String xml = transformService.createForeignSystemInterface(messageType, (BloomNetMessage) obj);
			
			logger.debug("xml created from Transform class: "+xml);
			
			int messageId = messageDAO.persistMessageAndActivity(
					userDAO.getUserByUserName(tfsiProperties.getProperty("userTFSI")).getUserId(), 
					generalIdentifiers, 
					xml, 
					messageType, 
					bomProperties.getProperty(BOMConstants.MESSAGE_SENT_TO_SHOP), 
					BOMConstants.COMMMETHOD_API, 
					fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), 
					originalSendingShopCode, 
					messageText);
			
			if(messageType.equals("ACKF")){
				updateMessage(originalGeneralIdentifiers[5], messageId);
			}
			
			return xml;
			
		} catch (Exception e) {
			throw new JMSException("Convert To FSIXML exception: " + e.getMessage());
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void updateMessage(String orderNumber, int messageId) throws Exception{
		
		// update the status of the original message
		String status = bomProperties.getProperty(BOMConstants.MESSAGE_WORK_COMPLETED);
		String messageIdStr = Integer.toString(messageId);
		long messageIdLong = Long.parseLong(messageIdStr);
		messageDAO.updateMessageStatus( messageIdLong , status );

	}
	
	/**
	 * retreives messages from teleflora based on a configured scheduler
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void getMessages(){
		
		logger.info("getMessages method called");
		
		Transform.PROPERTIES = fsiProperties;
		
		String dest1 = bomProperties.getProperty("first.destination");
		String dest2 = bomProperties.getProperty("second.destination");
		String dest3 = bomProperties.getProperty("third.destination");
		
		if(dest1.equalsIgnoreCase("TFSI") || dest2.equalsIgnoreCase("TFSI") || dest3.equalsIgnoreCase("TFSI")) {

			ArrayList<TFSIMessage> tfsiMessages = (ArrayList<TFSIMessage>) twsc.getTransactions().getTfsiMessages();
			
			if ( tfsiMessages.size()>0){
				
			Network network = shopDAO.getNetwork("Teleflora");
			int numOfmessages = tfsiMessages.size();
			
			WebserviceMonitor webservicemonitor = new WebserviceMonitor();
			//WebserviceMonitor( network, new Date(),Long.parseLong(Integer.toString(numOfmessages)));
			webservicemonitor.setNetwork(network);
			webservicemonitor.setStarttime(new Date());
			webservicemonitor.setLastquery(new Date());
			long n_mgs = Long.parseLong(Integer.toString(numOfmessages));
			webservicemonitor.setNumOfMessages(n_mgs);
			shopDAO.saveOrUpdateBloomlinkQuery(webservicemonitor);
			}
			
	 		for(int ii=0; ii<tfsiMessages.size(); ++ii){
	 			
	 			TFSIMessage tfsiMessage = tfsiMessages.get(ii);
	 			
	 			String orderNumber = "";
	 			String xml = tfsiMessage.getXmlMessage();
	 			if(tfsiMessage.getMessageType().equals("Validation") && tfsiMessage.getValType().equals("Order")){
	 				orderNumber = tfsiMessage.getTIDNumber();
	 			}else{
	 				orderNumber = tfsiMessage.getOriginalTIDNumber();
	 			}
				String bundleId = tfsiMessage.getBundleId(); 
				String sendingShopCode = tfsiMessage.getSendingShopCode();
				String messageType = tfsiMessage.getMessageType();
				
				try {
					sendToTFMessagesQueue(xml, orderNumber, sendingShopCode, messageType, bundleId);
				} catch (JMSException e) {
				}
				sendAcknowledgement(bundleId);
	 		}
	 		
	 		if ( tfsiMessages.size()>0){
	 		Date finishTime = new Date();
			logger.debug("**********Finish Time: "+finishTime+"*****");
		
			try {
				Network network = shopDAO.getNetwork("Teleflora");
				WebserviceMonitor webservicemonitor;
				webservicemonitor = shopDAO.getLastQuery(network);
				webservicemonitor.setFinishtime(finishTime);
				shopDAO.saveOrUpdateBloomlinkQuery(webservicemonitor);
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
			
	 		}
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void processMessage(String xml, String bId, String mType, String sendingShopCode, String oNumber) throws JMSException{
		
			String USER_NAME = tfsiProperties.getProperty("userTFSI");

			
			TFSIMessage tfsiMessage = new TFSIMessage();
			
			tfsiMessage.setBundleId(bId);
			tfsiMessage.setMessageType(mType);
			tfsiMessage.setSendingShopCode(sendingShopCode);
			tfsiMessage.setTIDNumber(oNumber);
		
			try{
				
				try{
					tfsiMessage.setTIDNumber(xml.split("&lt;Tid&gt;")[1].substring(0,6));
				}catch(Exception ee){}
				try{
					tfsiMessage.setStatus(xml.split("Status=")[1].split(" ")[0].replaceAll("\"",""));
				}catch(Exception ee){}
				try{
					tfsiMessage.setOriginalSequenceNumber(Long.valueOf(xml.split("OriginalSequenceNo=")[1].split(" ")[0].replaceAll("\"","")));
				}catch(Exception ee){}
				try{
					tfsiMessage.setPrice(xml.split("Price=")[1].split(" ")[0].replaceAll("\"",""));
				}catch(Exception ee){}
				try{
					tfsiMessage.setValType(xml.split("&lt;MessageText Line=\"1\"&gt;")[1].split(" ")[1].split(" ")[0]);
				}catch(Exception ee){}
				try{
					tfsiMessage.setSubType(xml.split(" Type=")[1].split(" ")[0].replaceAll("\"",""));
				}catch(Exception ee){}
				try{
					tfsiMessage.setOriginalTIDNumber(xml.split("OrigTID=")[1].replaceAll("\"","").substring(0,6));
				}catch(Exception ee){}
				
				List<String> messageLines = new ArrayList<String>();
				
				for(int xx=1; xx<15; ++xx){
					try{
						String[] messageLine = xml.split("MessageText Line=\""+xx+"\"&gt;");
						String[] line = messageLine[1].split("&lt;/");
						String message = line[0];
						messageLines.add(message);
					}catch(Exception ee){}
				}
				
				String message = "";
				
				for(int xx=0; xx<messageLines.size(); ++xx){
					message += messageLines.get(xx)+" ";
				}
					
				tfsiMessage.setMessageText(message);
				tfsiMessage.setXmlMessage(xml);
			
				String tfsiMessageType = tfsiMessage.getMessageType();
			
				if(tfsiMessageType.equals("Validation") && tfsiMessage.getValType().equals("Order")){
					
					String status = tfsiMessage.getStatus();
					String orderNumber = tfsiMessage.getTIDNumber();
					
					if(status.equals("A")){
						
						Bomorder order = orderDAO.getTFOrderByBMTSeqNumber(tfsiMessage.getOriginalSequenceNumber());
						Bomorder parentOrder = orderDAO.getOrderByOrderId(order.getParentorderId());
						
						String parentOrderNumber = parentOrder.getOrderNumber();
						
						if(order.getOrderNumber().startsWith("TFN") && order.getOrderNumber().length() > 6) order.setOrderNumber(orderNumber);
						
						MessageAckf messageAckf = new MessageAckf();
						String messageType = BOMConstants.ACKF_MESSAGE_DESC;
						
						String messageText = "ACKF";
						
						Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, order);
						String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
						
						orderDAO.updateChildOrder(order, String.valueOf(parentOrder.getOrderNumber()));
					
						orderDAO.persistChildOrderAndActivity(order, 
								parentOrderNumber,
								USER_NAME, 
							    BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_ACCEPTED_BY_SHOP),
								BOMConstants.TFSI_QUEUE);
							
						
						String fsiXML = convertToFSIXML(messageAckf, order, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
						sendMessageToBloomlink(fsiXML, messageType);
						
						MessageInfo messageInfo = sendStatusUpdates.sendSentToShop(parentOrder, order.getShopByReceivingShopId());
						String messageXml = transformService.createForeignSystemInterface(BOMConstants.INFO_MESSAGE_DESC, messageInfo);
						sendMessageToBloomlink(messageXml,BOMConstants.INFO_MESSAGE_DESC);
						
					}else{
						
						
						Bomorder order = orderDAO.getTFOrderByBMTSeqNumber(tfsiMessage.getOriginalSequenceNumber());
						
						String rejectOrderNumber = "RJCT" + new Date().getTime();
						
						order.setOrderNumber(rejectOrderNumber);
						
						Bomorder parentOrder = orderDAO.getOrderByOrderId(order.getParentorderId());
						
						orderDAO.updateChildOrder(order, parentOrder.getOrderNumber());
						
						String parentXML = parentOrder.getOrderXml();
						String parentSendingShopCode = shopDAO.getBloomNetShopnetwork(parentOrder.getShopBySendingShopId().getShopId()).getShopCode();
						
						ForeignSystemInterface fsi = transformService.convertToJavaFSI(parentXML);
						
						String occassion = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
						String city = order.getCity().getName();
						String zip = order.getZip().getZipCode();

						orderDAO.persistChildOrderAndActivity(order, 
								parentOrder.getOrderNumber(),
								USER_NAME, 
							    BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP), 
								BOMConstants.TFSI_QUEUE);
													
						//update parent to be worked
						orderDAO.persistOrderactivityByUser( USER_NAME, BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED), 
								parentOrder.getOrderNumber(), 
								BOMConstants.TFSI_QUEUE );
						long childId = 0;
						String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(parentOrder.getOrderNumber());
						if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
							childId = orderDAO.getCurrentFulfillingOrder(parentOrder.getOrderNumber()).getBomorderId();
						}
						
						byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TFSI_QUEUE);
						BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(parentOrder.getBomorderId());
						bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
						bomorderSts.setStsRoutingId(Byte.parseByte(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)));
						bomorderSts.setVirtualqueueId(virtualqueueId);
						
						orderDAO.updateBomorderSts(bomorderSts, childId);
							
						
						Shop us = shopDAO.getShopByCode(tfsiProperties.getProperty("BloomNetShopCode"));
						
						MessageOnOrderBean moob = new MessageOnOrderBean();
						
						moob.setSendingShop(us);
						moob.setReceivingShop(us);
						moob.setCommMethod(BOMConstants.COMMMETHOD_API_ID);
						moob.setOrderNumber(rejectOrderNumber);
						
						long messageNum = 0;
						byte mStatus = 1;
						
						String messageText = tfsiMessage.getMessageText();
						if(messageText == null || messageText.equals("")) messageText = "Rejection of Order";

						tfsiUtil.saveMessage(
								messageDAO, 
								userDAO, 
								orderDAO, 
								moob, 
								BOMConstants.RJCT_MESSAGE_DESC,
								messageNum, 
								tfsiMessage.getXmlMessage(), 
								messageText, 
								mStatus);
						
						sendBackToTFSI(parentXML, parentOrder, parentSendingShopCode, occassion, city, zip);
					}
					
				}else if(tfsiMessageType.equals("InquiryResponse") && tfsiMessage.getSubType().equals("I")){
					
					String deliveryFlag = "";
					
					try{
						String[] words = tfsiMessage.getMessageText().split(" ");
						if(words[0].equals("DELIVERY") && words[1].equals("CONFIRMATION")){
							deliveryFlag="YES";
						}
					}catch(Exception ee){}
					
					if(deliveryFlag.equals("YES")){
						
						String dateDelivered = tfsiMessage.getMessageText().split("DATE DELIVERED: ")[1].split("TIME DELIVERED:")[0].replaceAll(" ", "");
						String signedBy = "";
						String messageText = "";
						try{
							signedBy = tfsiMessage.getMessageText().split("SIGNED BY:")[1];
						}catch(Exception ee){}
						
						if(signedBy.replaceAll(" ", "").equals("")) signedBy = "BloomNet";
						
						MessageDlcf messageDLCF = new MessageDlcf();
						
						dateDelivered = DateUtil.toXmlFormatString(DateUtil.toDateFormat(dateDelivered));
						if(dateDelivered.length() != 14)dateDelivered = DateUtil.toXmlFormatString(new Date());
						
						messageDLCF.setDateOrderDelivered(dateDelivered);
						messageDLCF.setSignature("Intended Recipient");
						
						logger.debug("Signature is: "+messageDLCF.getSignature());
						logger.debug("Delivery Date is:"+messageDLCF.getDateOrderDelivered());
						
						Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());			
						
						Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
						
						Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
						String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
						
						messageText = tfsiMessage.getMessageText();
						String messageType = BOMConstants.DLCF_MESSAGE_DESC;
						
						orderDAO.persistChildOrderAndActivity(bomOrder, 
								parentOrder.getOrderNumber(),
								USER_NAME, 
							    BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP),
								BOMConstants.TFSI_QUEUE);
						
						String fsiXML = convertToFSIXML(messageDLCF, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);						
						sendMessageToBloomlink(fsiXML, messageType);
						
						
					}else{
					
						MessageInqr messageInqr = new MessageInqr();
						messageInqr.setMessageText(tfsiMessage.getMessageText());
					
						Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
						
						Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
						String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
						
						String messageText = tfsiMessage.getMessageText();
						String messageType = BOMConstants.INQR_MESSAGE_DESC;
						
						String fsiXML = convertToFSIXML(messageInqr, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
						sendMessageToBloomlink(fsiXML, messageType);
						
					}
				
				}else if(tfsiMessageType.equals("InquiryResponse") && tfsiMessage.getSubType().equals("R")){
					
					String deliveryFlag = "";
					
					try{
						String[] words = tfsiMessage.getMessageText().split(" ");
						if(words[0].equals("DELIVERY") && words[1].equals("CONFIRMATION")){
							deliveryFlag="YES";
						}
					}catch(Exception ee){}
					
					if(deliveryFlag.equals("YES")){
						
						String dateDelivered = tfsiMessage.getMessageText().split("DATE DELIVERED: ")[1].split("TIME DELIVERED:")[0].replaceAll(" ", "");
						String signedBy = "";
						String messageText = "";
						try{
							signedBy = tfsiMessage.getMessageText().split("SIGNED BY:")[1];
						}catch(Exception ee){}
						
						if(signedBy.replaceAll(" ", "").equals("")) signedBy = "BloomNet";
						
						MessageDlcf messageDLCF = new MessageDlcf();
						
						dateDelivered = DateUtil.toXmlFormatString(DateUtil.toDateFormat(dateDelivered));
						if(dateDelivered.length() != 14)dateDelivered = DateUtil.toXmlFormatString(new Date());
						
						messageDLCF.setDateOrderDelivered(dateDelivered);
						messageDLCF.setSignature("Intended Recipient");
						
						logger.debug("Signature is: "+messageDLCF.getSignature());
						logger.debug("Delivery Date is:"+messageDLCF.getDateOrderDelivered());
						
						Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());			
						
						Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
						
						Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
						String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
						
						messageText = tfsiMessage.getMessageText();
						String messageType = BOMConstants.DLCF_MESSAGE_DESC;
						
						orderDAO.persistChildOrderAndActivity(bomOrder, 
								parentOrder.getOrderNumber(),
								USER_NAME, 
							    BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP),
								BOMConstants.TFSI_QUEUE);
							
						String fsiXML = convertToFSIXML(messageDLCF, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);						
						sendMessageToBloomlink(fsiXML, messageType);
						
						
					}else{
					
						MessageResp messageResp = new MessageResp();
						messageResp.setMessageText(tfsiMessage.getMessageText());
						
						Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
						
						Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
						String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
						
						String messageText = tfsiMessage.getMessageText();
						String messageType = BOMConstants.RESP_MESSAGE_DESC;
						
						String fsiXML = convertToFSIXML(messageResp, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
						sendMessageToBloomlink(fsiXML, messageType);
						
					}
					
				}else if(tfsiMessageType.equals("ConfirmationMessage")){
					
					Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
					Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
					
					String messageText = tfsiMessage.getMessageText();
					
					orderDAO.persistChildOrderAndActivity(bomOrder, 
							parentOrder.getOrderNumber(),
							USER_NAME, 
						    BOMConstants.ACT_ROUTING, 
							bomProperties.getProperty(BOMConstants.ORDER_ACCEPTED_BY_SHOP),
							BOMConstants.TFSI_QUEUE);
					
					Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
					String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
					
					MessageAckf messageAckf = new MessageAckf();
					String messageType = BOMConstants.ACKF_MESSAGE_DESC;
					
					String fsiXML = convertToFSIXML(messageAckf, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
					sendMessageToBloomlink(fsiXML, messageType);
					
				
				}else if(tfsiMessageType.equals("RefuseOrder")){
					
					Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
					Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
					
					WebAppOrder wao = new WebAppOrder(bomOrder);
					
					if(wao != null && wao.getStatus() != null && !wao.getStatus().equals(BOMConstants.REJECTED_BY_SHOP)){
					
						orderDAO.persistChildOrderAndActivity(bomOrder, 
							parentOrder.getOrderNumber(),
							USER_NAME, 
						    BOMConstants.ACT_ROUTING, 
							bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP),
							BOMConstants.TFSI_QUEUE);
						//update parent to be worked
							
						orderDAO.persistOrderactivityByUser( USER_NAME, BOMConstants.ACT_ROUTING, 
								bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED), 
								parentOrder.getOrderNumber(), 
								BOMConstants.TFSI_QUEUE );
						long childId = 0;
						String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(parentOrder.getOrderNumber());
						if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
							childId = orderDAO.getCurrentFulfillingOrder(parentOrder.getOrderNumber()).getBomorderId();
						}
						
						byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TFSI_QUEUE);
						BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(parentOrder.getBomorderId());
						bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
						bomorderSts.setStsRoutingId(Byte.parseByte(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)));
						bomorderSts.setVirtualqueueId(virtualqueueId);
						
						orderDAO.updateBomorderSts(bomorderSts, childId);
						
						String parentXML = parentOrder.getOrderXml();
						String parentSendingShopCode = shopDAO.getBloomNetShopnetwork(parentOrder.getShopBySendingShopId().getShopId()).getShopCode();
						
						ForeignSystemInterface fsi = transformService.convertToJavaFSI(parentXML);
						
						String occassion = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
						String city = bomOrder.getCity().getName();
						String zip = bomOrder.getZip().getZipCode();
						
						sendBackToTFSI(parentXML, parentOrder, parentSendingShopCode, occassion, city, zip);
						
						String fsiShopCode = fsiProperties.getProperty("fsi.shopcode");
						
						MessageOnOrderBean moob = new MessageOnOrderBean();
						
						moob.setSendingShop(shopDAO.getShopByCode(tfsiMessage.getSendingShopCode()));
						moob.setReceivingShop(shopDAO.getShopByCode(fsiShopCode));
						moob.setStatus(Integer.valueOf(bomProperties.getProperty("mesage.sentshop")));
						moob.setOrderNumber(parentOrder.getOrderNumber());
						moob.setCommMethod(BOMConstants.COMMMETHOD_API_ID);
						
						byte status = 5;
						String messageText = tfsiMessage.getMessageText();
						if(messageText == null || messageText.equals("")) messageText = "Rejection of Order";
						
						tfsiUtil.saveMessage(
								messageDAO, 
								userDAO, 
								orderDAO, 
								moob, 
								BOMConstants.RJCT_MESSAGE_DESC, 
								tfsiMessage.getOriginalSequenceNumber(), 
								tfsiMessage.getXmlMessage(), 
								messageText,
								status);
					}
					
				
				}else if(tfsiMessageType.equals("ConfirmDenial") && tfsiMessage.getSubType().equals("C")){
					
					MessageConf messageConf = new MessageConf();
					
					Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
					
					Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
					
					Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
					String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
					
					String messageText = tfsiMessage.getMessageText();
					if(messageText == null || messageText.equals("")) messageText = "Confirmation of Cancellation";
					String messageType = BOMConstants.CONF_MESSAGE_DESC;
					
					messageConf.setMessageText(messageText);
						
					orderDAO.persistChildOrderAndActivity(bomOrder, 
							parentOrder.getOrderNumber(),
							USER_NAME, 
						    BOMConstants.ACT_ROUTING, 
							bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF),
							BOMConstants.TFSI_QUEUE);
					
					String fsiXML = convertToFSIXML(messageConf, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
					sendMessageToBloomlink(fsiXML, messageType);
					
				
				}else if(tfsiMessageType.equals("ConfirmDenial") && tfsiMessage.getSubType().equals("D")){
					
					MessageDeni messageDeni = new MessageDeni();
					
					Bomorder bomOrder = orderDAO.getOrderByOrderNumber(tfsiMessage.getOriginalTIDNumber());
					
					Bomorder parentOrder = orderDAO.getOrderByOrderId(bomOrder.getParentorderId());
					
					Shop originalSendingShop = getOriginalSendingShop(tfsiMessage, bomOrder);
					String originalSendingShopCode = shopDAO.getBloomNetShopnetwork(originalSendingShop.getShopId()).getShopCode();
					
					String messageText = tfsiMessage.getMessageText();
					if(messageText == null || messageText.equals("")) messageText = "Denial of Cancellation";
					String messageType = BOMConstants.DENI_MESSAGE_DESC;
					
					messageDeni.setMessageText(messageText);
											
					orderDAO.persistChildOrderAndActivity(bomOrder, 
							parentOrder.getOrderNumber(),
							USER_NAME, 
						    BOMConstants.ACT_ROUTING, 
							bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_DENI),
							BOMConstants.TFSI_QUEUE);
				
					String fsiXML = convertToFSIXML(messageDeni, bomOrder, originalSendingShop, originalSendingShopCode, messageText, messageType, tfsiMessage);
					sendMessageToBloomlink(fsiXML, messageType);
					
				
				}
			}catch(Exception ee){
				throw new JMSException("Error processing TF Message: " + ee.getMessage());
		}
	}
	
	
	@SuppressWarnings("unused")
	private void sendToNewMessagesQueue(String fsiXML, Bomorder parentOrder, City city, Zip zip, String originalSendingShopCode, String messageType) throws JMSException{
		
		Transform.PROPERTIES = fsiProperties;

		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(parentOrder.getOrderXml());
		
		String occassion = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
		String timeZone = zip.getTimeZone();

		
		messageProducer.produceMessage(
				BOMConstants.NEWMESSAGES_QUEUE, 
				fsiXML, 
				parentOrder.getOrderNumber(), 
				originalSendingShopCode, 
				DateUtil.toString(parentOrder.getDeliveryDate()), 
				city.getName(), 
				zip.getZipCode(), 
				occassion, 
				BOMConstants.SKILLSET_BASICAGENT, 
				messageType, 
				String.valueOf(parentOrder.getOrderType()),
				timeZone);
	}
	
	
	private void sendBackToTFSI(String parentXML, Bomorder parentOrder, String parentSendingShopCode, 
								String occassion, String city, String zip) throws JMSException{
		
		String timeZone = orderDAO.getZipByCode(zip).getTimeZone();

		messageProducer.produceMessage(
				BOMConstants.TFSI_QUEUE, 
				parentXML, 
				parentOrder.getOrderNumber(), 
				parentSendingShopCode, 
				DateUtil.toString(parentOrder.getDeliveryDate()), 
				city, 
				zip, 
				occassion, 
				BOMConstants.SKILLSET_BASICAGENT, 
				BOMConstants.ORDER_MESSAGE_DESC, 
				String.valueOf(parentOrder.getOrderType()),
				timeZone);
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIService#sendToTFMessagesQueue(java.lang.String, java.lang.String, com.bloomnet.bom.common.entity.Shop, java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void sendToTFMessagesQueue(String xml, String orderNumber, String sendingShopCode, String messageType, String bundleId) throws JMSException{
		
		logger.debug("Sending TF message to the message queue: " + xml);
		
		messageProducer.produceTFMessage(
						BOMConstants.TFSI_MESSAGES_QUEUE, 
						xml, 
						orderNumber, 
						sendingShopCode, 
						messageType, 
						bundleId);
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void sendMessageToBloomlink(String messageXml, String messageType) throws JMSException {

		String dataEncoded = "";
		String post = "";
		String response = "";

		
		try{
			
			dataEncoded = URLEncoder.encode(messageXml, "UTF-8");
			post = fsiProperties.getProperty("fsi.endpoint")+ BOMConstants.POST_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("xml encoded: " + dataEncoded);
				logger.debug("sendMessage xml " + messageXml);
				logger.debug("post " + post);

			}
	
			response = fsiClient.sendRequest(post,messageType);
			
			ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(response);
			String seqNumberOfOrder = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder();
			String seqNumberOfMessage = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
		
			System.out.println("seqNumberOfOrder: " + seqNumberOfOrder);
			System.out.println("seqNumberOfMessage: " + seqNumberOfMessage);

			
			int numOfErrors = 0;
			numOfErrors = foreignSystemInterface.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = foreignSystemInterface.getErrors().getError();

				for (Error error : errors) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());

				}
			}
			
				
			Errors messageAckBErrors = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getErrors();
			
			if(!messageAckBErrors.getError().isEmpty()){

			List<Error> messageAckBErrorList = messageAckBErrors.getError();
			
			for (Error messageAckBError : messageAckBErrorList){
				logger.error("recieved error acknowledgement from BloomLink");
				logger.error(messageAckBError.getErrorCode() + "|"
						+ messageAckBError.getDetailedErrorCode() + "|"
						+ messageAckBError.getErrorMessage());
				logger.error(messageAckBError.getErrorMessage());
				throw new JMSException("BloomLink Exception: " +messageAckBError.getErrorMessage());

			}
		}
		}catch (UnsupportedEncodingException e){
			throw new JMSException("BloomLink Method Exception: " + e.getMessage());
		}catch (BloomLinkException e){
			throw new JMSException("BloomLink Method Exception: " + e.getMessage());
		}catch(Exception ee){
			throw new JMSException("BloomLink Method Exception: " + ee.getMessage());
		}
	}
	
	public void sendAcknowledgement(String bundleId) {
		twsc.ackTransaction(bundleId);
	}

	
	public static void main(String[] args){
		System.out.println(new Date().getTime());
	}

	@Override
	public Integer getLastSeqNumber() {
		return Integer.valueOf(orderDAO.getLastOrderSequenceNumber());
	}
}
