package com.bloomnet.bom.webservice.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.Error;
import com.bloomnet.bom.common.jaxb.fsi.Errors;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.mdi.ExcludedShops;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectorySearchOptions;
import com.bloomnet.bom.common.jaxb.mdi.SearchAvailability;
import com.bloomnet.bom.common.jaxb.mdi.SearchByShopCode;
import com.bloomnet.bom.common.jaxb.mdi.SearchShopRequest;
import com.bloomnet.bom.common.jaxb.mdi.Security;
import com.bloomnet.bom.common.jaxb.mdi.ServicedZips;
import com.bloomnet.bom.common.jaxb.mdi.Shop;
import com.bloomnet.bom.common.jaxb.mdi.ShopAddress;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.common.util.XMLValidate;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.webservice.service.FSIAgentService;
import com.bloomnet.bom.webservice.utils.SendStatusUpdates;

@Service("fsiAgent")
@Transactional(propagation = Propagation.REQUIRED)
public class FSIAgentServiceImpl implements FSIAgentService  {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger(FSIAgentServiceImpl.class);
	
	static final String INTERNAL_USER = "FSI";

	
	// Injected property
	@Autowired private Properties bomProperties;
	// Injected property
	@Autowired private Properties fsiProperties;
	
	// Injected DAO implementation
	@Autowired private OrderDAO orderDAO;
	
	// Injected DAO implementation
	@Autowired private ShopDAO shopDAO;	
	
	@Autowired private UserDAO userDAO;	
	
	@Autowired private MessageDAO messageDAO;
	
	@Autowired private FSIClient fsiClient;
	
	@Autowired private TransformService transformService;	

	
	//@Autowired SendStatusUpdates sendStatusUpdates;
	
	
	// Injected dependency
	 @Autowired private MessageProducer messageProducer;
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.FSIAgentService#handleOrder(java.lang.String, java.lang.String, java.lang.String)
	 */

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public synchronized void handleOrder( String orderXML, String OrderNumber, String orderType ) throws Exception {
		
		Properties props  = new Properties();
		
		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}
		
		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}
		
		Transform.PROPERTIES = props;

		
		String status = props.getProperty(BOMConstants.ORDER_BEING_WORKED);
		/*
		logger.debug("updating status of order " + OrderNumber + " to being worked" );
		orderDAO.persistOrderactivityByUser( INTERNAL_USER, BOMConstants.ACT_ROUTING, status, OrderNumber, BOMConstants.FSI_QUEUE );*/



		boolean hasFullingshop = true;
		hasFullingshop = checkForActiveFullingShop(OrderNumber);
		
		if (hasFullingshop){
			return;
		}

		String sendingOrderXML = new String();

		
		if (orderType.equals(Byte.toString(BOMConstants.ORDER_TYPE_TEL))){
			Bomorder bomorder = orderDAO.getOrderByOrderNumber(OrderNumber);
			orderXML = bomorder.getOrderXml();
		}


		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(orderXML);
		MessageOrder order = fsi.getMessagesOnOrder().get(0).getMessageOrder();

		
		
		//
		String countryCode = order.getRecipient().getRecipientCountryCode();
		String returnedShopCode = null;
		if ( countryCode.equals("USA")||countryCode.equals("US")||countryCode.equals("CA")||countryCode.equals("CAN") ){
			 returnedShopCode = searchForShops(order,0);
		}
		else{
				returnedShopCode = searchforInternationalFlorist(order,countryCode);
		}
	

		//

		//String returnedShopCode = searchForShops(order,0);
		
	
		// unable to find shop or reached threshold so send to next queue
		String notFound = props.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND);
		if (returnedShopCode.equals(notFound)) {
			logger.debug("Unable to fulfill order by FSI, sending order to next destination");
			String orderNumber = order.getOrderDetails().getOrderNumber();
			if(orderNumber == null) orderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
			String sendingShopCode = order.getSendingShopCode();
			String skillset = BOMConstants.SKILLSET_BASICAGENT;
			String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
			String city = order.getRecipient().getRecipientCity();
			String zip = order.getRecipient().getRecipientZipCode();
			String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
			String occasionCode = order.getOrderDetails().getOccasionCode();
			sendOrderToQueue(orderXML, orderNumber, sendingShopCode, BOMConstants.ORDER_MESSAGE_DESC,
					skillset, deliveryDate,city, zip, occasionCode, Byte.toString(BOMConstants.ORDER_TYPE_BMT), timeZone);

		} else {

			ForeignSystemInterface sendingfsi = fsi;

			String receivingShopCode = returnedShopCode;
			String sendingShopCode = props.getProperty(BOMConstants.FSI_SHOPCODE);

			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().setOrderNumber(null);
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().getIdentifiers().getGeneralIdentifiers().setBmtOrderNumber(null);
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setSendingShopCode(sendingShopCode);
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setReceivingShopCode(receivingShopCode);
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setFulfillingShopCode(receivingShopCode);
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setOrderCaptureDate(DateUtil.toXmlFormatString(new Date()));	


			sendingOrderXML = transformService.convertToFSIXML(sendingfsi);

			String parentOrderNumber = order.getOrderDetails().getOrderNumber();
			if(parentOrderNumber == null) parentOrderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();

			//send order to shop, get new child order number then persist 
			//send order to bloomlink and retrieve new order number
			Identifiers identifiers = new Identifiers();
			GeneralIdentifiers generalIds = sendMessageToBloomLink(sendingOrderXML, orderXML,OrderNumber);
			generalIds.setExternalShopOrderNumber(BOMConstants.EXTERNAL_SHOP_NUMBER);

			String seqNumberOfOrder = generalIds.getBmtSeqNumberOfOrder();
			String seqNumberOfMessage = generalIds.getBmtSeqNumberOfMessage();

			String childOrderNumber =generalIds.getBmtOrderNumber();

			if (childOrderNumber!=null){

				OrderDetails orderdetails = sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails();
				orderdetails.setOrderNumber(childOrderNumber);
				sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setOrderDetails(orderdetails);
				identifiers.setGeneralIdentifiers(generalIds);
				sendingfsi.getMessagesOnOrder().get(0).getMessageOrder().setIdentifiers(identifiers);
				MessagesOnOrder sendingMessagesOnOrder = new MessagesOnOrder();
				sendingMessagesOnOrder.setMessageOrder(sendingfsi.getMessagesOnOrder().get(0).getMessageOrder());
				sendingfsi.getMessagesOnOrder().add(sendingMessagesOnOrder);
				sendingOrderXML = transformService.convertToFSIXML(sendingfsi);

				status = props.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);
				saveChildOrder(sendingOrderXML,
						seqNumberOfOrder, 
						seqNumberOfMessage,
						sendingfsi.getMessagesOnOrder().get(0).getMessageOrder(),
						BOMConstants.FSI_QUEUE,  
						status,
						OrderNumber,
						sendingShopCode);
				
				/*MessageInfo messageInfo = sendStatusUpdates.sendSentToShop(orderDAO.getOrderByOrderNumber(parentOrderNumber));
				String xml = transform.createForeignSystemInterface(BOMConstants.INFO_MESSAGE_DESC, messageInfo);
				sendMessageToBloomLink(xml, orderXML, parentOrderNumber);*/

				
			}else{
				
				List<Error> ackBErrors = generalIds.getAckBErrors();
				
				//using a boolean here just in case future scenarios occur that result in a blocked status
				boolean blockedError = false;
				
				for(Error errorMessage : ackBErrors){
					if(errorMessage.getErrorMessage().contains("blocked")){
						blockedError = true;
						break;
					}
				}
					
				if(blockedError == true){
					
					String timestamp = String.valueOf(new Date().getTime());
					
					seqNumberOfOrder = timestamp; //fake sequence number
					seqNumberOfMessage = timestamp; //fake order sequence
					OrderNumber = "BLCK" + timestamp; //fake order number
					
					//Save the child order with a status of blocked by shop
					status = props.getProperty(BOMConstants.ORDER_BLOCKED_BY_SHOP);
					saveChildOrder(sendingOrderXML,
							seqNumberOfOrder, 
							seqNumberOfMessage,
							sendingfsi.getMessagesOnOrder().get(0).getMessageOrder(),
							BOMConstants.FSI_QUEUE,  
							status,
							OrderNumber,
							sendingShopCode);
					
					//Update the parent as To be Worked
					status = bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED);
					orderDAO.persistOrderactivityByUser( INTERNAL_USER, BOMConstants.ACT_ROUTING, status, OrderNumber, BOMConstants.FSI_QUEUE );
					
					//Put the order back onto the queue
					String skillset = BOMConstants.SKILLSET_BASICAGENT;
					String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
					String city = order.getRecipient().getRecipientCity();
					String zip = order.getRecipient().getRecipientZipCode();
					String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
					String occasionCode = order.getOrderDetails().getOccasionCode();
					sendOrderToQueue(orderXML, parentOrderNumber, sendingShopCode, BOMConstants.ORDER_MESSAGE_DESC, skillset, deliveryDate, city, zip, occasionCode, orderType, timeZone);
				}
			}
		}

	}

	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class, isolation=Isolation.READ_UNCOMMITTED)
	private boolean checkForActiveFullingShop(String orderNumber) {
		
		boolean activeShop = false;
		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);

		if (!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
			Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
			byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
			if ((Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
					||(Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
					||(Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))
					||(Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))){
				
				return false;
			}
			else{
				logger.debug("order " +orderNumber + " already has a fulfilling shop " + currentFulfillingShop);
				return true;
			}
	

		}
		
		return activeShop;
	}



	/**
	 * Bypass WS component and send Order to BloomLink
	 * 
	 * @param sendingOrderXML
	 * @throws Exception 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private GeneralIdentifiers sendMessageToBloomLink(String sendingOrderXML, String orderXML, String parentOrderNumber) throws Exception {
		
		Properties props  = new Properties();
		
		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}
		
		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}
		
		Transform.PROPERTIES = props;


		String dataEncoded = "";
		String post = "";
		String response = "";
		String orderNumber = "";
		GeneralIdentifiers generalIdentifiers = new  GeneralIdentifiers();



		try {

			dataEncoded = URLEncoder.encode(sendingOrderXML, "UTF-8");
			post = props.getProperty("fsi.endpoint")
					+ BOMConstants.POST_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("sendMessage xml " + sendingOrderXML);
				logger.debug("post " + post);

			}
			response = fsiClient.sendRequest(post, BOMConstants.ORDER_MESSAGE_DESC);
			
			ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(response);
			orderNumber = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
			String seqNumberOfOrder = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder();
			String seqNumberOfMessage = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			
			generalIdentifiers.setBmtOrderNumber(orderNumber);
			generalIdentifiers.setBmtSeqNumberOfOrder(seqNumberOfOrder);
			generalIdentifiers.setBmtSeqNumberOfMessage(seqNumberOfMessage);
			
			String fulfillingShop = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getFulfillingShopCode();

			
			logger.debug("Order number: " +parentOrderNumber);
			logger.debug("Outbound Order Number returned from BloomLink: " + orderNumber);
			logger.debug("Fulfilling shop: " + fulfillingShop);
			
			int numOfErrors = 0;
			numOfErrors = foreignSystemInterface.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = foreignSystemInterface.getErrors().getError();
				
				for (Error error : errors) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());

				}
				
				ForeignSystemInterface parentFsi = transformService.convertToJavaFSI(orderXML);
				MessageOrder order = parentFsi.getMessagesOnOrder().get(0).getMessageOrder();
				String sendingShopCode = order.getSendingShopCode();
				String skillset = BOMConstants.SKILLSET_BASICAGENT;
				String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
				String city = order.getRecipient().getRecipientCity();
				String zip = order.getRecipient().getRecipientZipCode();
				String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
				String occasionCode = order.getOrderDetails().getOccasionCode();
				
				logger.error("sending order " + parentOrderNumber + " to next destination");
				logger.debug("sending order " + parentOrderNumber + " to next destination");
				sendOrderToQueue(orderXML,parentOrderNumber, sendingShopCode, BOMConstants.ORDER_MESSAGE_DESC,
						skillset, deliveryDate,city, zip, occasionCode, Byte.toString(BOMConstants.ORDER_TYPE_BMT), timeZone);
			}
			
			Errors messageAckBErrors = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getErrors();
			
			List<Error> messageAckBErrorList = messageAckBErrors.getError();
			
			generalIdentifiers.setAckBErrors(messageAckBErrorList);
			
			for (Error messageAckBError : messageAckBErrorList) {
				logger.error("recieved error acknowledgement from BloomLink");
				logger.error(messageAckBError.getErrorCode() + "|"
						+ messageAckBError.getDetailedErrorCode() + "|"
						+ messageAckBError.getErrorMessage());
				logger.error(messageAckBError.getErrorMessage());
				
				ForeignSystemInterface parentFsi = transformService.convertToJavaFSI(orderXML);
				MessageOrder order = parentFsi.getMessagesOnOrder().get(0).getMessageOrder();
				String sendingShopCode = order.getSendingShopCode();
				String skillset = BOMConstants.SKILLSET_BASICAGENT;
				String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
				String city = order.getRecipient().getRecipientCity();
				String zip = order.getRecipient().getRecipientZipCode();
				String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
				String occasionCode = order.getOrderDetails().getOccasionCode();
				
				logger.error("sending order " + parentOrderNumber + " to next destination");
				logger.debug("sending order " + parentOrderNumber + " to next destination");
				sendOrderToQueue(orderXML,parentOrderNumber, sendingShopCode, BOMConstants.ORDER_MESSAGE_DESC,
						skillset, deliveryDate,city, zip, occasionCode, Byte.toString(BOMConstants.ORDER_TYPE_BMT),timeZone);
				
				if(messageAckBError.getErrorMessage().contains("blocked"))
					return generalIdentifiers;
				else
					throw new Exception("BloomLink Exception: " +messageAckBError.getErrorMessage());

			}


		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		return generalIdentifiers;

	}

	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.FSIAgentService#searchForShops(com.bloomnet.bom.common.jaxb.fsi.MessageOrder, int)
	 */
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor=Exception.class, isolation=Isolation.READ_UNCOMMITTED)
	public String searchForShops( MessageOrder order, int count ) throws Exception {

		String shopsXml = new String();
		//Shop shop = new Shop();
		String shopCode = new String();
		String orderNumber  = order.getOrderDetails().getOrderNumber();
		if(orderNumber == null) orderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		String validZip = order.getRecipient().getRecipientZipCode().trim();
		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
		String orderXML = bomorder.getOrderXml();

		
		try {
			
			if (validZip.isEmpty()){
				//shop.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND));
				return fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND);
			}
			MemberDirectoryInterface mdi = new MemberDirectoryInterface();
			

			String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
			String zipCode = order.getRecipient().getRecipientZipCode();
			
			String countryCode = order.getRecipient().getRecipientCountryCode();
			String state = order.getRecipient().getRecipientState();
			boolean isStateValid = orderDAO.isStateValid(state);
			

			if ((countryCode.equals("USA")||countryCode.equals("US")) 
					&& (isStateValid)){

				//shopsXml =  searchShopCodeByDeliveryDateAndZipCode(deliveryDate, zipCode);
				shopsXml =  searchAvailability(deliveryDate, zipCode,bomorder.getBomorderId());
			}
			else if (countryCode.equals("CA")||countryCode.equals("CAN")) {

				shopsXml =  searchForCanadianShop(deliveryDate, zipCode,bomorder.getBomorderId());
			}
			else {
				throw new Exception("Order number: " +orderNumber + " Invalid country code " + countryCode);
			}
			mdi = transformService.convertToJavaMDI(shopsXml);
			
			com.bloomnet.bom.common.jaxb.mdi.Errors ers = mdi.getSearchShopResponse().getErrors();
			if (!ers.getError().isEmpty() ){
				throw new Exception(ers.getError().get(0).getErrorMessage());
			}

			/*if (logger.isDebugEnabled()) {
				logger.debug("Number of shop Found "+ mdi.getSearchShopResponse().getShops().getShop().size());
			}

			shop = mdi.getSearchShopResponse().getShops().getShop().get(0);
			
			logger.debug("Found the following shop: ");
			logger.debug("Shop name: " + shop.getName());
			logger.debug("Shop code: " + shop.getShopCode());
			logger.debug("Shop sunday indicator: " +shop.getSundayIndicator());

			logger.debug("order delivery date: " +order.getDeliveryDetails().getDeliveryDate());
*/
			
			//CHECK if shop code is valid
			
		//	if (!shop.getShopCode().equals(fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND))){

			try {
				shopCode = mdi.getSearchShopResponse().getShopCode();
				com.bloomnet.bom.common.entity.Shop receivingShop =shopDAO.getShopByCode(shopCode);
				if (receivingShop==null){
					logger.error("Shop not found by shop code: " + shopCode);
					//shopCode = saveShop(mdi.getSearchShopResponse().getShopCode());
					saveShop(mdi.getSearchShopResponse().getShopCode());

				}
				else{
					logger.debug(receivingShop.getShopName() + " shop is a valid BMT shop");
				}
			} catch (NullPointerException e) {
					logger.error("Shop not found by shop code: " + shopCode);
					logger.error("Searching for another shop.........");

					shopCode = searchForShops(order,count);
			}
			
			//}


			// verify that shop has not been previously attempted
			
	/*		logger.debug("checking previously attempted shops for order " + bomorder.getOrderNumber());
			List<String> previousShops = shopDAO.getPreviousAttemptedShops(bomorder.getBomorderId());

			String shopAlreadyAttemped = "";
			for (int i = 0; i < previousShops.size(); i++) {
				if (previousShops.get(i).equalsIgnoreCase(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)))
					continue;
				if (previousShops.get(i).equalsIgnoreCase(shop.getShopCode())) {
					shopAlreadyAttemped = shop.getShopCode();
					shop.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND));
					return shop;
				
				}
			}*/
		} catch (Exception e) {
			logger.error("Unable to search for shop for order " + orderNumber + " because "+ e.getMessage());
			logger.debug("Unable to fulfill order by FSI, sending order to next destination");
			String sendingShopCode = order.getSendingShopCode();
			String skillset = BOMConstants.SKILLSET_BASICAGENT;
			String deliveryDate = order.getDeliveryDetails().getDeliveryDate();
			String city = order.getRecipient().getRecipientCity();
			String zip = order.getRecipient().getRecipientZipCode();
			String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
			String occasionCode = order.getOrderDetails().getOccasionCode();
			sendOrderToQueue(orderXML, orderNumber, sendingShopCode, BOMConstants.ORDER_MESSAGE_DESC,
					skillset, deliveryDate,city, zip, occasionCode, Byte.toString(BOMConstants.ORDER_TYPE_BMT), timeZone);
			e.printStackTrace();
			throw new JMSException("Unable to search for shop for order " + orderNumber + " because "+ e.getMessage());

		}

		return shopCode;
	}
	
	private String searchforInternationalFlorist(MessageOrder order, String countryCode) {
		
		String shop = new String();
		shop = shopDAO.getInternationalShopByCountry(countryCode);
		
		String orderNumber  = order.getOrderDetails().getOrderNumber();
		if(orderNumber == null) orderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

		
		logger.debug("checking previously attempted shops for order " + bomorder.getOrderNumber());
		List<String> previousShops = shopDAO.getPreviousAttemptedShops(bomorder.getBomorderId());

		for (int i = 0; i < previousShops.size(); i++) {
			if (previousShops.get(i).equalsIgnoreCase(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)))
				continue;
			if (previousShops.get(i).equalsIgnoreCase(shop)) {
				shop = fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND);
				return shop;
			
			}
		}
		
		
		
		return shop;
	}
	
private String searchAvailability(String deliveryDate, String zipCode, Long bomorderId) throws Exception {
		
		SearchAvailability searchAvailability = new SearchAvailability();
		searchAvailability.setDeliveryDate(deliveryDate);
		searchAvailability.setZipCode(zipCode);
		
		List<String> previousShops = shopDAO.getPreviousAttemptedShops(bomorderId);
		
		logger.debug("excluding the following shop codes from searchAvailability: ");
		
		ExcludedShops excludedShops = new ExcludedShops();
		for (int i = 0; i < previousShops.size(); i++) {
			if (!previousShops.get(i).equalsIgnoreCase(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)))
			{
				excludedShops.getExcludedShopCodes().add(previousShops.get(i));	
				logger.debug("previousShops.get(i)");

			}
		}
		
		
		searchAvailability.setExcludedShops(excludedShops);
		
		MemberDirectoryInterface mdi =  new MemberDirectoryInterface();
		SearchShopRequest searchshoprequest = new SearchShopRequest();
		MemberDirectorySearchOptions searchOptions = new MemberDirectorySearchOptions();
		searchshoprequest.setMemberDirectorySearchOptions(searchOptions);
		searchOptions.getMemberDirectorySearchOptionsGroup().add(searchAvailability);
		Security security = new Security();
		
		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));


		searchshoprequest.setSecurity(security);

		mdi.setSearchShopRequest(searchshoprequest);

		String mdiXml = transformService.convertToMDIXML(mdi);

		String xmlEncoded = URLEncoder.encode(mdiXml, "UTF-8");
		String post = fsiProperties.getProperty("fsi.endpoint")+ BOMConstants.GET_MEMBER + xmlEncoded;

		String response = fsiClient.sendRequest(post,"GET_MEMBER");//TODO Need to check if shop is found

		MemberDirectoryInterface results = transformService.convertToJavaMDI(response);
		
		if (results.getSearchShopResponse().getErrors().getError().isEmpty()){
			
		
		}
		
		
		return response;
	}


	/**
	 * FSI Search request
	 * 
	 * @param deliveryDate
	 * @param zipCode
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String searchShopCodeByDeliveryDateAndZipCode(String deliveryDate,String zipCode) {
		
		Properties props  = new Properties();
		
		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}
		
		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}
		
		Transform.PROPERTIES = props;

		
		String post = "";

		String response = "";

		String xml = new String();
		XMLValidate validate = new XMLValidate();

		try {

			StringBuilder params = new StringBuilder();
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"
					+ props.getProperty(BOMConstants.FSI_USERNAME)
					+ "</username>");
			params.append("<password>"
					+ props.getProperty(BOMConstants.FSI_PASSWORD)
					+ "</password>");
			params.append("<shopCode>"
					+ props.getProperty(BOMConstants.FSI_SHOPCODE)
					+ "</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchShopCodeByDeliveryDateAndZipCode>");
			params.append("<deliveryDate>" + deliveryDate + "</deliveryDate>");
			if (zipCode != null)
				params.append("<zipCode>" + zipCode + "</zipCode>");
			else
				params.append("<zipCode></zipCode>");
			params.append("</searchShopCodeByDeliveryDateAndZipCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			xml = params.toString();
			logger.debug("Search for shop xml " + xml);

			validate.checkXmlForm(xml);

			String xmlEncoded = URLEncoder.encode(xml, "UTF-8");

			post = props.getProperty("fsi.endpoint") + BOMConstants.GET_MEMBER
					+ xml;

			logger.debug("postreq " + post);

			response = fsiClient.sendRequest(post, "GET_MEMBER");

			
			ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(response);
		
			if (foreignSystemInterface.getErrors()!=null){
				int numOfErrors;

			 numOfErrors = foreignSystemInterface.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = foreignSystemInterface.getErrors().getError();

				for (Error error : errors) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());

				}
			}
			
			Errors messageAckBErrors = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getErrors();
			
			List<Error> messageAckBErrorList = messageAckBErrors.getError();
			
			for (Error messageAckBError : messageAckBErrorList) {
				logger.error("recieved error acknowledgement from BloomLink");
				logger.error(messageAckBError.getErrorCode() + "|"
						+ messageAckBError.getDetailedErrorCode() + "|"
						+ messageAckBError.getErrorMessage());
				logger.error(messageAckBError.getErrorMessage());
				throw new Exception("BloomLink Exception: " +messageAckBError.getErrorMessage());

			}

			
			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
private String searchForCanadianShop(String deliveryDate,String zipCode, Long bomorderId) throws Exception {
		

		
		String newZip = zipCode.replaceAll("\\s+","");
		//String shopsXml = searchShopCodeByDeliveryDateAndZipCode(deliveryDate, newZip);
		String shopsXml =  searchAvailability(deliveryDate, newZip,bomorderId);
		
		MemberDirectoryInterface mdi = transformService.convertToJavaMDI(shopsXml);
		//Shop shop = mdi.getSearchShopResponse().getShops().getShop().get(0);
		if (mdi.getSearchShopResponse().getShopCode().equals(fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND))){
		
		/*logger.debug("Found the following shop: ");
		logger.debug("Shop name: " + shop.getName());
		logger.debug("Shop code: " + shop.getShopCode());
		logger.debug("Shop sunday indicator: " +shop.getSundayIndicator());*/

		//if (shop.getShopCode().equals(fsiProperties.getProperty(BOMConstants.FSI_SHOP_NOT_FOUND))){
			
			String shortZip = zipCode.substring(0, 3);
			//shopsXml = searchShopCodeByDeliveryDateAndZipCode(deliveryDate, shortZip);
			shopsXml =  searchAvailability(deliveryDate, shortZip,bomorderId);
		}
		return shopsXml;
	
	}




	/**
	 * Checks if order is a same day order
	 * 
	 * @param deliveryDate
	 * @return
	 */
	public boolean isSameDayOrder(String deliveryDate) {

		boolean sameDay = false;

		String today = DateUtil.toXmlFormatString(new Date());

		if (today.equals(deliveryDate)) {
			sameDay = true;
		}

		return sameDay;

	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.FSIAgentService#sendOrderToQueue(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void sendOrderToQueue( String message, 
			                        String orderNumber,
		                        	String sendingShopCode, 
		                        	String messageType, 
		                        	String skillset,
		                        	String deliveryDate,
		                        	String city,
		                        	String zip,
		                        	String occasionCode,
		                        	String orderType,
		                        	String timeZone) {
		
		Properties props  = new Properties();
		Transform transform = new Transform();

		
		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}
		
		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}
		
		transform.PROPERTIES = props;

		String destination = determineRoute(deliveryDate,orderNumber);
		
		if (!destination.equals(BOMConstants.TLO_QUEUE)){
		
			messageProducer.produceMessage(destination, message, orderNumber, sendingShopCode, deliveryDate,
					city, zip, occasionCode,  skillset, messageType, Byte.toString(BOMConstants.ORDER_TYPE_BMT), timeZone);
			}

		

		String status = props.getProperty(BOMConstants.ORDER_TO_BE_WORKED);
		
		String notify =props.getProperty(BOMConstants.EMAIL_NOTIFY);
		
		try {
			logger.debug("updating status of order " + orderNumber + "to be worked" );
			orderDAO.persistOrderactivityByUser( INTERNAL_USER, BOMConstants.ACT_ROUTING, status, orderNumber, destination);
			long childId = 0;
			String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);
			if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
				childId = orderDAO.getCurrentFulfillingOrder(orderNumber).getBomorderId();
			}
			
			byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
			Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomorder.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(status));
			bomorderSts.setVirtualqueueId(virtualqueueId);

			orderDAO.updateBomorderSts(bomorderSts, childId);
			
			/*if (notify.equals("ON")&& destination.equals(BOMConstants.TLO_QUEUE)){
				//messageProducer.produceOutboundMessage(destination, orderString, orderNumber, receivingShop, deliveryDate, occasionCode, toEmail);
				String toEmail = props.getProperty(BOMConstants.EMAIL_NOTIFY_TO);
				messageProducer.produceOutboundMessage(BOMConstants.EMAIL_QUEUE, message, orderNumber, " ", deliveryDate, occasionCode, toEmail);
			}*/

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Persist child order
	 * @param orderXml
	 * @param seqNumberOfOrder
	 * @param seqNumberOfMessage
	 * @param messageOrder
	 * @param destination
	 * @param status
	 * @param parentOrderNumber
	 * @param sendingShopCode
	 * @throws Exception
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private void saveChildOrder( String orderXml, String seqNumberOfOrder, String seqNumberOfMessage, MessageOrder messageOrder, 
			String destination, String status, String parentOrderNumber,String sendingShopCode) throws Exception {
		
		final Commmethod commmethod= orderDAO.getCommmethodById(BOMConstants.COMMMETHOD_API_ID);
		
		String cityName = messageOrder.getRecipient().getRecipientCity();
		String stateName = messageOrder.getRecipient().getRecipientState();
		String countryName = messageOrder.getRecipient().getRecipientCountryCode();

		
		City city = orderDAO.getCityByNameAndState(cityName,stateName);
		if(city == null || city.getCityId()==null){
			logger.error(cityName +" not found in " + stateName);
			city = orderDAO.getCityByZip(messageOrder.getRecipient().getRecipientZipCode(), messageOrder.getRecipient().getRecipientCity());

			if(city == null || city.getCityId()==null){
				logger.error(cityName +" not found in " + messageOrder.getRecipient().getRecipientZipCode());
				city = orderDAO.persistCity(cityName, stateName, countryName);
			}
		}
		
		Zip  zip = orderDAO.getZipByCode(messageOrder.getRecipient().getRecipientZipCode());
		if (zip==null){
			logger.error("Recipient zip code does not exist: " + messageOrder.getRecipient().getRecipientZipCode());
			logger.error("Finding zip code by city and state ....");
			zip = orderDAO.getZipByCityAndState(cityName,stateName);
			Long zipId = zip.getZipId();
			if (zipId==null){
				String errorMsg = "There is an issue with the recipient's address information: " +
				"Recipient zip code: " + messageOrder.getRecipient().getRecipientZipCode() + 
				" Recipient zip city: " + messageOrder.getRecipient().getRecipientCity();
				logger.error(errorMsg);
				zip = orderDAO.persistZip(messageOrder.getRecipient().getRecipientZipCode());

			}

		}
		final com.bloomnet.bom.common.entity.Shop receivingShop = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
		final com.bloomnet.bom.common.entity.Shop sendingShop   = shopDAO.getShopByCode(sendingShopCode);
		
		String deliveryDateStr = messageOrder.getDeliveryDetails().getDeliveryDate();
		Date deliveryDate = DateUtil.toDateFormat(deliveryDateStr);
		
		double totalCost = 0;
		String totalCostStr = messageOrder.getOrderDetails().getTotalCostOfMerchandise();
		if (totalCostStr!=null){
		totalCost = Double.parseDouble(totalCostStr);
		}
		
		Bomorder order = new Bomorder( messageOrder.getOrderDetails().getOrderNumber(),
									   BOMConstants.ORDER_TYPE_BMT,
									   orderXml,
									   city,
									   zip,
									   commmethod,
									   DateUtil.childOrderDate(new Date()),
									   deliveryDate,
									   totalCost,
									   receivingShop,
									   sendingShop,
									   seqNumberOfOrder,
									   seqNumberOfMessage
									  );
		
		
		String orderNumber =orderDAO.persistChildOrderAndActivity(order,
				parentOrderNumber, 
				INTERNAL_USER, 
				BOMConstants.ACT_ROUTING, 
				status, 
				BOMConstants.FSI_QUEUE);
				
		
		//set order status to sent to shop
		logger.debug("updating status of order " + orderNumber + "sent to shop" );
		logger.debug("Completed order: "+orderNumber);
		
	}
	
	
	/**
	 * Determines where to send shop based on destinations set in 
	 * bom.properties 
	 * @param deliveryDate 
	 * @param orderNumber
	 * @return
	 */
	private String determineRoute(String deliveryDate, String orderNumber) {
		
		String destination = new String();
		String currentDestination = BOMConstants.FSI_QUEUE;
		String firstDestination = bomProperties.getProperty(BOMConstants.FIRST_DESTINATION);
		String secondDestination = bomProperties.getProperty(BOMConstants.SECOND_DESTINATION);
		String thirdDestination = bomProperties.getProperty(BOMConstants.THIRD_DESTINATION);
		
		String currentDateStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date delDate = DateUtil.toDateFormat(deliveryDate);
		Date currentDate = DateUtil.toDate(currentDateStr);
		int eq = delDate.compareTo(currentDate);
	
		if (currentDestination.equals(firstDestination)){
			destination = secondDestination;
		}
		else if (currentDestination.equals(secondDestination)){
			destination = 	thirdDestination;
		}
		else if (currentDestination.equals(thirdDestination)){
			destination = 	firstDestination;
		}
		else{
			destination = 	BOMConstants.TLO_QUEUE;
		}
		
		if(eq<0){
			logger.error("Order " +  orderNumber + "is past-dated.  Routing to TLO Queue");
			destination = 	BOMConstants.TLO_QUEUE;
		}
			
		return destination;
	}
	
	private void saveShop(String sendingShopCode) throws Exception {

		logger.debug("Saving shop: " + sendingShopCode);
		com.bloomnet.bom.common.entity.Shop shopEntity = new com.bloomnet.bom.common.entity.Shop();
		com.bloomnet.bom.common.jaxb.mdi.Shop shop  = new Shop();
		



		SearchByShopCode searchShopCode = new SearchByShopCode();
		searchShopCode.setShopCode(sendingShopCode);
		searchShopCode.setReturnNonDeliveryDates("true");
		searchShopCode.setReturnFulfilledZipCodes("true");
		searchShopCode.setReturnCommunicationCode("true");

		MemberDirectoryInterface mdi =  new MemberDirectoryInterface();
		SearchShopRequest searchshoprequest = new SearchShopRequest();
		MemberDirectorySearchOptions searchOptions = new MemberDirectorySearchOptions();
		searchshoprequest.setMemberDirectorySearchOptions(searchOptions);
		searchOptions.getMemberDirectorySearchOptionsGroup().add(searchShopCode);
		Security security = new Security();

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));


		searchshoprequest.setSecurity(security);

		mdi.setSearchShopRequest(searchshoprequest);

		String mdiXml = transformService.convertToMDIXML(mdi);

		String xmlEncoded = URLEncoder.encode(mdiXml, "UTF-8");
		String post = fsiProperties.getProperty("fsi.endpoint")+ BOMConstants.GET_MEMBER + xmlEncoded;

		String response = fsiClient.sendRequest(post,"GET_MEMBER");//TODO Need to check if shop is found

		MemberDirectoryInterface results = transformService.convertToJavaMDI(response);
		
		if (results.getSearchShopResponse().getErrors().getError().isEmpty()){

		shop = results.getSearchShopResponse().getShops().getShop().get(0);

		ShopAddress address = shop.getAddress();
		String shopAddress1 = address.getAddressLine1();
		String shopAddress2 = address.getAddressLine2();
		String zip = address.getZip();
		String city = address.getCity();
		String state = address.getState();
		String shopContact = address.getAttention();
		String country = address.getCountryCode();


	/*	if ( (country.equals("USA"))
				||(country.equals("US")) ){*/


			//state short name is returned

			String shopPhone = shop.getPhoneNumber();
			logger.debug("shopPhone: " +shopPhone);
			if (shopPhone.isEmpty()){
				shopPhone = "0000000000";
			}
			String shopFax = shop.getFaxNumber();
			String shopEmail = shop.getEmail();
			String shopName = shop.getName();

				String shopCode = shop.getShopCode();
				String sundayIndicator = shop.getSundayIndicator();
				boolean openSunday = true;
				if (sundayIndicator.equals("N")){
					openSunday = false;
				}
				

				shopEntity.setShopAddress1(shopAddress1);
				shopEntity.setShopAddress2(shopAddress2);
				shopEntity.setShopPhone(shopPhone);
				shopEntity.setShopContact(shopContact);
				shopEntity.setShopFax(shopFax);
				shopEntity.setShopEmail(shopEmail);
				shopEntity.setShopName(shopName);
				//shopEntity.setShop800(shop800);

				City cityEntity = orderDAO.getCityByZip(zip, city);
				if(cityEntity.getCityId()==null){
					logger.error("Saving shop: " + sendingShopCode + ", " +city +" not found ");
					cityEntity = orderDAO.persistCity(city, state, country);
				}
				Zip zipEntity = verifyZipCode( zip); 

				shopEntity.setCity(cityEntity);
				shopEntity.setZip(zipEntity);

				User user = userDAO.getUserByUserName(INTERNAL_USER);
				Date createdDate = new Date();

				shopEntity.setUserByCreatedUserId(user);
				shopEntity.setCreatedDate(createdDate);

				shopDAO.persistShop(shopEntity);

				Network network = shopDAO.getNetwork( "BloomNet" );

				Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );

				Commmethod commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );

				Shopnetwork shopNetwork = new Shopnetwork(shopEntity, network, shopnetworkstatus, 
						user, commmethod, user, shopCode, 
						openSunday, createdDate, createdDate);

				shopDAO.persitShopnetwork( shopNetwork );


				Shopnetworkcoverage shopNetworkCoverage = new Shopnetworkcoverage(user,shopNetwork, zipEntity);
				shopNetworkCoverage.setCreatedDate(createdDate);
				shopDAO.persitShopnetworkcoverage(shopNetworkCoverage);

				ServicedZips serviceZips = shop.getServicedZips();
				if (serviceZips!=null){
					List<String> zips = serviceZips.getZip();
					for (String serviceZip:zips){

						Zip zipCovered = verifyZipCode(serviceZip);

						Shopnetworkcoverage netCoverage2 = new Shopnetworkcoverage( user, shopNetwork, zipCovered );

						netCoverage2.setCreatedDate( new Date() );

						shopDAO.persitShopnetworkcoverage(netCoverage2);

							}
				}
			
	/*	}
		else{
			logger.error("sending shop is from country: " + country);
		}*/

		}
		else{
			List<com.bloomnet.bom.common.jaxb.mdi.Error> errors = results.getSearchShopResponse().getErrors().getError();
			for (com.bloomnet.bom.common.jaxb.mdi.Error error: errors){
				logger.error("error searching for shop " + sendingShopCode+ ": " + error.getDetailedErrorCode()+ " | " + error.getErrorCode() + " | " + error.getErrorMessage());
			}
		}
			//return shop;
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private Zip verifyZipCode(String zip) throws Exception {
		
		Zip zipEntity = orderDAO.getZipByCode(zip);
		Zip results = new Zip();
		
		if (zipEntity==null){
			logger.error(zip +" not found ");
			results = orderDAO.persistZip(zip);
			return results;

		}else{
			
		return zipEntity;
		}
	}

	public void setMessageProducer(MessageProducer messageProducer) {
		this.messageProducer = messageProducer;
	}
	
	

}
