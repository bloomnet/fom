package com.bloomnet.bom.webservice.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.webservice.service.WSService;


@Transactional(propagation = Propagation.REQUIRED)
public class WSServiceImpl implements WSService {
	
	@Autowired private OrderDAO orderDAO;
	
	@Autowired private ShopDAO shopDAO;
	
	@Autowired private Properties fsiProperties;
	
	static Logger logger = Logger.getLogger( WSServiceImpl.class );

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void setDefaultCurrentFulfillingShop(String orderNumber)throws Exception{
		
		String bomShop = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
		final Shop fulfillingshop    = shopDAO.getShopByCode(bomShop);
		String childOrderNumber = generateBMTOrderNumber("BOM");
		
		Bomorder parentOrder = orderDAO.getOrderByOrderNumber(orderNumber);
		
		Bomorder order = new Bomorder( childOrderNumber,
				   parentOrder.getOrderType(),
				   parentOrder.getOrderXml(),
				   parentOrder.getCity(),
				   parentOrder.getZip(),
				   parentOrder.getCommmethod(),
				   new Date(),
				   parentOrder.getDeliveryDate(),
				   parentOrder.getPrice(),
				   fulfillingshop,
				   parentOrder.getShopBySendingShopId(),
				   parentOrder.getBmtOrderSequenceNumber(),
				   parentOrder.getBmtMessageSequenceNumber()
				 );
		
		orderDAO.persistChildOrder(order,orderNumber);
		
		
	}
	
	private String generateBMTOrderNumber(String orderName){
		boolean isNew = false;

		String orderNumber = new String();

		SimpleDateFormat xmlFormat = new SimpleDateFormat("yyyyMMddhhmmss");

		orderNumber = xmlFormat.format(new Date());
		orderNumber = orderName + orderNumber;



		try {
			isNew = orderDAO.isUniqueOrderNumber(orderNumber);
			if(!isNew){
				generateBMTOrderNumber(orderName);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return orderNumber;
	}
	
	

}
