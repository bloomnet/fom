package com.bloomnet.bom.webservice.service;

import javax.jms.JMSException;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;

public interface TFSIAgentService {

	public abstract void sendMessage(String messageXML, String messageType)
			throws Exception;

	@Transactional(propagation = Propagation.REQUIRED)
	public abstract void sendToTLOQueue(String xml, MessageOrder order)
			throws JMSException;

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public abstract Shop searchForShops(MessageOnOrderBean moob,
			String originalSendingShopCode);

}