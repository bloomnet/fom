package com.bloomnet.bom.webservice.test;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.client.impl.FSIClientImpl;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;
import com.bloomnet.bom.webservice.utils.BOMProperties;

public class FSIClientTest {
	
	FSIClient client = new FSIClientImpl();
	Properties props = new Properties();
	
	@Before
	public void setup(){	
		try {
			props = BOMProperties.load();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	@Test
	public void testSendRequest() throws BloomLinkException{
		String orderXML="";
		orderXML="<foreignSystemInterface><security><username>M177</username><password></password><shopCode>M1770000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>Q8830000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>10/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11550</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";

		String dataEncoded = "";
		try {
			dataEncoded = URLEncoder.encode(orderXML, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("xml encoded: " + dataEncoded);

		String post = props.getProperty("fsi.endpoint") + BOMConstants.POST_MESSAGE
				+ dataEncoded;

		System.out.println("post request " + post);
		
	
	String messages = client.sendRequest(post,"order");
	Assert.assertNotNull("messages in null", messages);
	}
	
	@Test
	public void testSendRequestMalformedURLEx() throws BloomLinkException{
		String orderXML="";
		orderXML="<foreignSystemInterface><security><username>M177</username><password></password><shopCode>M1770000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>Q8830000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>10/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11550</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";

		String dataEncoded = "";
		try {
			dataEncoded = URLEncoder.encode(orderXML, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("xml encoded: " + dataEncoded);

		String post = "" + BOMConstants.POST_MESSAGE
				+ dataEncoded;

		System.out.println("post request " + post);
		
	
	String messages = client.sendRequest(post,"order");
	
	Assert.assertNull("messages in null", messages);
	}
	
	@Test
	public void testSendRequestConnectionEx() throws BloomLinkException{
		String orderXML="";
		orderXML="<foreignSystemInterface><security><username>M177</username><password></password><shopCode>M1770000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>Q8830000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>10/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11550</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";

		String dataEncoded = "";
		try {
			dataEncoded = URLEncoder.encode(orderXML, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("xml encoded: " + dataEncoded);

		String post = "http://11.180.1.220:/fsiv2/processor" + BOMConstants.POST_MESSAGE
				+ dataEncoded;

		System.out.println("post request " + post);
		
	
	String messages = client.sendRequest(post,"order");
	
	Assert.assertNull("messages in null", messages);
	}

}
