package com.bloomnet.bom.webservice.test;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.webservice.service.TFSIAgentService;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
@Transactional
public class TFSIAgentTest  extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired protected TFSIAgentService tfsiAgent;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	@SuppressWarnings("unused")
	public void testTFSIAgent() {
		
		final String orderXML = "Hello World";
		
		try {
			
			tfsiAgent.sendMessage(orderXML,"ORDER");
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSearchForShops() {
		fail("Not yet implemented"); // TODO
	}
}
