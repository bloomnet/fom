package com.bloomnet.bom.webservice.test;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.client.impl.FSIClientImpl;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;

public class sendOrderTest {
	
	 String orderXML="";
		FSIClient client = new FSIClientImpl();



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			sendOrderTest test = new sendOrderTest();
		} catch (BloomLinkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	
	}
	
	public sendOrderTest() throws BloomLinkException{
		
//orderXML="<foreignSystemInterface><security><username>M177</username><password></password><shopCode>M1770000</shopCode></security><errors/><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>Q8830000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber/><bmtSeqNumberOfOrder/><bmtSeqNumberOfMessage/><externalShopOrderNumber>10001</externalShopOrderNumber><externalShopMessageNumber/></generalIdentifiers></identifiers><messageCreateTimestamp>20110907015618</messageCreateTimestamp><orderDetails><orderNumber/><occasionCode>2</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>$10.00</costOfSingleProduct><productDescription>1 flower</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good Luck!</orderCardMessage><orderType>1</orderType><orderTypeMessage>order type message</orderTypeMessage></orderDetails><orderCaptureDate>20110907015618</orderCaptureDate><deliveryDetails><deliveryDate>09/10/2011</deliveryDate><specialInstruction>leave at back door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>S</recipientLastName><recipientAttention/><recipientAddress1>one old country rd</recipientAddress1><recipientAddress2/><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11415</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516 230 7708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber/><shipperCode/><shippingMethod/><shippingDate/></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";
	//orderXML="<foreignSystemInterface><security><username>B756</username><password></password><shopCode>B7560000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>12/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11550</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";
		orderXML="<foreignSystemInterface><security><username>B756</username><password></password><shopCode>B7560000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>B7560000</sendingShopCode><receivingShopCode>M1770000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>12/1/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>beverly hills</recipientCity><recipientState>CA</recipientState><recipientZipCode>99903</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";

		
	String dataEncoded = "";
		try {
			dataEncoded = URLEncoder.encode(orderXML, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("xml encoded: " + dataEncoded);

		//IN FLOWERS NETWORK ENDPOINT
		String post = BOMConstants.QA_ENDPOINT + BOMConstants.POST_MESSAGE	+ dataEncoded;
		//OUT FLOWERS NETWORK ENDPOINT
		//String post = BOMConstants.QA2_ENDPOINT + BOMConstants.POST_MESSAGE+ dataEncoded;

		System.out.println("post request " + post);
		
	String messages = client.sendRequest(post,"Order");
	System.out.println("Order sent to bloomlink");
	
	}
	

}
