package com.bloomnet.bom.webservice.test;
import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.JmsTemplate;

//import com.bloomnet.bom.webservice.jms.MessageConsumer;

public class MessageConsumerTest {
	
	private static ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
	private static Connection conn = null;
	private static Session session = null;
	//int occassion = 4;
	//String sSelector = "occassion  = '" + occassion+ "'";

	//MessageConsumer consumer = new MessageConsumer();
	
	@Before
	public void setup(){
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));

		JmsTemplate jmsWSConsumer = (JmsTemplate) factory.getBean("jmsWSConsumer");

		//consumer.setJmsWSConsumer(jmsWSConsumer);

		try {
			conn =cf.createConnection();
			session=conn.createSession(false,Session.AUTO_ACKNOWLEDGE);
			Destination destination=new ActiveMQQueue("TEST-QUEUE");


			MessageProducer producer=session.createProducer(destination);
			producer.setPriority(8);
			
			TextMessage message=session.createTextMessage();
			message.setText("Test");
			
			message.setStringProperty("occassion", "6");

			producer.send(message);
			} catch(JMSException e){
			// handleexception?
			} finally{
			try {
			if (session!=null){
			session.close();
			}
			if (conn!=null){
			conn.close();
			}
			} catch(JMSException ex){
			}
			
			}
		
	}
	
	@Test
	public void testConsumeMessage()  {
		
		String message = new String();
		
		
		//message = consumer.consumeMessage();
		
		Assert.assertNotNull("message is null", message);

	}
	
	@Test
	public void testConsumeMessageEmpty()  {
		
		String message = new String();
		String message2 = new String();

		
		/*BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));
		
		JmsTemplate jmsWSConsumer = (JmsTemplate) factory.getBean("jmsWSConsumer");
		consumer.setJmsTemplate(jmsWSConsumer);*/
	//	message = consumer.consumeMessage();
		
		Assert.assertNotNull("message is null", message);
	//	message2 = consumer.consumeMessage();
		
		Assert.assertEquals("message is blank", "", message2);


	}
}
