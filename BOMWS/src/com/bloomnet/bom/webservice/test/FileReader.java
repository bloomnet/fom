package com.bloomnet.bom.webservice.test;

import java.io.File;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.StringTokenizer;

public class FileReader {

	public String getFileAsString(File file){ FileInputStream fis = null;
	BufferedInputStream bis = null;
	DataInputStream dis = null;
	StringBuffer sb = new StringBuffer();
	try {
	fis = new FileInputStream(file);
	bis = new BufferedInputStream(fis);
	dis = new DataInputStream(bis);

	while (dis.available() != 0) {
	sb.append( dis.readLine() +"\n");
	}
	fis.close();
	bis.close();
	dis.close();

	} catch (FileNotFoundException e) {
	e.printStackTrace();
	} catch (IOException e) {
	e.printStackTrace();
	}
	return sb.toString();
	}
	public static void main(String[] args) {

	File file = new File("test-msgs/fsi_inbound_ackf_general_type.xml");
	FileReader fd = new FileReader();
	String s = fd.getFileAsString(file);
	System.out.print(s);
	
	String msg = removeSpaces(s);
	System.out.print(msg);

	}
	
	public static String removeSpaces(String s) {
		  StringTokenizer st = new StringTokenizer(s," ",false);
		  String t="";
		  while (st.hasMoreElements()) t += st.nextElement();
		  return t;
		}




    
    
}
