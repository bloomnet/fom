package com.bloomnet.bom.webservice.test;


import javax.jms.JMSException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.JmsTemplate;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.mvc.jms.MessageProducer;

public class MessageProducerTest {
	
	MessageProducer producer = new MessageProducer();

	@Before
	public void setUp() throws Exception {
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));

		JmsTemplate jmsProducer = (JmsTemplate) factory.getBean("jmsProducer");
		
		producer.setJmsProducer(jmsProducer);

	}
	
	@Test
	public void testProduceMessage(){
		String message="<foreignSystemInterface><security><username>B756</username><password></password><shopCode>B7560000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><externalShopOrderNumber>10001</externalShopOrderNumber></generalIdentifiers></identifiers><messageCreateTimestamp>20110926062836</messageCreateTimestamp><orderDetails><occasionCode>1</occasionCode><totalCostOfMerchandise>5.99</totalCostOfMerchandise><orderProductsInfo><orderProductInfoDetails><units>1</units><costOfSingleProduct>5.99</costOfSingleProduct><productDescription>10 flowers</productDescription></orderProductInfoDetails></orderProductsInfo><orderCardMessage>good luck!</orderCardMessage><orderType>1</orderType></orderDetails><orderCaptureDate>20110926062836</orderCaptureDate><deliveryDetails><deliveryDate>12/10/2011</deliveryDate><specialInstruction>please leave at door</specialInstruction></deliveryDetails><recipient><recipientFirstName>Mark</recipientFirstName><recipientLastName>Slvr</recipientLastName><recipientAddress1>2 old country rd</recipientAddress1><recipientCity>carle place</recipientCity><recipientState>NY</recipientState><recipientZipCode>11550</recipientZipCode><recipientCountryCode>USA</recipientCountryCode><recipientPhoneNumber>516237708</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber></trackingNumber><shipperCode></shipperCode><shippingMethod></shippingMethod><shippingDate></shippingDate></shippingDetails></messageOrder></messagesOnOrder></foreignSystemInterface>";
		String orderNumber=new String();
		String destination=new String();
		
		orderNumber="3418037";
		destination="TLO";
		String skillset = "2";
		String sendingShopCode = "Z999";
		String deliveryDate = "12/10/2099";
		String occasionCode = "3";
		String city = "new york";
		String zip = "00602";
		String timeZone = "-5";

		producer.produceMessage(destination, message, orderNumber,sendingShopCode,deliveryDate,city,zip, occasionCode , skillset,BOMConstants.INQR_MESSAGE_DESC,"1", timeZone);
	}

}
