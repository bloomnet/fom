package com.bloomnet.bom.webservice.exceptions;

public class BloomLinkException extends Exception {

	
	private static final long serialVersionUID = 1L;
	
	public BloomLinkException(String error){
		super("Recieved error acknowledgement from BloomLink while trying to send message to shop: " + error);
	}

}
