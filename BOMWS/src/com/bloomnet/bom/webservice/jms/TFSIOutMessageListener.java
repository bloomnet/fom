package com.bloomnet.bom.webservice.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.bloomnet.bom.webservice.service.TFSIService;



@SuppressWarnings("rawtypes")
public class TFSIOutMessageListener implements SessionAwareMessageListener {
	
	@Autowired private TFSIService tfsiService;
	
	static final Logger logger = Logger.getLogger(TFSIOutMessageListener.class);


	@Override
	public void onMessage(Message arg0, Session arg1) throws JMSException {
		
		Message message = arg0;
		
		TextMessage msg = (TextMessage) message;
		
		try {
			if(logger.isDebugEnabled()) {   	
		  		logger.debug("TFSI outbound messagelistener received the following for order number " 
		  				+ message.getStringProperty("ORDERNUMBER") + ": " + msg.getText());
				}
			tfsiService.processMessage(msg.getText(), msg.getStringProperty("BUNDLEID"), msg.getStringProperty("MESSAGETYPE"),
					msg.getStringProperty("SENDINGSHOP"), msg.getStringProperty("ORDERNUMBER"));
		}catch (Exception e) {
			throw new JMSException("An error occured while attempting to send the incoming TF message to bloomlink: "+ e.getMessage());
		}
	}


}
