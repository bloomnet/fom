package com.bloomnet.bom.webservice.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.bloomnet.bom.webservice.service.TFSIAgentService;

public class TFSIMessageListener implements SessionAwareMessageListener<Message> {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger( TFSIMessageListener.class );

	// Injected dependency
	@Autowired private TFSIAgentService tfsiAgent;

	@Override
	public void onMessage(Message message, Session session) throws JMSException {
		// TODO
		TextMessage msg = (TextMessage) message;
		try {
			if (logger.isDebugEnabled()) {

				logger.debug("***************TFSIMessageListener onMessage***************");
				logger.debug("received: " + msg.getText());
			}
			String tfsiMessage = "";
			tfsiMessage = msg.getText();
			String orderType = msg.getStringProperty("MESSAGETYPE");

			tfsiAgent.sendMessage(tfsiMessage,orderType);
	

		} catch (JMSException ex) {
			
			ex.printStackTrace();
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setTfsiAgent(TFSIAgentService tfsiAgent) {
		this.tfsiAgent = tfsiAgent;
	}

	public TFSIAgentService getTfsiAgent() {
		return tfsiAgent;
	}
}
