package com.bloomnet.bom.webservice.jms;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.listener.SessionAwareMessageListener;


import com.bloomnet.bom.webservice.service.FSIService;

public class BloomlinkMessageListener implements SessionAwareMessageListener<Message> {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger(BloomlinkMessageListener.class);

	// Injected dependency
	@Autowired private FSIService fsiService;

	@Override
	public void onMessage(Message message, Session session) throws JMSException {
		System.out.println("***************BloomlinkMessageListener onMessage***************");
		TextMessage msg = (TextMessage) message;
		try {
			if (logger.isDebugEnabled()) {

				logger.debug("***************BloomlinkMessageListener onMessage***************");
				//logger.debug("received: " + msg.getText());
			}
			
			String messages = msg.getText();
			logger.debug("Processing messages");
			logger.debug("Start time: " + new Date());

			getFsiService().processMessage(messages);
			
			logger.debug("End time: " + new Date());


		
		}catch (Exception e) {
			//logger.error("Unable to get message from NEW-MESSAGES destination \n message: " +  msg.getText());
			throw new JMSException("Unable to get message from NEW-MESSAGES destination \n Received exception: " + e.getMessage());
			//logger.error("Unable to get message from NEW-MESSAGES destination \n Received exception: " + e.getMessage());

		}
	}


	public void setFsiService(FSIService fsiService) {
		this.fsiService = fsiService;
	}


	public FSIService getFsiService() {
		return fsiService;
	}


	
}
