package com.bloomnet.bom.webservice.client;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.tfsi.TFSIResponse;

public interface TelefloraWebServiceClient {

	public abstract TFSIResponse sendOrderTransaction(String tempOrderNumber, MessageOnOrderBean moob);

	TFSIResponse ackTransaction(String bundleId);

	TFSIResponse getTransactions();

	TFSIResponse inquiryResponseTransaction(MessageOnOrderBean moob);

	TFSIResponse inquiryTransaction(MessageOnOrderBean moob);

	TFSIResponse cancelTransaction(MessageOnOrderBean moob);

	TFSIResponse resendOrderTransaction(String h, String b,
			long originalSequenceNumber);

	TFSIResponse refusalTransaction(MessageOnOrderBean moob);

	void setMessageSequenceNumber();

	TFSIResponse priceChangeTransaction(MessageOnOrderBean moob);

}