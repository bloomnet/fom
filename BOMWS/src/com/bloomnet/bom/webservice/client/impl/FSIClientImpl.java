package com.bloomnet.bom.webservice.client.impl;

import java.net.ConnectException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.WebserviceMonitor;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;
import com.bloomnet.bom.webservice.jms.BloomLinkMessageProducer;
import com.bloomnet.bom.webservice.service.FSIService;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.common.jaxb.fsi.Error;
import com.bloomnet.bom.common.jaxb.fsi.Errors;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckb;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.PendingMessages;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.XMLValidate;

@Service("fsiClient")
public class FSIClientImpl implements FSIClient {
	
	@Autowired private Properties fsiProperties;
	@Autowired private TransformService transformService;
	
	@Autowired private ShopDAO shopDAO;

	
	//@Autowired private FSIService fsiService;

	
	// Injected dependency
	@Autowired private BloomLinkMessageProducer bloomLinkMessageProducer;
	
	static final Logger logger = Logger.getLogger(FSIClientImpl.class);
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.client.FSIClient#checkForMessages()
	 */
	@Override
	public void checkForMessages() throws Exception {
		
		Transform.PROPERTIES = fsiProperties;


		if (logger.isDebugEnabled()) {
			logger.debug("Checking for messages .... ");
		}
		logger.info("Checking for messages .... ");

		XMLValidate validate = new XMLValidate();
		String messages = "";
		String data = "";
		String post = "";
		try {

			data = "<foreignSystemInterfaceOutboundRequest><security>"
					+ "<username>"
					+ fsiProperties.getProperty(BOMConstants.FSI_USERNAME)
					+ "</username>"
					+ "<password>"
					+ fsiProperties.getProperty(BOMConstants.FSI_PASSWORD)
					+ "</password>"
					+ "<shopCode>"
					+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
					+ "</shopCode></security>"
					+ "<fulfillerShopCode>"
					+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
					+ "</fulfillerShopCode>"
					+ "<systemType>"
					+ BOMConstants.SYSTEM_TYPE
					+ "</systemType>"
					+ "<maxNumOfGeneralMessages>"
					+ fsiProperties.getProperty(BOMConstants.NUM_GENERAL_MESSAGES)
					+ "</maxNumOfGeneralMessages>"
					+ "<maxNumOfAckfMessages>"
					+ fsiProperties.getProperty(BOMConstants.NUM_ACKF_MESSAGES)
					+ "</maxNumOfAckfMessages>"
					+ "<maxNumOfOrderMessages>"
					+ fsiProperties.getProperty(BOMConstants.NUM_ORDER_MESSAGES)
					+ "</maxNumOfOrderMessages>"
					+ "</foreignSystemInterfaceOutboundRequest>";

			//if (logger.isDebugEnabled()) {
			//	logger.debug("get message xml " + data);
			//}
			//disable for performance
			//validate.checkXmlForm(data); 

			String dataEncoded;
			dataEncoded = URLEncoder.encode(data, "UTF-8");
			//if (logger.isDebugEnabled()) {
			//	logger.debug("xml encoded: " + dataEncoded);
			//}
			
			post = fsiProperties.getProperty("fsi.endpoint") + BOMConstants.GET_MESSAGE+ dataEncoded;

			//if (logger.isDebugEnabled()) {
			//	logger.debug("get Message request " + post);
			//}
			
			messages = sendRequest(post,"");


		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(messages);
		int numOfmessages = fsi.getMessagesOnOrder().size();
		
		PendingMessages pendingMessages = fsi.getPendingMessages();
		
		if (numOfmessages > 0) {
			Network network = shopDAO.getNetwork("BloomNet");

			WebserviceMonitor webservicemonitor = new WebserviceMonitor();
			//WebserviceMonitor( network, new Date(),Long.parseLong(Integer.toString(numOfmessages)));
			webservicemonitor.setNetwork(network);
			webservicemonitor.setStarttime(new Date());
			webservicemonitor.setLastquery(new Date());
			long n_mgs = Long.parseLong(Integer.toString(numOfmessages));
			webservicemonitor.setNumOfMessages(n_mgs);
			shopDAO.saveOrUpdateBloomlinkQuery(webservicemonitor);
			//put messages on Queue
		//	fsiService.processMessage(messages);
			bloomLinkMessageProducer.produceMessage(BOMConstants.NEWMESSAGES_QUEUE, messages, "","","","");

		} else {
			
			if (logger.isDebugEnabled()) {
				logger.debug("No Messages...");
			}
			logger.info("No Messages...");

		}
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.client.FSIClient#sendRequest(java.lang.String, java.lang.String)
	 */
		@Override
		public String sendRequest(String outgoingMessage,String messageType) throws BloomLinkException{
			
			String response = null;
			
			  try {
		            
		            HttpURLConnection conn = get_connection(outgoingMessage);
		            conn.connect();
		            response = handleResponse(conn,messageType);
		            if ((response==null)||(response=="")){
						throw new BloomLinkException("No response from BloomLink");
					}
		        }
		        catch(ConnectException e) { 
		        	System.err.println(e); 
		            //logger.error("A network error has occurred. ConnectExcpetion" );
		            logger.error("A network error has occurred. Can not connect to BloomLink.  Please verify endpoint configured");
		            throw new BloomLinkException("A network error has occurred. Can not connect to BloomLink.  Please verify endpoint configured");
		        	}
		 catch (MalformedURLException e) {
			System.err.println(e);
		} 
		        catch(IOException e) { 
		        	System.err.println(e); 
		        	logger.error(e);}
		        catch(NullPointerException e) { 
		        	System.err.println(e);
		        	logger.debug(e);} 
		        catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			  return response;
		  }

	
/**
 * get HttpURLConnection
 * @param url_string
 * @return
 */
	private HttpURLConnection get_connection(String url_string) {
		HttpURLConnection conn = null;
		String verb=BOMConstants.POST_VERB;
		try {
			URL url = new URL(url_string);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(verb);
		} catch (MalformedURLException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
		return conn;
	}

	/**
	 * Receives response and converts it to String
	 * @param conn
	 * @return
	 * @throws Exception 
	 */
	private String handleResponse(HttpURLConnection conn,String messageType) throws Exception {
		String xml = "";

	
			
			BufferedReader reader;
			try {
				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			
			String next = null;
			while ((next = reader.readLine()) != null)
				xml += next;
			if(logger.isDebugEnabled()){
				logger.debug("BLOOMLINK RESPONSE MESSAGE XML:\n" + xml);
			}
			
			if (messageType!="GET_MEMBER"){
			
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
			
			int numOfErrors = 0;
			numOfErrors = fsi.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = fsi.getErrors().getError();

				for (Error error : fsi.getErrors().getError()) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					throw new BloomLinkException(error.getErrorMessage());

				}
				//String receivingshop = fsi.getMessagesOnOrder().get(0).getMessageAckb().get(0).getReceivingShopCode();
			}else if ((!fsi.getMessagesOnOrder().isEmpty())&&
			(numOfErrors ==0)){
				if (logger.isDebugEnabled()) {
					logger.debug("recieved acknowledgement from BloomLink with no errors");
				}
				
			}
			
			}
	
		
						
			} catch (IOException e) {
				logger.error("Server returned HTTP response code: 408  - Request timeout");
				e.printStackTrace();
			}
			 
		return xml;
	

	}

}
