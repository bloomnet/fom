package com.bloomnet.bom.common.test;
import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

public class NonSpringProducer {
	
	private static ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
	private static Connection conn = null;
	private static Session session = null;
	//int occassion = 4;
	//String sSelector = "occassion  = '" + occassion+ "'";

	
	public static void main(String[] args)  {
		try {
		conn =cf.createConnection();
		session=conn.createSession(false,Session.AUTO_ACKNOWLEDGE);
		//Destination destination=new ActiveMQQueue("spitter.queue");
		Destination destination=new ActiveMQQueue("queue");

		MessageProducer producer=session.createProducer(destination);
		producer.setPriority(8);
		
		TextMessage message=session.createTextMessage();
		message.setText("Helloworld!");
		
		message.setStringProperty("occassion", "6");
		System.out.println("Helloworld");
		producer.send(message);
		} catch(JMSException e){
		// handleexception?
		} finally{
		try {
		if (session!=null){
		session.close();
		}
		if (conn!=null){
		conn.close();
		}
		} catch(JMSException ex){
		}
		
		}
	}
}
