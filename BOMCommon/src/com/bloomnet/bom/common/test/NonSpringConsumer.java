package com.bloomnet.bom.common.test;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;


public class NonSpringConsumer {

	public static void main(String[] args) {
		ConnectionFactory cf = new ActiveMQConnectionFactory(
				"tcp://localhost:61616");
		Connection conn = null;
		Session session = null;
		int priority = 4;
		//String sSelector = "occassion  = '" + priority+ "'";
		String OR = "";
		int i=priority+1;
		while(i<10){
			 OR = OR + " OR occassion = '" + i + "'";
			 i++;
		}
		//String OR = " OR occassion = '" + priority+ "'";
		//String sSelector = "occassion = '4'" + OR;
		System.out.println("OR: " + OR);
		String sSelector = "occassion = '"+ priority+"'" + OR;
		System.out.println("selector:  " + sSelector);


		try {
			conn = cf.createConnection();
			conn.start();
			session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			//Destination destination = new ActiveMQQueue("spitter.queue");
			Destination destination = new ActiveMQQueue("queue");
			//MessageConsumer consumer = session.createConsumer(destination);
			javax.jms.MessageConsumer consumer = session.createConsumer(destination, sSelector) ;
			Message message = consumer.receive();
			//
			TextMessage textMessage = (TextMessage) message;
			System.out.println("GOTAMESSAGE:" + textMessage.getText());
			conn.start();
		} catch (JMSException e) {
			// handleexception?
		} finally {
			try {
				if (session != null) {
					session.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (JMSException ex) {
			}
		}
	}
}
