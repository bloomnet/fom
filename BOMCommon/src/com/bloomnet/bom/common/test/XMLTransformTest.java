package com.bloomnet.bom.common.test;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.jaxb.fsi.DeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckb;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckf;
import com.bloomnet.bom.common.jaxb.fsi.MessageCanc;
import com.bloomnet.bom.common.jaxb.fsi.MessageConf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDeni;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlca;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlou;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageNftn;
import com.bloomnet.bom.common.jaxb.fsi.MessageOfflineOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;
import com.bloomnet.bom.common.jaxb.fsi.MessageShipped;
import com.bloomnet.bom.common.jaxb.fsi.MessageShippedAckb;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductsInfo;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.jaxb.fsi.ShippingDetails;
import com.bloomnet.bom.common.jaxb.fsi.SystemType;
import com.bloomnet.bom.common.jaxb.fsi.WireServiceCode;
import com.bloomnet.bom.common.jaxb.fsi.*;
import com.bloomnet.bom.common.util.DateUtil;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;





public class XMLTransformTest {

	
	
	
	

	public String convertToXML(ForeignSystemInterface foreignSystemInterface){ //marshall
		  StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance( "generated" );
			Marshaller m = jc.createMarshaller();
			//m.marshal( foreignSystemInterface, System.out );
			m.marshal( foreignSystemInterface,xml);
			
			System.out.println(xml.toString());

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();

		
	}
	
	public ForeignSystemInterface convertToJava(String xmlString)//
	{

		ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();
		try{
			JAXBContext jc = JAXBContext.newInstance( "generated" );
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			JAXBElement<ForeignSystemInterface> jaxbElem  = unmarshaller.unmarshal( new StreamSource( new StringReader( xmlString ) ),ForeignSystemInterface.class );

			foreignSystemInterface = jaxbElem.getValue();
		
			int numOfOrders = 0;
			String numOfPending = "";
			int numOfErrors = 0;
		 numOfOrders = foreignSystemInterface.getMessagesOnOrder().size();
		 numOfPending = foreignSystemInterface.getPendingMessages().getTotal();
		 numOfErrors = foreignSystemInterface.getErrors().getError().size();
		 System.out.println("numOfOrders " + numOfOrders);
		 System.out.println("numOfPending " + numOfPending);
		 System.out.println("numOfErrors " + numOfErrors);
		 
		for (MessagesOnOrder orderList: foreignSystemInterface.getMessagesOnOrder()) {
			System.out.println(orderList.getMessageCount());
			List<MessageAckb> messageackbList=  orderList.getMessageAckb();
			List<MessageAckf> messageackfList = orderList.getMessageAckf();
			List<MessageCanc> messageCancList = orderList.getMessageCanc();
			List<MessageConf> messageConfList = orderList.getMessageConf();
			List<MessageDeni> messageDeniList = orderList.getMessageDeni();
			List<MessageDlca> messageDlcaList = orderList.getMessageDlca();
			List<MessageDlcf> messageDlcfList = orderList.getMessageDlcf();
			List<MessageDlou> messageDlouList = orderList.getMessageDlou();
			List<MessageInfo> messageInfoList = orderList.getMessageInfo();
			List<MessageInqr> messageInqrList = orderList.getMessageInqr();
			List<MessageNftn> messageNftnList = orderList.getMessageNftn();
			MessageOfflineOrder messageOfflineList = orderList.getMessageOfflineOrder();
			MessageOrder messageOrderList = orderList.getMessageOrder();
			List<MessagePchg> messagePchgList = orderList.getMessagePchg();
			List<MessageResp> messageRespList=  orderList.getMessageResp();
			List<MessageRjct> messageRjctList = orderList.getMessageRjct();
			List<MessageShipped> messageShippedList = orderList.getMessageShipped();
			List<MessageShippedAckb> messageShippedAckBfList = orderList.getMessageShippedAckb();
			

			
			if(!messageackbList.isEmpty()){
				System.out.println("Messageackb");
			}
			if(!messageackfList.isEmpty()){
				System.out.println("Messageackf");
			}
			if(!messageCancList.isEmpty()){
				System.out.println("MessageCanc");
			}
			if(!messageConfList.isEmpty()){
				System.out.println("MessageConf");
			}
			if(!messageDeniList.isEmpty()){
				System.out.println("MessageDeni");
			}
			if(!messageDlcaList.isEmpty()){
				System.out.println("MessageaDlca");
			}
			if(!messageDlcfList.isEmpty()){
				System.out.println("MessageDlcf");
			}
			if(!messageDlouList.isEmpty()){
				System.out.println("MessageDlou");
			}
			if(!messageInfoList.isEmpty()){
				System.out.println("MessageInfo");
			}
			if(!messageInqrList.isEmpty()){
				System.out.println("MessageInqr");
			}
			if(!messageNftnList.isEmpty()){
				System.out.println("MessageNftn");
			}
			if(messageOfflineList!=null){
				System.out.println("MessageOffline");
			}
			if(messageOrderList!=null){
				System.out.println("MessageOrder");
			}
			if(!messagePchgList.isEmpty()){
				System.out.println("MessagePchg");
			}
			if(!messageRespList.isEmpty()){
				System.out.println("MessageResp");
			}
			if(!messageRjctList.isEmpty()){
				System.out.println("MessageRjct");
			}
			if(!messageShippedList.isEmpty()){
				System.out.println("MessageShipped");
			}
			if(!messageShippedAckBfList.isEmpty()){
				System.out.println("MessageShippedackb");
			}
			
		}
		
		}catch(Exception e){
		e.printStackTrace();
		}
		return foreignSystemInterface;
		}
	
	
	
	
	private static ForeignSystemInterface createForeignSystemInterface() {
		
	       ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();
	        MessagesOnOrder aMessageOnOrder = new MessagesOnOrder();
			List<MessagesOnOrder> messagesOnOrderList = new ArrayList<MessagesOnOrder>();

			MessageOrder messageOrder = new MessageOrder();
			Recipient recipient = new Recipient();
			DeliveryDetails deliveryDetails = new DeliveryDetails();
			Identifiers identifiers = new Identifiers();
			GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();
			ShippingDetails shippingDetails = new ShippingDetails();
			OrderDetails orderDetails = new OrderDetails();
			OrderProductsInfo orderproductsInfo = new OrderProductsInfo();
			OrderProductInfoDetails aOrderProductInfoDetails = new OrderProductInfoDetails();
			Security security = new Security();



			
			//messagesOnOrder.setMessageCount(BigInteger.valueOf(1));
			
			messageOrder.setSendingShopCode("M1770000");
			messageOrder.setReceivingShopCode("Q8830000");
			messageOrder.setFulfillingShopCode("Q8830000");
			messageOrder.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageOrder.setMessageType(BOMConstants.ORDER_MESSAGE_TYPE);
			messageOrder.setOrderCaptureDate(DateUtil.toXmlFormatString(new Date()));
			messageOrder.setSystemType(BOMConstants.SYSTEM_TYPE);
			messageOrder.setWireServiceCode(WireServiceCode.BMT);
			
			recipient.setRecipientAddress1("2 old country rd");
			recipient.setRecipientCity("carle place");
			recipient.setRecipientCountryCode("USA");
			recipient.setRecipientFirstName("Mark");
			recipient.setRecipientLastName("Slvr");
			recipient.setRecipientPhoneNumber("516237708");
			recipient.setRecipientState("NY");
			recipient.setRecipientZipCode("11550");
			
			messageOrder.setRecipient(recipient);
			
			
			deliveryDetails.setDeliveryDate("9/10/2011");
			deliveryDetails.setSpecialInstruction("please leave at door");
			messageOrder.setDeliveryDetails(deliveryDetails);
			
			
			generalIdentifiers.setExternalShopOrderNumber("10001");
			
			identifiers.setGeneralIdentifiers(generalIdentifiers );
			messageOrder.setIdentifiers(identifiers );
			
			messageOrder.setShippingDetails(shippingDetails);
			
			
			orderDetails.setOccasionCode("1");
			orderDetails.setOrderCardMessage("good luck!");
			orderDetails.setOrderType("1");
			orderDetails.setTotalCostOfMerchandise("5.99");
			
			
			aOrderProductInfoDetails.setCostOfSingleProduct("5.99");
			aOrderProductInfoDetails.setProductDescription("10 flowers");
			aOrderProductInfoDetails.setUnits(1);
			
			//orderproductsInfo.setOrderProductInfoDetails(orderProductInfoDetails);
			orderproductsInfo.getOrderProductInfoDetails().add(aOrderProductInfoDetails);

			
			orderDetails.setOrderProductsInfo(orderproductsInfo);
			
			
		

			
			messageOrder.setOrderDetails(orderDetails);
			
			aMessageOnOrder.setMessageCount(1);
			aMessageOnOrder.setMessageOrder(messageOrder);
			
			
			
			messagesOnOrderList.add(aMessageOnOrder);
			//messagesOnOrder.setMessageOrder(messageOrder);
			
			//foreignSystemInterface.setMessagesOnOrder(messagesOnOrder );
			foreignSystemInterface.getMessagesOnOrder().add(aMessageOnOrder);

			
			security.setPassword("");
			security.setUsername("M177");
			security.setShopCode("M1770000");
			foreignSystemInterface.setSecurity(security);
		
		return foreignSystemInterface;
	}

	/*public static void main(String[] args) {

	ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();
	foreignSystemInterface = createForeignSystemInterface();
	

	String xml = "";
	//xml = marshall(foreignSystemInterface);
	marshall(foreignSystemInterface);
	xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383383</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383382</bmtOrderNumber><bmtSeqNumberOfOrder>36262988</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267519</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3118</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383381</bmtOrderNumber><bmtSeqNumberOfOrder>36262987</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267518</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3117</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383379</bmtOrderNumber><bmtSeqNumberOfOrder>36262719</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267251</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3116</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383380</bmtOrderNumber><bmtSeqNumberOfOrder>36262986</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267520</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3119</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383386</bmtOrderNumber><bmtSeqNumberOfOrder>36263258</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267789</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3122</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383385</bmtOrderNumber><bmtSeqNumberOfOrder>36263257</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267788</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3121</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
	unmarshall(xml);


}*/


}
