package com.bloomnet.bom.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.text.MaskFormatter;

import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;

public class MessageOnOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int messageType;
	private String systemType;
	private String bmtOrderNumber;
	private long bmtSeqNumberOfOrder;
	private long bmtSeqNumberOfMessage;
	private long externalShopOrderNumber;
	private long externalShopMessageNumber;
	private Date messageCreateTimestamp;
	private int messageCount;
	private int status;
	private List<Error> globalErrors;
	private String messageText;
	private String dateOrderDelivered; // specific to messageDlcf
	private String loadedDate; // specific to messageDlou
	private String price; // specific to messagePchg

	// Order message
	private Shop sendingShop;
	private Shop receivingShop;
	private Shop fulfillingShop;
	
	private String originalSendingShopCode;
	private String originalReceivingShopCode;

	private String recipientFirstName;
	private String recipientLastName;
	private String recipientAttention;
	private String recipientAddress1;
	private String recipientAddress2;
	private String recipientCity;
	private String recipientState;
	private String recipientZipCode;
	private String recipientCountryCode;
	private String recipientPhoneNumber;
	private String occasionId;
	private String totalCost;
	private String cardMessage;
	private Date captureDate;
	private String deliveryDate;
	private String flexDeliveryDate;
	private String specialInstruction;
	private String orderMessageType;
	private String trackingNumber;
	private String shipperCode;
	private String shippingMethod;
	private Date shippingDate;
	private String orderNumber;
	private String wireServiceCode;
	private List<OrderProductInfoDetails> products;
	private byte orderType;
	private Byte commMethod;
	
	private List<ActMessage> orderMessages;
	
	private String skillset;
	
	private City newCity;
	private Zip  newZip;
	private Date newDeliveryDate;

	
	public MessageOnOrderBean() {
		
		this.orderMessages = new ArrayList<ActMessage>();
		this.products = new ArrayList<OrderProductInfoDetails>();
	}

	public int getMessageCount() {
		return messageCount;
	}
	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public String getSystemType() {
		return systemType;
	}
	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	

	public String getBmtOrderNumber() {
		return bmtOrderNumber;
	}
	public void setBmtOrderNumber(String bmtOrderNumber) {
		this.bmtOrderNumber = bmtOrderNumber;
	}

	public long getBmtSeqNumberOfOrder() {
		return bmtSeqNumberOfOrder;
	}
	public void setBmtSeqNumberOfOrder(long bmtSeqNumberOfOrder) {
		this.bmtSeqNumberOfOrder = bmtSeqNumberOfOrder;
	}

	public long getBmtSeqNumberOfMessage() {
		return bmtSeqNumberOfMessage;
	}
	public void setBmtSeqNumberOfMessage(long bmtSeqNumberOfMessage) {
		this.bmtSeqNumberOfMessage = bmtSeqNumberOfMessage;
	}

	public long getExternalShopOrderNumber() {
		return externalShopOrderNumber;
	}
	public void setExternalShopOrderNumber(long externalShopOrderNumber) {
		this.externalShopOrderNumber = externalShopOrderNumber;
	}

	public long getExternalShopMessageNumber() {
		return externalShopMessageNumber;
	}
	public void setExternalShopMessageNumber(long externalShopMessageNumber) {
		this.externalShopMessageNumber = externalShopMessageNumber;
	}

	public Date getMessageCreateTimestamp() {
		return messageCreateTimestamp;
	}
	public void setMessageCreateTimestamp(Date messageCreateTimestamp) {
		this.messageCreateTimestamp = messageCreateTimestamp;
	}

	public List<Error> getGlobalErrors() {
		return globalErrors;
	}
	public void setGlobalErrors(List<Error> globalErrors) {
		this.globalErrors = globalErrors;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessageText() {
		return messageText;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	public void setLoadedDate(String loadedDate) {
		this.loadedDate = loadedDate;
	}
	public String getLoadedDate() {
		return loadedDate;
	}

	public void setDateOrderDelivered(String dateOrderDelivered) {
		this.dateOrderDelivered = dateOrderDelivered;
	}
	public String getDateOrderDelivered() {
		return dateOrderDelivered;
	}

	public Shop getSendingShop() {
		return sendingShop;
	}
	public void setSendingShop(Shop sendingShop) {
		this.sendingShop = sendingShop;
	}

	public Shop getReceivingShop() {
		return receivingShop;
	}
	public void setReceivingShop(Shop receivingShop) {
		this.receivingShop = receivingShop;
	}

	public Shop getFulfillingShop() {
		return fulfillingShop;
	}
	public void setFulfillingShop(Shop fulfillingShop) {
		this.fulfillingShop = fulfillingShop;
	}

	public String getRecipientFirstName() {
		return recipientFirstName;
	}
	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}

	public String getRecipientLastName() {
		return recipientLastName;
	}
	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getRecipientAddress1() {
		return recipientAddress1;
	}
	public void setRecipientAddress1(String recipientAddress1) {
		this.recipientAddress1 = recipientAddress1;
	}

	public String getRecipientAddress2() {
		return recipientAddress2;
	}
	public void setRecipientAddress2(String recipientAddress2) {
		this.recipientAddress2 = recipientAddress2;
	}

	public String getRecipientCity() {
		return recipientCity;
	}
	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}


	public String getRecipientState() {
		return recipientState;
	}
	
	public String getRecipientStateFormated() {
		
		String m_name = getRecipientState();
		
		if ( m_name != null && ! m_name.equals("") ) {
			
			try {
				
				StringBuffer shortName = new StringBuffer();
				
				String[] names = m_name.split(" ");
				
				if ( names.length >= 2 ) {
					for ( int i = 0; i < names.length; i++ ) {
						
						if ( ! names[i].equals( "" ) ) {
							String s = names[i].toUpperCase();
							char c = s.charAt(0);
							shortName.append( c );
						}
					}
					m_name = shortName.toString();
				}
				else {
					
					m_name = m_name.toUpperCase();
				}
			}
			catch ( Exception e ) {
				// do nothing
			}
		}
		
		return m_name;
	}

	public void setRecipientState(String recipientState) {
		this.recipientState = recipientState;
	}

	public String getRecipientZipCode() {
		return recipientZipCode;
	}
	public void setRecipientZipCode(String recipientZipCode) {
		this.recipientZipCode = recipientZipCode;
	}
	
	public String getRecipientCountryCode() {
		return recipientCountryCode;
	}
	public void setRecipientCountryCode(String recipientCountryCode) {
		this.recipientCountryCode = recipientCountryCode;
	}
	
	public String getRecipientPhoneNumber() {
		return recipientPhoneNumber;
	}
	
	public String getRecipientPhoneNumberFormated() {
		
		String m_shopPhone = getRecipientPhoneNumber();
		
		if ( m_shopPhone != null && ! m_shopPhone.equals("") ) {
			
			try {
				
				MaskFormatter mf = new MaskFormatter();
				
				if ( m_shopPhone.length() == 10 ) {
					
				    mf.setMask( "(AAA) AAA-AAAA" );
				    mf.setValueContainsLiteralCharacters(false);
				}
				else if ( m_shopPhone.length() == 7 ) {
					
				    mf.setMask( "AAA-AAAA" );
				    mf.setValueContainsLiteralCharacters(false);
				}
				else {
					return m_shopPhone;
				}

				m_shopPhone = mf.valueToString( m_shopPhone );
				
			}
			catch ( Exception e ) {
				// do nothing
			}
		}
		
		return m_shopPhone;
	}

	/**
	 * @param recipientPhoneNumber the recipientPhoneNumber to set
	 */
	public void setRecipientPhoneNumber(String recipientPhoneNumber) {
		this.recipientPhoneNumber = recipientPhoneNumber;
	}

	public String getOccasionId() {
		return occasionId;
	}

	public void setOccasionId(String ocasionId) {
		this.occasionId = ocasionId;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setShipperCode(String shipperCode) {
		this.shipperCode = shipperCode;
	}

	public String getShipperCode() {
		return shipperCode;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public Date getShippingDate() {
		return shippingDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setCaptureDate(Date captureDate) {
		this.captureDate = captureDate;
	}

	public Date getCaptureDate() {
		return captureDate;
	}

	public void setCardMessage(String cardMessage) {
		this.cardMessage = cardMessage;
	}

	public String getCardMessage() {
		return cardMessage;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setWireServiceCode(String wireServiceCode) {
		this.wireServiceCode = wireServiceCode;
	}

	public String getWireServiceCode() {
		return wireServiceCode;
	}

	public void setOrderMessageType(String orderMessageType) {
		this.orderMessageType = orderMessageType;
	}

	public String getOrderMessageType() {
		return orderMessageType;
	}

	public void setProducts(List<OrderProductInfoDetails> products) {
		this.products = products;
	}

	public List<OrderProductInfoDetails> getProducts() {
		return products;
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getTotalCost() {
		return totalCost;
	}

	public void setOrderType(byte orderType) {
		this.orderType = orderType;
	}

	public byte getOrderType() {
		return orderType;
	}

	public void setCommMethod(Byte commMethod) {
		this.commMethod = commMethod;
	}

	public Byte getCommMethod() {
		return commMethod;
	}

	public void setOrderMessages(List<ActMessage> orderMessages) {
		this.orderMessages = orderMessages;
	}

	public List<ActMessage> getOrderMessages() {
		return orderMessages;
	}

	public void setSkillset(String skillset) {
		this.skillset = skillset;
	}

	public String getSkillset() {
		return skillset;
	}
	
	public void setOriginalSendingShopCode(String originalSendingShopCode) {
		this.originalSendingShopCode = originalSendingShopCode;
	}

	public String getOriginalSendingShopCode() {
		return originalSendingShopCode;
	}

	public void setOriginalReceivingShopCode(String originalReceivingShopCode) {
		this.originalReceivingShopCode = originalReceivingShopCode;
	}

	public String getOriginalReceivingShopCode() {
		return originalReceivingShopCode;
	}
	
	public Date getNewDeliveryDate() {
		return newDeliveryDate;
	}

	public void setNewDeliveryDate(Date newDeliveryDate) {
		this.newDeliveryDate = newDeliveryDate;
	}

	public City getNewCity() {
		return newCity;
	}

	public void setNewCity(City newCity) {
		this.newCity = newCity;
	}

	public Zip getNewZip() {
		return newZip;
	}

	public void setNewZip(Zip newZip) {
		this.newZip = newZip;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		
		builder
		.append("MessageOnOrderBean [messageType=").append(messageType)
		.append(", systemType=").append(systemType)
		.append(", bmtOrderNumber=").append(bmtOrderNumber)
		.append(", bmtSeqNumberOfOrder=").append(bmtSeqNumberOfOrder)
		.append(", bmtSeqNumberOfMessage=").append(bmtSeqNumberOfMessage)
		.append(", externalShopOrderNumber=").append(externalShopOrderNumber)
		.append(", externalShopMessageNumber=").append(externalShopMessageNumber)
		.append(", messageCreateTimestamp=").append(messageCreateTimestamp)
		.append(", messageCount=").append(messageCount)
		.append(", status=").append(status)
		.append(", globalErrors=").append(globalErrors)
		.append(", messageText=").append(messageText)
		.append(", dateOrderDelivered=").append(dateOrderDelivered)
		.append(", loadedDate=").append(loadedDate)
		.append(", price=").append(price)
		.append(", sendingShop=").append(sendingShop)
		.append(", receivingShop=").append(receivingShop)
		.append(", fulfillingShop=").append(fulfillingShop)
		.append(", recipientFirstName=").append(recipientFirstName)
		.append(", recipientLastName=").append(recipientLastName)
		.append(", recipientAddress1=").append(recipientAddress1)
		.append(", recipientAddress2=").append(recipientAddress2)
		.append(", recipientCity=").append(recipientCity)
		.append(", recipientState=").append(recipientState)
		.append(", recipientZipCode=").append(recipientZipCode)
		.append(", recipientCountryCode=").append(recipientCountryCode)
		.append(", recipientPhoneNumber=").append(recipientPhoneNumber)
		.append(", occasionId=").append(occasionId)
		.append(", totalCost=").append(totalCost)
		.append(", cardMessage=").append(cardMessage)
		.append(", captureDate=").append(captureDate)
		.append(", deliveryDate=").append(deliveryDate)
		.append(", specialInstruction=").append(specialInstruction)
		.append(", orderMessageType=").append(orderMessageType)
		.append(", trackingNumber=").append(trackingNumber)
		.append(", shipperCode=").append(shipperCode)
		.append(", shippingMethod=").append(shippingMethod)
		.append(", shippingDate=").append(shippingDate)
		.append(", orderNumber=").append(orderNumber)
		.append(", wireServiceCode=").append(wireServiceCode)
		.append(", products=").append(products).append("]");
		
		return builder.toString();
	}

	public void setRecipientAttention(String attention) {
		this.recipientAttention = attention;
	}

	public String getRecipientAttention() {
		return recipientAttention;
	}

	public String getFlexDeliveryDate() {
		return flexDeliveryDate;
	}

	public void setFlexDeliveryDate(String flexDeliveryDate) {
		this.flexDeliveryDate = flexDeliveryDate;
	}
}
