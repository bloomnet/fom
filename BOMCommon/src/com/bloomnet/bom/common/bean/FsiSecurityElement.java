package com.bloomnet.bom.common.bean;

/**
 * An object representation of the security element for messages.
 * 
 * @author Danil Svirchtchev
 *
 */
public class FsiSecurityElement implements FsiSecurityElementInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String username;
	private String shopcode;
	private String password;
	
	@SuppressWarnings("unused")
	private FsiSecurityElement() {}

	/**
	 * @param username
	 * @param shopcode
	 * @param password
	 */
	public FsiSecurityElement(String username, String shopcode, String password) {
		super();
		this.username = username;
		this.shopcode = shopcode;
		this.password = password;
	}

	/**
	 * @return the username
	 */
	@Override
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the shopcode
	 */
	@Override
	public String getShopcode() {
		return shopcode;
	}

	/**
	 * @param shopcode the shopcode to set
	 */
	@Override
	public void setShopcode(String shopcode) {
		this.shopcode = shopcode;
	}

	/**
	 * @return the password
	 */
	@Override
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FsiSecurityElement [username=").append(username)
			   .append(", shopcode=").append(shopcode)
			   .append(", password=").append(password).append("]");
		return builder.toString();
	}
	
	public String toXmlString() {
		StringBuilder builder = new StringBuilder();
		builder.append("<security>")
			   .append("<username>").append(username).append("</username>")
		       .append("<password>").append(password).append("</password>")
		       .append("<shopCode>").append(shopcode).append("</shopCode>")
		       .append("</security>");
	   return builder.toString();
	}
}
