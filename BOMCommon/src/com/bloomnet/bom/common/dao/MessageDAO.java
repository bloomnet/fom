package com.bloomnet.bom.common.dao;


import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;


public interface MessageDAO {
	
	List<Messagetype> getMessageTypes() throws HibernateException;
	
	List<ActMessage> getMessagesByOrderId(Long orderId) throws HibernateException;
	
	/**
	 * Get an orders Commmethod description 
	 * @param orderNumber
	 * @return
	 */
	String getCommunicationMethod(String orderNumber) throws HibernateException;
	
	List<Commmethod> getCommmethods() throws HibernateException; 
	
	int persistMessage(ActMessage message)throws HibernateException;	
		
	int deleteMessage( Long messageId ) throws HibernateException;	

	Messagetype getMessageTypeByShortDesc( String shortDesc ) throws HibernateException;
	
	Commmethod getCommethodByDesc( String desc ) throws HibernateException;
	
	Commmethod getCommethodByCommId(byte commId) throws HibernateException;
	
	public String getLastMessageSequenceNumber() throws HibernateException;
	
	StsMessage getStsById( byte id ) throws HibernateException;

	int persistMessageAndActivity ( Long userId, 
						             GeneralIdentifiers identifiers, 
						             String xml,
						             String messageTypeDesc, 
						             String status, 
						             String commmethod,
						             String sendingShopCode, 
						             String receivingShopCode, 
						             String messageText ) throws HibernateException;

	void updateMessageStatus(ActMessage message, String status) throws HibernateException;
	
	List<ActMessage> getMessageActivity(Bomorder bomorder)throws HibernateException;
	
	List<ActMessage> getMessageByBmtMessageSequenceNumber(String bmtMessageSequenceNumber)throws HibernateException;
	
	boolean isUniqueMessage(String bmtMessageSequenceNumber, String messageText)throws Exception;

	List<BloomNetMessage> getMessagesEnteredToday( Date today ) throws HibernateException;

	void updateMessageStatus(Long messageId, String status) throws HibernateException;
	
}
