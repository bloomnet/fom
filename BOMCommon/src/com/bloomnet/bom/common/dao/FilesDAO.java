package com.bloomnet.bom.common.dao;

import com.bloomnet.bom.common.entity.Files;

public interface FilesDAO {

	public abstract Files find(int id);

}