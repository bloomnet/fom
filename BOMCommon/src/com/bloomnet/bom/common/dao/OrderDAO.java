package com.bloomnet.bom.common.dao;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.AuditInterface;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Paymenttype;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;

public interface OrderDAO {
	
	static final Comparator<Orderactivity> ORDER_ORDERACTIVITY_BY_DATE = new Comparator<Orderactivity> () {
        
		@Override
		public int compare(Orderactivity o1, Orderactivity o2) {
			
			 return o1.getCreatedDate().compareTo ( o2.getCreatedDate() );
		}
    };
    
	static final Comparator<Bomorder> ORDER_ORDERENTITY_BY_DELIVERY_DATE = new Comparator<Bomorder> () {

		@Override
		public int compare(Bomorder o1, Bomorder o2) {

			return o1.getDeliveryDate().compareTo ( o2.getDeliveryDate() );
		}
    };
    static final Comparator<Bomorder> ORDER_BY_DELIVERY_DATE = new Comparator<Bomorder> () {

		@Override
		public int compare(Bomorder o1, Bomorder o2) {
			long n1 = o1.getDeliveryDate().getTime();
	        long n2 = o2.getDeliveryDate().getTime();
	        if (n1 < n2) return -1;
	        else if (n1 > n2) return 1;
	        else return 0;

			
		}
    };
    
    static final Comparator<Bomorder> ORDER_BY_ORDER_CREATED_DATE = new Comparator<Bomorder> () {

		@Override
		public int compare(Bomorder o1, Bomorder o2) {
			long n1 = o1.getCreatedDate().getTime();
	        long n2 = o2.getCreatedDate().getTime();
	        if (n1 < n2) return -1;
	        else if (n1 > n2) return 1;
	        else return 0;
		}
    };
    
    static final Comparator<OrderProductInfoDetails> ORDER_BY_PRODUCT_COST = new Comparator<OrderProductInfoDetails> () {

		@Override
		public int compare(OrderProductInfoDetails o1, OrderProductInfoDetails o2) {
			double n1 = Double.parseDouble(o1.getCostOfSingleProduct());
			double n2 = Double.parseDouble(o2.getCostOfSingleProduct());
	        if (n1 < n2) return -1;
	        else if (n1 > n2) return 1;
	        else return 0;
		}
    };
    
    
	String deleteOrder( Bomorder order ) throws HibernateException ;

	Oactivitytype getActivityTypeByDesc( String desc ) throws HibernateException;

	/**
	 * returns list of order status
	 * @param orderNumber
	 * @return
	 */
	List<ActRouting> getActRouting( String orderNumber ) throws HibernateException ;
	
	Commmethod getCommmethodById(byte id) throws HibernateException ;
	
	String getLastOrderSequenceNumber() throws HibernateException;

	/**
	 * Returns receiving shop from latest child order 
	 * @param orderNumber
	 * @return
	 */
	String getCurrentFulfillingShopCode(String orderNumber) throws HibernateException ;
	

	
	Bomorder getOrderByOrderId(Long orderID) throws HibernateException ;
	
	Bomorder getParentOrderByOrderNumber( String orderNumber ) throws HibernateException ;

	List<Bomorder> getOrdersByOrderNumber( String orderNumber ) throws HibernateException ;
	
	List<Orderactivity> getOrderHistory( String orderNumber ) throws HibernateException ;
	
	/**
	 * This method will return a list of order entities which are associated with a user.
	 * 
	 * If stsOrderId parameter is null or not existing, it will not be included into a criteria.
	 * 
	 * @param user User object
	 * @param stsOrderId An order status id for values in sts_routing table
	 * @return A list of Bomorders
	 * @throws HibernateException
	 */
	List<Bomorder> getOrdersByAgent( String userName, String stsOrderId ) throws HibernateException ;
	
	
	
	List<Bomorder> getOrdersByDate( Date date ) throws HibernateException ;
	
	
	/**
	 * The list is sorted by id field, deliveryDate field is used for criteria.
	 * 
	 * @return a list of {@link Bomorder} entities
	 * @throws HibernateException
	 */
	List<Bomorder> getOrdersByToday() throws HibernateException ;
	
	/**
	 * The list is sorted by id field, deliveryDate field is used for criteria.
	 * 
	 * @return a list of {@link Bomorder} entities
	 * @throws HibernateException
	 */
	List<Bomorder> getOrdersByTommorrow() throws HibernateException ;
	
	/**
	 * get orders with the same sts_routing description
	 * @param workflowstatus
	 * @return
	 * @throws HibernateException
	 */
	List<Bomorder> getOrdersByStatus(String status) throws HibernateException ;
	
	/**
	 * get order that were worked by user with order status delivery confirmation
	 * @param user
	 * @return
	 */
	List<Bomorder> getOrdersCompletedByAgent(User user) throws HibernateException ;
	
	
	List<Shop> getTFShopsAlreadySentTo(long parentOrderId) throws HibernateException;
	
	Bomorder getOrderByBMTSeqNumber(long bmtSeqNum) throws HibernateException;
	
	Bomorder getTFOrderByBMTSeqNumber(long bmtSeqNum) throws HibernateException;
	
	List<Bomorder> getOrdersToBeWorked() throws HibernateException ;
	
	Bomorder getMostRecentOrderByAgent( Long userId, String status ) throws HibernateException ;
	
	/**
	 * Returns sending shop from parent order
	 * @param orderNumber
	 * @return
	 */
	String getOriginalSendingShopCode(String orderNumber) throws HibernateException ;
	
	StsPayment getPaymentStatusById(byte id) throws HibernateException ;
	
	Paymenttype getPaymentTypeById(byte id) throws HibernateException ;
	
	List<Paymenttype> getPaymentTypes() throws HibernateException ;
	
	/**
	 * get orders with status to be worked that are in the same zip of orderID
	 * @param orderID
	 * @return
	 * @throws HibernateException
	 */
	List<Bomorder> getRelatedOrders(String orderID) throws HibernateException ;
	
	Zip getZipByCode( String zipCode ) throws HibernateException ;
	
	Bomorder persistChildOrder( Bomorder order, String parentOrderNumber ) throws HibernateException ;
	
	String updateChildOrder(Bomorder order, String parentOrderNumber) throws HibernateException;
	
	
	
	/**
	 * This method will persist an order activity done by the application.
	 * 
	 * @param userName A userName of the existing user.
	 * @param activityRoutingDescription Order activity type description from "oactivitytype" table.
	 * @param status Status of the order activity
	 * @param orderNumber BOMorder orderNumber
	 * @param destination MQ destination
	 * @throws Exception General exception
	 * 
	 * 
	 */
	void persistOrderactivityByUser ( final String userName,
									  final String activityRoutingDescription,
									  final String status,
									  final String orderNumber,
									  final String destination ) throws Exception ;

	
	Bomorder persistParentOrder( Bomorder order ) throws HibernateException ;
	
	
	City getCityByNameAndState( String city, String stateName ) throws Exception;
	
	boolean isUniqueOrderNumber(String orderNumber) throws Exception;
	
	boolean isUniqueSeqNumberOrder(String bmtSeqNumberOfOrder) throws Exception;
	
	Bomorder getOrderByOrderNumber(String orderNumber) throws HibernateException;
	
	public Bomorder getCurrentFulfillingOrder(String orderNumber) throws HibernateException;
	
	public void persistChildOrderactivityByUser( final String userName,
												 final String activityRoutingDescription, 
												 final String status, 
												 final String orderNumber,
												 final String destination) throws Exception;
	
	Bomorder getOrderByOrderActivityId(long orderActivityId) throws Exception;
	
	List<Orderactivity> getOrderActivities( Bomorder order ) throws HibernateException;
	
	Zip getSimilarZipByCode( String zip ) throws HibernateException;
	
	Zip getZipByCityAndState(String city, String stateName) throws Exception;
	
	City getCityByZip(String zipCode, String recipientCity) throws Exception;

	List<Bomorder> getAllOrders() throws HibernateException;
	
	List<Bomorder> getAllOrders( int startIndex, int pageSize ) throws HibernateException;
	
	Long getAllOrdersCount() throws HibernateException;
	
	City persistCity(String cityName, String stateName, String countryName) throws HibernateException;

    Zip persistZip(String zipCode)throws HibernateException;
    
	byte getCurrentOrderStatus(Bomorder bomorder) throws HibernateException;
	
	Orderactivity getLatestOrderActivity(Bomorder bomorder)throws HibernateException;
	
		
	
	/**
	 * Copy the state of the given object onto the persistent object with the same identifier. 
	 * If there is no persistent instance currently associated with the session, it will be loaded. 
	 * Return the persistent instance. If the given instance is unsaved, save a copy of and return 
	 * it as a newly persistent instance. The given instance does not become associated with the 
	 * session. This operation cascades to associated instances if the association is mapped 
	 * with cascade="merge".
	 * 
	 * @param order
	 * @return
	 * @throws HibernateException
	 */
	Bomorder updateOrder( Bomorder order ) throws HibernateException;

	
	/**
	 * Store an audit record of an order
	 * 
	 * @param actAudit
	 * @throws HibernateException
	 */
	void persistOrderHistory( AuditInterface actAudit ) throws HibernateException;

	List<Bomorder> getOrdersForToday()throws HibernateException;

	List<Bomorder> getAllParentOrders() throws HibernateException;
	
	List<Bomorder> getAllParentOrders( int startIndex, int pageSize ) throws HibernateException;
	
	Long getAllParentOrdersCount() throws HibernateException;
		
	List<Bomorder> getTLOOrdersBeingWorked() throws HibernateException;
	
	List<Bomorder> getOrdersEnteredToday( Date today ) throws HibernateException;
	
	List<Bomorder> getOrdersForTommorrow(  ) throws HibernateException;
	
	boolean isStateValid(String state) throws HibernateException;
	
	//boolean isCountryValid(String countryCode) throws HibernateException;

	
	boolean isOrderLocked(String orderNumber) throws HibernateException;
	
	List<ActRouting> getAllOrderStatus(Bomorder bomorder)throws HibernateException;

	String persistParentOrderAndActivity(Bomorder bomorder, String userName,
			String activityRoutingDescription, String status, String destination)
			throws Exception;

	String persistChildOrderAndActivity(Bomorder bomorder,
			String parentOrderNumber, String userName,
			String activityRoutingDescription, String status, String destination)
			throws Exception;

	List<Bomorder> getOrdersByDate(String dateStr) throws HibernateException;

	BomorderSts persistBomorderSts(BomorderSts bomorderSts, long childId)
			throws HibernateException;

	BomorderSts updateBomorderSts(BomorderSts bomorderSts, long childId)
			throws HibernateException;

	BomorderSts getBomorderStsOrderId(Long orderID) throws HibernateException;


	Map<String, String> getAllStates() throws HibernateException;
	
	List<String> getOrderTypes(Map<String,String> orderOcassions) throws HibernateException;

	List<Bomorder> managerSearch(Map<String, String> searchParams)
			throws Exception;

	List<Bomorder> getOrdersByOcassion(List<Bomorder> results, String ocassionId)
			throws Exception;

	Map<String,Integer> getManualMessageCounts(Date lastReportDate);

	Map<String,Integer> getAutomatedMessageCounts(Date lastReportDate);

	Map<String, Byte> getAllQueues() throws HibernateException;

	Map<String, Byte> getAllStatuses() throws HibernateException;

	Map<String, String> getLastOrderAndSequenceNumberTest()
			throws HibernateException;
	
	ActRouting getCurrentOrderStatusBeforeBeingWorked(Bomorder bomorder)
		throws HibernateException;

	BomorderviewV2 getBomorderviewV2ByOrderNumber(String orderNumber);

	List searchShops(Map<String, String> searchParams) throws Exception;
	
	boolean isCountryValid(String countryCode) throws HibernateException;
	
	public City persistIntCity(String cityName, String stateName, String countryCode) throws HibernateException;

	List<Bomorder> getMultipleOrdersByOrderIds(List<String> orderIds) throws HibernateException;
	
	List<Bomorder> getOrdersInProgress(Long userId);
	
	List<Bomorder> getOrdersByOrderIds(String inString);

	List<Bomorder> getOrdersByOrderNumbersAndState(String inString, String state);

	public void persistCityCoverage(City city);



}
