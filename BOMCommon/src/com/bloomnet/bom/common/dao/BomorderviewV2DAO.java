package com.bloomnet.bom.common.dao;

import java.util.Date;
import java.util.List;

import com.bloomnet.bom.common.entity.Bomlatestdispositionview;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
import com.bloomnet.bom.common.entity.BomorderviewV2;

public interface BomorderviewV2DAO {
	
	List<BomorderviewV2> getAllOrders();
	
	List<BomorderviewV2> getOrdersForToday();
	
	List<BomorderviewV2> getOrdersEnteredToday();
		
	List<BomorderviewV2> getOrdersForTommorrow();
	
	List<BomorderviewV2> getOrdersPastDue();
	
	List<BomorderviewV2> getOutstandingOrders();
	
	List<BomorderviewV2> getOutstandingOrdersForToday();
	
	List<BomorderviewV2> getOutstandingOrdersPriorToday();

	List<BomorderviewV2> getShopOrdersForToday(long shopCode);
	
	List<BomorderviewV2> getFutureOrders();
	
	List<Bomlatestordernoteview> getAllOrderNoteView();
	
	Bomlatestordernoteview getOrderNoteView(String orderNumber);
	
	List<Bomlatestdispositionview> getAllDispositionview();
	
	Bomlatestdispositionview getDispositionView(String orderNumber);

	List<BomorderviewV2> getOrdersEnteredByDate(Date date);

	List<BomorderviewV2> getOrderByDeliveryDate(Date date);

	List<BomorderviewV2> getOutstandingOrdersByDate(Date date);

	List<BomorderviewV2> getShopOrdersByDate(long shopCode, Date date);

	List<BomorderviewV2> getOrdersWaitingForResponse();

	List<BomorderviewV2> getManualOrdersEnteredByDate(Date date);

	List<BomorderviewV2> getAutomatedOrdersEnteredByDate(Date date);

	List<BomorderviewV2> getOutstandingOrdersNoWfrs();

	List<BomorderviewV2> getFrozenAutomatedOrders();

	List<BomorderviewV2> getPreviousOutstandingOrdersNoWfrs();

	List<BomorderviewV2> getFutureOutstandingOrdersNoWfrs();

	List<BomorderviewV2> getTodayOutstandingOrdersNoWfrs();

	List<BomorderviewV2> getOutstandingAutomatedOrders();

	List<BomorderviewV2> getOrderByOrderNumber(String orderNumber);

	List<BomorderviewV2> getOutstandingMessages();

	

}
