package com.bloomnet.bom.common.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;

public interface FulfillmentSummaryDAO {

	FulfillmentSummaryDaily getFulfillmentSummaryDaily() throws HibernateException;
	
	FulfillmentSummaryMtd getFulfillmentSummaryMtd() throws HibernateException;
	
	List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDate(Date date) throws HibernateException;
	
	List<FulfillmentSummaryMtd> getFulfillmentSummaryMtdByDate(Date date) throws HibernateException;

	void callBomDailyMetricsSP(String month, String beginDate, String endDate);



	List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDateRange(
			String dateStr, String dateToStr) throws HibernateException, Exception;

}
