package com.bloomnet.bom.common.dao;

import com.bloomnet.bom.common.entity.Businesslogic;


public interface BusinessLogicDAO {
	
	public Businesslogic getCurrentQueueRoute();
	
	public long setCurrentQueueRoute(Businesslogic businessLogic);
	
	public boolean isDefaultRoute();
	
	public long setRouteToDefault();

	
	

}
