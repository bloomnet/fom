package com.bloomnet.bom.common.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.WorkOverviewDaily;

public interface WorkOverviewDailyDAO {
	
	WorkOverviewDaily getAutomatedWorkOverviewDaily() throws HibernateException;
	
	WorkOverviewDaily getTLOWorkOverviewDaily() throws HibernateException;

	List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDate(Date date)
			throws HibernateException;

	List<WorkOverviewDaily> getTLOWorkOverviewDailyByDate(Date date)
			throws HibernateException;

	List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDateRange(Date date,
			Date toDate) throws HibernateException;

	List<WorkOverviewDaily> getTLOWorkOverviewDailyByDateRange(Date date,
			Date toDate) throws HibernateException;


}
