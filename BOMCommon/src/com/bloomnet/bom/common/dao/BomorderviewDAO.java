package com.bloomnet.bom.common.dao;

import java.util.Date;
import java.util.List;

import com.bloomnet.bom.common.entity.Bomlatestdispositionview;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
import com.bloomnet.bom.common.entity.Bomorderview;

public interface BomorderviewDAO {
	
	List<Bomorderview> getAllOrders();
	
	List<Bomorderview> getOrdersForToday();
	
	List<Bomorderview> getOrdersEnteredToday();
		
	List<Bomorderview> getOrdersForTommorrow();
	
	List<Bomorderview> getOrdersPastDue();
	
	List<Bomorderview> getOutstandingOrders();
	
	List<Bomorderview> getOutstandingOrdersForToday();
	
	List<Bomorderview> getOutstandingOrdersPriorToday();

	List<Bomorderview> getShopOrdersForToday(String shopCode);
	
	List<Bomorderview> getFutureOrders();
	
	List<Bomlatestordernoteview> getAllOrderNoteView();
	
	Bomlatestordernoteview getOrderNoteView(String orderNumber);
	
	List<Bomlatestdispositionview> getAllDispositionview();
	
	Bomlatestdispositionview getDispositionView(String orderNumber);

	List<Bomorderview> getOrdersEnteredByDate(Date date);

	List<Bomorderview> getOrderByDeliveryDate(Date date);

	List<Bomorderview> getOutstandingOrdersByDate(Date date);

	List<Bomorderview> getShopOrdersByDate(String shopCode, Date date);

	

}
