package com.bloomnet.bom.common.dao;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.ShopNotes;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.WebserviceMonitor;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.exceptions.AddressException;


public interface ShopDAO {
	
	static final String[] BLOOMNET_NETWORK_NAMES  = {"BMT","BloomNet"};
	static final String[] TELEFLORA_NETWORK_NAMES = {"Teleflora"};
	static final String[] FTD_NETWORK_NAMES = {"FTD"};
	static final String[] OTHER_NETWORK_NAMES = {"Other"};
	
static final Comparator<Shopnetwork> SHOP_NETWORK_BY_ID = new Comparator<Shopnetwork> () {
        
		@Override
		public int compare(Shopnetwork o1, Shopnetwork o2) {
			
			 return o1.getNetwork().getNetworkId().compareTo ( o2.getNetwork().getNetworkId() );
		}
    };
    
static final Comparator<Shop> SHOP_BY_NAME = new Comparator<Shop> () {
        
		@Override
		public int compare(Shop o1, Shop o2) {
			
			 return o1.getShopName().toUpperCase().compareTo ( o2.getShopName().toUpperCase() );
		}
    };
	
	Shop getShopByShopId(Long shopID) throws HibernateException;
	
	Shop getShopByCode(String shopCode) throws HibernateException;
		
	List<Shop> getShopsByZipCode(String zipCode) throws HibernateException;
	
	List<Shop> getShopByCity(String cityName) throws HibernateException;
	
	/**
	 * Shops that an order was sent to fulfill and was rejected 
	 * @param orderNumber
	 * @return
	 * @throws HibernateException
	 */
	List<String> getPreviousAttemptedShops(long orderId) throws HibernateException;
	
	/**
	 * TLO shops are shops in BloomNet and Other network
	 */
	List<Shop> getAvailableTLOShops(String deliveryDate,String zip) throws HibernateException;
	
	/**
	 * TFSI shops are shops in Teleflora network
	 * 
	 * @param zip
	 * @return
	 * @throws HibernateException
	 */
	List<Shop> getAvailableTFSIShops(String dayOfTheWeek, Zip zip) throws HibernateException;
		
	void setShopStatus( Shop shop, Byte shopnetworkStatusId ) throws HibernateException;

	String persistShop(Shop shop) throws HibernateException;
	
	String deleteShop(Shop shop) throws HibernateException;

	void deleteShop( long shopId ) throws HibernateException;
	
	/**
	 * This method returns a list of shops by zip code and communication method description
	 * using "ShopNetworkCoverage" data with associated network status
	 * 
	 * @param zip
	 * @param comMethodDescription
	 * @return
	 * @throws AddressException 
	 * @throws Exception
	 */
	Map<Shop, Shopnetworkstatus> getShopsWithStatus( String zip, String comMethodDescription ) throws AddressException;
	
	/**
	 * This method returns a list of shops by zip code and communication method description
	 * using "ShopNetworkCoverage" data
	 * 
	 * @param zip
	 * @param comMethodDescription
	 * @return
	 * @throws AddressException 
	 * @throws Exception
	 */
	List<Shop> getShops( String zip, String comMethodDescription ) throws HibernateException, AddressException;
	
	/**
	 * Returns a list of networks which are associated with a shop
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	List<Shopnetwork> getShopNetworks( Long shopId ) throws HibernateException;
	
	/**
	 * Returns a network which is associated with a shop and it's name is in the list of:
	 * {@link com.bloomnet.bom.common.dao.ShopDAO.BLOOMNET_NETWORK_NAMES}
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	Shopnetwork getBloomNetShopnetwork( Long shopId ) throws HibernateException;
	
	/**
	 * Returns a network which is associated with a shop and it's name is in the list of:
	 * {@link com.bloomnet.bom.common.dao.ShopDAO.TELEFLORA_NETWORK_NAMES}
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	Shopnetwork getTelefloraShopnetwork( Long shopId ) throws HibernateException;
	
	/**
	 * Returns a network which is associated with a shop and it's name is in the list of:
	 * {@link com.bloomnet.bom.common.dao.ShopDAO.FTD_NETWORK_NAMES}
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	Shopnetwork getFTDShopnetwork( Long shopId ) throws HibernateException;
	
	/**
	 * Returns a network which is associated with a shop and it's name is in the list of:
	 * {@link com.bloomnet.bom.common.dao.ShopDAO.OTHER_NETWORK_NAMES}
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	Shopnetwork getOtherShopnetwork( Long shopId ) throws HibernateException;
	
	/**
	 * Returns a list of {@link com.bloomnet.bom.common.entity.Shopnetwork} which are associated 
	 * with a shop and it's name is in the list of {@link #networkNames}, if the networkNames
	 * parameter is null, the default lists will be used:
	 * 
	 * 1) TELEFLORA - {@link com.bloomnet.bom.common.dao.ShopDAO.TELEFLORA_NETWORK_NAMES}
	 * 2) BLOOMNET - {@link com.bloomnet.bom.common.dao.ShopDAO.BLOOMNET_NETWORK_NAMES}
	 * 
	 * @param shopId
	 * @param networkNames
	 * @return
	 * @throws HibernateException
	 */
	List<Shopnetwork> getShopnetworks( Long shopId, String[] networkNames ) throws HibernateException;
	
	/**
	 * Returns a shop code which is associated with a shop id and a list of network names, the network names
	 * are the default names provided by arrays:
	 * 
	 * 1) TELEFLORA - {@link com.bloomnet.bom.common.dao.ShopDAO.TELEFLORA_NETWORK_NAMES}
	 * 2) BLOOMNET - {@link com.bloomnet.bom.common.dao.ShopDAO.BLOOMNET_NETWORK_NAMES}
	 * 
	 * 
	 * @param shopId
	 * @return
	 * @throws HibernateException
	 */
	String getShopCodeFromDefaultShopnetworks( Long shopId ) throws HibernateException; 
	
	List<State> getStates() throws HibernateException; 
	
	State getState( Long stateId ) throws HibernateException; 
	
	Zip getZip( String zipCode ) throws HibernateException, AddressException;
	
	City getCity ( String cityName ) throws HibernateException, AddressException;
	
	void persitShopnetwork( Shopnetwork shopnetwork ) throws HibernateException; 
	
	void persitShopnetworkcoverage( Shopnetworkcoverage shopnetworkcoverage ) throws HibernateException; 
	
	Network getNetwork ( String networkName ) throws HibernateException; 
	
	Shopnetworkstatus getShopnetworkstatus( Byte shopNetworkStatusId ) throws HibernateException; 
	
	Shop getShopByAddressAndPhone(String address, String phone) throws HibernateException;

	String getShopCodeByPhoneNumber(String phoneNumber) throws HibernateException;

	Map<String, Long> getAllNetworks() throws HibernateException;

	void mergeShop(Shop shop) throws HibernateException;

	void mergeShopNetwork(Shopnetwork network)throws HibernateException;

	List<Shopnetworkcoverage> getShopNetworkCoverage(Shopnetwork shopnetwork);
	
	public String getInternationalShopByCountry(String countryCode);
	
	public WebserviceMonitor getLastQuery(Network network) throws Exception;
	
	public void saveOrUpdateBloomlinkQuery(WebserviceMonitor webservicemonitor);
	
	public void persistShopNotes ( ShopNotes shopNotes );
	
	public void removeCoverage(Shop myShop, Zip zip, User user);


}
