package com.bloomnet.bom.common.dao;

import java.util.List;

import com.bloomnet.bom.common.entity.Tloview;
import com.bloomnet.bom.common.entity.Tloviewv2;

public interface TloviewDAO {
	
	Tloview getOrder() throws Exception;

	List<Tloview> getAllOrders();

	Tloviewv2 getOrder(String timeZone) throws Exception;

}
