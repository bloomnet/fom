package com.bloomnet.bom.common.dao;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.ActPayment;
import com.bloomnet.bom.common.entity.PaymentInterface;
import com.bloomnet.bom.common.entity.StsPayment;

public interface PaymentDAO {

	void persistPayment(ActPayment payment) throws HibernateException;
	
	ActPayment getPayment(String orderNumber) throws HibernateException;
	
	void deletePayment(String orderNumber) throws HibernateException;

	void persistPayment(PaymentInterface actPayment) throws HibernateException;

	StsPayment getStatusById(Byte id) throws HibernateException;

	StsPayment getStatusById(String id) throws HibernateException;
}
