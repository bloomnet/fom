package com.bloomnet.bom.common.util;

import com.sendgrid.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SendGridEmailer {

   public SendGridEmailer(String body) throws IOException {

       Email from = new Email("fom@bloomnet.net");
       Email to = new Email("msilver@bloomnet.net");
       
       Personalization p = new Personalization();
       p.addTo(new Email("msilver@bloomnet.net"));
       p.addTo(new Email("lelcook@bloomnet.net"));
       p.addTo(new Email("jpursoo@bloomnet.net"));
       p.addTo(new Email("jwaterhouse@1800flowers.com"));
       p.addTo(new Email("awilliams@1800flowers.com"));
       p.addTo(new Email("ldarner@1800flowers.com"));
       p.addTo(new Email("tsahebzada@bloomnet.net"));

       String subject = "FOM Error Alert - Production";
       Content content = new Content("text/html", body);

       Mail mail = new Mail(from, subject, to, content);
       mail.addPersonalization(p);
       
       InputStream input = null;
		try {
			input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		Properties props = new Properties();
      try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}

       SendGrid sg = new SendGrid(props.getProperty("apikey"));
       
       Request request = new Request();

       request.setMethod(Method.POST);
       request.setEndpoint("mail/send");
       request.setBody(mail.build());

       Response response = sg.api(request);

       System.out.println(response.getStatusCode());
       System.out.println(response.getHeaders());
       System.out.println(response.getBody());
   }
}