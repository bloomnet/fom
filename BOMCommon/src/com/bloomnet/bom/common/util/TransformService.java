package com.bloomnet.bom.common.util;

import java.io.IOException;

import org.jdom.JDOMException;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.audit.AuditInterface;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.recipe.RecipeInfoInterface;
import com.bloomnet.bom.common.tfsi.Transaction;

public interface TransformService {

	/** 
	 * Marshalls foreignSystemInterface 
	 * @param foreignSystemInterface
	 * @return
	 */
	public abstract String convertToFSIXML(
			ForeignSystemInterface foreignSystemInterface);

	/**
	 * UnMarshalls foreignSystemInterface 
	 * @param xmlString
	 * @return
	 */
	public abstract ForeignSystemInterface convertToJavaFSI(String xmlString);

	public abstract String convertToAuditXML(AuditInterface auditInterface);

	public abstract AuditInterface convertToJavaAudit(String xmlString);

	/**
	 * Creates foreignSystemInterface using MessagesOnOrder and Security objects
	 * @param aMessageOnOrder
	 * @param security
	 * @return
	 */
	public abstract String createForeignSystemInterface(
			MessagesOnOrder aMessageOnOrder, Security security);

	/**
	 * Creates foreignSystemInterface from messageType string and BloomNetMessage object
	 * @param messageType
	 * @param message
	 * @return
	 * @throws Exception 
	 */
	public abstract String createForeignSystemInterface(String messageType,
			BloomNetMessage message) throws Exception;

	/**
	 * Creates foreignSystemInterface outbound Message from elements of existing message
	 * @param messageType
	 * @param message
	 * @param originalSendingShopCode
	 * @param currentFulfillingShopCode
	 * @return
	 * @throws Exception 
	 */
	public abstract String createOutboundMessage(String messageType,
			BloomNetMessage message, String originalSendingShopCode,
			String currentFulfillingShopCode, String parentOrderNumber,
			String bmtSeqNumberOfOrder, String bmtSeqNumberOfMessage)
			throws Exception;

	//XXX
	public abstract String convertToTFSIXML(Transaction transaction);

	public abstract String convertToMDIXML(
			MemberDirectoryInterface memberDirectoryInterface);

	public abstract MemberDirectoryInterface convertToJavaMDI(String xmlString);

	/**
	 * Creates MessageOnOrderBean for Model from foreignSystemInterface object
	 * @param fsi
	 * @return
	 * @throws Exception 
	 */
	public abstract MessageOnOrderBean createBeanFromFSI(
			ForeignSystemInterface fsi, Shop receivingShop, Shop sendingShop,
			Shop fulfillingShop) throws Exception;

	@SuppressWarnings("unchecked")
	public abstract MessageOnOrderBean createBeanFromTFSI(String tfsiXML,
			Shop sendingShop, Shop receivingShop) throws JDOMException,
			IOException;

	String convertToRecipeXML(RecipeInfoInterface recipeInterface);

	RecipeInfoInterface convertToJavaRecipe(String xmlString);

}