package com.bloomnet.bom.common.util;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckf;
import com.bloomnet.bom.common.jaxb.fsi.MessageCanc;
import com.bloomnet.bom.common.jaxb.fsi.MessageConf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDeni;
import com.bloomnet.bom.common.jaxb.fsi.MessageDisp;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlca;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlou;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspc;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspd;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspr;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;

public class MessageFactory {

    static private MessageFactory instance = null;
    
 
    // private constructor, we don't want the 
    // class to be instantiated from others.
    private MessageFactory() {

    }
 
    /**
     * Singleton Getter
     */
	public static MessageFactory getInstance() {
		if(instance == null) {
			synchronized(MessageFactory.class) { 
				instance = new MessageFactory();
			}
		}
		return instance;
	}
      
    /**
     * This method will return a Message Object based on its type in {@link BOMConstants}
     * 
     *  ORDER_MESSAGE_TYPE = "0";
     *  INQR_MESSAGE_TYPE = "1";
     *  RESP_MESSAGE_TYPE = "2";
     *  RJCT_MESSAGE_TYPE = "4";
     *  CANC_MESSAGE_TYPE = "3";
     *  DENI_MESSAGE_TYPE = "6";
     *  DLCF_MESSAGE_TYPE = "7";
     *  DLOU_MESSAGE_TYPE = "8";
     *  CONF_MESSAGE_TYPE = "11";
     *  INFO_MESSAGE_TYPE = "12";
     *  PCHG_MESSAGE_TYPE = "18";
     *  ACKF_MESSAGE_TYPE = "24";
     *  DLCA_MESSAGE_TYPE = "26";
     * 
     * @param type
     * @return
     */
    public BloomNetMessage createMessageObject( int type ) {
    	
    	BloomNetMessage result;
    	
    	switch ( type ) {
    	
	    	case 0: 
	    		result = new MessageOrder();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 1: 
	    		result = new MessageInqr();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 2: 
	    		result = new MessageResp();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 4: 
	    		result = new MessageRjct();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 3: 
	    		result = new MessageCanc();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 6: 
	    		result = new MessageDeni();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 7: 
	    		result = new MessageDlcf();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 8: 
	    		result = new MessageDlou();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 11: 
	    		result = new MessageConf();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 12: 
	    		result = new MessageInfo();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 18: 
	    		result = new MessagePchg();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 24: 
	    		result = new MessageAckf();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	case 26: 
	    		result = new MessageDlca();
	    		result.setMessageType( String.valueOf( type ) );
	    		
	    	default: result = null;
    	}
    	
    	return result;
    }
    
    
    public BloomNetMessage createMessageObject( String type ) throws Exception {
    	
    	BloomNetMessage result = null;

		try {
			
			result = createMessageObject( Integer.parseInt( type ) );
			
			if( result == null ) {
				
				throw new Exception( "Could not create message object for "+type );
			}
			
		} catch ( NumberFormatException e ) {
			
			throw new Exception( "Could not create message object for "+type );
		}

    	return result;
    }
    
    
    public BloomNetMessage createMessageObjectFromMessageType( String shortDescription ) throws Exception {
    	
    	BloomNetMessage result = null;

    	if( BOMConstants.ORDER_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageOrder();
    		result.setMessageType( BOMConstants.ORDER_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.INQR_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageInqr();
    		result.setMessageType( BOMConstants.INQR_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.RESP_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageResp();
    		result.setMessageType( BOMConstants.RESP_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.RJCT_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageRjct();
    		result.setMessageType( BOMConstants.RJCT_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.CANC_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageCanc();
    		result.setMessageType( BOMConstants.CANC_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.DENI_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageDeni();
    		result.setMessageType( BOMConstants.DENI_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.DLCF_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageDlcf();
    		result.setMessageType( BOMConstants.DLCF_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.DLCA_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageDlca();
    		result.setMessageType( BOMConstants.DLCA_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.DLOU_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageDlou();
    		result.setMessageType( BOMConstants.DLOU_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.CONF_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageConf();
    		result.setMessageType( BOMConstants.CONF_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.INFO_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageInfo();
    		result.setMessageType( BOMConstants.INFO_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.PCHG_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessagePchg();
    		result.setMessageType( BOMConstants.PCHG_MESSAGE_TYPE );
    	}
    	else if( BOMConstants.ACKF_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ) {
    		
    		result = new MessageAckf();
    		result.setMessageType( BOMConstants.ACKF_MESSAGE_TYPE );
    		
    	}else if( BOMConstants.DSPC_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ){
    		
    		result = new MessageDspc();
    		result.setMessageType( BOMConstants.DSPC_MESSAGE_DESC );
    		
    	}else if( BOMConstants.DSPD_MESSAGE_DESC.equalsIgnoreCase( shortDescription ) ){
    		
    		result = new MessageDspd();
    		result.setMessageType( BOMConstants.DSPD_MESSAGE_DESC );
    		
    	}


		if( result == null ) {
			
			throw new Exception( "Could not create message object for "+shortDescription );
		}

    	return result;
    }
}
