package com.bloomnet.bom.common.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.apache.log4j.net.SMTPAppender;


public class BOMAppender extends SMTPAppender {
	private boolean startTLS = false;
    private static final String SMTP_AUTH_USER = "";
    private static final String SMTP_AUTH_PWD  = "";



	@Override
	protected Session createSession() {
		Properties props = null;
		try {
			props = new Properties(System.getProperties());
		} catch (SecurityException ex) {
			props = new Properties();
		}
		
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.host", this.getSMTPHost());
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	        System.out.println(props);


		Authenticator auth = new SMTPAuthenticator();
		
		Session session = Session.getInstance(props, auth);
		//if (this.getSMTPProtocol() != null)
		//	session.setProtocolForAddress("rfc822", this.getSMTPProtocol());
		if (this.getSMTPDebug())
			session.setDebug(this.getSMTPDebug());
		return session;
	}

	public boolean isStartTLS() {
		return this.startTLS;
	}

	public void setStartTLS(boolean startTLS) {
		this.startTLS = startTLS;
	}
	
	 private class SMTPAuthenticator extends javax.mail.Authenticator {

			public PasswordAuthentication getPasswordAuthentication() {
	           String username = SMTP_AUTH_USER;
	           String password = SMTP_AUTH_PWD;
	           return new PasswordAuthentication(username, password);
	        }
	 }


}
