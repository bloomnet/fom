package com.bloomnet.bom.common.util;

import com.bloomnet.bom.common.constants.BOMConstants;

public class MessageTypeUtil {
	
	public static int getMessageTypeInt(String messageTypeDesc){
		
		int messageType = 0;
		
		if (messageTypeDesc.equals(BOMConstants.ORDER_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.ORDER_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.INQR_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.INQR_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.RESP_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.RESP_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.RJCT_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.RJCT_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.CANC_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.CANC_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.DENI_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.DENI_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.DLCF_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.DLCF_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.DLCA_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.DLCA_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.DLOU_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.DLOU_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.CONF_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.CONF_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.INFO_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.INFO_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.PCHG_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.PCHG_MESSAGE_TYPE);
		}
		else if (messageTypeDesc.equals(BOMConstants.ACKF_MESSAGE_DESC)){
			messageType = Integer.parseInt(BOMConstants.ACKF_MESSAGE_TYPE);
		}
		
		return messageType;
		
	}
	
	public static String getMessageDesc(int messageType){
		
		String messageTypeDesc = new String();
		
		if (Integer.toString(messageType).equals(BOMConstants.ORDER_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.ORDER_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.INQR_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.INQR_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.RESP_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.RESP_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.RJCT_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.RJCT_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.CANC_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.CANC_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.DENI_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.DENI_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.DLCF_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.DLCF_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.DLCA_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.DLCA_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.DLOU_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.DLOU_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.CONF_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.CONF_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.INFO_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.INFO_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.PCHG_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.PCHG_MESSAGE_DESC;
		}
		else if (Integer.toString(messageType).equals(BOMConstants.ACKF_MESSAGE_TYPE)){
			messageTypeDesc = BOMConstants.ACKF_MESSAGE_DESC;
		}
		
		return messageTypeDesc;
		
	}

}
