package com.bloomnet.bom.common.entity;

import java.io.Serializable;

public interface OrderActivityCarrier extends Serializable {

	Orderactivity getOrderactivity();
	void setOrderactivity(Orderactivity orderactivity);
}
