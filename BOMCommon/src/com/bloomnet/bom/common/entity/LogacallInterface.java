package com.bloomnet.bom.common.entity;

public interface LogacallInterface extends OrderActivityCarrier {

	String getLogAcallText();
	void setLogAcallText(String logAcallText);
	
	void setShopCalled(Shop shopCalled);
	Shop getShopCalled();
	
	Calldisp getCalldisp();
	void setCalldisp(Calldisp calldisp);
}
