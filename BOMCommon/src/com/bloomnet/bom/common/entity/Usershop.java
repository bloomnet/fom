package com.bloomnet.bom.common.entity;

// default package
// Generated Oct 3, 2011 5:44:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;


public class Usershop implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long userShopId;
	private User userByCreatedUserId;
	private User userByUserId;
	private Shop shop;
	private User userByModifiedUserId;
	private Date createdDate;
	private Date modifiedDate;

	public Usershop() {
	}

	public Usershop(User userByCreatedUserId, User userByUserId, Shop shop,
			Date createdDate) {
		this.userByCreatedUserId = userByCreatedUserId;
		this.userByUserId = userByUserId;
		this.shop = shop;
		this.createdDate = createdDate;
	}

	public Usershop(User userByCreatedUserId, User userByUserId, Shop shop,
			User userByModifiedUserId, Date createdDate,
			Date modifiedDate) {
		this.userByCreatedUserId = userByCreatedUserId;
		this.userByUserId = userByUserId;
		this.shop = shop;
		this.userByModifiedUserId = userByModifiedUserId;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}

	public Long getUserShopId() {
		return this.userShopId;
	}

	protected void setUserShopId(Long userShopId) {
		this.userShopId = userShopId;
	}

	public User getUserByCreatedUserId() {
		return this.userByCreatedUserId;
	}

	public void setUserByCreatedUserId(User userByCreatedUserId) {
		this.userByCreatedUserId = userByCreatedUserId;
	}

	public User getUserByUserId() {
		return this.userByUserId;
	}

	public void setUserByUserId(User userByUserId) {
		this.userByUserId = userByUserId;
	}

	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public User getUserByModifiedUserId() {
		return this.userByModifiedUserId;
	}

	public void setUserByModifiedUserId(User userByModifiedUserId) {
		this.userByModifiedUserId = userByModifiedUserId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
