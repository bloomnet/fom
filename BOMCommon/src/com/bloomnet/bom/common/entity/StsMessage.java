package com.bloomnet.bom.common.entity;

// default package
// Generated Oct 3, 2011 5:44:13 PM by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * StsMessage generated by hbm2java
 */
public class StsMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Byte stsMessageId;
	private User userByCreatedUserId;
	private User userByModifiedUserId;
	private String description;
	private Date createdDate;
	private Date modifiedDate;
	private Set<ActMessage> actMessages = new HashSet<ActMessage>(0);

	public StsMessage() {
	}

	public StsMessage(User userByCreatedUserId, String description,
			Date createdDate) {
		this.userByCreatedUserId = userByCreatedUserId;
		this.description = description;
		this.createdDate = createdDate;
	}

	public StsMessage(User userByCreatedUserId, User userByModifiedUserId,
			String description, Date createdDate, Date modifiedDate,
			Set<ActMessage> actMessages) {
		this.userByCreatedUserId = userByCreatedUserId;
		this.userByModifiedUserId = userByModifiedUserId;
		this.description = description;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.actMessages = actMessages;
	}

	public Byte getStsMessageId() {
		return this.stsMessageId;
	}

	public void setStsMessageId(Byte stsMessageId) {
		this.stsMessageId = stsMessageId;
	}

	public User getUserByCreatedUserId() {
		return this.userByCreatedUserId;
	}

	public void setUserByCreatedUserId(User userByCreatedUserId) {
		this.userByCreatedUserId = userByCreatedUserId;
	}

	public User getUserByModifiedUserId() {
		return this.userByModifiedUserId;
	}

	public void setUserByModifiedUserId(User userByModifiedUserId) {
		this.userByModifiedUserId = userByModifiedUserId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Set<ActMessage> getActMessages() {
		return this.actMessages;
	}

	public void setActMessages(Set<ActMessage> actMessages) {
		this.actMessages = actMessages;
	}
}
