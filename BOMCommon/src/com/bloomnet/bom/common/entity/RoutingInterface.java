package com.bloomnet.bom.common.entity;


public interface RoutingInterface extends OrderActivityCarrier {

	StsRouting getStsRouting();
	void setStsRouting(StsRouting stsRouting);

	String getVirtualQueue();
	void setVirtualQueue(String virtualQueue);

	String getStatus();
	void setStatus(String status);
}
