//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.09.08 at 03:23:15 PM EDT 
//


package com.bloomnet.bom.common.jaxb.fsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.DeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.MessageOfflineOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.fsi.ShippingDetails;
import com.bloomnet.bom.common.jaxb.fsi.WireServiceCode;


/**
 * <p>Java class for messageOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageOrder">
 *   &lt;complexContent>
 *     &lt;extension base="{}bloomNetMessage">
 *       &lt;sequence>
 *         &lt;element name="orderDetails" type="{}orderDetails"/>
 *         &lt;element name="orderCaptureDate" type="{}timestampFormat"/>
 *         &lt;element name="deliveryDetails" type="{}deliveryDetails"/>
 *         &lt;element name="recipient" type="{}recipient"/>
 *         &lt;element name="wireServiceCode" type="{}wireServiceCode"/>
 *         &lt;element name="shippingDetails" type="{}shippingDetails"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageOrder", propOrder = {
    "orderDetails",
    "orderCaptureDate",
    "deliveryDetails",
    "recipient",
    "wireServiceCode",
    "shippingDetails",
    "pickupCode",
    "containsPerishables"
})
@XmlSeeAlso({
    MessageOfflineOrder.class
})
public class MessageOrder
    extends BloomNetMessage
{

    @XmlElement(required = true)
    protected OrderDetails orderDetails;
    @XmlElement(required = true)
    protected String orderCaptureDate;
    @XmlElement(required = true)
    protected DeliveryDetails deliveryDetails;
    @XmlElement(required = true)
    protected Recipient recipient;
    @XmlElement(required = true)
    protected WireServiceCode wireServiceCode;
    @XmlElement(required = true)
    protected ShippingDetails shippingDetails;
    @XmlElement(required = false)
    protected String pickupCode;
    @XmlElement(required = false)
    protected String containsPerishables;

    /**
     * Gets the value of the orderDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OrderDetails }
     *     
     */
    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    /**
     * Sets the value of the orderDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderDetails }
     *     
     */
    public void setOrderDetails(OrderDetails value) {
        this.orderDetails = value;
    }

    /**
     * Gets the value of the orderCaptureDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderCaptureDate() {
        return orderCaptureDate;
    }

    /**
     * Sets the value of the orderCaptureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderCaptureDate(String value) {
        this.orderCaptureDate = value;
    }

    /**
     * Gets the value of the deliveryDetails property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryDetails }
     *     
     */
    public DeliveryDetails getDeliveryDetails() {
        return deliveryDetails;
    }

    /**
     * Sets the value of the deliveryDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryDetails }
     *     
     */
    public void setDeliveryDetails(DeliveryDetails value) {
        this.deliveryDetails = value;
    }

    /**
     * Gets the value of the recipient property.
     * 
     * @return
     *     possible object is
     *     {@link Recipient }
     *     
     */
    public Recipient getRecipient() {
        return recipient;
    }

    /**
     * Sets the value of the recipient property.
     * 
     * @param value
     *     allowed object is
     *     {@link Recipient }
     *     
     */
    public void setRecipient(Recipient value) {
        this.recipient = value;
    }

    /**
     * Gets the value of the wireServiceCode property.
     * 
     * @return
     *     possible object is
     *     {@link WireServiceCode }
     *     
     */
    public WireServiceCode getWireServiceCode() {
        return wireServiceCode;
    }

    /**
     * Sets the value of the wireServiceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link WireServiceCode }
     *     
     */
    public void setWireServiceCode(WireServiceCode value) {
        this.wireServiceCode = value;
    }

    /**
     * Gets the value of the shippingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public ShippingDetails getShippingDetails() {
        return shippingDetails;
    }
    
    /**
     * Sets the value of the shippingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setShippingDetails(ShippingDetails value) {
        this.shippingDetails = value;
    }
    
    /**
     * Sets the value of the pickupCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setPickupCode(String value) {
        this.pickupCode = value;
    }
    
    /**
     * Gets the value of the pickupCode property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public String getPickupCode() {
        return pickupCode;
    }
    

    /**
     * Sets the value of the containsPerishables property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingDetails }
     *     
     */
    public void setContainsPerishables(String value) {
        this.containsPerishables = value;
    }
    
    /**
     * Gets the value of the containsPerishables property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingDetails }
     *     
     */
    public String getContainsPerishables() {
        return containsPerishables;
    }

}
