package com.bloomnet.bom.common.jaxb.fsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for messageOrderRelated complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageOrderRelated">
 *   &lt;complexContent>
 *     &lt;extension base="{}bloomNetMessage">
 *       &lt;sequence>
 *         &lt;element name="messageText" type="{}messageText"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageOrderRelated", propOrder = {
    "messageText"
})
@XmlSeeAlso({
    MessageCanc.class,
    MessageConf.class,
    MessageInfo.class,
    MessageRjct.class,
    MessageShipped.class,
    MessagePchg.class,
    MessageDspu.class,
    MessageDspr.class,
    MessageDspd.class,
    MessageDspc.class,
    MessageInqr.class,
    MessageResp.class,
    MessageDeni.class
})
public class MessageOrderRelated
    extends BloomNetMessage
{

    @XmlElement(required = true)
    protected String messageText;

    /**
     * Gets the value of the messageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Sets the value of the messageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageText(String value) {
        this.messageText = value;
    }

}
