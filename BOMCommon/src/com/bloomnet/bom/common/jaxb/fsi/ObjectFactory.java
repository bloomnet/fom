package com.bloomnet.bom.common.jaxb.fsi;


import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ForeignSystemInterface_QNAME = new QName("", "foreignSystemInterface");
    private final static QName _ForeignSystemInterfaceOutboundRequest_QNAME = new QName("", "foreignSystemInterfaceOutboundRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MessageNftn }
     * 
     */
    public MessageNftn createMessageNftn() {
        return new MessageNftn();
    }

    /**
     * Create an instance of {@link RedeliveryDetails }
     * 
     */
    public RedeliveryDetails createRedeliveryDetails() {
        return new RedeliveryDetails();
    }

    /**
     * Create an instance of {@link OrderDetails }
     * 
     */
    public OrderDetails createOrderDetails() {
        return new OrderDetails();
    }

    /**
     * Create an instance of {@link MessageAckf }
     * 
     */
    public MessageAckf createMessageAckf() {
        return new MessageAckf();
    }

    /**
     * Create an instance of {@link CustomPosIdentifiers }
     * 
     */
    public CustomPosIdentifiers createCustomPosIdentifiers() {
        return new CustomPosIdentifiers();
    }

    /**
     * Create an instance of {@link MessageCanc }
     * 
     */
    public MessageCanc createMessageCanc() {
        return new MessageCanc();
    }

    /**
     * Create an instance of {@link MessageResp }
     * 
     */
    public MessageResp createMessageResp() {
        return new MessageResp();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link MessageInqr }
     * 
     */
    public MessageInqr createMessageInqr() {
        return new MessageInqr();
    }

    /**
     * Create an instance of {@link OperationalSummary }
     * 
     */
    public OperationalSummary createOperationalSummary() {
        return new OperationalSummary();
    }

    /**
     * Create an instance of {@link MessageShippedAckb }
     * 
     */
    public MessageShippedAckb createMessageShippedAckb() {
        return new MessageShippedAckb();
    }

    /**
     * Create an instance of {@link Recipient }
     * 
     */
    public Recipient createRecipient() {
        return new Recipient();
    }

    /**
     * Create an instance of {@link MessageDisp }
     * 
     */
    public MessageDisp createMessageDisp() {
        return new MessageDisp();
    }

    /**
     * Create an instance of {@link MessageGenericDlcf }
     * 
     */
    public MessageGenericDlcf createMessageGenericDlcf() {
        return new MessageGenericDlcf();
    }

    /**
     * Create an instance of {@link MessagePchg }
     * 
     */
    public MessagePchg createMessagePchg() {
        return new MessagePchg();
    }

    /**
     * Create an instance of {@link MessageDlca }
     * 
     */
    public MessageDlca createMessageDlca() {
        return new MessageDlca();
    }

    /**
     * Create an instance of {@link Identifiers }
     * 
     */
    public Identifiers createIdentifiers() {
        return new Identifiers();
    }

    /**
     * Create an instance of {@link MessageConf }
     * 
     */
    public MessageConf createMessageConf() {
        return new MessageConf();
    }

    /**
     * Create an instance of {@link MessageDlou }
     * 
     */
    public MessageDlou createMessageDlou() {
        return new MessageDlou();
    }

    /**
     * Create an instance of {@link MessageDspd }
     * 
     */
    public MessageDspd createMessageDspd() {
        return new MessageDspd();
    }

    /**
     * Create an instance of {@link Errors }
     * 
     */
    public Errors createErrors() {
        return new Errors();
    }

    /**
     * Create an instance of {@link PendingMessages }
     * 
     */
    public PendingMessages createPendingMessages() {
        return new PendingMessages();
    }

    /**
     * Create an instance of {@link FSIOutboundRequestData }
     * 
     */
    public FSIOutboundRequestData createFSIOutboundRequestData() {
        return new FSIOutboundRequestData();
    }

    /**
     * Create an instance of {@link MessageAckb }
     * 
     */
    public MessageAckb createMessageAckb() {
        return new MessageAckb();
    }

    /**
     * Create an instance of {@link OrderProductsInfo }
     * 
     */
    public OrderProductsInfo createOrderProductsInfo() {
        return new OrderProductsInfo();
    }

    /**
     * Create an instance of {@link MessageShipped }
     * 
     */
    public MessageShipped createMessageShipped() {
        return new MessageShipped();
    }

    /**
     * Create an instance of {@link MessageOrderRelated }
     * 
     */
    public MessageOrderRelated createMessageOrderRelated() {
        return new MessageOrderRelated();
    }

    /**
     * Create an instance of {@link MessageInfo }
     * 
     */
    public MessageInfo createMessageInfo() {
        return new MessageInfo();
    }

    /**
     * Create an instance of {@link ShippingDetails }
     * 
     */
    public ShippingDetails createShippingDetails() {
        return new ShippingDetails();
    }

    /**
     * Create an instance of {@link ForeignSystemInterface }
     * 
     */
    public ForeignSystemInterface createForeignSystemInterface() {
        return new ForeignSystemInterface();
    }

    /**
     * Create an instance of {@link MessageDspr }
     * 
     */
    public MessageDspr createMessageDspr() {
        return new MessageDspr();
    }

    /**
     * Create an instance of {@link MessageDeni }
     * 
     */
    public MessageDeni createMessageDeni() {
        return new MessageDeni();
    }

    /**
     * Create an instance of {@link MessageOfflineOrder }
     * 
     */
    public MessageOfflineOrder createMessageOfflineOrder() {
        return new MessageOfflineOrder();
    }

    /**
     * Create an instance of {@link MessageOrder }
     * 
     */
    public MessageOrder createMessageOrder() {
        return new MessageOrder();
    }

    /**
     * Create an instance of {@link MessageDlcf }
     * 
     */
    public MessageDlcf createMessageDlcf() {
        return new MessageDlcf();
    }

    /**
     * Create an instance of {@link MessageDspc }
     * 
     */
    public MessageDspc createMessageDspc() {
        return new MessageDspc();
    }

    /**
     * Create an instance of {@link MessageRjct }
     * 
     */
    public MessageRjct createMessageRjct() {
        return new MessageRjct();
    }

    /**
     * Create an instance of {@link OrderProductInfoDetails }
     * 
     */
    public OrderProductInfoDetails createOrderProductInfoDetails() {
        return new OrderProductInfoDetails();
    }

    /**
     * Create an instance of {@link MessageMesg }
     * 
     */
    public MessageMesg createMessageMesg() {
        return new MessageMesg();
    }

    /**
     * Create an instance of {@link MessagesOnOrder }
     * 
     */
    public MessagesOnOrder createMessagesOnOrder() {
        return new MessagesOnOrder();
    }

    /**
     * Create an instance of {@link MessageGenericDlca }
     * 
     */
    public MessageGenericDlca createMessageGenericDlca() {
        return new MessageGenericDlca();
    }

    /**
     * Create an instance of {@link DeliveryDetails }
     * 
     */
    public DeliveryDetails createDeliveryDetails() {
        return new DeliveryDetails();
    }

    /**
     * Create an instance of {@link Security }
     * 
     */
    public Security createSecurity() {
        return new Security();
    }

    /**
     * Create an instance of {@link GeneralIdentifiers }
     * 
     */
    public GeneralIdentifiers createGeneralIdentifiers() {
        return new GeneralIdentifiers();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForeignSystemInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "foreignSystemInterface")
    public JAXBElement<ForeignSystemInterface> createForeignSystemInterface(ForeignSystemInterface value) {
        return new JAXBElement<ForeignSystemInterface>(_ForeignSystemInterface_QNAME, ForeignSystemInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FSIOutboundRequestData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "foreignSystemInterfaceOutboundRequest")
    public JAXBElement<FSIOutboundRequestData> createForeignSystemInterfaceOutboundRequest(FSIOutboundRequestData value) {
        return new JAXBElement<FSIOutboundRequestData>(_ForeignSystemInterfaceOutboundRequest_QNAME, FSIOutboundRequestData.class, null, value);
    }

}
