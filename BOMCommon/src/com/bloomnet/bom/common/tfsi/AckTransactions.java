
package com.bloomnet.bom.common.tfsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IncomingEnvelope" type="{http://www.teleflora.com/webservices/}Envelope" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "incomingEnvelope"
})
@XmlRootElement(name = "AckTransactions")
public class AckTransactions {

    @XmlElement(name = "IncomingEnvelope")
    protected Envelope incomingEnvelope;

    /**
     * Gets the value of the incomingEnvelope property.
     * 
     * @return
     *     possible object is
     *     {@link Envelope }
     *     
     */
    public Envelope getIncomingEnvelope() {
        return incomingEnvelope;
    }

    /**
     * Sets the value of the incomingEnvelope property.
     * 
     * @param value
     *     allowed object is
     *     {@link Envelope }
     *     
     */
    public void setIncomingEnvelope(Envelope value) {
        this.incomingEnvelope = value;
    }

}
