package com.bloomnet.bom.common.tfsi;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

public class UUIDHandler implements SOAPHandler<SOAPMessageContext> {
	
//	private static final String LoggerName = "ClientSideLogger";
//	private Logger logger;
//	private final boolean log_p = true;
	
	static final Logger logger = Logger.getLogger(UUIDHandler.class);
	
	private String transactionType;
	private String ourShopCode;
	private String ourPassword;
	
	public UUIDHandler(String transactionType, String ourShopCode, String ourPassword){
		this.transactionType = transactionType;
		this.ourPassword = ourPassword;
		this.ourShopCode = ourShopCode;
//		logger = Logger.getLogger(LoggerName);
	}

	public boolean handleMessage(SOAPMessageContext ctx){
//		if (log_p) logger.info("handleMessage");
		if (logger.isDebugEnabled()) {
			logger.debug("handleMessage");
		}
		
		//Is this an outbound message, i.e., a request
		Boolean request_p = (Boolean) ctx.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		
		//Manipulate the SOAP only if its a request
		if(request_p){
			//Generate a UUID and a timestamp to place in the message header
			UUID uuid = UUID.randomUUID();
			
			try{
				SOAPMessage msg = ctx.getMessage();
				SOAPEnvelope env = msg.getSOAPPart().getEnvelope();
				
				env.setAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema-instance");
				env.setAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
				env.setAttribute("xmlns:wsa","http://schemas.xmlsoap.org/ws/2004/03/addressing");
				env.setAttribute("xmlns:wsse","http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
				env.setAttribute("xmlns:wsu","http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				env.setAttribute("xmlns:soap","http://schemas.xmlsoap.org/soap/envelope/");
				
				SOAPHeader hdr = env.getHeader();
				//Ensure that the SOAP message has a header
				if (hdr==null) hdr = env.addHeader();
				
				//QName qname = new QName("http://ch03.fib", "uuid");
				//SOAPHeaderElement helem = hdr.addHeaderElement(qname);
				
				//helem.setActor(SOAPConstants.URI_SOAP_ACTOR_NEXT);//default
				//helem.addTextNode(uuid.toString());
				
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	            String date = format.format(new Date());
	            date = date.replace(" ", "T");
	            //System.out.println("**********" + date);
				
					SOAPElement action = 
                    hdr.addChildElement("Action", "wsa");
				action.addTextNode("http://www.teleflora.com/webservices/"+transactionType+"");
				
				SOAPElement messageId = 
                    hdr.addChildElement("MessageID", "wsa");
				messageId.addTextNode("uuid:"+uuid.toString());
				
				SOAPElement to = 
                    hdr.addChildElement("To", "wsa");
				to.addTextNode("urn:TelefloraWebServices/Gateway");
			
				SOAPElement replyto = 
                    hdr.addChildElement("ReplyTo", "wsa");
				SOAPElement address = 
					replyto.addChildElement("Address", "wsa");
				address.addTextNode("http://schemas.xmlsoap.org/ws/2004/03/addressing/role/anonymous");
				

				
				
				 SOAPElement security =
                     hdr.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

				 				 
				 SOAPElement TimeStampToken =
                     security.addChildElement("Timestamp", "wsu");
				 TimeStampToken.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				 TimeStampToken.setAttribute("wsu:Id", "Timestamp-" + UUID.randomUUID());

             
             SOAPElement created =
            	 TimeStampToken.addChildElement("Created", "wsu");
             created.addTextNode(date);

				
             SOAPElement usernameToken =
                     security.addChildElement("UsernameToken", "wsse");
             usernameToken.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
             usernameToken.setAttribute("wsu:Id", "SecurityToken-" + UUID.randomUUID());

             
             SOAPElement username =
                     usernameToken.addChildElement("Username", "wsse");
             username.addTextNode(ourShopCode);

             SOAPElement password =
                     usernameToken.addChildElement("Password", "wsse");
             password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
            password.addTextNode(ourPassword);
            
            SOAPElement nonce = 
            	usernameToken.addChildElement("Nonce", "wsse");
            nonce.addTextNode(UUID.randomUUID().toString());
            
            
            
            SOAPElement timestamp = 
            	usernameToken.addChildElement("Created", "wsu");
            timestamp.addTextNode(date);
            

            
				msg.saveChanges();
				
				//For tracking, write to standard output
				//msg.writeTo(System.out);
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				msg.writeTo(out);
				String strMsg = new String(out.toByteArray());
				System.out.println(strMsg);
				if (logger.isDebugEnabled()) {
					logger.debug(msg.toString());
				}
				out.close();
			}
			catch (SOAPException e) {System.err.println(e);	}
			catch (IOException e) {
				e.printStackTrace();
			}
			}
			return true;  //continue down the chain
		}
			
	public boolean handleFault(SOAPMessageContext ctx)	{
//		if (log_p) logger.info("handleFault");
		if (logger.isDebugEnabled()) {
			logger.debug("handleFault");
		}
		try{
		ctx.getMessage().writeTo(System.out);	
		}
		catch (SOAPException e) {System.err.println(e);	}
		catch (IOException e) {System.err.println(e);	}
		return true;
	}
	
	public Set<QName> getHeaders(){
//		if (log_p) logger.info("getHeaders");
		if (logger.isDebugEnabled()) {
			logger.debug("getHeaders");
		}

		return null;
	}
	
	public void close(MessageContext messageContext){
		
	}
	
}
