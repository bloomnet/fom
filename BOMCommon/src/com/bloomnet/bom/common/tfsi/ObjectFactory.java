
package com.bloomnet.bom.common.tfsi;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendTransactions }
     * 
     */
    public SendTransactions createSendTransactions() {
        return new SendTransactions();
    }

    /**
     * Create an instance of {@link ArrayOfTransaction }
     * 
     */
    public ArrayOfTransaction createArrayOfTransaction() {
        return new ArrayOfTransaction();
    }

    /**
     * Create an instance of {@link ReceiveTransactions }
     * 
     */
    public ReceiveTransactions createReceiveTransactions() {
        return new ReceiveTransactions();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link SendTransactionsResponse }
     * 
     */
    public SendTransactionsResponse createSendTransactionsResponse() {
        return new SendTransactionsResponse();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link ReceiveTransactionsResponse }
     * 
     */
    public ReceiveTransactionsResponse createReceiveTransactionsResponse() {
        return new ReceiveTransactionsResponse();
    }

    /**
     * Create an instance of {@link AckTransactionsResponse }
     * 
     */
    public AckTransactionsResponse createAckTransactionsResponse() {
        return new AckTransactionsResponse();
    }

    /**
     * Create an instance of {@link Envelope }
     * 
     */
    public Envelope createEnvelope() {
        return new Envelope();
    }

    /**
     * Create an instance of {@link ArrayOfByte }
     * 
     */
    public ArrayOfByte createArrayOfByte() {
        return new ArrayOfByte();
    }

    /**
     * Create an instance of {@link AckTransactions }
     * 
     */
    public AckTransactions createAckTransactions() {
        return new AckTransactions();
    }

}
