
package com.bloomnet.bom.common.tfsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Transaction complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BodyFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BinaryBody" type="{http://www.teleflora.com/webservices/}ArrayOfByte" minOccurs="0"/>
 *         &lt;element name="Body" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Header" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Result" type="{http://www.teleflora.com/webservices/}Result" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="TransactionGuid" use="required" type="{http://microsoft.com/wsdl/types/}guid" />
 *       &lt;attribute name="SubDestinationId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="LocalTime" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TimeZone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SortIndex" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="TransactionType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="BundleId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DestinationId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SourceId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transaction", propOrder = {
	"header",
	"bodyFormat",
    "binaryBody",
    "body",
    "result"
})
@XmlRootElement
public class Transaction {

	@XmlElement(name = "Header")
    protected String header;
    @XmlElement(name = "BodyFormat")
    protected String bodyFormat;
    @XmlElement(name = "BinaryBody")
    protected ArrayOfByte binaryBody;
    @XmlElement(name = "Body")
    protected String body;
    
    @XmlElement(name = "Result")
    protected Result result;
    @XmlAttribute(name = "TransactionGuid", required = true)
    protected String transactionGuid;
    @XmlAttribute(name = "SubDestinationId")
    protected String subDestinationId;
    @XmlAttribute(name = "LocalTime")
    protected String localTime;
    @XmlAttribute(name = "TimeZone")
    protected String timeZone;
    @XmlAttribute(name = "Priority")
    protected String priority;
    @XmlAttribute(name = "SortIndex", required = true)
    protected int sortIndex;
    @XmlAttribute(name = "TransactionType")
    protected String transactionType;
    @XmlAttribute(name = "BundleId")
    protected String bundleId;
    @XmlAttribute(name = "DestinationId")
    protected String destinationId;
    @XmlAttribute(name = "SourceId")
    protected String sourceId;

    /**
     * Gets the value of the bodyFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBodyFormat() {
        return bodyFormat;
    }

    /**
     * Sets the value of the bodyFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBodyFormat(String value) {
        this.bodyFormat = value;
    }

    /**
     * Gets the value of the binaryBody property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfByte }
     *     
     */
    public ArrayOfByte getBinaryBody() {
        return binaryBody;
    }

    /**
     * Sets the value of the binaryBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfByte }
     *     
     */
    public void setBinaryBody(ArrayOfByte value) {
        this.binaryBody = value;
    }

    /**
     * Gets the value of the body property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBody() {
        return body;
    }

    /**
     * Sets the value of the body property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBody(String value) {
        this.body = value;
    }

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeader(String value) {
        this.header = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setResult(Result value) {
        this.result = value;
    }

    /**
     * Gets the value of the transactionGuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionGuid() {
        return transactionGuid;
    }

    /**
     * Sets the value of the transactionGuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionGuid(String value) {
        this.transactionGuid = value;
    }

    /**
     * Gets the value of the subDestinationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubDestinationId() {
        return subDestinationId;
    }

    /**
     * Sets the value of the subDestinationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubDestinationId(String value) {
        this.subDestinationId = value;
    }

    /**
     * Gets the value of the localTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalTime() {
        return localTime;
    }

    /**
     * Sets the value of the localTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalTime(String value) {
        this.localTime = value;
    }

    /**
     * Gets the value of the timeZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * Sets the value of the timeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeZone(String value) {
        this.timeZone = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the sortIndex property.
     * 
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * Sets the value of the sortIndex property.
     * 
     */
    public void setSortIndex(int value) {
        this.sortIndex = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the bundleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundleId() {
        return bundleId;
    }

    /**
     * Sets the value of the bundleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundleId(String value) {
        this.bundleId = value;
    }

    /**
     * Gets the value of the destinationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationId() {
        return destinationId;
    }

    /**
     * Sets the value of the destinationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationId(String value) {
        this.destinationId = value;
    }

    /**
     * Gets the value of the sourceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * Sets the value of the sourceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceId(String value) {
        this.sourceId = value;
    }

}
