
package com.bloomnet.bom.common.tfsi;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebService(name = "TelefloraWebServiceSoap", targetNamespace = "http://www.teleflora.com/webservices/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface TelefloraWebServiceSoap {


    /**
     * 
     * @param incomingEnvelope
     * @return
     *     returns client.Envelope
     */
    @WebMethod(operationName = "SendTransactions", action = "http://www.teleflora.com/webservices/SendTransactions")
    @WebResult(name = "SendTransactionsResult", targetNamespace = "http://www.teleflora.com/webservices/")
    @RequestWrapper(localName = "SendTransactions", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.SendTransactions")
    @ResponseWrapper(localName = "SendTransactionsResponse", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.SendTransactionsResponse")
    public Envelope sendTransactions(
        @WebParam(name = "IncomingEnvelope", targetNamespace = "http://www.teleflora.com/webservices/")
        Envelope incomingEnvelope);

    /**
     * 
     * @param incomingEnvelope
     * @return
     *     returns client.Envelope
     */
    @WebMethod(operationName = "ReceiveTransactions", action = "http://www.teleflora.com/webservices/ReceiveTransactions")
    @WebResult(name = "ReceiveTransactionsResult", targetNamespace = "http://www.teleflora.com/webservices/")
    @RequestWrapper(localName = "ReceiveTransactions", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.ReceiveTransactions")
    @ResponseWrapper(localName = "ReceiveTransactionsResponse", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.ReceiveTransactionsResponse")
    public Envelope receiveTransactions(
        @WebParam(name = "IncomingEnvelope", targetNamespace = "http://www.teleflora.com/webservices/")
        Envelope incomingEnvelope);

    /**
     * 
     * @param incomingEnvelope
     * @return
     *     returns client.Envelope
     */
    @WebMethod(operationName = "AckTransactions", action = "http://www.teleflora.com/webservices/AckTransactions")
    @WebResult(name = "AckTransactionsResult", targetNamespace = "http://www.teleflora.com/webservices/")
    @RequestWrapper(localName = "AckTransactions", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.AckTransactions")
    @ResponseWrapper(localName = "AckTransactionsResponse", targetNamespace = "http://www.teleflora.com/webservices/", className = "client.AckTransactionsResponse")
    public Envelope ackTransactions(
        @WebParam(name = "IncomingEnvelope", targetNamespace = "http://www.teleflora.com/webservices/")
        Envelope incomingEnvelope);

}
