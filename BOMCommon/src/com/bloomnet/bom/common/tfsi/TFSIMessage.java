package com.bloomnet.bom.common.tfsi;

public class TFSIMessage {
	private String messageType;
	private String messageText;
	private String TIDNumber;
	private String originalTIDNumber;
	private String status;
	private String sendingShopCode;
	private String bundleId;
	private String xmlMessage;
	private String price;
	private String valType;
	private String subType;

	private long originalSequenceNumber;
	
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
	public String getMessageText() {
		return messageText;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus() {
		return status;
	}
	public void setOriginalSequenceNumber(long originalSequenceNumber) {
		this.originalSequenceNumber = originalSequenceNumber;
	}
	public long getOriginalSequenceNumber() {
		return originalSequenceNumber;
	}
	public void setSendingShopCode(String sendingShopCode) {
		this.sendingShopCode = sendingShopCode;
	}
	public String getSendingShopCode() {
		return sendingShopCode;
	}
	public void setTIDNumber(String tIDNumber) {
		TIDNumber = tIDNumber;
	}
	public String getTIDNumber() {
		return TIDNumber;
	}
	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}
	public String getBundleId() {
		return bundleId;
	}
	public void setXmlMessage(String xmlMessage) {
		this.xmlMessage = xmlMessage;
	}
	public String getXmlMessage() {
		return xmlMessage;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getPrice() {
		return price;
	}
	public void setValType(String valType) {
		this.valType = valType;
	}
	public String getValType() {
		return valType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getSubType() {
		return subType;
	}
	public void setOriginalTIDNumber(String originalTIDNumber) {
		this.originalTIDNumber = originalTIDNumber;
	}
	public String getOriginalTIDNumber() {
		return originalTIDNumber;
	}
}
