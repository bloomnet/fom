package com.bloomnet.bom.common.constants;

public final class BOMConstants {

	// bloomLink environments
	public static final String QA_ENDPOINT = "http://acpnyblkqa01:9080/fsiv2/processor";
	public static final String QA2_ENDPOINT = "http://qa.bloomlink.net/fsiv2/processor";
	public static final String PROD_ENPOINT = "http://www.bloomlink.net/fsiv2/processor";

	public static final String ORDER_MESSAGE_TYPE = "0";
	public static final String INQR_MESSAGE_TYPE = "1";
	public static final String RESP_MESSAGE_TYPE = "2";
	public static final String RJCT_MESSAGE_TYPE = "4";
	public static final String CANC_MESSAGE_TYPE = "3";
	public static final String DENI_MESSAGE_TYPE = "6";
	public static final String DLCF_MESSAGE_TYPE = "7";
	public static final String DLOU_MESSAGE_TYPE = "8";
	public static final String DISP_MESSAGE_TYPE = "9";
	public static final String CONF_MESSAGE_TYPE = "11";
	public static final String INFO_MESSAGE_TYPE = "12";
	public static final String DSPC_MESSAGE_TYPE = "14";
	public static final String DSPD_MESSAGE_TYPE = "15";
	public static final String DSPR_MESSAGE_TYPE = "16";
	public static final String PCHG_MESSAGE_TYPE = "18";
	public static final String ACKF_MESSAGE_TYPE = "24";
	public static final String DLCA_MESSAGE_TYPE = "26";

	
	public static final String TO_BE_WORKED = "To be worked";
	public static final String ACTIVELY_BEING_WORKED = "Actively being worked";
	public static final String SENT = "Sent to Shop";
	public static final String ACCEPTED = "Accepted by Shop";
	public static final String OUT_FOR_DELIVERY = "Out for Delivery by Shop";
	public static final String DELIVERED = "Delivered by Shop";
	public static final String DELIVERY_ATTEMPTED = "Delivery Attempted by Shop";
	public static final String REJECTED_BY_SHOP = "Rejected by Shop";
	public static final String REJECTED_BY_BLOOMNET = "Rejected by BloomNet";
	public static final String CANCELLED_BY_SENDING_SHOP = "Cancelled by Sending Shop";
	public static final String CANCEL_CONFIRMED = "Cancel Confirmed by BloomNet";
	public static final String CANCEL_DENIED = "Cancel Denied by BloomNet";
	public static final String WAITING_FOR_RESPONSE = "Waiting for Response";
	public static final String BEING_RESEARCHED = "Being Researched";
	public static final String COMPLETED_OUTSIDE_OF_BOM = "Completed outside of BOM";
	public static final String COMPLETED = "Order Completed";

	// TODO what is dlca numeric type

	// Broadcast from bmt, not a message on order
	public static final String MSG_MESSAGE_TYPE = "19";

	public static final String OFF_MESSAGE_TYPE = "0";

	public static final String SYSTEM_TYPE = "GENERAL";
	public static final String WIRE_SERVICE_CODE = "BMT";
	public static final String EXTERNAL_SHOP_NUMBER="10001";

	// message types
	public static final String POST_MESSAGE = "?func=postmessages&data=";
	public static final String GET_MESSAGE = "?func=getmessages&messageAckn=true&data=";
	public static final String GET_STATEMENT = "?func=getStatement&data=";
	public static final String GET_MEMBER = "?func=getMemberDirectory&data=";
	public static final String AUDIT_MESSAGE ="?func=getAuditInfo&data=";
	public static final String RECIPE_MESSAGE ="?func=getRecipeInfo&data=";


	public static final String POST_VERB = "POST";

	public static final String NUM_GENERAL_MESSAGES = "fsi.maxNumOfGeneralMessages";
	public static final String NUM_ACKF_MESSAGES = "fsi.maxNumOfAckfMessages";
	public static final String NUM_ORDER_MESSAGES = "fsi.maxNumOfOrderMessages";

	// Occasion codes
	public static final String Funeral = "1";
	public static final String Illness = "2";
	public static final String Birthday = "3";
	public static final String BusinessGifts = "4";
	public static final String Holiday = "5";
	public static final String Maternity = "6";
	public static final String Anniversary = "7";
	public static final String Others = "8";

	// FSI properties
	public static final String FSI_SHOPCODE = "fsi.shopcode";
	public static final String FSI_PASSWORD = "fsi.password";
	public static final String FSI_USERNAME = "fsi.username";
	public static final String NUM_OF_SHOP_ATTEMPTS = "fsi.numOfShopAttempts";
	public static final String NUM_OF_SHOP_ATTEMPTS_SAME_DAY = "fsi.numOfShopAttemptsSameDay";
	public static final String FSI_SHOP_NOT_FOUND = "fsi.shopNotFound";

	public static final String PAYMENT_TYPE_CREDITCARD = "payment.type.creditcard";
	public static final String PAYMENT_TYPE_CHECK = "payment.type.check";

	// Payment status
	public static final String PAYMENT_STATUS_NOTPAID = "payment.status.notpaid";
	public static final String PAYMENT_STATUS_PAID = "payment.status.paid";
	public static final String PAYMENT_STATUS_OPEN = "payment.status.open";

	// Message status
	public static final String MESSAGE_TO_BE_WORKED = "message.towork";
	public static final String MESSAGE_BEING_WORKED = "message.curwork";
	public static final String MESSAGE_SENT_TO_SHOP = "mesage.sentshop";
	public static final String MESSAGE_ACCEPTED_BY_SHOP = "mesage.accept";
	public static final String MESSAGE_WORK_COMPLETED = "message.completed";

	// Order status
	public static final String ORDER_TO_BE_WORKED = "order.towork";
	public static final String ORDER_BEING_WORKED = "order.curwork";
	public static final String ORDER_SENT_TO_SHOP = "order.sentshop";
	public static final String ORDER_ACCEPTED_BY_SHOP = "order.accept";
	public static final String ORDER_OUT_FOR_DELIVERY = "order.dlou";
	public static final String ORDER_DELIVERED_BY_SHOP = "order.dlcf";
	public static final String ORDER_DELIVERY_ATTEMPTED = "order.dlattempt";
	public static final String ORDER_REJECTED_BY_SHOP = "order.rjct";
	public static final String ORDER_REJECTED_BY_BMT = "order.bmtrjct";
	public static final String ORDER_CANCELLED_BY_SSHOP ="order.canc";
	public static final String ORDER_CANCELLED_CONF ="order.conf";
	public static final String ORDER_CANCELLED_DENI ="order.deni";
	public static final String ORDER_WAITING_FOR_RESP ="order.wfr";
	public static final String ORDER_BEING_RESEARCHED ="order.research";
	public static final String ORDER_COMPLETED_OUTSIDE_BOM="order.outside";
	public static final String ORDER_COMPLETED="order.complete";


	

	// Destinations on Queue
	public static final String NEWMESSAGES_QUEUE = "NEW-MESSAGES";
	public static final String MESSAGES_QUEUE = "MESSAGES";
	public static final String TFSI_MESSAGES_QUEUE = "TFSI-MESSAGES";
	public static final String FSI_QUEUE = "BMTFSI";
	public static final String TFSI_QUEUE = "TFSI";
	public static final String TLO_QUEUE = "TLO";
	public static final String FSI_OUTBOUND_ORDER_QUEUE = "FSIoutbound";
	public static final String TFSI_OUTBOUND_ORDER_QUEUE = "TFSIoutbound";
	public static final String FAX_OUTBOUND_ORDER_QUEUE = "FAXoutbound";
	public static final String EMAIL_QUEUE = "EMAIL";

	
	public static final String FIRST_DESTINATION = "first.destination";
	public static final String SECOND_DESTINATION = "second.destination";
	public static final String THIRD_DESTINATION = "third.destination";

	

	// BloomLink JAXB packages
	public static final String FSI_JAXB_PACKAGE = "com.bloomnet.bom.common.jaxb.fsi";
	public static final String TFSI_JAXB_PACKAGE = "com.bloomnet.bom.common.tfsi";
	public static final String MDI_JAXB_PACKAGE = "com.bloomnet.bom.common.jaxb.mdi";
	public static final String AUDIT_JAXB_PACKAGE = "com.bloomnet.bom.common.jaxb.audit";
	public static final String RECIPE_JAXB_PACKAGE = "com.bloomnet.bom.common.jaxb.recipe";


	// Shops that bypass FSI Destination
	public static final String FROM_YOU_FLOWERS = "X0690000";
	public static final String BLOOMS_TODAY = "C5730000";
	public static final String JUST_FLOWERS = "P5390000";
	public static final String ONE_EIGHT_HUNDRED_FLOWERS = "00800000";
	
	//Website order
	public static final String WEBSITE_ORDER = "Z1000000";

	
	public static final String FROM_YOU_FLOWERS_ID = "x069.id";
	public static final String BLOOMS_TODAY_ID = "p539.id";
	public static final String JUST_FLOWERS_ID = "c573.id";

	// Order type abbreviations
	public static final String ORDER_MESSAGE_DESC = "ORDER";
	public static final String INQR_MESSAGE_DESC = "INQR";
	public static final String RESP_MESSAGE_DESC = "RESP";
	public static final String RJCT_MESSAGE_DESC = "RJCT";
	public static final String CANC_MESSAGE_DESC = "CANC";
	public static final String DENI_MESSAGE_DESC = "DENI";
	public static final String DLCF_MESSAGE_DESC = "DLCF";
	public static final String DLCA_MESSAGE_DESC = "DLCA";
	public static final String DLOU_MESSAGE_DESC = "DLOU";
	public static final String CONF_MESSAGE_DESC = "CONF";
	public static final String DISP_MESSAGE_DESC = "DISP";
	public static final String DSPC_MESSAGE_DESC = "DSPC";
	public static final String DSPD_MESSAGE_DESC = "DSPD";
	public static final String DSPR_MESSAGE_DESC = "DSPR";
	public static final String INFO_MESSAGE_DESC = "INFO";
	public static final String PCHG_MESSAGE_DESC = "PCHG";
	public static final String ACKF_MESSAGE_DESC = "ACKF";
	
	//DLCA reason for non delivery
	public static final String DLCA_NEED_INFO = "94";
	public static final String DLCA_NOT_AVAIL = "95";
	public static final String DLCA_INC_DATE = "96";
	public static final String DLCA_OTHER = "97";
	
	//DLCA additional reason for non delivery
	public static final String DLCA_WILL_ATTEMPT = "99";
	public static final String DLCA_WILL_ATTEMPT_CONT = "98";
	
	// Order Activity Types
	public static final String ACT_ROUTING = "Act_Routing";
	public static final String ACT_AUDIT = "Act_Audit";
	public static final String ACT_MESSAGE = "Act_Message";
	public static final String ACT_PAYMENT = "Act_Payment";
	public static final String ACT_LOGACALL = "Act_LogACall";
	
	// Call disposition types
	public static final String LOGACALL_ORDER_NOTE = "Order Note";
	public static final String LOGACALL_WFR = "Waiting for Response";
	public static final String LOGACALL_OUTSIDE = "Completed outside of BOM";
	public static final String LOGACALL_OTHER = "Other";
	public static final String LOGACALL_COMPLETED = "Order Completed";


	

	// Commmethod descriptions
	public static final String COMMMETHOD_API = "API";
	public static final String COMMMETHOD_PHONE = "PHONE";
	public static final String COMMMETHOD_FAX = "FAX";
	public static final String COMMMETHOD_EMAIL = "EMAIL";
	public static final String COMMMETHOD_OTHER = "OTHER";
	
	// Commmethod id
	public static final byte COMMMETHOD_API_ID = 1;
	public static final byte COMMMETHOD_PHONE_ID = 2;
	public static final byte COMMMETHOD_FAX_ID = 3;
	public static final byte COMMMETHOD_EMAIL_ID = 4;
	public static final byte COMMMETHOD_OTHER_ID = 5;
	
	//Order type
	public static final byte ORDER_TYPE_BMT = 1;
	public static final byte ORDER_TYPE_TEL = 2;
	public static final byte ORDER_TYPE_FTD = 3;
	public static final byte ORDER_TYPE_OTHER = 4;
	

	// Shopnetworkstatus 
	public static final Byte SHOPNETWORKSTATUS_ACTIVE = 1;
	public static final Byte SHOPNETWORKSTATUS_INACTIVE = 2;
	public static final Byte SHOPNETWORKSTATUS_PENDING = 3;
	
	//skillset/role
	public static final String SKILLSET_ADMIN = "1";
	public static final String SKILLSET_BASICAGENT = "1";
	public static final String SKILLSET_REJECTION = "3";
	public static final String SKILLSET_INQUIRE = "4";
	public static final String SKILLSET_RESPONSE = "5";
	
	//TFSI Occasion Codes
	public static final String TFSI_FUNERAL = "1";
	public static final String TFSI_ILLNESS = "2";
	public static final String TFSI_BIRTHDAY = "3";
	public static final String TFSI_HOLIDAY = "4";
	public static final String TFSI_BUSINESSGIFT = "5";
	public static final String TFSI_MATERNITY = "6";
	public static final String TFSI_ANNIVERSARY = "7";
	public static final String TFSI_OTHER = "8";
	
	//fax
	public static final String FAX_USERNAME = "fax.username";
	public static final String FAX_PASSWORD = "fax.password";
	public static final String FAX_NUMBER = "fax.number";
	public static final String FAX_DIR = "fax.dir";

	//email 
	public static final String EMAIL_FROM = "email.from";
	public static final String EMAIL_USERNAME = "email.username";
	public static final String EMAIL_PASSWORD = "email.password";
	public static final String EMAIL_RPT = "email.rpt";
	public static final String EMAIL_RPT_SUBJECT = "email.rpt.sbj";
	public static final String EMAIL_NOTIFY = "email.notify";
	public static final String EMAIL_NOTIFY_TO = "email.notify.to";
	public static final String EMAIL_NOTIFY_FROM = "email.notify.from";


	
	//metrics report
	public static final String REPORT_DIR = "rpt.dir";

	
	
	//Time zones
	public static final String SAMOA_TIME="-11";
	public static final String HAWAII_TIME="-10";
	public static final String ALASKA_TIME="-9";
	public static final String PACIFIC_TIME="-8";
	public static final String MTN_TIME="-7";
	public static final String CENTRAL_TIME="-6";
	public static final String EAST_TIME="-5";
	public static final String ATLANTIC_TIME="-4";
	public static final String NEWFOUNDLAND_TIME="-3.5";
	public static final String UTC9_TIME="9";
	public static final String UTC10_TIME="10";
	public static final String UTC11_TIME="11";
	public static final String UTC12_TIME="12";
	
	//WFR
	public static final String NUM_OF_WFR="wfr.rjct";
	public static final String NUM_OF_WFR_SAMEDAY="wfr.sameday.rjct";
	
	public static final String AUTOREJECT_MESSAGE="fsi.autorjctMessage";
	



	
}
