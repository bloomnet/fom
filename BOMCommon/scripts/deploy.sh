#!/bin/bash
if [ -z "$1" ]; then 
              echo usage: $0 directory
              exit
          fi

BUILDNUMBER=$1
USER_DIR="/home/jpursoo/"
BOM_DIR="/var/lib/tomcat6/webapps/"
CATALINA_LOG_DIR="/var/lib/tomcat6/logs/"
ACTIVEMQ_DIR="/home/activemq/apache-activemq-5.4.3"
TIMESTAMP=$(date +%Y-%m-%d)


echo "making directory "$BUILDNUMBER
mkdir $BUILDNUMBER
cd $BUILDNUMBER

echo "copying wars from " $USER_DIR$BUILDNUMBER
cp $USER_DIR$BUILDNUMBER/*.war .

#echo " renaming  BOMMVC"$BUILDNUMBER".war" 
echo "renaming wars"
mv BOMMVC$BUILDNUMBER.war BOMMVC.war
mv BOMWS$BUILDNUMBER.war BOMWS.war

echo "copying wars to "$BOM_DIR
cp BOMMVC.war $BOM_DIR
cp BOMWS.war $BOM_DIR

echo "deploying wars...waiting 60 seconds..."
sleep 60
echo "done deploying wars"
cd $BOM_DIR
ls -l 

cd $ACTIVEMQ_DIR
echo "restarting activeMQ"
bin/activemq stop
bin/activemq status
bin/activemq start
bin/activemq status

cd $CATALINA_LOG_DIR
echo "restarting tomcat"
service tomcat6 stop
mv catalina.out catalina-$TIMESTAMP.log
service tomcat6 start

#echo $TIMESTAMP 
