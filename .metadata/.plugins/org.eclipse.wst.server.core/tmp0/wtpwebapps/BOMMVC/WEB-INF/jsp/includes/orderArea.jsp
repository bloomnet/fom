<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="orderBean"     scope="page" value="${MASTERORDER.bean}" />
<c:set var="receivingShop" scope="page" value="${orderBean.receivingShop}" />
<c:set var="sendingShop"   scope="page" value="${MASTERORDER.sendingShop}" />
<c:set var="orderNumber"   scope="page" value="${orderBean.orderNumber}" />

	<c:set var="containsHyperlinkaccess" value="false" scope="page" />
	<c:forEach var="item" items="${roles}">
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(item.key,'hlaccess') }">
				<c:set var="containsHyperlinkaccess" value="true" scope="page" />
			</c:when>
		</c:choose>
	</c:forEach>

<div class="headline">
    <div class="outbound">
    Outbound Order: <span class="order-number">
    <c:choose>
    <c:when test="${ fn:length(MASTERORDER.outboundOrderNumber) > 0}">
        <c:out value="${ MASTERORDER.outboundOrderNumber }" />
    </c:when>
    <c:otherwise>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </c:otherwise>
    </c:choose>
    
    </span>
    </div> <!-- Close DIV outbound -->
    Master Order: <span class="order-number"><c:out value="${MASTERORDER.bloomnetOrderNumber}" /></span> 
    <a href="#" onclick="openMyModal('orderHistory.htm?query=masterorder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;">View History</a>
    <div class="clear"></div>
</div> <!-- Close DIV headline -->

<div class="order-boxes">
    <div class="small-box">
        <div class="sub-headline">
        Shop I'm Working For
        </div> <!-- Close DIV sub-headline -->
        <div class="box-content">
        <strong><c:out value="${USER.myShop.shopName}" /></strong><br />
        BloomNet Shop Code: <c:out value="${USER.securityElement.shopcode}" /><br />
        <c:out value="${USER.myShop.shopPhoneFormatted}" /><br />
	 Teleflora Shop Code: 43030600
        <!-- <a href="#">more info</a> -->
        </div> <!-- Close DIV box-content -->
    </div> <!-- Close DIV small-box -->
    <div class="small-box">
        <div class="sub-headline">
        Main Sending Shop 
        </div> <!-- Close DIV sub-headline -->
        <div class="box-content">
        	<c:choose>
        	<c:when test="${empty sendingShop}">
        	</c:when>
        	<c:otherwise>
		        <strong><c:out value="${sendingShop.shopName}" /></strong><br />
		        <c:out value="${MASTERORDER.shopCode['sending'].shopCode}" /><br />
 	            <c:out value="${sendingShop.shopAddress1}" /><br />
				<c:if test='${ ( fn:trim(sendingShop.shopAddress2) != null ) && ( fn:trim(sendingShop.shopAddress2) != "" ) }'>
					<c:out value="${sendingShop.shopAddress2}" /><br />
				</c:if>
	            <c:out value="${sendingShop.city.name}" />, <c:out value="${sendingShop.city.state.shortName}" />, <c:out value="${sendingShop.zip.zipCode}" /><br />
		        <c:out value="${sendingShop.shopPhoneFormatted}" /><br />
                <!-- <a href="#">more info</a> -->
        	</c:otherwise>
        	</c:choose>
        	
        	
        </div> <!-- Close DIV box-content -->
    </div> <!-- Close DIV small-box -->
    <div class="small-box order-actions">
        <div class="sub-headline">
        Order Actions
        </div> <!-- Close DIV sub-headline -->
        <div class="box-content">
            <center><br/>
            <!-- <a href="#"><img src="images/view-related-orders.png" alt="" /></a> -->
            <a href="#" onclick="openMyModal('orderHistory.htm?query=suborder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;"><img src="images/view-order-history.png" alt="" /></a>
            <c:choose>
              <c:when test="${ MASTERORDER.complete }">
                <a href="default.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/exit-order.png" alt="" /></a>
              </c:when>
              <c:otherwise>
                <a href="AgentViewExitOrder.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/exit-order.png" alt="" /></a>
              </c:otherwise>
            </c:choose>
            </center>
        </div> <!-- Close DIV box-content -->
    </div> <!-- Close DIV small-box -->
    <div class="clear"></div>
</div> <!-- Close DIV order-boxes -->
<div class="main-order-area">
    <div class="sub-headline">
    <div class="edit">
   		<c:choose>
	    	<c:when test='${ MASTERORDER.status == "To be worked" || MASTERORDER.status == "Actively being worked" || MASTERORDER.status == "Waiting for Response" || MASTERORDER.status == "Message to be Worked"}'>
	             <a href="#" onclick="openMyModalEdit('editOrder.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;">Edit</a>
	        </c:when>
        </c:choose>
    </div>
    <span class="push-down">Order Details</span><a href="AgentViewMessageNew.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/add-a-message.png" alt="" /></a>
    <span class="showStatus" id="showStatus"><strong>Status: </strong><c:out value="${ MASTERORDER.status }" /></span>
    &nbsp;&nbsp;
    <span class="showStatus"><a href="AgentViewShowAllMessages.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Show Cancel Messages</a></span>
    </div>
    <div class="order-information">
        <div class="column1 column">
            <strong>Fulfilling Shop</strong><br />
            <c:choose>
            <c:when test="${not empty receivingShop.shopName}">
	            <c:forEach items="${ receivingShop.shopnetworks }" var="sn" >
	            	<c:set var="rsCode"  value="empty" />
		            <c:choose>
		            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet')}">
		            		<c:if test='${MASTERORDER.orderQueue == "BMTFSI"}'>
		            			Shop Code: <c:out value='${sn.shopCode}' />
		            			<c:set var="rsCode"  value="${sn.shopCode}" />
		            			<br />
		            		</c:if>
		            		<c:if test='${MASTERORDER.orderQueue == "TLO"}'>
		            			Shop Code: <c:out value='${sn.shopCode}' />
		            			<c:set var="rsCode"  value="${sn.shopCode}" />
		            			<br />
		            		</c:if>
		            	</c:when>
		            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora')}">
		            		<c:if test='${MASTERORDER.orderQueue == "TFSI"}'>
		            			Shop Code: <c:out value='${sn.shopCode}' />
		            			<c:set var="rsCode"  value="${sn.shopCode}" />
		            			<br />
		            		</c:if>
		            		<c:if test='${MASTERORDER.orderQueue == "TLO" && rsCode != "empty"}'>
		            			Shop Code: <c:out value='${sn.shopCode}' />
		            			<c:set var="rsCode"  value="${sn.shopCode}" />
		            			<br />
		            		</c:if>
		            	</c:when>
		            </c:choose>
		        </c:forEach>
	            <c:out value="${receivingShop.shopName}" />
	            <br />
 	            <c:out value="${receivingShop.shopAddress1}" /><br />
				<c:if test='${ ( fn:trim(receivingShop.shopAddress2) != null ) && ( fn:trim(receivingShop.shopAddress2) != "" ) }'>
					<c:out value="${receivingShop.shopAddress2}" /><br />
				</c:if>
	            <c:out value="${receivingShop.city.name}" />, <c:out value="${receivingShop.city.state.shortName}" />, <c:out value="${receivingShop.zip.zipCode}" /><br />
	            <c:out value="${receivingShop.shopPhoneFormatted}" />
            </c:when>
            <c:otherwise>
            Shop not yet assigned.
            </c:otherwise>
            </c:choose>
        </div> <!-- Close DIV column1 -->
        <div class="column2 column">
            <strong>Recipient</strong><br />
            <c:out value="${orderBean.recipientFirstName}" /> <c:out value="${orderBean.recipientLastName}" /><br />
            <c:if test='${ ( fn:trim(orderBean.recipientAttention) != null ) && ( fn:trim(orderBean.recipientAttention) != "" )}'>
            	<c:out value="${orderBean.recipientAttention}" /><br />
            </c:if>
            <c:out value="${orderBean.recipientAddress1}" /><br />
            <c:if test='${ ( fn:trim(orderBean.recipientAddress2) != null ) && ( fn:trim(orderBean.recipientAddress2) != "" )}'>
            	<c:out value="${orderBean.recipientAddress2}" /><br />
            </c:if>
            <c:out value="${orderBean.recipientCity}" />, <c:out value="${orderBean.recipientStateFormated}" />, <c:out value="${orderBean.recipientCountryCode}" />, <c:out value="${orderBean.recipientZipCode}" /> <input type="hidden" id="orderRecipientZip" value="<c:out value="${orderBean.recipientZipCode}" />" /><br />
            <c:out value="${orderBean.recipientPhoneNumberFormated}" />
        </div> <!-- Close DIV column2 -->
        <div class="column3 column">
            <strong>Order Date</strong><br />
            <fmt:formatDate value="${orderBean.captureDate}" pattern="MM/dd/yyyy" /><br /><br />
            <strong>Delivery Date</strong><br />
            <c:out value="${orderBean.deliveryDate}"  />
            <c:if test='${ orderBean.flexDeliveryDate != null  }'>
            <br/><br/>
             <strong>Flex Delivery Date</strong><br />
            <c:out value="${orderBean.flexDeliveryDate}"  />
            </c:if>
        </div> <!-- Close DIV column3 -->
        <div class="column4 column">
            <strong>Order Total</strong><br />
            <fmt:formatNumber value="${orderBean.totalCost}" type="CURRENCY" /><br /><br />
            <strong>Occasion</strong><br />
            <c:out value="${MASTERORDER.occassionCode}" />
        </div> <!-- Close DIV column4 -->
        <div class="clear"></div>
    </div> <!-- Close DIV order-information -->
    <ul class="products">
		<c:forEach var="p" items="${orderBean.products}" varStatus="status" > 
	        <li>
	            <div class="quantity">
	            <strong>Quantity:</strong> <c:out value="${p.units}" />
	            </div> <!-- Close DIV quantity -->
	        	<strong>Product</strong>&nbsp;<c:out value="${p.productCode}" /><br />
		        <c:out value="${p.productDescription}" /><br />
		        <c:if test='${ ( fn:trim(p.recipe) != null )&& ( fn:trim(p.recipe) != "" )}'>
		        <strong>Recipe</strong>&nbsp;<c:out value="${p.recipe}" /><br />
		        </c:if>
		        <fmt:formatNumber value="${p.costOfSingleProduct}" type="CURRENCY" /><br />
	            <div class="clear"></div>
	        </li>
		</c:forEach> 
    </ul> <!-- Close UL products -->
    <div class="card-message">
    <strong>Card Message</strong><br />
    <c:out value='${  fn:replace(fn:replace(orderBean.cardMessage, "%0A", ""), "%0D", "")  }' />
    </div> <!-- Close DIV card-message -->
    <div class="clear"></div>
	
 <!-- Close DIV headline -->
        <div class="notes">
	        <strong>Special Instructions:</strong><br />
	        <c:out value="${orderBean.specialInstruction}" />
	        <br /><br />
	        <strong>Order Notes ( <a href="" onclick="openMyModalNotes('orderNotes.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;">Add</a> ):</strong><br />
        <!-- Close DIV notes -->
        
	        <c:forEach var="note" items="${MASTERORDER.notes}" varStatus="status" >
	        	<c:out value="${ note.text }" /><hr>
	        	<div align="right"><c:out value="${ note.creatorName }" />&nbsp;[<fmt:formatDate value="${ note.createDate }"  pattern='MM/dd/yyyy hh:mm:ss' />]</div><br />
	        </c:forEach>
        </div>

<div class="headline" style="height:20px;">
    <div align="left">
        <a href="http://directory.bloomnet.net/Directory/signin.htm" target="_blank">  Directory Online</a> &nbsp;|
        <a href="http://www.locatemyflorist.com/" target="_blank">Locate My Florist</a> &nbsp;|
        <a href="http://ww11.1800flowers.com/" target="_blank">1-800-Flowers</a> &nbsp;|
        <a href="http://beta.bloomlink.net/bloomjsp/Bloomlink/web/drg.htm" target="_blank">DRG</a> &nbsp;|
        <a href="http://beta.bloomlink.net/bloomjsp/Bloomlink/web/Essentials%20Full%20Workroom%20Manual_1_6_14.pdf" target="_blank">FSG</a> &nbsp;|
        <a href="https://maps.google.com/maps?q=florists+near+${orderBean.recipientCity}+${orderBean.recipientStateFormated}+${orderBean.recipientZipCode}" target="_blank">'Florists near' Google Search</a>
    </div>
</div>
        
</div> <!-- Close DIV main-order-area -->


<div class="clear"></div> <!-- Clear Floats -->


