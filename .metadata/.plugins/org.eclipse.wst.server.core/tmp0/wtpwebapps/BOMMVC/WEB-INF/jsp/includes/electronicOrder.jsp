<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="headline">Send Order</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="send-order-electronic">
		<form>
			<ul>
				<li><strong>Electronic</strong></li>
				<li class="label"><label>Wire Service</label></li>
				<li><select name="wire-service"><option>Checking Account</option></select></li>
                <li class="label"><label>Shop Code</label></li>
				<li><input name="shop-code" type="text" class="shop-code" value="52411" disabled="disabled" />
				</li>
			</ul>
		</form>
	</div>
	<!-- Close DIV send-order-electronic -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer"></div>
<!-- Close DIV sidebar-footer -->
