<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
    <body>
      <form:form method="post" commandName="formMessage">
        <div id="container">
            <div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
            </div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
        <c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
        </c:import>
        </div>
        <div class="sidebar">
        <c:choose>
          <c:when test="${ formMessage.sent }">
			<div class="headline">&nbsp;</div>
			<!-- Close DIV headline -->
			<div class="sidebar-content">
			    <div class="messages-sent">
			        <ul>
			            <li>Your Message Has Been Sent!</li>
			        </ul>
			    </div>
			    <!-- Close DIV messages-reply -->
			</div>
			<!-- Close DIV sidebar-content -->
			<div class="sidebar-footer">
			    <a href="AgentView.htm?query=showNextOrder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">
			      <img src="images/next-assignment.png" alt="Get Next Assignment" class="next-order" />
			    </a>
			</div>
			<!-- Close DIV sidebar-footer -->
          </c:when>
          <c:otherwise>
			<div class="headline">New Message</div>
			<!-- Close DIV headline -->
			<div class="sidebar-content">
			    <div class="messages-reply">
			        <ul>
			            <li>
		                 <form:select path="messageTypeId" name="Reason" class="field-width">
		                    <form:option value="">Choose Message Type</form:option>
		                    <c:forEach var="mt" items="${ messageTypes }">
		                    <c:choose>
		                    <c:when test="${ (mt.messageTypeId == 4 || mt.messageTypeId == 5 || mt.messageTypeId == 6 ||  mt.messageTypeId == 10 || mt.messageTypeId == 11 )}">
		                        <form:option value="${mt.shortDesc }"><c:out value="${mt.description }" /></form:option>
		                    </c:when>
		                    </c:choose>
		                    </c:forEach>
		                 </form:select>
			            </li>
			         </ul>
			       </div>
				   <div class="slidingDivMessageTxt">
				     <ul>
			            <li>
			              <form:textarea path="messageText" name="reply" class="reply-message-box" />
			            </li>
			            </ul>
			            </div>
 					</div>
 					<div class="slidingDivPCHG">
				     <ul>
			            <li>
					     New Price*:<form:input path="newPrice" class="reply-message-text" />
			            </li> 
			         </ul>
 					</div>
			    <!-- Close DIV messages-reply -->

			<!-- Close DIV sidebar-content -->
			<div class="sidebar-footer">
			             <input type="image" src="images/btn-send.png" name="submit" value="submit" class="bottom-buttons" onclick="this.disabled=true,this.form.submit();" />
			</div>
			<!-- Close DIV sidebar-footer -->
		  </c:otherwise>	
		</c:choose>
        <center>
        <font face="Arial" size="2" color="red">
          <spring:bind path="webAppBomorder.*">
            <c:if test="${status.errors.errorCount > 0}">
              <c:forEach var="error" items="${status.errors.allErrors}">
                <spring:message message="${error}"></spring:message><br />
              </c:forEach>
            </c:if>
          </spring:bind>
        </font>
        </center>
        </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
      </form:form>
    </body>
</html>