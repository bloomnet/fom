<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page import="com.bloomnet.bom.mvc.businessobjects.WebAppUser" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<c:set var="contactFlorist" scope="page" value="${MASTERORDER.contactFlorist}" />
<c:set var="dispositions" scope="page" value="${MASTERORDER.dispositions}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
        <form:form method="post" commandName="callDisposition">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
	    <div class="sidebar">
		<div class="headline">You Are Contacting...</div>
		<!-- Close DIV headline -->
		<div class="sidebar-content">
		    <div class="florist-info">
		        <a href="#"><c:out value="${contactFlorist.shopName}" /></a><br />
		        <c:out value="${contactFlorist.shopAddress1}" /><br /> 
		        <c:out value="${contactFlorist.city.name}" />, <c:out value="${contactFlorist.city.state.shortName}" /> <c:out value="${contactFlorist.zip.zipCode}" /><br />
		        <strong>Phone:</strong>&nbsp;<c:out value="${contactFlorist.shopPhoneFormatted}" /><br />
		       <strong> Fax:</strong>&nbsp;<c:out value="${contactFlorist.shopFax}" />
		       		<c:forEach items="${ contactFlorist.shopnetworks }" var="sn" >
                        <c:choose>
                        <c:when test="${(fn:containsIgnoreCase(sn.network.name,'bloomnet')) && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                            <br />
                            <strong><c:out value='${sn.network.name}' /></strong>: <c:out value='${sn.shopCode}' />
                        </c:when> 
                        </c:choose>
                      </c:forEach>

                      <c:forEach items="${ contactFlorist.shopnetworks }" var="sn" >
                        <c:choose>
                         <c:when test="${(fn:containsIgnoreCase(sn.network.name,'teleflora')) && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                            <br />
                           <strong><c:out value='${sn.network.name}' /></strong>: <c:out value='${sn.shopCode}' />
                        </c:when>
                        </c:choose>
                      </c:forEach>
		       <br /><br />
                      <a href="AgentViewEditFlorist.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />" >Edit Florist</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onclick='confirmCoverageRemoval("<c:out value="${MASTERORDER.bean.recipientZipCode}" />","<c:out value="${contactFlorist.shopName}" />")' >Remove Coverage</a>

                      <br /><br />
                <div class="send-order">
				    <a href="AgentViewSelectSendMethod.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/send-order.png" alt="Send Order" /></a>
				</div>
		        <!-- Close DIV send-order -->
		    </div>
		    <!-- Close DIV florist-info -->
		    <div class="choose-florist">
            <h3>Choose another florist</h3>
            <br />
            <ul>
                <li>
                <form:select path="callDispDescription" name="Reason" class="field-width">
                    <form:option value=""></form:option>
                    <c:forEach var="d" items="${ dispositions }">
                    	<c:if test="${d.parentId == 1 }">
	                    	<form:option value="${d.description }"><c:out value="${d.description }" /></form:option>
	                    </c:if>
                    </c:forEach>
                </form:select>
                </li>
                <li><form:input path="logAcallText" class="field-width" /></li>
                <!-- Shop network status id -->
                <li><form:checkbox path="shopNetworkStatusId" value="3" /><label>Do Not Contact</label></li>
            </ul>
            <br /> 
            <br /> 
           	 <div id="shopNotes" style="height:150px; overflow-y: auto;">
				<c:forEach items="${ contactFlorist.shopNotes }" var="sn" >
				   <c:out value="${sn.user.firstName}" /> <c:out value="${sn.user.lastName}" />    <c:out value="${sn.formattedDate}" />:    <c:out value="${sn.shopNote}" /><br /> <br />
				</c:forEach>
			</div>
			<% WebAppUser user = (WebAppUser) request.getSession(true).getAttribute( "USER" ); 
				String firstName = user.getFirstName();
				String lastName = user.getLastName();
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				String date = sdf.format(new Date());
			%>
			<script type="text/javascript">
				function addShopNote(){
					var shopNote = $("#shopNoteText").val();
					var shopId = $("#shopIdText").val();
					if(shopNote === ""){
						alert("Please enter a note.")
						return;
					}
					$.ajax({
				    	
				        type: 'GET',
				        url: 'shopNotes.htm?shopID='+shopId+'&shopNote='+shopNote,
				        dataType: 'xml',
				        
				        success: function(data) {
							//nothing, a redirect should occur from the controller.
				        },
				        
				        error: function(msg) {
				        	// do nothing
				        }
				        
				     });
					var shopNotesHtml = $("#shopNotes").html();
					$("#shopNotes").html('<%=firstName %>'+" "+'<%=lastName %>'+"    "+'<%=date%>'+":    "+shopNote+"<br /><br />"+shopNotesHtml);
					$("#shopNoteText").val("");
				}
			</script>
			<div style="height: 50px;">
				<textarea id="shopNoteText" style="width: 100%;"></textarea>
				<input type="hidden" id="shopIdText" value="<c:out value="${contactFlorist.shopId}" />"></input>
				<br />
				<a href="#" onclick="addShopNote();">Add Shop Note</a>
			</div>
		    </div>
		    <!-- Close DIV choose-florist -->
		</div>
		<!-- Close DIV sidebar-content -->
		<div class="sidebar-footer">
	            <input align="middle" type="image" src="images/more-florists.png" alt="More Florists" name="MoreFlorists" class="bottom-buttons" />
 	        </div>
		<!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="webAppBomorder.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>