<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.Date"%>
<%
	pageContext.setAttribute("newLineChar", "\n");
%>
<c:set var="now" value="<%=new java.util.Date()%>" />
<div class="blank-headline">Universal Overview for <fmt:formatDate value="${ now }" pattern=" EEEEEEEEE MMMM dd, yyyy" /></div>
<div class="default-content"
	style="margin-top: 22px; margin-right: 83px;">




	<c:set var="containsMoAccess" value="false" scope="page" />
	<c:set var="containsHyperlinkaccess" value="false" scope="page" />
	<c:forEach var="item" items="${roles}">
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(item.key,'hlaccess') }">
				<c:set var="containsHyperlinkaccess" value="true" scope="page" />
			</c:when>
			<c:when test="${fn:containsIgnoreCase(item.key,'moaccess') }">
				<c:set var="containsMoAccess" value="true" scope="page" />
			</c:when>
		</c:choose>
	</c:forEach>
	
	
	<div class="sub-headline" >
	<div style="text-align: center;">Outstanding Orders&nbsp;</div>
	</div>
	
	<table class="landing-table" width="420">
          <tr>
            <td class="col1 header">Total</td>
            <td class="col2 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prior</td>
            <td class="col3 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Today</td>
            <td class="col4 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Future</td>
            <td class="col5 header">Messages</td>
          </tr>
    </table>

	<table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="420">
		<tr>
			<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="20%"><a
						href="<c:url value="/outstanding.htm?timestamp="/><%=new Date().getTime()%>"><c:out
								value="${totalWaiting}" />
					</a>
					</td>

				</c:when>
				<c:otherwise>
					<td width="20%"><c:out value="${totalWaiting}" />
					</td>

				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="20%"><a
						href="<c:url value="/outstandingPrevious.htm?timestamp="/><%=new Date().getTime()%>"><c:out
								value="${previousWaiting}" />
					</a>
					</td>

				</c:when>
				<c:otherwise>
			<td width="20%"><c:out value="${previousWaiting}" />
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="20%"><a
						href="<c:url value="/outstandingToday.htm?timestamp="/><%=new Date().getTime()%>"><c:out
								value="${todayWaiting}" />
					</a>
					</td>
				</c:when>
				<c:otherwise>
			<td width="20%"><c:out value="${todayWaiting}" />
			</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="20%"><a
						href="<c:url value="/outstandingFuture.htm?timestamp="/><%=new Date().getTime()%>"><c:out
								value="${futureWaiting}" />
					</a>
					</td>
				</c:when>
				<c:otherwise>
			<td width="20%"><c:out value="${futureWaiting}" />
			</td>
			</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="20%"><a
						href="<c:url value="/outstandingMessages.htm?timestamp="/><%=new Date().getTime()%>"><c:out
								value="${totalMessages}" />
					</a>
					</td>
				</c:when>
				<c:otherwise>
			<td width="20%"><c:out value="${totalMessages}" />
			</td>
			</c:otherwise>
			</c:choose>
		</tr>
	</table>

	<br />
	
	<div class="sub-headline">
	<div style="text-align: center;">Orders Waiting for Response&nbsp;</div>
	</div>
	
	<table class="landing-table">
          <tr>
            <td class="col1 header">Total</td>
            <td class="col2 header">Prior</td>
            <td class="col3 header">Today</td>
            <td class="col4 header">Future</td>
          </tr>
    </table>

	<table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="420">
		<tr>
		<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>

					<td width="25%"><a
				href="<c:url value="/wfr.htm?timestamp="/><%=new Date().getTime()%>"><c:out
						value="${totalWFR}" />
			</a>
			</td>

				</c:when>
				<c:otherwise>
					<td width="25%"><c:out value="${totalWFR}" />
					</td>

				</c:otherwise>
			</c:choose>
			
			<td width="25%"><c:out value="${previousWFR}" />
			</td>
			<td width="25%"><c:out value="${todayWFR}" />
			</td>
			<td width="25%"><c:out value="${futureWFR}" />
			</td>
		</tr>
	</table>

	<br />

	<br />
	<c:choose>
	<c:when test='${containsMoAccess == "true"}'>
		Search Open Orders By State: 
		<select id="stateSelection" onchange="getOrdersByState()">
			<option value=""></option>
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District Of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>
		</select>	
	</c:when>		
	</c:choose>
<%-- 
<div class="sub-headline">
	<div style="text-align: center;">Prior Orders by Occasion&nbsp;</div>
	</div>
	<table class="landing-table">
          <tr>
            <td class="col5 header">Funeral</td>
            <td class="col6 header">Anniversary</td>
            <td class="col7 header">Birthday</td>
          </tr>
    </table>
	<table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="420">
		
		<tr>
			<td width="33%"><c:out value="${funeralPrev}" />
			</td>
			<td width="33%"><c:out value="${anniversaryPrev}" />
			</td>
			<td width="33%"><c:out value="${birthdayPrev}" />
			</td>
		</tr>
	</table>

	<br />

<div class="sub-headline">
	<div style="text-align: center;">Today's Orders by Occasion&nbsp;</div>
	</div>
	<table class="landing-table">
          <tr>
            <td class="col5 header">Funeral</td>
            <td class="col6 header">Anniversary</td>
            <td class="col7 header">Birthday</td>
          </tr>
    </table>
	<table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="420">
		<tr>
			<td width="33%"><c:out value="${funeralToday}" />
			</td>
			<td width="33%"><c:out value="${anniversaryToday}" />
			</td>
			<td width="33%"><c:out value="${birthdayToday}" />
			</td>
		</tr>
	</table>

	<br />

<div class="sub-headline">
	<div style="text-align: center;">Future Orders by Occasion&nbsp;</div>
	</div>
	<table class="landing-table">
          <tr>
            <td class="col5 header">Funeral</td>
            <td class="col6 header">Anniversary</td>
            <td class="col7 header">Birthday</td>
          </tr>
    </table>
	<table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="420">
		<tr>
			<td width="33%"><c:out value="${funeralFuture}" />
			</td>
			<td width="33%"><c:out value="${anniversaryFuture}" />
			</td>
			<td width="33%"><c:out value="${birthdayFuture}" />
			</td>
		</tr>
	</table> --%>

	<br />


	<center>
		<c:forEach var="msg" items="${ messages }">
			<font color="red" face="Arial" size="3"><c:out
					value='${ fn:replace(fn:replace( msg, newLineChar, "<br>"),"Transaction rolled back because it has been marked as rollback-only","Could not get a next assignment because there are no eligible orders") }'
					escapeXml="false" />
			</font>
			<br />
		</c:forEach>
	</center>
	<% if(request.getParameter("messages") != null){ %>
		<center>
				<font color="red" face="Arial" size="3">
				<%= request.getParameter("messages") %>
				</font>
				<br />
		</center>
	<%} %>
</div>

