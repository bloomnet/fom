<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
        <form:form method="post" commandName="formOrder">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
        <div class="sidebar">
		<c:choose>
		    <c:when test="${ MASTERORDER.complete }">
		        <div class="headline">&nbsp;</div>
		        <!-- Close DIV headline -->
		        <div class="sidebar-content">
		            <div class="order-sent">
		                <ul>
		                    <li><strong>Your Order Has Been Sent!</strong></li>
		                    <li>Commitment to Coverage</li>
		                    <li>Order: #<c:out value="${MASTERORDER.outboundOrderNumber}" /></li>
		                    <c:if test="${MASTERORDER.bean.commMethod != 2}">
		                    <li>Bloomnet Order: #<c:out value="${MASTERORDER.bloomnetOrderNumber}" /></li>
		                    </c:if>
		                </ul>
		            </div>
		            <!-- Close DIV order-sent -->
		        </div>
		        <!-- Close DIV sidebar-content -->
		        <div class="sidebar-footer">
		            <a href="default.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">
		              <img src="images/exit-order.png" alt="Exit Order" class="exit-order" />
		            </a>
		        </div>
		    </c:when>
		    <c:otherwise>
				<div class="headline">Order Confirmation</div>
				<!-- Close DIV headline -->
				<div class="sidebar-content">
				    <div class="order-confirmation">
				        <ul>
				            <c:if test='${ fn:containsIgnoreCase( MASTERORDER.methodToSend.description, "fax" ) }'>
							<li>
							  <strong>This order will be sent via Fax to the following fax number:</strong>
							</li>
				            <li>
				              <c:out value="${MASTERORDER.bean.receivingShop.shopFaxFormatted}" />
				            </li>
 				            </c:if>
							<li>
							  <strong>If the shop would like to receive an email confirmation, leave or put their email here:</strong>
							</li>
				            <li>
				              <form:input path="emailConfirmation" name="email-confirmation" type="text" class="email-confirmation" value="${MASTERORDER.bean.receivingShop.shopEmail}" />
				            </li>
				        </ul>
				    </div>
				    <!-- Close DIV order-confirmation -->
				</div>
				<!-- Close DIV sidebar-content -->
				<div class="sidebar-footer">
				    <input type="image" src="images/send-order.png" alt="Exit Order" value="submit" class="bottom-buttons" onclick="this.disabled=true,this.form.submit();"/>
				</div>
		    </c:otherwise>
		</c:choose>
		<!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="webAppBomorder.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>