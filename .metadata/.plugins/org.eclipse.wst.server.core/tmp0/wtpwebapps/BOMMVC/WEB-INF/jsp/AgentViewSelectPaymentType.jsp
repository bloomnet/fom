<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />

<c:forEach items="${ MASTERORDER.contactFlorist.shopnetworks }" var="sn" >
	<c:choose>
		<c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet') && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
		  <c:set var="bloomnet" value="bloomnet" scope="page" />
		</c:when>
		<c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora') && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
			<c:set var="teleflora" value="teleflora" scope="page" />
		</c:when>
	</c:choose>
</c:forEach>
                      
	<body>
		<div style='display: none' class="overlay-wrapper" id="overlay-1">
<!-- IMPORT OVERLAYS HERE AND MAKE VISIBLE BASED ON WHAT SELECTION IS MADE (CURRENTLY EDITORDER OR ORDERHISTORY -->
		</div>
        <form:form method="post" commandName="formOrder">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
	    <div class="sidebar">
		<div class="headline">Payment Options</div> <!-- Close DIV headline -->
		    <div class="sidebar-content">
		      <div class="phone-order">
		          <ul>
				  <li class="no-border">
					  <strong>Pay To:</strong>
					  <form:input path="payTo" class="field-width" value="${MASTERORDER.bean.receivingShop.shopName}" />
				  </li>
				  <li class="no-border no-padding">
					  <strong>Amount:</strong>
					  <form:input path="paymentAmountStr" class="field-width" value="${MASTERORDER.bean.totalCost}" />
				  </li>
				  <li>
				  </li>
	              <c:forEach var="pt" items="${MASTERORDER.paymentTypes}">
	                <c:if test="${fn:containsIgnoreCase(pt.description, 'clearinghouse' ) }">
	                	<c:if test="${ bloomnet=='bloomnet' || teleflora=='teleflora' }">
			                <li class="no-border">
			                	<form:radiobutton path="paymentTypeStr" value="${pt.paymentTypeId}" /><label><c:out value="${pt.description}" /></label>
			                </li>
			             </c:if>
	                </c:if>
	                <li class="no-border"></li>
	              </c:forEach>
	              <c:forEach var="pt" items="${MASTERORDER.paymentTypes}">
	                <c:if test="${fn:containsIgnoreCase(pt.description, 'check' ) }">
	                <li class="no-border">
	                <form:radiobutton path="paymentTypeStr" value="${pt.paymentTypeId}" /><label><c:out value="${pt.description}" /></label>
	                </li>
	                <li class="no-border"></li>
	                </c:if>
	              </c:forEach>
	              <c:forEach var="pt" items="${MASTERORDER.paymentTypes}">
	                <c:if test="${fn:containsIgnoreCase(pt.description, 'card' ) }">
	                <li class="no-border">
	                <form:radiobutton path="paymentTypeStr" value="${pt.paymentTypeId}" /><label><c:out value="${pt.description}" /></label>
	                </li>
	                </c:if>
	              </c:forEach>
				  <li>
				  </li>
		          </ul>
		          <ul>
		          	               <li class="no-border">
		          <div class="slidingDivNetwork">
		          <c:set var="BMT" value="false" scope="page" />
		          <c:set var="BMTID" value="" scope="page" />
		          <c:set var="BMTName" value="" scope="page" />
		          <c:set var="BMTShopCode" value="" scope="page" />
		          <c:set var="TF" value="false" scope="page" />
		          <c:set var="TFID" value="" scope="page" />
		          <c:set var="TFName" value="" scope="page" />
		          <c:set var="TFShopCode" value="" scope="page" />
					<form:select path="selectedShopnetwork" class="field-width" >
                      <c:forEach items="${ MASTERORDER.contactFlorist.shopnetworks }" var="sn" >
                        <c:choose>
                        <c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet') && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                            <c:set var="BMT" value="true" scope="page" />
                            <c:set var="BMTID" value="${ sn.shopNetworkId  }" scope="page" />
		          			<c:set var="BMTName" value="${ sn.network.name }" scope="page" />
		          			<c:set var="BMTShopCode" value="${ sn.shopCode }" scope="page" />
                        </c:when>
                         <c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora') && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                            <c:set var="TF" value="true" scope="page" />
                            <c:set var="TFID" value="${ sn.shopNetworkId  }" scope="page" />
		          			<c:set var="TFName" value="${ sn.network.name }" scope="page" />
		          			<c:set var="TFShopCode" value="${ sn.shopCode }" scope="page" />
                        </c:when>
                        </c:choose>
                      </c:forEach>
                      <c:choose>
                        <c:when test='${BMT == "true"}'>
                            <form:option selected="selected" value="${ BMTID  }"><c:out value="${ BMTName }" /> : <c:out value="${ BMTShopCode }" /></form:option>
                        </c:when>
                         <c:when test='${TF == "true" }'>
                            <form:option selected="selected" value="${ TFID  }"><c:out value="${ TFName }" /> : <c:out value="${ TFShopCode }" /></form:option>
                        </c:when>
                       </c:choose>
                    </form:select>
				  </div>
				  </li>
					<li class="no-border">
			          <div class="slidingDivBOA">
						    <strong><a href="https://payment2.works.com/wpm/bookmark" target="BOA_site">Bank of America PO</a></strong>
						</div>
					</li>
		          </ul>
		      </div><!-- Close DIV phone-order -->
		    </div> <!-- Close DIV sidebar-content -->
		<div class="sidebar-footer">
		          <input type="image" src="images/next-btn.png" alt="Next" value="submit" class="bottom-buttons" />
		</div> <!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="webAppBomorder.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>