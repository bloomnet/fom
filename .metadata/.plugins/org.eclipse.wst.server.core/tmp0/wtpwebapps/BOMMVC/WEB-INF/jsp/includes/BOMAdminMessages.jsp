<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>

<body>
<br/>
<br/>

<table border="0" >
	<form:form method="post" commandName="order">
     <tr> 
     <th> View Messages</th>
      </tr> <tr>
 <td>Order Number:<form:input path="bloomnetOrderNumber"
			class="reply-message-text" /></td>
			</tr>
		
		<tr>
			<td>
		<input type="image" src="images/btn-send.png" name="submit"
			value="submit" class="bottom-buttons" />
			</td>
</tr>
		
		<br/>
	</form:form>
	</table>
	<br />
	
	<c:set var="action" scope="session" value="${MASTERORDER.actionStr}" />

<c:choose>
	<c:when test="${action == 'vieworderhistory'}">
		<a href="#" onclick="openMyModal('orderHistory.htm?query=masterorder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;"><img src="images/view-order-history.png" alt="" /></a>
	</c:when>
	
</c:choose>


</body>
</html>