<%@ page language="java" contentType="text/xml" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<result>
<c:choose>
  <c:when test="${ not empty error }">
    <error><c:out value="${ error }" escapeXml="false" /></error>
  </c:when>
  <c:otherwise>
  	<orders>
  	  <count><c:out value="${ fn:length( orders ) }" /></count>
  	  <c:forEach var="order" items="${ orders }" varStatus="status" >
    	<c:set var="bean"  value="${ order.bean }" />
	    <order><![CDATA[
	    <table class="orders-table">
          <tr>
            <td class="orders-content-col1"><c:out value="${ order.orderNumber }"/></td>
            <td class="orders-content-col2"><c:out value="${ fn:length( bean.products ) }" /></td>
            <td class="orders-content-col3"><fmt:formatNumber value="${ bean.totalCost }" type="CURRENCY" /></td>
            <td class="orders-content-col4"><c:out value="${ bean.recipientZipCode }"/></td>
            <td class="orders-content-col5"><fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" /></td>
            <td class="orders-content-col6"><c:out value="${ order.virtualQueue }"/></td>
            <td class="orders-content-col7">
            	<c:out value="${ order.status }"/></a>
            </td>
          </tr>
        </table>
	    ]]></order>
	  </c:forEach>
    </orders>
  </c:otherwise>
</c:choose>
</result>

