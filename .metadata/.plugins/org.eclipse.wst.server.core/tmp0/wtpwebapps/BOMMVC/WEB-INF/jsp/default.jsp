<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
<body>
	<div id="container">
		<div id="header">
			<c:import url="/WEB-INF/jsp/includes/headerLogin.jsp">
			</c:import>
		</div>
		<!-- Close DIV header -->
		<div class="content-container">
		<div class="order-area">
				<c:import url="/WEB-INF/jsp/includes/landingArea.jsp">
				</c:import>
			<div class="clear"></div>
			<!-- Clear Floats -->
			</div>
	    <div class="sidebar">
            <c:import url="/WEB-INF/jsp/includes/landingSidebar.jsp" >
            </c:import>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
		<div class="content-footer">
			<c:import url="/WEB-INF/jsp/includes/footer.jsp">
			</c:import>
		</div>
		<!-- Close DIV content-footer -->
		<div id="footer"></div>
		<!-- Close DIV footer -->
	</div>
</body>
</html>