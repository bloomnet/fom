<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="login-area">Welcome <c:out value="${USER.firstName}" /> <c:out value="${USER.lastName}" /> | <a href="signout.htm">Log Out</a>
</div><!-- Close DIV login-area -->
<div class="stat-bar">
    <div class="stat-label">
   
    </div>
    <div class="stat-label">
   
    </div><!-- Close DIV stat-label -->
    <div class="clear"></div>
</div> <!-- Close DIV stat-bar -->
<a href="default.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/logo.png" alt="Bloomnet" class="logo" /></a>