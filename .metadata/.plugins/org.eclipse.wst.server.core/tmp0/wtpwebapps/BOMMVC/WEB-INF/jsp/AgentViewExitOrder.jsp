<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contactFlorist" scope="page" value="${MASTERORDER.contactFlorist}" />
<c:set var="dispositions" scope="page" value="${MASTERORDER.dispositions}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
        <form:form method="post" commandName="callDisposition">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
	    <div class="sidebar">
		<div class="headline">Exiting Order...</div>
		<!-- Close DIV headline -->
		<div class="sidebar-content">
		    <!-- Close DIV florist-info -->
		    <div class="choose-florist">
            <h3>Create disposition</h3>
            <br />
            <ul>
                <li>
                
<c:set var="containsWFRView" value="false" scope="page" /> 
	<c:forEach var="item" items="${roles}">
		<c:choose>
 			<c:when test="${fn:containsIgnoreCase(item.key,'wfrOrders') }">
  				<c:set var="containsWFRView" value="true" scope="page" />
  			</c:when> 
  		</c:choose> 
	</c:forEach> 
                
                
                <form:select path="callDispDescription" name="Reason" class="field-width">
                    <form:option value=""></form:option>
                    <c:forEach var="d" items="${ dispositions }">
                    	<c:if test="${d.parentId == 2 }">
                    		<c:choose>
	                    		<c:when test='${d.description == "Waiting for Response" && MASTERORDER.virtualQueue != "TLO"}'> 
	                    		</c:when>
	                    		<c:when test='${d.description == "Order Completed" && containsWFRView == "false"}'>
	                    		</c:when>
		                    	<c:otherwise>
		                    		<form:option value="${d.description }"><c:out value="${d.description }" /></form:option>
		                    	</c:otherwise>
		                    </c:choose>
	                    </c:if>
                    </c:forEach>
                </form:select>
                </li>
                <li><form:input path="logAcallText" class="field-width" /></li>
                <li>
                <br />
                <br />
                </li>
            </ul>
            <br /> 
            <br /> 
		    </div>
		    <!-- Close DIV choose-florist -->
		</div>
		<!-- Close DIV sidebar-content -->
		<div class="sidebar-footer">
				        <input type="image" src="images/exit-order.png" alt="Next" value="submit" class="bottom-buttons" />
        </div>
		<!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="webAppBomorder.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>