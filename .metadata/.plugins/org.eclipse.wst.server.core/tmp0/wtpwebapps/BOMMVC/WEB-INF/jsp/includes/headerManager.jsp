<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="login-area">Welcome <c:out value="${USER.firstName}" /> <c:out value="${USER.lastName}" /> | <a href="signout.htm">Log Out</a>
</div>
<!-- Close DIV login-area -->
<div class="stat-bar agent">
	<a href="AgentView.htm"><img src="images/switch-agent.png" alt="Switch to Agent View" /></a>
	<div class="clear"></div>
</div>
<!-- Close DIV stat-bar -->
<a href="default.htm"><img src="images/logo.png" alt="Bloomnet" class="logo" /></a>
<a href="AgentViewExitMultipleOrders.htm" >Exit My Orders</a>