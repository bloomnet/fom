<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div class="headline">
	Advanced Search
	<div class="clear" ></div>
</div>
<!-- Close DIV headline -->
<div class="manager-all-orders">
	<div class="sub-headline">
	<div style="float: left;">Orders&nbsp;</div>
	<div id="orderStaticCount" style="float: left;"> [<c:out value="${ ordersCount }"/>] </div>
	<div id="orderSearchCount" style="float: left;"></div>
	</div>
	<!-- Close DIV sub-headline -->
    <table class="orders-table">
          <tr>
            <td class="col1 header">Order#</td>
            <td class="col2 header">Items</td>
            <td class="col3 header">Price</td>
            <td class="col4 header">Zip Code</td>
            <td class="col5 header">Delivery Date</td>
            <td class="col6 header">Queue</td>
            <td class="col7 header">Status</td>
          </tr>
    </table>
    <div class="order-table-container" id="orderStaticPane">
      <table border="0" cellspacing="0" cellpadding="0" class="orders-table">
	    <c:forEach var="order" items="${ orders }" varStatus="counter">	    
	    	<c:set var="bean"  value="${ order.bean }" />
	          <tr>
	          <c:choose>
	          <c:when test="${ fn:length(order.orderNumber) > 8 }">
	            <td class="orders-content-col1" title="<c:out value="${ order.orderNumber }"/>"  ><c:out value="${ fn:substring(order.orderNumber,0,7)}"/>...</td>
	          </c:when>
	          <c:otherwise>
	            <td class="orders-content-col1"><c:out value="${ order.orderNumber }"/></td>
	          </c:otherwise>
	          </c:choose>
	            <td class="orders-content-col2"><c:out value="${ fn:length( bean.products ) }" /></td>
	            <td class="orders-content-col3"><fmt:formatNumber value="${ bean.totalCost }" type="CURRENCY" /></td>
	            <td class="orders-content-col4"><c:out value="${ bean.recipientZipCode }"/></td>
	            <td class="orders-content-col5"><fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" /></td>
	            <td class="orders-content-col6"><c:out value="${ order.virtualQueue }"/></td>
	            <td class="orders-content-col6"><c:out value="${ order.status }"/></td>
	          </tr>
			<div class="clear"></div>
			<!-- Clear Floats -->
	    </c:forEach>
	  </table>
    </div> <!-- Close DIV order-table-container -->
	<div id="orderSearchPane"></div><!-- dynamic div for ajax search -->
</div><!-- Close DIV manager-all-orders -->
<div class="clear"></div><!-- Clear Floats -->



