<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:set var="orderBean"     scope="page" value="${MASTERORDER.bean}" />
<c:set var="receivingShop" scope="page" value="${orderBean.receivingShop}" />
<c:set var="sendingShop"   scope="page" value="${MASTERORDER.sendingShop}" />
<c:set var="orderNumber"   scope="page" value="${orderBean.orderNumber}" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
  <body>
    <div class="overlay-wrapper" id="overlay-1">
	<div id="overlay" style="width:478px; height:550px;">
		<form:form method="post" commandName="formOrder">
			<div class="title">Edit Order</div>
			<!-- Close DIV overlay-title -->
			<div class="order-info">
				<ul class="left">
					<li><strong>First Name</strong>
					</li>
					<li><form:input path="bean.recipientFirstName" type="text" value="${MASTERORDER.bean.recipientFirstName}" />
					</li>
					<li><strong>Last Name</strong>
					</li>
					<li><form:input path="bean.recipientLastName" type="text" value="${MASTERORDER.bean.recipientLastName}" />
					</li>
					<li><strong>Phone</strong>
					</li>
					<li><form:input path="bean.recipientPhoneNumber" type="text" value="${MASTERORDER.bean.recipientPhoneNumber}" />
					</li>
					<li><strong>Delivery Date</strong>
					</li>
					<li><form:input path="bean.deliveryDate" type="text" value="${MASTERORDER.bean.deliveryDate}" />
					</li>
					 <c:if test='${ orderBean.flexDeliveryDate != null  }'>
					<li><strong>Flex Delivery Date</strong>
					</li>
					<li><form:input path="bean.flexDeliveryDate" type="text" value="${MASTERORDER.bean.flexDeliveryDate}" />
					</li>
					</c:if>
					<li><strong>Occassion</strong>
					</li>
					<li>
                    <form:select path="bean.occasionId" name="occasion">
                       <c:forEach var="o" items="${ occassions }">
                           <form:option value="${ o.key }"><c:out value="${ o.value }" /></form:option>
                       </c:forEach>
                    </form:select>
                    </li>
				</ul>
				<ul class="right">
					<li><strong>Address1</strong>
					</li>
					<li><form:input path="bean.recipientAddress1" type="text" value="${MASTERORDER.bean.recipientAddress1}" />
					</li>
					<li><strong>Address2</strong>
					</li>
					<li><form:input path="bean.recipientAddress2" type="text" value="${MASTERORDER.bean.recipientAddress2}" />
					</li>
					<li><strong>City</strong>
					</li>
					<li><form:input path="bean.recipientCity" type="text" value="${MASTERORDER.bean.recipientCity}" />
					</li>
					<li><strong>State</strong>
					</li>
						<li style="width:144px;">
						<form:select path="bean.recipientState" name="state">
						   <c:forEach var="s" items="${ states }">
						   	   <c:choose>
		                           <c:when test="${MASTERORDER.bean.recipientState == s.key}">
		                           	<form:option value="${ s.key }" selected="selected"><c:out value="${ s.key }" /></form:option>
		                           </c:when>
		                           <c:otherwise>
		                           	<form:option value="${ s.key }"><c:out value="${ s.key }" /></form:option>
		                           </c:otherwise>
		                       </c:choose>
	                       </c:forEach>
						</form:select>
					</li>
					<li><strong>Zip</strong>
					</li>
					<li><form:input path="bean.recipientZipCode" type="text" value="${MASTERORDER.bean.recipientZipCode}" />
					</li>
				</ul>
				<div class="clear"></div>
				<!-- Close DIV clear -->
			</div>
			<!-- Close DIV order-info -->
			<div class="product-info">
				<ul>
					<!-- Product container - start product loop here -->
					<!-- CLOSE Product container - start product loop here -->
					<c:forEach var="p" items="${orderBean.products}" varStatus="status" >         
					<!-- Product container -->
					<li class="container">
						<div class="product-image">
							<img src="images/noimage.PNG" />
						</div> <!-- Close DIV product-image -->
						<div class="product-data">
							<ul>
								<li>
									<strong>Product Code</strong>
								</li>
								<li>
									<form:input path="bean.products[${status.index}].productCode" value="${p.productCode}" type="text" class="product-code"  />
								</li>
							</ul>
							<ul>
								<li>
									<strong>Quantity</strong>
								</li>
								<li>
 									<form:input path="bean.products[${status.index}].units" value="${p.units}" type="text" class="quantity" />
 								</li>
							</ul>
							<div class="clear"></div> <!-- Close DIV clear -->
							<br />
							<div class="description">
							  <form:textarea path="bean.products[${status.index}].productDescription" value="${p.productDescription}" />
							</div>
						</div>
						<!-- Close DIV product-data -->
						<div class="clear"></div> <!-- Close DIV clear -->
					</li>
					<form:hidden path="bean.products[${status.index}].costOfSingleProduct" value="${p.costOfSingleProduct}" />
					</c:forEach> 
					<!-- CLOSE Product container -->
				</ul>
			</div>
			<!-- Close DIV product-info -->
			<div class="messaging">
			<ul>
				<li><strong>Card Message: <font color="red"><form:errors path="bean.cardMessage" cssClass="error" /></font></strong></li>
				<li>
				  <form:textarea path="bean.cardMessage" name="cardmessage" class="cardmessage" value="${fn:trim(orderBean.cardMessage)}" />
				</li>
				<li class="break"><strong>Special Instructions: <font color="red"><form:errors path="bean.specialInstruction" cssClass="error" /></font></strong></li>
				<li>
				  <form:textarea path="bean.specialInstruction" name="specialinstructions" class="specialinstructions" value="${fn:trim(orderBean.specialInstruction)}" />
				</li>
			</ul>
			</div>
			<!-- Close DIV messaging -->
			<div class="save-buttonEdit">
				<input type="image" src="images/save-button.png" value="submit" />
			</div>
			<!-- Close DIV save-button  -->
	        <center>
	        <font face="Arial" size="2" color="red">
	          <spring:bind path="webAppBomorder.*">
	            <c:if test="${status.errors.errorCount > 0}">
	              <c:forEach var="error" items="${status.errors.allErrors}">
	                <spring:message message="${error}"></spring:message><br />
	              </c:forEach>
	            </c:if>
	          </spring:bind>
	        </font>
	        </center>
		</form:form >
	</div>
	<!-- Close DIV overlay -->
	</div>
  </body>
</html>