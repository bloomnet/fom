<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>

<body>
	<br />
	<br />
	<br />

	<table border="0">
		<form:form method="post" commandName="order">
			<tr>
				<th>Send order to Queue</th>
			</tr>
			<tr>
				<td>Order Number:<form:input path="bloomnetOrderNumber"
						class="reply-message-text" />
				</td>
			</tr>
			<tr>
				<td>Queue: <form:select path="queues" name="queues">
						<c:forEach var="d" items="${ destinations }">

							<form:option value="${ d.key }">
								<c:out value="${ d.key }" />
							</form:option>

						</c:forEach>
					</form:select></td>
			</tr>
			<tr>
				<td><input type="image" src="images/btn-send.png" name="submit"
					value="submit" class="bottom-buttons" /></td>
			</tr>
		</form:form>
	</table>
	<br />




</body>
</html>