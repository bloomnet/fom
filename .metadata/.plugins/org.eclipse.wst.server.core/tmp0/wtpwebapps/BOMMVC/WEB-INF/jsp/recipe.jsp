<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"        prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"   prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"         prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags"      prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
  <body>
    <div class="overlay-wrapper" id="overlay-1">
	<div id="overlay" class="orderNotesOverlay" style="width:478px; height:300px;">
		<form:form method="post" commandName="formOrder">
			<div class="title">Recipe</div>
			<div class="orderNotes">
			dsfafasf
			</div>
			<!-- Close DIV messaging -->
			
	        <center>
	        <font face="Arial" size="2" color="red">
	          <spring:bind path="webAppBomorder.*">
	            <c:if test="${status.errors.errorCount > 0}">
	              <c:forEach var="error" items="${status.errors.allErrors}">
	                <spring:message message="${error}"></spring:message><br />
	              </c:forEach>
	            </c:if>
	          </spring:bind>
	        </font>
	        </center>
		</form:form >
	</div>
	<!-- Close DIV overlay -->
	</div>
  </body>
</html>