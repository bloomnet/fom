<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import ="java.util.Date" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
    <div class="sidebar">
        <div class="headline">
	&nbsp;</div> <!-- Close DIV headline -->
<div class="sidebar-content">
  <br>
  <br>
  <c:set var="containsHyperlinkaccess" value="false" scope="page" />
	<c:forEach var="item" items="${roles}">
		<c:choose>
			<c:when test="${fn:containsIgnoreCase(item.key,'hlaccess') }">
				<c:set var="containsHyperlinkaccess" value="true" scope="page" />
			</c:when>
		</c:choose>
	</c:forEach>
  <div class="florist-list">
  <c:forEach var="view" items="${USER.userViews}"> 
    <font color="red" face="Arial" size="3">
      <c:choose>
        <c:when test='${fn:containsIgnoreCase( view.url,"AgentView" )}'>
          <a id="nextAssignment" href="AgentView.htm?query=showNextOrder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Next Assignment</a><br />
        </c:when>
      </c:choose>
      <!-- <c:out value="${ view.url }" /><br /> -->
    </font>
  </c:forEach>
  <br><br />
  <c:forEach var="view" items="${USER.userViews}"> 
  <font color="red" face="Arial" size="3">
    <c:choose>
            <c:when test='${fn:containsIgnoreCase( view.url,"WFRView" )}'>
            <a href="<c:url value="/wfr.htm?timestamp="/><%= new Date().getTime() %>">Orders Waiting for Response</a><br />
        </c:when>
    </c:choose>
  </font>
  </c:forEach> 
  <br />
<br />
<font color="red" face="Arial" size="3">
<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>
            <a href="<c:url value="/auto.htm?timestamp="/><%= new Date().getTime() %>">Outstanding Automated Orders</a><br />
        </c:when>
    </c:choose>
    </font>
  <br />
<br />
<font color="red" face="Arial" size="3">
<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>
            <a href="<c:url value="/ProductivityView.htm?timestamp="/><%= new Date().getTime() %>">Manager Console</a><br />
        </c:when>
    </c:choose>
    </font>
  <br />
<br />
 <font color="red" face="Arial" size="3">
<c:choose>
				<c:when test='${containsHyperlinkaccess == "true"}'>
            <a href="<c:url value="/ReportView.htm?timestamp="/><%= new Date().getTime() %>&query=1">Daily Metrics</a><br />
        </c:when>
    </c:choose>
  </font>
  <br />
<br />
  <c:forEach var="view" items="${USER.userViews}"> 
  <font color="red" face="Arial" size="3">
    <c:choose>
            <c:when test='${fn:containsIgnoreCase( view.url,"ManagerView" )}'>
            <a href="<c:url value="/ManagerView.htm?timestamp="/><%= new Date().getTime() %>&query=1">Advanced Search</a><br />
        </c:when>
    </c:choose>
  </font>
  </c:forEach> 
<br />
<br />
  <c:forEach var="view" items="${USER.userViews}"> 
  <font color="red" face="Arial" size="3">
    <c:choose>
            <c:when test='${fn:containsIgnoreCase( view.url,"BOMAdminView" )}'>
            <a href="<c:url value="/BOMAdminView.htm?timestamp="/><%= new Date().getTime() %>">Administrator Console</a><br />
        </c:when>
    </c:choose>
  </font>
  </c:forEach> 
 
  
  <br />
<br />
  Your working shop: <br>
  <c:out value="${USER.myShop.shopName}" />
    <c:out value="${USER.securityElement.shopcode}" /><br /><br />
  <br>
<!--   Your roles: <br>
  <c:forEach var="u" items="${USER.userrolesForUserId}"> 
  <c:out value="${u.role.description}" /><br>
  </c:forEach> 
  <br>--> 
  <br> 
	<center>
	  <c:forEach var="msg" items="${ messages }"> 
	    <font color="red" face="Arial" size="3"><c:out value='${ fn:replace(fn:replace( msg, newLineChar, "<br>"),"Transaction rolled back because it has been marked as rollback-only","Could not get a next assignment because there are no eligible orders") }'
	     escapeXml="false"/></font><br />
	  </c:forEach> 
	</center>
  </div>
</div>
<!-- <div class="sidebar-footer">
</div>  --><!-- Close DIV sidebar-footer -->
</div>
