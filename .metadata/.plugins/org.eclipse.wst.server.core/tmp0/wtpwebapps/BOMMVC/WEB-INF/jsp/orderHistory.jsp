<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="orderBean" scope="page" value="${MASTERORDER.bean}" />
<c:set var="currentFulfillingOrderNumber" scope="page" value="${MASTERORDER.outboundOrderNumber}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
  <body>
	<div id="overlay">
		<form>
			<div class="title">
				<table>
					<tr>
						<td>
							Order History
						</td>
						<td style="width: 100px">&nbsp;</td>
						<td>
							Status: <c:out value='${MASTERORDER.status }'/>
						</td>
					</tr>
				</table>
			</div>
			<!-- Close DIV overlay-title -->
			<div class="overlay-bar">
				<ul>
					<li class="bar-left"><strong>Sending shop</strong>
					</li>
					<li class="bar-center"><strong>Fullfilling shop</strong>
					</li>
					<li class="bar-right"><strong>Status</strong>
					</li>
					<li class="bar-extreme-right"><strong>Time/Date</strong>
					</li>
				</ul>
				<div class="clear"></div>
				<!-- Close DIV clear -->
			</div>
			<!-- Close DIV overlay-bar -->
			<div class="order-overlay-info">
				<!-- START Loop here -->
				<c:set var="lastStatusTime" value =""/>
				<c:set var="isFirst" value ="true"/>
				<c:forEach var="orderItem" items="${history }" varStatus="oItemStatus">
					<ul id="order-info-<c:out value='${ oItemStatus.count }' />">
					 <li class="bar-left">
					   <a href="javascript:expand('<c:out value='${ oItemStatus.count }' />');"	class="box-button" id="button-<c:out value='${ oItemStatus.count }' />">+</a>
					   <strong><c:out value='${ orderItem.key.shopBySendingShopId.shopName }' /></strong>
					</li>
					<li class="bar-center">
					<c:set var="conditionVariable" value ="true"/>
					<c:set var="status" value="" />
					  <c:forEach var="h" items="${orderItem.value}" varStatus="status" >
						<c:if test="${ not empty h.actRouting }">
							<c:if test="${conditionVariable eq 'true'}"> 
								<c:set var="conditionVariable" value="false"/>
								<c:set var="status" value='${h.actRouting.stsRouting.description}'/> 
								<c:choose>
									<c:when test='${ orderItem.key.orderNumber == MASTERORDER.orderNumber }'>
								  		<strong><c:out value='${MASTERORDER.shopByReceivingShopId.shopName }' /></strong>
								  	</c:when>
								  	<c:otherwise>
								  		<strong><c:out value='${ orderItem.key.shopByReceivingShopId.shopName }' /></strong>
								  	</c:otherwise>
								</c:choose>
								<c:forEach items="${ orderItem.key.shopByReceivingShopId.shopnetworks }" var="sn" >
						            <c:set var="rsCode"  value="empty" />
						            <c:choose>
						            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet')}">
				   							 <c:choose>
				   							 	<c:when test="${ not empty h.actRouting }">
				   							 		<c:choose>
										            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet')}">
										            		<c:if test='${ h.actRouting.virtualQueue == "BMTFSI" && sn.shopCode != "Z9980000"}'>
										            			<strong>(<c:out value='${sn.shopCode}' />)</strong>
										            			<c:set var="rsCode"  value="${ sn.shopCode }" />
										            		</c:if>
										            		<c:if test='${ h.actRouting.virtualQueue == "TLO" && sn.shopCode != "43030600"}'>
										            			<strong>(<c:out value='${sn.shopCode}' />)</strong>
										            			<c:set var="rsCode"  value="${ sn.shopCode }" />
										            		</c:if>
									            		</c:when>
									            	</c:choose>
												</c:when>
											</c:choose>
						            	</c:when>
						            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora')}">
				   							 <c:choose>
				   							 	<c:when test="${ not empty h.actRouting }">
				   							 		<c:choose>
										            	<c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora')}">
										            		<c:if test='${ h.actRouting.virtualQueue == "TFSI" && sn.shopCode != "43030600"}'>
										            			<strong>(<c:out value='${sn.shopCode}' />)</strong>
										            			<c:set var="rsCode"  value="${ sn.shopCode }" />
										            		</c:if>
										            		<c:if test='${ h.actRouting.virtualQueue == "TLO" && sn.shopCode != "43030600" && rsCode != "empty"}'>
										            			<strong>(<c:out value='${sn.shopCode}' />)</strong>
										            		</c:if>
									            		</c:when>
									            	</c:choose>
												</c:when>
											</c:choose>
						            	</c:when>
						            </c:choose>
						        </c:forEach>
								<c:choose>
								  	<c:when test='${ orderItem.key.orderNumber == currentFulfillingOrderNumber 
								  		&& status != "Rejected by Shop" 
								  		&& status != "Cancel Confirmed by BloomNet" 
								  		&& MASTERORDER.status != "Actively being worked" 
								  		&& MASTERORDER.status != "To be worked"
								  		&& MASTERORDER.status != "Waiting for Response"}'>
								  		<strong>(Current&nbsp;Fulfilling)</strong> 
								  	</c:when>
					  			</c:choose>
							</c:if>
						</c:if>
					</c:forEach>
					</li>
					<li class="bar-right">
					<c:set var="conditionVariable" value ="true"/>
					<c:set var="status" value="" />
					<c:forEach var="h" items="${orderItem.value}" varStatus="status" >
						<c:if test="${ not empty h.actRouting }">
							<c:if test="${conditionVariable eq 'true'}"> 
								<c:set var="conditionVariable" value="false"/>
								<c:choose>
									<c:when test='${orderItem.key.orderNumber == MASTERORDER.orderNumber }'>
										<strong><c:out value='${MASTERORDER.status}' /></strong>
									</c:when>
									<c:otherwise>
										<c:set var="status" value='${h.actRouting.stsRouting.description}'/> 
										<strong><c:out value='${status}' /></strong>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:if>
					</c:forEach>
					</li>
					<li class="bar-extreme-right">
					<c:set var="conditionVariable" value ="true"/> 
					<c:forEach var="h" items="${orderItem.value}" varStatus="status" >
						<c:if test="${isFirst eq 'true'}">
							<c:set var="isFirst" value="false" />
							<c:set var="lastStatusTime" value='${h.createdDate}'/> 
						</c:if>
						<c:if test='${orderItem.key.orderNumber == MASTERORDER.orderNumber 
									&& conditionVariable eq "true"
									&& MASTERORDER.status != "Actively being worked" 
							  		&& MASTERORDER.status != "To be worked"
							  		&& MASTERORDER.status != "Waiting for Response"}' >
							<c:set var="conditionVariable" value="false"/>
							<strong><fmt:formatDate value="${lastStatusTime}" type="time" timeStyle="short" /> on <fmt:formatDate value="${lastStatusTime}" pattern="MM/dd/yyyy" /></strong>
						</c:if>
						<c:if test="${conditionVariable eq 'true'}">
								<c:set var="conditionVariable" value="false"/> 
								<strong><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /></strong>
						</c:if>
					</c:forEach>
					</li>
                                <div class="clear"></div> 
                                <!-- Close DIV clear --> 
						<li id="details-<c:out value='${ oItemStatus.count }' />" class="details">
				          <c:forEach var="h" items="${orderItem.value}" varStatus="status" > 
						    <c:choose>
							    <c:when test="${ not empty h.actMessage }">
									<div class="message-header">
										<strong><u>Message</u></strong><br />
										<strong>Message Type:&nbsp;</strong><c:out value='${ h.actMessage.messagetype.description }' /><br />
										<strong>Sender:&nbsp;</strong><c:out value='${ h.actMessage.shopBySendingShopId.shopName }' /><br />
										<strong>Recipient:&nbsp;</strong><c:out value='${ h.actMessage.shopByReceivingShopId.shopName }' /><br />
										<strong>Time:&nbsp;</strong><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /><br />
										<c:choose>
										  	<c:when test="${ h.user.firstName == h.user.lastName }">
										  		<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /><br />
										  	</c:when>
											<c:otherwise>
												<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /> <c:out value='${h.user.lastName}' /><br />
											</c:otherwise>
										</c:choose>
										<strong>Message Text:&nbsp;</strong><c:out value='${  fn:replace(fn:replace(h.actMessage.messageText, "%0A", ""), "%0D", "")  }' /><br />
									</div> <!-- Close DIV message-header -->
	                                <div class="clear"></div>
	                                <!-- Close DIV clear --> 
	                                <br />
								</c:when>
							    <c:when test="${ not empty h.actRouting }">
									<div class="message-header">
										<strong><u>Routing</u></strong><br />
										<strong>Queue:&nbsp;</strong><c:out value='${ h.actRouting.virtualQueue }' /> - <c:out value='${ h.actRouting.stsRouting.description }' /><br />
										<strong>Time:&nbsp;</strong><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /><br />
										<c:choose>
										  	<c:when test="${ h.user.firstName == h.user.lastName }">
										  		<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /><br />
										  	</c:when>
											<c:otherwise>
												<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /> <c:out value='${h.user.lastName}' /><br />
											</c:otherwise>
										</c:choose>
									</div> <!-- Close DIV message-header -->
	                                <div class="clear"></div> 
	                                <!-- Close DIV clear -->
	                                <c:set var="orderStatus" value='${ h.actRouting.stsRouting.description }'></c:set>
	                                <br />
								</c:when>
							    <c:when test="${ not empty h.actLogacall }">
									<div class="message-header">
										<strong><u>Call Disposition</u></strong><br />
										<strong>Shop:&nbsp;</strong><c:out value='${ h.actLogacall.shopCalled.shopName }' /><br />
										<strong>Disposition:&nbsp;</strong><c:out value='${ h.actLogacall.calldisp.description }' /> - <c:out value='${ h.actRouting.stsRouting.description }' /><br />
										<strong>Details:&nbsp;</strong><c:out value='${ h.actLogacall.logAcallText }' /><br />
										<strong>Time:&nbsp;</strong><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /><br />
										<c:choose>
										  	<c:when test="${ h.user.firstName == h.user.lastName }">
										  		<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /><br />
										  	</c:when>
											<c:otherwise>
												<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /> <c:out value='${h.user.lastName}' /><br />
											</c:otherwise>
										</c:choose>
									</div> <!-- Close DIV message-header -->
	                                <div class="clear"></div> 
	                                <!-- Close DIV clear --> 
	                                <br />
								</c:when>
							    <c:when test="${ not empty h.actPayment }">
									<div class="message-header">
										<strong><u>Payment</u></strong><br />
										<strong>Type:&nbsp;</strong><c:out value='${ h.actPayment.paymenttype.description }' /><br />
										<strong>Amount:&nbsp;</strong><c:out value='${ h.actPayment.paymentAmount }' /><br />
										<strong>Paid To:&nbsp;</strong><c:out value='${ h.actPayment.payTo }' /><br />
										<strong>Status:&nbsp;</strong><c:out value='${ h.actPayment.stsPayment.description }' /><br />
										<strong>Time:&nbsp;</strong><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /><br />
										<c:choose>
										  	<c:when test="${ h.user.firstName == h.user.lastName }">
										  		<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /><br />
										  	</c:when>
											<c:otherwise>
												<strong>User:&nbsp;</strong><c:out value='${h.user.firstName}' /> <c:out value='${h.user.lastName}' /><br />
											</c:otherwise>
										</c:choose>
									</div> <!-- Close DIV message-header -->
	                                <div class="clear"></div> 
	                                <!-- Close DIV clear --> 
	                                <br />
								</c:when>
						    </c:choose>
				          </c:forEach>
                       </li>
					</ul>
				</c:forEach>
		</div>
			<!-- Close DIV order-overlay-info -->
			<div class="odetails-header">Further Order Information</div>
			<!-- Close DIV odetails-header -->
			<div class="message-order-overlay-info">
				<ul id="order-info-9999">
					<li class="bar-left">
						<a href="javascript:expand('9999');" class="box-button" id="button-9999">+</a>
						<strong>Messages</strong>
					</li>
					<li class="bar-center">
					</li>
					<li class="bar-right">
					</li>
					<div class="clear"></div> 
                    <!-- Close DIV clear --> 
					<li id="details-9999" class="details">
						<table id="headersTable">
						<tr>
							<td class="messageColumnSmall">
								<strong>Time:</strong>
							</td>
							<td class="messageColumnSmall">
								<strong>Type:</strong>
							</td>
							<td class="messageColumn">
								<strong>Sender:</strong>
							</td>
							<td class="messageColumn">
								<strong>Recipient:</strong>
							</td>
							<td class="messageColumnLarge">
								<strong>Details:</strong>
							</td>
						</tr>
						</table>
					<div id="message-order-container">
						<table class="messageTable">
						<c:forEach var="orderItem" items="${allMessages }">
							<c:forEach var="h" items="${orderItem.value}" varStatus="status" > 
								<c:choose>
								    <c:when test="${ not empty h.actMessage }">
										<tr class="messageRow">
											<td class="messageColumnSmall"><fmt:formatDate value="${h.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${h.createdDate}" pattern="MM/dd/yyyy" /></td>
											<td class="messageColumnSmall"><c:out value='${ h.actMessage.messagetype.description }' /></td>
											<td class="messageColumn"><c:out value='${ h.actMessage.shopBySendingShopId.shopName }' /></td>
											<td class="messageColumn"><c:out value='${ h.actMessage.shopByReceivingShopId.shopName }' /></td>
											<td class="messageColumnLarge"><c:out value='${ fn:replace(fn:replace(h.actMessage.messageText, "%0A", ""), "%0D", "") }' /></td>
										</tr> 
									</c:when>
									</c:choose>
							</c:forEach>
						</c:forEach>
					</table>
				</div>
				</li>
			</ul>
		</div>
			<!-- Close DIV messaging -->
		</form>
	</div>
	<!-- Close DIV overlay -->
  </body>
</html>