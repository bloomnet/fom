function expand(x){
	var bar = "details-" + x;
	var buttonID= "button-" + x;
	var orderInfo = "order-info-" + x;
	if (document.getElementById(bar).style.display=='block'){
		document.getElementById(bar).style.display='none';
		document.getElementById(orderInfo).style.background='#fff';
		document.getElementById(buttonID).innerHTML='+';
	}else{
		document.getElementById(bar).style.display='block';
		document.getElementById(orderInfo).style.background='#d5edcb';
		document.getElementById(buttonID).innerHTML='-';
	}
}