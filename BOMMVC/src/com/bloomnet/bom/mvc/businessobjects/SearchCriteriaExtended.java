package com.bloomnet.bom.mvc.businessobjects;

import java.io.Serializable;

/**
 * Form backing bean to hold search details for an extended
 * order search based on the objects field values.
 * 
 * @author Danil Svirchtchev
 */
public class SearchCriteriaExtended extends SearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static final String KEY_ORDER_NUMBER = "orderNumber";
	public static final String KEY_ORDER_OCCASION = "occassion";
	public static final String KEY_ORDER_SENDING_SHOP = "sendingShop";
	public static final String KEY_ORDER_FULFILLING_SHOP = "fulfillingShop";
	public static final String KEY_ORDER_ZIPCODE = "zipCode";
	public static final String KEY_ORDER_DATE = "date";
	public static final String KEY_ORDER_TO_DATE = "date2";
	public static final String KEY_ORDER_STATUS = "status";


	
	
	private String orderNumber;
	private String occassion;
	private String sendingShop;
	private String fulfillingShop;
	private String zipCode;
	private String date;
	private String date2;
	private String status;



	
	public SearchCriteriaExtended() { }

	
	public SearchCriteriaExtended( String query ) {
		super(query);
	}


	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOccassion() {
		return occassion;
	}
	public void setOccassion(String occassion) {
		this.occassion = occassion;
	}

	public String getSendingShop() {
		return sendingShop;
	}
	public void setSendingShop(String sendingShop) {
		this.sendingShop = sendingShop;
	}

	public String getFulfillingShop() {
		return fulfillingShop;
	}
	public void setFulfillingShop(String fulfillingShop) {
		this.fulfillingShop = fulfillingShop;
	}

	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}


	public String getDate2() {
		return date2;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatus() {
		return status;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchCriteriaExtended [");
		if (orderNumber != null)
			builder.append("orderNumber=").append(orderNumber).append(", ");
		if (occassion != null)
			builder.append("occassion=").append(occassion).append(", ");
		if (sendingShop != null)
			builder.append("sendingShop=").append(sendingShop).append(", ");
		if (fulfillingShop != null)
			builder.append("fulfillingShop=").append(fulfillingShop).append(", ");
		if (zipCode != null)
			builder.append("zipCode=").append(zipCode).append(", ");
		if (date != null)
			builder.append("date=").append(date).append(", ");
		if (date2 != null)
			builder.append("date2=").append(date2);
		builder.append("]");
		return builder.toString();
	}
}
