package com.bloomnet.bom.mvc.businessobjects;

import java.util.List;

import com.bloomnet.bom.common.entity.ActMessage;

/**
 * A wrapper object for entity ActMessage
 * 
 * @author Danil Svirchtchev
 *
 */
public class WebAppActMessage extends ActMessage implements CommandClassInterface {

	private static final long serialVersionUID = 1L;
	
	private String actionStr;
	private String messageTypeId;
	private ActMessage originalMessage;
	private String dateOrderDelivered;
	private String dateOrderDeliveredParsed;
	private String dateOut;
	private String newPrice;
	private String signature;
	private boolean sent;
	
	// List used for UI messages/error text
	private List<String> messages;

	public WebAppActMessage() {
		super();
	}
	
	@Override
	public String getActionStr() {
		return actionStr;
	}
	@Override
	public void setActionStr(String actionStr) {
		this.actionStr = actionStr;
	}

	public void setMessageTypeId(String messageTypeId) {
		this.messageTypeId = messageTypeId;
	}
	public ActMessage getOriginalMessage() {
		return originalMessage;
	}

	public void setOriginalMessage(ActMessage originalMessage) {
		this.originalMessage = originalMessage;
	}

	public String getMessageTypeId() {
		return messageTypeId;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}
	public boolean isSent() {
		return sent;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	public List<String> getMessages() {
		return messages;
	}

    public void setDateOrderDelivered(String dateOrderDelivered) {
        this.dateOrderDelivered = dateOrderDelivered;
    }
    public String getDateOrderDelivered() {
        return dateOrderDelivered;
    }
    
    public void setDateOrderDeliveredParsed(String dateOrderDeliveredParsed) {
        this.dateOrderDeliveredParsed = dateOrderDeliveredParsed;
    }
    public String getDateOrderDeliveredParsed() {
        return dateOrderDeliveredParsed;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
    public String getSignature() {
        return signature;
    }

	public void setDateOut(String dateOut) {
		
		String[] dateArray = dateOut.split("/");
		
		try{
			if(dateOut.length() == 8) this.dateOut = "20"+dateArray[2]+dateArray[0]+dateArray[1]+"000000";
			else this.dateOut = "";
		}catch(Exception ee){
			this.dateOut = "";
		}
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setNewPrice(String newPrice) {
		this.newPrice = newPrice;
	}

	public String getNewPrice() {
		return newPrice;
	}
}
