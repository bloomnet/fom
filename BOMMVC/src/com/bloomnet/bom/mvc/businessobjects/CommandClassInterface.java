package com.bloomnet.bom.mvc.businessobjects;

import java.io.Serializable;

/**
 * A form command class interface to be implemented by 
 * all of the command classes in the application.
 *
 * @author Danil Svirchtchev
 */
public interface CommandClassInterface extends Serializable {
    String getActionStr();
    void setActionStr(String actionStr);
}
