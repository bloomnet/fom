package com.bloomnet.bom.mvc.businessobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActPayment;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.AuditInterface;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.LogacallInterface;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.PaymentInterface;
import com.bloomnet.bom.common.entity.Paymenttype;
import com.bloomnet.bom.common.entity.RoutingInterface;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.util.OccassionUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.utils.HibernateHelper;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

public class WebAppOrder extends Bomorder implements CommandClassInterface,
													 AuditInterface,
													 PaymentInterface,
													 LogacallInterface,
													 RoutingInterface {
	

	private static final long serialVersionUID = 1L;
	
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( WebAppOrder.class );
    
    
	static final Comparator<OrderNote> ORDER_NOTES_BY_DATE = new Comparator<OrderNote> () {
        
		@Override
		public int compare(OrderNote o1, OrderNote o2) {
			
			 return o1.getCreateDate().compareTo ( o2.getCreateDate() );
		}
    };
	
    
    // Adopted objects
    private Bomorder    entity;
    private ActRouting  actRouting;
    private ActPayment  actPayment;
    private ActLogacall actLogacall;
    
	private String actionStr;
	private MessageOnOrderBean bean;
	private ActMessage selectedMessage;
	private List<Shop> recommendedFlorists;
	private List<Commmethod> comMethods;
	private Shop contactFlorist;
	private Commmethod methodToSend;   
	private String paymentTypeStr;
	private String paymentAmountStr;
	private boolean complete;
	private boolean edited;
	private boolean callsLogged;
	private String bloomnetOrderNumber;
	private String outboundOrderNumber;
	private List<Paymenttype> paymentTypes;
	private Map<String,Shopnetwork> shopCode;
	private Shop sendingShop;
	private Shop addedShop;
	private String emailConfirmation;
	private String faxConfirmation;
	private String selectedShopnetwork;
	private Orderactivity orderactivity;
	private List<String> messages; // List used for UI messages/error text
	private List<Calldisp> dispositions;
	private List<ActMessage> orderMessages;
	private String orderStatus;
	private String orderQueue;
	private List<OrderNote> notes;
	private Long lockerId;
	private String lockerName;
	private String queues;
	private Shop currentFulfillingShop;
	private byte currentOrderStatus;


	
	public WebAppOrder() {
		
		super();
		
		// set the adapted object types
		this.entity      = new Bomorder();
		this.actRouting  = new ActRouting();
		this.actPayment  = new ActPayment();
		this.actLogacall = new ActLogacall();
		
		// set the empty collections
		this.recommendedFlorists = new ArrayList<Shop>();
		this.comMethods = new ArrayList<Commmethod>();
		this.paymentTypes = new ArrayList<Paymenttype>();
		this.shopCode = new HashMap<String,Shopnetwork>();
		this.messages = new ArrayList<String>();
		this.dispositions = new ArrayList<Calldisp>();
		this.orderMessages = new ArrayList<ActMessage>();
		this.notes = new ArrayList<OrderNote>();
	}
	
	
	public WebAppOrder( Bomorder order ) {
		this();
		setEntity( order );
	}
	
	
	public WebAppOrder( Bomorder order, PaymentInterface pi ) {
		this( order );
		this.actPayment.setPaymentAmount( pi.getPaymentAmount() );
		this.actPayment.setPaymenttype( pi.getPaymenttype() );
		this.actPayment.setPayTo( pi.getPayTo() );
		this.actPayment.setStsPayment( pi.getStsPayment() );
		this.actPayment.setOrderactivity( pi.getOrderactivity() );
	}


	public Bomorder getEntity() {
		return this.entity;
	}
	public void setEntity( Bomorder entity ) {

		if( entity != null ) {
			
			this.entity = entity;
			
			this.setOrderType( entity.getOrderType() );
			this.setBomorderId( entity.getBomorderId() );
			this.setParentorderId( entity.getParentorderId() );
			this.setZip( entity.getZip() );
			this.setShopBySendingShopId( entity.getShopBySendingShopId() );
			this.setSendingShop( entity.getShopBySendingShopId() );
			this.setShopByReceivingShopId( entity.getShopByReceivingShopId() );
			this.setCommmethod( entity.getCommmethod() );
			this.setCity( entity.getCity() );
			this.setOrderNumber( entity.getOrderNumber() );
			this.setBmtOrderSequenceNumber( entity.getBmtOrderSequenceNumber() );
			this.setBmtMessageSequenceNumber( entity.getBmtMessageSequenceNumber() );
			this.setDeliveryDate( entity.getDeliveryDate() );
			this.setPrice( entity.getPrice() );
			this.setCreatedDate( entity.getCreatedDate() );
			this.setBomorders( entity.getBomorders() );
			this.setOrderactivities( entity.getOrderactivities() );
			this.setUser( entity.getUser() );
			this.setOrderXml( entity.getOrderXml() );
		}
		else {
			
			this.entity = new Bomorder();
		}
	}
	
	
	@Override
	public String getActionStr() {
		return this.actionStr;
	}
	@Override
	public void setActionStr(String actionStr) {
		this.actionStr = actionStr;
	}
	
	
	/**
	 * @return An occasion code for this order if it's set in the "bean".
	 */
	public String getOccassionCode() {
		
		final String code;
		
		if ( bean != null ) {
			
			code = OccassionUtil.getOccassionDescription( bean.getOccasionId() );
		}
		else {
			
			code = null;
		}
		
		return code;
	}
	
	
	@Override
	public Orderactivity getOrderactivity() {
		return orderactivity;
	}
	@Override
	public void setOrderactivity( Orderactivity orderactivity ) {
		this.orderactivity = orderactivity;
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	//
	// Start adapting ActLogacall object
	//
	@Override
	public String getLogAcallText() {
		return this.actLogacall.getLogAcallText();
	}
	@Override
	public void setLogAcallText(String logAcallText) {
		this.actLogacall.setLogAcallText(logAcallText);
	}

	@Override
	public Shop getShopCalled() {
		return this.actLogacall.getShopCalled();
	}
	@Override
	public void setShopCalled(Shop shopCalled) {
		this.actLogacall.setShopCalled(shopCalled);
	}

	@Override
	public Calldisp getCalldisp() {
		return this.actLogacall.getCalldisp();
	}
	@Override
	public void setCalldisp(Calldisp calldisp) {
		this.actLogacall.setCalldisp(calldisp);
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Start adapting ActPayment object
	//
	@Override
	public StsPayment getStsPayment() {
		return actPayment.getStsPayment();
	}
	@Override
	public void setStsPayment(StsPayment stsPayment) {
		actPayment.setStsPayment(stsPayment);
	}

	@Override
	public Paymenttype getPaymenttype() {
		return actPayment.getPaymenttype();
	}
	@Override
	public void setPaymenttype(Paymenttype paymenttype) {
		actPayment.setPaymenttype(paymenttype);
	}
	
	@Override
	public double getPaymentAmount() {
		return actPayment.getPaymentAmount();
	}

	@Override
	public String getPayTo() {
		return actPayment.getPayTo();
	}
	@Override
	public void setPayTo(String payTo) {
		actPayment.setPayTo(payTo);
	}
	
	public String getPaymentTypeStr() {
		return paymentTypeStr;
	}
	public void setPaymentTypeStr(String paymentTypeStr) {
		this.paymentTypeStr = paymentTypeStr;
	}

	public String getPaymentAmountStr() {
		return paymentAmountStr;
	}
	public void setPaymentAmountStr(String paymentAmountStr) {
		this.paymentAmountStr = paymentAmountStr;
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Start adapting ActAudit object
	//
	@Override
	public long getCityId() {
		return entity.getCity().getCityId().longValue();
	}
	@Override
	public long getZipId() {
		return entity.getZip().getZipId().longValue();
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Start adapting ActRouting object
	//
	@Override
	public String getVirtualQueue() {
		return actRouting.getVirtualQueue();
	}
	@Override
	public void setVirtualQueue(String virtualQueue) {
		actRouting.setVirtualQueue( virtualQueue );
	}

	@Override
	public StsRouting getStsRouting() {
		return actRouting.getStsRouting();
	}
	@Override
	public void setStsRouting(StsRouting stsRouting) {
		actRouting.setStsRouting( stsRouting );
	}
		
	@Override
	public String getStatus() {
		return orderStatus;
	}
	
	@Override
	public void setStatus(String status){
		this.orderStatus = status;
	}

	///////////////////////////////////////////////////////////////////////////
	//
	// Start adapting Bomorder object
	//
	@Override
	public void setZip(Zip zip) {
		super.setZip(zip);
		entity.setZip(zip);
	}

	@Override
	public void setShopBySendingShopId(Shop shopBySendingShopId) {
		super.setShopBySendingShopId(shopBySendingShopId);
		entity.setShopBySendingShopId(shopBySendingShopId);
	}

	@Override
	public void setShopByReceivingShopId(Shop shopByReceivingShopId) {
		super.setShopByReceivingShopId(shopByReceivingShopId);
		entity.setShopByReceivingShopId(shopByReceivingShopId);
	}

	@Override
	public void setCommmethod(Commmethod commmethod) {
		super.setCommmethod(commmethod);
		entity.setCommmethod(commmethod);
	}

	@Override
	public void setCity(City city) {
		super.setCity(city);
		entity.setCity(city);
	}

	@Override
	public void setOrderNumber(String orderNumber) {
		super.setOrderNumber(orderNumber);
		entity.setOrderNumber(orderNumber);
	}

	@Override
	public void setBmtOrderSequenceNumber(String bmtOrderSequenceNumber) {
		super.setBmtOrderSequenceNumber(bmtOrderSequenceNumber);
		entity.setBmtOrderSequenceNumber(bmtOrderSequenceNumber);
	}

	@Override
	public void setBmtMessageSequenceNumber(String bmtMessageSequenceNumber) {
		super.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);
		entity.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);
	}

	@Override
	public void setOrderType(byte orderType) {
		super.setOrderType(orderType);
		entity.setOrderType(orderType);
	}

	@Override
	public void setDeliveryDate(Date deliveryDate) {
		super.setDeliveryDate(deliveryDate);
		entity.setDeliveryDate(deliveryDate);
	}

	@Override
	public void setPrice( double price ) {
		super.setPrice(price);
		entity.setPrice(price);
		actPayment.setPaymentAmount(price);
		if( bean != null ) {
			bean.setTotalCost( Double.valueOf( price ).toString() );
		}
	}

	@Override
	public void setCreatedDate(Date createdDate) {
		super.setCreatedDate(createdDate);
		entity.setCreatedDate(createdDate);
	}

	@Override
	public void setOrderactivities(Set<Orderactivity> orderactivities) {
		
		super.setOrderactivities(orderactivities);
		entity.setOrderactivities(orderactivities);
		
		try {
			
			actRouting = getLastRoutingActivity();
			orderStatus = actRouting.getStatus();
			setOrderQueue(getQueue());
			notes = getOrderNotes();
		}
		catch ( Exception e ) {
			if(!entity.getOrderNumber().startsWith("BMT")){
				e.printStackTrace();
				logger.error( e );
			}
		}
	}

	private String getQueue() throws Exception {
		
		ActRouting result = null;
		
		String queue = "";
		
		List<Orderactivity> m_routings = getRoutingActivities();
		int xx = m_routings.size();
		
		if ( ! m_routings.isEmpty() ) {
			
			for(int ii=0; ii<m_routings.size(); ++ii){
				xx--;
				result = m_routings.get(xx).getActRouting();
				queue = result.getVirtualQueue();
				if(!result.getStatus().equals(BOMConstants.ACTIVELY_BEING_WORKED)){
					break;
				}
			}
		}
		return queue;
	}


	@Override
	public void setBomorders(List<Bomorder> bomorders) {
		super.setBomorders(bomorders);
		entity.setBomorders(bomorders);
	}

	@Override
	public void setParentorderId(Long parentorderId) {
		super.setParentorderId(parentorderId);
		entity.setParentorderId(parentorderId);
	}

	@Override
	public void setUser( User user ) {
		
		super.setUser(user);
		entity.setUser(user);
		
		if ( user != null ) {
			
			final WebAppUser wau = new WebAppUser( user );
			this.lockerId = wau.getUserId();
			this.lockerName = wau.getFullName();
		}
	}
	
	/**
	 * @return the user id of the user who is having lock on this order
	 */
	public Long getLockerId() {
		return lockerId;
	}

	/**
	 * @return the full name (first last) of the user who is having lock on this order
	 */
	public String getLockerName() {
		return lockerName;
	}

	/**
	 * A shallow copy of the {@link MessageOnOrderBean} is serialized into a JSON string.
	 * 
	 * @return A JSON representation of this object
	 */
	@Override
	public String toString() {
		
		JSONSerializer serializer = new JSONSerializer();		
		return serializer.exclude( "*.class",
								   "*.userByCreatedUserId", 
				                   "*.userByModifiedUserId",
				                   "*.createdDate",
				                   "*.modifiedDate" )
				                   .include( "products" ).serialize( bean );
	}
	//
	// finish adapting of the Bomorder
	//
	///////////////////////////////////////////////////////////////////////////

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.entity.Bomorder#setOrderXml(java.lang.String)
	 */
	@Override
	public void setOrderXml( String orderXml ) {
		
		TransformService transform = new Transform();

		
		// set the field value in a parent object
		super.setOrderXml( orderXml );
		// set the field value in a wrapped object
		entity.setOrderXml( orderXml );
		
		if ( bean == null && orderXml != null ) {
		
			try {
				
				if ( BOMConstants.ORDER_TYPE_BMT == getOrderType() ){
					
					ForeignSystemInterface fsi = transform.convertToJavaFSI( orderXml );
					bean = transform.createBeanFromFSI( fsi, 
							                            getShopByReceivingShopId(), 
														getShopBySendingShopId(), 
														getShopByReceivingShopId() );
				}
				else if (( BOMConstants.ORDER_TYPE_TEL == getOrderType() )||( BOMConstants.ORDER_TYPE_FTD == getOrderType() )
						||( BOMConstants.ORDER_TYPE_OTHER == getOrderType() )){
					try{	
						bean = transform.createBeanFromTFSI( orderXml, 
								getShopBySendingShopId(), 
								getShopByReceivingShopId() );
					}
					catch ( Exception e ) {

						logger.debug( "Could not generate bean  using TFSI xml " + e );

						ForeignSystemInterface fsi = transform.convertToJavaFSI( orderXml );
						bean = transform.createBeanFromFSI( fsi, 
								getShopByReceivingShopId(), 
								getShopBySendingShopId(), 
								getShopByReceivingShopId() );
					}
				}
			} 
			catch ( Exception e ) {
				e.printStackTrace();
				logger.error( "Could not generate bean from ["+orderXml+"]: " + e );
				// Return new object if something wrong happened
				bean = new MessageOnOrderBean();
			}
		}
	}
		

	public MessageOnOrderBean getBean() {
		return bean;
	}
	public void setBean( MessageOnOrderBean bean ) {
		this.bean = bean;
	}

	public List<Shop> getRecommendedFlorists() {
		return recommendedFlorists;
	}
	public void setRecommendedFlorists(List<Shop> recommendedFlorists) {
		this.recommendedFlorists = recommendedFlorists;
	}

	public List<Commmethod> getComMethods() {
		return comMethods;
	}
	public void setComMethods(List<Commmethod> comMethods) {
		this.comMethods = comMethods;
	}

	public Shop getContactFlorist() {
		return contactFlorist;
	}
	public void setContactFlorist(Shop contactFlorist) {
		this.contactFlorist = contactFlorist;
	}

	public Commmethod getMethodToSend() {
		return methodToSend;
	}
	public void setMethodToSend( Commmethod methodToSend ) {
		
		this.methodToSend = methodToSend;
		this.setCommmethod( methodToSend );
		
		if( bean != null ) {
			bean.setCommMethod( methodToSend.getCommMethodId() );
		}
	}

	public boolean isComplete() {
		return complete;
	}
	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public void setEdited(boolean edited) {
		this.edited = edited;
	}
	public boolean isEdited() {
		return edited;
	}

	public final boolean isCallsLogged() {
		return callsLogged;
	}
	public final void setCallsLogged(boolean callsLogged) {
		this.callsLogged = callsLogged;
	}

	public String getBloomnetOrderNumber() {
		return bloomnetOrderNumber;
	}
	public void setBloomnetOrderNumber(String bloomnetOrderNumber) {
		this.bloomnetOrderNumber = bloomnetOrderNumber;
	}

	public String getOutboundOrderNumber() {
		return outboundOrderNumber;
	}
	public void setOutboundOrderNumber(String outboundOrderNumber) {
		this.outboundOrderNumber = outboundOrderNumber;
	}

	public List<Paymenttype> getPaymentTypes() {
		return paymentTypes;
	}
	public void setPaymentTypes(List<Paymenttype> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}

	public Map<String, Shopnetwork> getShopCode() {
		return shopCode;
	}
	public void setShopCode(Map<String, Shopnetwork> shopCode) {
		this.shopCode = shopCode;
	}

	public Shop getSendingShop() {
		return sendingShop;
	}
	public void setSendingShop(Shop sendingShop) {
		this.sendingShop = sendingShop;
	}

	public void setAddedShop(Shop addedShop) {
        this.addedShop = addedShop;
    }
    public Shop getAddedShop() {
        return addedShop;
    }

    public void setEmailConfirmation(String emailConfirmation) {
		this.emailConfirmation = emailConfirmation;
	}
	public String getEmailConfirmation() {
		return emailConfirmation;
	}

	public String getFaxConfirmation() {
		return faxConfirmation;
	}
	public void setFaxConfirmation(String faxConfirmation) {
		this.faxConfirmation = faxConfirmation;
	}

	/**
	 * Set a list of UI help messages
	 * 
	 * @param messages
	 */
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	/**
	 * @return A list of UI help messages
	 */
	public List<String> getMessages() {
		return messages;
	}

	public void setDispositions(List<Calldisp> dispositions) {
		this.dispositions = dispositions;
	}
	public List<Calldisp> getDispositions() {
		return dispositions;
	}

	public void setSelectedMessage(ActMessage selectedMessage) {
		this.selectedMessage = selectedMessage;
	}
	public ActMessage getSelectedMessage() {
		return selectedMessage;
	}

	public final List<ActMessage> getOrderMessages() {
		return orderMessages;
	}
	public final void setOrderMessages(List<ActMessage> orderMessages) {
		this.orderMessages = orderMessages;
	}

	public String getSelectedShopnetwork() {
		return selectedShopnetwork;
	}
	public void setSelectedShopnetwork(String selectedShopnetwork) {
		this.selectedShopnetwork = selectedShopnetwork;
	}

	/**
	 * @return the collection of notes associated with this order
	 */
	public List<OrderNote> getNotes() {
		Collections.sort ( notes, WebAppOrder.ORDER_NOTES_BY_DATE );
		return notes;
	}


	/**
	 * @return
	 * @throws Exception
	 */
	public Orderactivity getMostRecentActivity() throws Exception {
		
		final Orderactivity result;
		
		if( getOrderactivities().isEmpty() ) {
			
			result = null;
		}
		else {
			
			List<Orderactivity> m_activities = new LinkedList<Orderactivity>( getOrderactivities() );
			Collections.sort ( m_activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			
			result = m_activities.get( m_activities.size()-1 );
		}
		
		return result;
	}
	
	
	/**
	 * @return A collection of order "routing" activities sorted by date.
	 */
	public List<Orderactivity> getRoutingActivities() throws Exception {
		
		List<Orderactivity> result = new LinkedList<Orderactivity>();
		
		if ( ! entity.getOrderactivities().isEmpty() ) {
			
			Iterator<Orderactivity> it = getOrderactivities().iterator();
			
			while ( it.hasNext() ) {
				
				final Orderactivity m_activity = it.next();
				
				if ( m_activity.getActRouting() != null ) {
					
					result.add( m_activity );
				}
			}
			
			Collections.sort ( result, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
		}
		
		return result;
	}
	
	
	/**
	 * @return A collection of order notes sorted by date.
	 */
	public List<OrderNote> getOrderNotes() throws Exception {
		
		List<OrderNote> result = new ArrayList<OrderNote>();
		
		if ( ! entity.getOrderactivities().isEmpty() ) {
			
			Iterator<Orderactivity> it = getOrderactivities().iterator();
			
			while ( it.hasNext() ) {
				
				final Orderactivity act = it.next();
				
				if ( act.getActLogacall() != null ) {
					
					final ActLogacall alc = act.getActLogacall();
					
					Calldisp disp     = act.getActLogacall().getCalldisp(); 	
					Calldisp realDisp = HibernateHelper.initializeAndUnproxy( disp );
					
					if ( BOMConstants.LOGACALL_ORDER_NOTE.equals( realDisp.getDescription() ) ) {
						
						final StringBuffer text = new StringBuffer( alc.getLogAcallText() );
						final StringBuffer name = new StringBuffer( act.getUser().getUserName() );
						final Date createDate = act.getCreatedDate();
						
						OrderNote note = new OrderNote( text.toString(), name.toString(), createDate );
						
						result.add( note );
					}
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * @return A last element from the order activities list which represents order routing
	 * 
	 * @throws Exception
	 */
	public ActRouting getLastRoutingActivity() throws Exception {
		
		ActRouting result = null;
		
		List<Orderactivity> m_routings = getRoutingActivities();
		
		if ( ! m_routings.isEmpty() ) {
			
			final int last = ( m_routings.size() - 1 );
			
			result = m_routings.get( last ).getActRouting();
		}
		
		return result;
	}
	
	
	/**
	 * @return A description of the order occasion
	 */
	public String getOccassionDescription() {
		
		String result = null;
		
		if( this.bean != null ) {
			
			result = OccassionUtil.getOccassionDescription( bean.getOccasionId() );
		}
		
		return result;
	}
	
	
	/**
	 * Add a child order to this order
	 * 
	 * @param childOrder
	 * @throws Exception
	 */
	public void addChildOrder( Bomorder childOrder ) throws Exception {
		
		List<Bomorder> childOrders;
		
		if( getBomorders() == null ) {
			
			childOrders = new LinkedList<Bomorder>();
		}
		else {
			
			childOrders = getBomorders();
		}
		
		childOrder.setParentorderId( entity.getBomorderId() );
		childOrders.add( childOrder );
		setBomorders( childOrders );
	}
	
	
	/**
	 * Returns a last order object from the "Bomorders" collection.
	 * 
	 * TODO: complete implementation as needed for carrying over nested objects
	 * 
	 * Such as:
	 * 
	 * ActPayment : Paymenttype 
	 * 
	 * @return
	 * @throws Exception
	 */
	public WebAppOrder getLastChild() throws Exception {
		
		final Bomorder lastOrder;
		
		if ( getBomorders() != null && ! getBomorders().isEmpty() ){
			
			int index = getBomorders().size()-1;
			lastOrder = getBomorders().get(index);
		}
		else {
			
			lastOrder = null;
		}
		
		WebAppOrder wao = new WebAppOrder( lastOrder, this );
		
		return wao;
	}
	
	
	/**
	 * Serializes order editable fields into a JSON 
	 * 
	 * @return JSON object
	 * @throws Exception
	 */
	public String getJsonForInputFields() throws Exception {
		
		JSONSerializer serializer = new JSONSerializer();		
		return serializer.include( "deliveryDate" )         
						.include( "occasionId" )    
						.include( "cardMessage" )         
						.include( "specialInstruction" )   
						.include( "recipientFirstName" )   
						.include( "recipientLastName" )    
						.include( "recipientPhoneNumber" )   
						.include( "recipientAddress1" )    
						.include( "recipientAddress2" )    
						.include( "recipientCity" )        
						.include( "recipientState" )       
						.include( "recipientZipCode" )     
						.include( "products" )    
						.exclude( "bmtOrderNumber" )            
						.exclude( "bmtSeqNumberOfMessage" )        
						.exclude( "bmtSeqNumberOfOrder" )          
						.exclude( "captureDate" )                  
						.exclude( "*class" )                        
						.exclude( "commMethod" )                  
						.exclude( "dateOrderDelivered" )           
						.exclude( "externalShopMessageNumber" )   
						.exclude( "externalShopOrderNumber" )      
						.exclude( "fulfillingShop" )              
						.exclude( "loadedDate" )                  
						.exclude( "logacall" )                     
						.exclude( "messageCount" )                 
						.exclude( "messageCreateTimestamp" )       
						.exclude( "messageText" )                  
						.exclude( "messageType" )                  
						.exclude( "newCity" )                      
						.exclude( "newDeliveryDate" )              
						.exclude( "newZip" )                       
						.exclude( "orderMessageType" )             
						.exclude( "orderNumber" )                  
						.exclude( "orderType" )                    
						.exclude( "originalReceivingShopCode" )    
						.exclude( "originalSendingShopCode" )      
						.exclude( "price" )                        
						.exclude( "receivingShop" )                
						.exclude( "recipientCountryCode" )         
						.exclude( "recipientPhoneNumberFormated" ) 
						.exclude( "recipientStateFormated" )       
						.exclude( "sendingShop" )                  
						.exclude( "shipperCode" )                  
						.exclude( "shippingDate" )                 
						.exclude( "shippingMethod" )               
						.exclude( "skillset" )                     
						.exclude( "status" )                       
						.exclude( "systemType" )                   
						.exclude( "totalCost" )                    
						.exclude( "trackingNumber" )               
						.exclude( "wireServiceCode" )
						.serialize( bean );
	}
	
	
	public void setCurrentFulfillingShop(Shop currentFulfillingShop) {
		this.currentFulfillingShop = currentFulfillingShop;
	}


	public Shop getCurrentFulfillingShop() {
		return currentFulfillingShop;
	}


	public void setCurrentOrderStatus(byte currentOrderStatus) {
		this.currentOrderStatus = currentOrderStatus;
	}


	public byte getCurrentOrderStatus() {
		return currentOrderStatus;
	}

	public void setQueues(String queues) {
		this.queues = queues;
	}


	public String getQueues() {
		return queues;
	}

	/**
	 * An inner class to represent a note on an order
	 * 
	 * NOTE: this can be an "Enum" type
	 * 
	 * @author Danil Svirchtchev
	 *
	 */
	public class OrderNote implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		private String text;
		private String creatorName;
		private Date   createDate;
		
		public OrderNote( String text, String creatorName, Date createDate ) {

			this.text = text;
			this.creatorName = creatorName;
			this.createDate = createDate;
		}
		
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}

		public String getCreatorName() {
			return creatorName;
		}
		public void setCreatorName(String creatorName) {
			this.creatorName = creatorName;
		}

		public Date getCreateDate() {
			return createDate;
		}
		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			JSONSerializer serializer = new JSONSerializer();		
			return serializer.exclude( "*.class" )
					         .transform(new DateTransformer("MM/dd/yyyy hh:mm a"), "createDate")
					         .serialize( this );
		}
	}

	public String getOrderStatus() {
		return orderStatus;
	}


	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}


	public void setOrderQueue(String orderQueue) {
		this.orderQueue = orderQueue;
	}


	public String getOrderQueue() {
		return orderQueue;
	}
}
