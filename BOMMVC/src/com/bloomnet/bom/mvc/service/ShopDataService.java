package com.bloomnet.bom.mvc.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.exceptions.AddressException;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;

public interface ShopDataService {

	Shop getShopByCode( String shopCode ) throws Exception;
	
	/**
	 * This method returns a list of shops by zip and communication method description
	 * 
	 * @param zip
	 * @param comMethodDescription
	 * @return
	 * @throws Exception
	 */
	List<Shop> getShops( String zip, String comMethodDescription ) throws Exception;
	
	/**
	 * This method returns a list of shops by zip and communication method description.
	 * 
	 * Also if a call disposition record exists for shop based on the orderNumber parameter,
	 * this shop will be excluded from the list.
	 * 
	 * @param orderNumber
	 * @param zipCode
	 * @param comMethodDescription
	 * @return
	 * @throws Exception
	 */
	Collection<? extends Shop> getShops( String orderNumber, String zipCode, String comMethodDescription ) throws Exception;
	
	Map<? extends String, ? extends Shopnetwork> getShopnetwork( String key, Long shopId ) throws Exception;
	
	List<State> getStates() throws Exception;
	
	Shop persistFlorist ( WebAppUser user, WebAppShop shop ) throws Exception;
	
	/**
	 * Will return a map with validated city name and zip
	 * 
	 * Mapping of key/value in the result map is:
	 * 
	 * key: city,  value: {@link City}  object
	 * key: zip,   value: {@link Zip}   object
	 * key: state, value: {@link State} object TODO: The state is not included in the result
	 * 
	 * 
	 * @param cityName
	 * @param zipCode
	 * @return
	 * @throws Exception
	 * @throws AddressException
	 */
	Map<String, Object> validateCityAndZip(String cityName, String zipCode, String state) throws Exception, AddressException;

	void mergeFlorist(WebAppUser user, WebAppShop shop) throws Exception;
	
	List <Shopnetworkcoverage>getShopCoverage(String shopCode) throws Exception;
	
}
