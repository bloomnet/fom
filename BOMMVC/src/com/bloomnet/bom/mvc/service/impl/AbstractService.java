package com.bloomnet.bom.mvc.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.jaxb.fsi.DeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductsInfo;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.jaxb.fsi.ShippingDetails;
import com.bloomnet.bom.common.jaxb.fsi.WireServiceCode;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.Transform;

/**
 * An abstract service for handeling service shared functionality.
 * 
 * @author Danil Svirchtchev
 *
 */
public abstract class AbstractService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( AbstractService.class );
    

	protected abstract Properties getFsiProperties() throws Exception;

	protected abstract ShopDAO getShopDAO() throws Exception;


	/**
	 * Creates foreignSystemInterface with MessageOrder object from MessageOnOrderBean
	 * @param orderBean
	 * @return
	 * @throws Exception
	 */
	@Transactional( propagation = Propagation.SUPPORTS )
	protected ForeignSystemInterface createFSIOrderFromBean(MessageOnOrderBean orderBean, boolean isOutbound) throws Exception{

		MessagesOnOrder aMessageOnOrder = new MessagesOnOrder();
		Security security = new Security();

		Recipient recipient = new Recipient();
		ShippingDetails shippingDetails = new ShippingDetails();

		MessageOrder messageOrder = new MessageOrder();

		messageOrder.setWireServiceCode(WireServiceCode.valueOf(BOMConstants.WIRE_SERVICE_CODE));
		messageOrder.setMessageType(BOMConstants.ORDER_MESSAGE_TYPE);

		DeliveryDetails deliveryDetails = new DeliveryDetails();
		deliveryDetails.setDeliveryDate(orderBean.getDeliveryDate());
		deliveryDetails.setFlexDeliveryDate(orderBean.getFlexDeliveryDate());
		deliveryDetails.setSpecialInstruction(orderBean.getSpecialInstruction());

		messageOrder.setDeliveryDetails(deliveryDetails);
		String fulfillingShopCode = "";
		if(orderBean.getFulfillingShop() == null )
			fulfillingShopCode = getFsiProperties().getProperty(BOMConstants.FSI_SHOPCODE);
		else 
			fulfillingShopCode = getShopDAO().getShopCodeFromDefaultShopnetworks( orderBean.getFulfillingShop().getShopId() );

		final String receivingShopCode;
		final Long   receivingShopId = orderBean.getReceivingShop().getShopId();

		if ( receivingShopId == null ) {

			receivingShopCode = orderBean.getOriginalReceivingShopCode();
		}
		else {
			receivingShopCode = getShopDAO().getShopCodeFromDefaultShopnetworks( receivingShopId );
		}

		String sendingShopCode = getShopDAO().getShopCodeFromDefaultShopnetworks( orderBean.getSendingShop().getShopId() );

		messageOrder.setFulfillingShopCode(fulfillingShopCode);
		messageOrder.setReceivingShopCode(receivingShopCode);
		messageOrder.setSendingShopCode(sendingShopCode);

		//messageOrder.setMessageStatus()
		messageOrder.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		messageOrder.setOrderCaptureDate(DateUtil.toXmlFormatString(orderBean.getCaptureDate()));
		recipient.setRecipientAddress1(orderBean.getRecipientAddress1());
		recipient.setRecipientAddress2(orderBean.getRecipientAddress2());
		//recipient.setRecipientAttention(orderBean.getre);
		recipient.setRecipientCity(orderBean.getRecipientCity());
		recipient.setRecipientCountryCode(orderBean.getRecipientCountryCode());
		recipient.setRecipientFirstName(orderBean.getRecipientFirstName());
		recipient.setRecipientLastName(orderBean.getRecipientLastName());
		recipient.setRecipientPhoneNumber(orderBean.getRecipientPhoneNumber());
		recipient.setRecipientState(orderBean.getRecipientState());
		recipient.setRecipientZipCode(orderBean.getRecipientZipCode());

		messageOrder.setRecipient(recipient);

		shippingDetails.setShipperCode(orderBean.getShipperCode());
		shippingDetails.setShippingDate(DateUtil.toString(orderBean.getShippingDate()));
		shippingDetails.setShippingMethod(orderBean.getShippingMethod());
		shippingDetails.setTrackingNumber(orderBean.getTrackingNumber());

		messageOrder.setShippingDetails(shippingDetails);

		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();

		//XXX set externalShopOrderNumber
		
		generalIdentifiers.setExternalShopOrderNumber(BOMConstants.EXTERNAL_SHOP_NUMBER);
		if (!isOutbound){
		generalIdentifiers.setBmtOrderNumber(orderBean.getOrderNumber());
		generalIdentifiers.setBmtSeqNumberOfOrder(Long.toString(orderBean.getBmtSeqNumberOfOrder()));
		}
		 /* generalIdentifiers.setBmtSeqNumberOfMessage(value)
		generalIdentifiers.setExternalShopMessageNumber(value)
		generalIdentifiers.setExternalShopOrderNumber(value)
		generalIdentifiers.setInwireSequenceNo(value)
		*/

		identifiers.setGeneralIdentifiers(generalIdentifiers);

		messageOrder.setIdentifiers(identifiers );
		messageOrder.setSystemType(BOMConstants.SYSTEM_TYPE);

		OrderProductsInfo orderproductsInfo = new OrderProductsInfo();
		for (OrderProductInfoDetails product : orderBean.getProducts()) {

			OrderProductInfoDetails aOrderProductInfoDetails = new OrderProductInfoDetails();

			aOrderProductInfoDetails.setCostOfSingleProduct(product.getCostOfSingleProduct());
			aOrderProductInfoDetails.setProductDescription(product.getProductDescription());
			aOrderProductInfoDetails.setUnits(product.getUnits());
			aOrderProductInfoDetails.setProductCode(product.getProductCode());

			orderproductsInfo.getOrderProductInfoDetails().add(aOrderProductInfoDetails);

		}

		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setOrderProductsInfo(orderproductsInfo);
		orderDetails.setOccasionCode(orderBean.getOccasionId());
		orderDetails.setOrderCardMessage(orderBean.getCardMessage());
		//orderDetails.setOrderNumber(orderBean.getOrderNumber());
		orderDetails.setTotalCostOfMerchandise(orderBean.getTotalCost());

		messageOrder.setOrderDetails(orderDetails);

		aMessageOnOrder.setMessageCount(1);
		aMessageOnOrder.setMessageOrder(messageOrder);

		security.setPassword(getFsiProperties().getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(getFsiProperties().getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(getFsiProperties().getProperty(BOMConstants.FSI_USERNAME));

		Transform.PROPERTIES = getFsiProperties();

		ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();

		aMessageOnOrder.setMessageCount(1);


		foreignSystemInterface.getMessagesOnOrder().add(aMessageOnOrder);

		foreignSystemInterface.setSecurity(security);

		return foreignSystemInterface;
	}
}
