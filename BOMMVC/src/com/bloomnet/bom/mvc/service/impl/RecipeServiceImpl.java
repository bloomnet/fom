package com.bloomnet.bom.mvc.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.jaxb.recipe.GetAllRecipeInfo;
import com.bloomnet.bom.common.jaxb.recipe.RecipeByOrderNumber;
import com.bloomnet.bom.common.jaxb.recipe.RecipeResponse;
import com.bloomnet.bom.common.jaxb.recipe.Security;
import com.bloomnet.bom.common.jaxb.recipe.RecipeByProductCode;
import com.bloomnet.bom.common.jaxb.recipe.RecipeInfo;
import com.bloomnet.bom.common.jaxb.recipe.RecipeInfoInterface;
import com.bloomnet.bom.common.jaxb.recipe.RecipeRequest;
import com.bloomnet.bom.common.jaxb.recipe.RecipeSearchOptions;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.mvc.service.RecipeService;
import com.bloomnet.bom.webservice.client.FSIClient;
@Service("recipeService")
public class RecipeServiceImpl implements RecipeService{
	

    // Define a static logger variable
    static Logger logger = Logger.getLogger( RecipeServiceImpl.class );
	
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected property
	@Autowired private Properties fsiProperties;
	
	// Injected dependency
	@Autowired private FSIClient fsiClient;
	
	@Autowired private MessagingService messagingService;

	
	@Autowired private TransformService transformService;	

	
	public List<RecipeInfo>  getRecipeByProductCode(String productCode) throws Exception{
		
		List<RecipeInfo> recipeInfo = new ArrayList<RecipeInfo>();
		RecipeInfoInterface rii = new RecipeInfoInterface();
		
		RecipeRequest recipeRequest = new RecipeRequest();
		RecipeSearchOptions recipeSearchOptions = new RecipeSearchOptions();
		RecipeByProductCode recipeByProductCode = new RecipeByProductCode();
		Security security = new Security();


		recipeByProductCode.setProductCode(productCode);
		recipeSearchOptions.getRecipeSearchOptionsGroup().add(recipeByProductCode);
		recipeRequest.setRecipeSearchOptions(recipeSearchOptions);
		

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));
		
		recipeRequest.setSecurity(security);
		
		rii.setRecipeRequest(recipeRequest);
		
		String recipeXml = transformService.convertToRecipeXML(rii);
		
		RecipeResponse recipeResponse = sendMessageDirect(recipeXml);
		recipeInfo = recipeResponse.getRecipeInfo();

		return recipeInfo;
	}
	
	public List<RecipeInfo>  getRecipeByOrderNumber(String orderNumber) throws Exception{
		
		List<RecipeInfo> recipeInfo = new ArrayList<RecipeInfo>();
		RecipeInfoInterface rii = new RecipeInfoInterface();
		
		RecipeRequest recipeRequest = new RecipeRequest();
		RecipeSearchOptions recipeSearchOptions = new RecipeSearchOptions();
		RecipeByOrderNumber recipeByOrderNumber = new RecipeByOrderNumber();
		Security security = new Security();


		recipeByOrderNumber.setBloomOrderNumber(orderNumber);
		recipeSearchOptions.getRecipeSearchOptionsGroup().add(recipeByOrderNumber);
		recipeRequest.setRecipeSearchOptions(recipeSearchOptions);
		

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));
		
		recipeRequest.setSecurity(security);
		
		rii.setRecipeRequest(recipeRequest);
		
		String recipeXml = transformService.convertToRecipeXML(rii);
		
		RecipeResponse recipeResponse = sendMessageDirect(recipeXml);
		recipeInfo = recipeResponse.getRecipeInfo();


		return recipeInfo;
	}
	
	public List<RecipeInfo>  getAllRecipeInfo() throws Exception{
		
		List<RecipeInfo> recipeInfo = new ArrayList<RecipeInfo>();
		RecipeInfoInterface rii = new RecipeInfoInterface();
		
		RecipeRequest recipeRequest = new RecipeRequest();
		RecipeSearchOptions recipeSearchOptions = new RecipeSearchOptions();
		GetAllRecipeInfo getAllRecipeInfo = new GetAllRecipeInfo();
		Security security = new Security();


		
		recipeSearchOptions.getRecipeSearchOptionsGroup().add(getAllRecipeInfo);
		recipeRequest.setRecipeSearchOptions(recipeSearchOptions);
		

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));
		
		recipeRequest.setSecurity(security);
		
		rii.setRecipeRequest(recipeRequest);
		
		String recipeXml = transformService.convertToRecipeXML(rii);
		
		RecipeResponse recipeResponse = sendMessageDirect(recipeXml);
		recipeInfo = recipeResponse.getRecipeInfo();


		return recipeInfo;

	}
	
private RecipeResponse sendMessageDirect(String sendingOrderXML ) throws Exception {

		
		String dataEncoded = "";
		String post = "";
		String response = "";
		int numOfErrors = 0;

		RecipeResponse recipeResponse = new RecipeResponse();


		try {

			dataEncoded = URLEncoder.encode(sendingOrderXML, "UTF-8");
			post = fsiProperties.getProperty("fsi.endpoint")
					+ BOMConstants.RECIPE_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("xml encoded: " + dataEncoded);
				logger.debug("sendMessage xml " + sendingOrderXML);
				logger.debug("audit " + post);

			}
			response = messagingService.sendRequest(post);

			if (response==null){
				throw new Exception("No response from BloomLink");
			}

			RecipeInfoInterface recipeInfoInterface = transformService.convertToJavaRecipe(response);
			
			recipeResponse = recipeInfoInterface.getRecipeResponse();
			
			if (recipeResponse.getErrors()!=null){
		    	numOfErrors = recipeResponse.getErrors().getError().size();
			}

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				 List<com.bloomnet.bom.common.jaxb.recipe.Error> errors = recipeResponse.getErrors().getError();

				for (com.bloomnet.bom.common.jaxb.recipe.Error error : errors) {
					
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());
					throw new Exception("BloomLink Exception: " +error.getErrorMessage());


				}
			}

		}
		catch (UnsupportedEncodingException e) {

			logger.error(e);
			
		}

		return recipeResponse;
	}

}
