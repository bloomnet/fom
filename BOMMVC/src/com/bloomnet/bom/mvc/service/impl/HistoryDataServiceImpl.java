/**
 * 
 */
package com.bloomnet.bom.mvc.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.mvc.service.HistoryDataService;
import com.bloomnet.bom.mvc.utils.HibernateHelper;

/**
 * @author Danil Svirchtchev
 *
 */
@Service("historyDataService")
@Transactional(propagation = Propagation.SUPPORTS)
public class HistoryDataServiceImpl implements HistoryDataService {
	
    // Define a static logger variable
    static private Logger logger = Logger.getLogger( HistoryDataServiceImpl.class );
     
	// Injected DAO implementations
	@Autowired private OrderDAO orderDAO;
	
	// Private constructor
	private HistoryDataServiceImpl() {}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.HistoryDataService#getOrderHistory(java.lang.String)
	 */
	@Override
	@Transactional(readOnly=true)
	public List<Orderactivity> getOrderHistory(String orderNumber) throws Exception {

		List<Orderactivity> m_orderactivities = orderDAO.getOrderHistory(orderNumber);
		
		return m_orderactivities;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.HistoryDataService#getOrderAndActivities(java.lang.String)
	 */
	@Override
	@Transactional(readOnly=true)
	public Map<Bomorder, List<Orderactivity>> getOrderAndActivities( String orderNumber ) throws Exception {

		Map<Bomorder, List<Orderactivity>> result = new TreeMap<Bomorder, List<Orderactivity>>();
		
		logger.debug( "\n---> For order ["+orderNumber+"] found:" );
		
		for ( Bomorder entity : orderDAO.getOrdersByOrderNumber( orderNumber ) ) {
			
			final Bomorder m_order = HibernateHelper.initializeAndUnproxy( entity );
			
			final ArrayList<Orderactivity> m_activities = new ArrayList<Orderactivity>( m_order.getOrderactivities() );
			
			Collections.sort( m_activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			
			result.put( m_order,m_activities );
			
			logger.debug( "\n---> Order ID ("+m_order.getBomorderId()+"), Activities ("+m_order.getOrderactivities().size()+")");
		}

		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.HistoryDataService#getMasterOrderAndActivities(java.lang.String)
	 */
	@Override
	@Transactional(readOnly=true)
	public Map<Bomorder, List<Orderactivity>> getMasterOrderAndActivities( String orderNumber ) throws Exception {

		Map<Bomorder, List<Orderactivity>> result = new LinkedHashMap<Bomorder, List<Orderactivity>>();
		
		Bomorder entity = orderDAO.getOrderByOrderNumber( orderNumber );
		
		if( ! entity.isParent() ) {
			
			entity = orderDAO.getOrderByOrderId( entity.getParentorderId() );
		}
		
		final Bomorder m_order = HibernateHelper.initializeAndUnproxy( entity );
		
		List<Bomorder> childOrders = m_order.getBomorders();
		
		Collections.sort(childOrders, OrderDAO.ORDER_BY_ORDER_CREATED_DATE);
		Collections.reverse(childOrders);
		
		for ( Bomorder m_innerOrder : childOrders ) {
			
			final ArrayList<Orderactivity> m_activities = new ArrayList<Orderactivity>( m_innerOrder.getOrderactivities() );
			Collections.sort( m_activities,  OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse(m_activities);
			result.put( m_innerOrder, m_activities );
		}

		return result;
	}
	
	@Override
	@Transactional(readOnly=true)
	public Map<Bomorder, List<Orderactivity>> getMasterOrderAndMessages( String orderNumber ) throws Exception {

		Map<Bomorder, List<Orderactivity>> result = new LinkedHashMap<Bomorder, List<Orderactivity>>();
		
		Bomorder entity = orderDAO.getOrderByOrderNumber( orderNumber );
		
		if( ! entity.isParent() ) {
			
			entity = orderDAO.getOrderByOrderId( entity.getParentorderId() );
		}
		
		final Bomorder m_order = HibernateHelper.initializeAndUnproxy( entity );
		
		List<Bomorder> childOrders = m_order.getBomorders();
		
		List<Orderactivity> allMessages = new ArrayList<Orderactivity>();
		
		for ( Bomorder m_innerOrder : childOrders ) {
			
			final ArrayList<Orderactivity> m_activities = new ArrayList<Orderactivity>( m_innerOrder.getOrderactivities() );
			for(Orderactivity activity : m_activities){
				if(activity.getOactivitytype().getOactivityTypeId() == 3){
					allMessages.add(activity);
				}
			}
		}
		
		Collections.sort( allMessages,  OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
		Collections.reverse(allMessages);
		
		result.put(entity, allMessages);

		return result;
	}
}
