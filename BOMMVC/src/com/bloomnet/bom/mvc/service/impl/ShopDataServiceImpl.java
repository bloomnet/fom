package com.bloomnet.bom.mvc.service.impl;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.CallDispDAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.CityZipXref;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.exceptions.AddressException;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectorySearchOptions;
import com.bloomnet.bom.common.jaxb.mdi.SearchByShopCode;
import com.bloomnet.bom.common.jaxb.mdi.SearchShopRequest;
import com.bloomnet.bom.common.jaxb.mdi.Security;
import com.bloomnet.bom.common.jaxb.mdi.ServicedZips;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.WorkingShopNotFoundException;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.mvc.service.ShopDataService;
import com.bloomnet.bom.mvc.utils.HibernateHelper;
import com.bloomnet.bom.webservice.client.FSIClient;

/**
 * @author Danil Svirchtchev
 *
 */
@Service("shopDataService")
@Transactional( propagation = Propagation.SUPPORTS )
public class ShopDataServiceImpl implements ShopDataService {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( AgentServiceImpl.class );
    
	// Injected DAO implementations
	@Autowired private ShopDAO     shopDAO;
	@Autowired private MessageDAO  messageDAO;
	@Autowired private UserDAO     userDao;
	@Autowired private CallDispDAO callDispDAO;
	@Autowired private OrderDAO orderDAO;
	
	// Injected property
		@Autowired private Properties fsiProperties;
		
			
		// Injected dependency
		@Autowired private FSIClient fsiClient;
		
		@Autowired private TransformService transformService;	

	
	// Private constructor
	private ShopDataServiceImpl() {}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#getShopByCode(java.lang.String)
	 */
	@Override
	@Transactional( readOnly = true )
	public Shop getShopByCode( String shopCode ) throws WorkingShopNotFoundException,
														Exception {
		
		Shop result = null;
		
		try {
			
			result = shopDAO.getShopByCode(shopCode);
			
		} 
		catch ( HibernateException e ) {

			logger.error(e);
			throw new Exception( e );
		}
		finally {
			
			if ( result == null ) {
				
				throw new WorkingShopNotFoundException(shopCode);
			}
		}
		
		return result;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#getShops(java.lang.String, java.lang.String)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	@Transactional( readOnly = true )
	public List<Shop> getShops(String zip, String comMethodDescription) throws Exception {

		List<Shop> result = new ArrayList<Shop>();
		
		try {
			
    		Set s = shopDAO.getShopsWithStatus( zip, comMethodDescription ).entrySet();
			Iterator it = s.iterator();
			
			while ( it.hasNext() ) {
				
				Map.Entry m =(Map.Entry)it.next();
				Shop entity = (Shop) m.getKey();
				result.add( HibernateHelper.initializeAndUnproxy( entity ) );
			}		
		} 
		catch ( HibernateException e ) {
			
			logger.error(e);
			throw new Exception( e );
		}
		
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#getShops(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional( readOnly = true )
	public Collection<? extends Shop> getShops( String orderNumber, String zipCode, String comMethodDescription ) throws Exception {

		List<Shop> workingList = getShops(zipCode, comMethodDescription);
		List<Shop> result = new ArrayList<Shop>();
		
		Iterator<Shop> it = workingList.iterator();
		while ( it.hasNext() ) {
			
			final Shop m_shop = (Shop) it.next();
			
			//if ( !hasCalls( orderNumber, m_shop ) ) {
				result.add(m_shop);
			//}
		}
		
		return result;
	}

	
	@Transactional( readOnly = true )
	private boolean hasCalls( String orderNumber, Shop shop ) throws Exception {

		boolean result = false;
		
		for ( Iterator<ActLogacall> it = callDispDAO.getCallsByShop( shop ).iterator(); it.hasNext(); ) {
			
			final ActLogacall call = it.next();
			
			final String activityOrderNumber = call.getOrderactivity().getBomorder().getOrderNumber();
			
			if ( orderNumber.equals( activityOrderNumber ) ) {
				
				result = true;
			}
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#getShopnetwork(java.lang.String, java.lang.Long)
	 */
	@Override
	@Transactional( readOnly = true )
	public Map<String, Shopnetwork> getShopnetwork( final String key, final Long shopId ) throws Exception {
		
		Map<String, Shopnetwork> result = new HashMap<String, Shopnetwork>();
		
		Shopnetwork shopnetwork = shopDAO.getBloomNetShopnetwork( shopId );
		
		if( shopnetwork == null ) {
			
			shopnetwork = shopDAO.getTelefloraShopnetwork( shopId );
		}
		
		if( shopnetwork == null ) {
			
			shopnetwork = shopDAO.getFTDShopnetwork( shopId );
		}

		if( shopnetwork == null ) {
			
			shopnetwork = shopDAO.getOtherShopnetwork( shopId );
		}

		if ( shopnetwork == null ) {
			
			throw new Exception( "Could not find Shopnetwork for Shop ["+shopId+"]" );
		}
		else {
			
			result.put( key, shopnetwork );
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#getStates()
	 */
	@Override
	@Transactional( readOnly = true )
	public List<State> getStates() throws Exception {

		List<State> result = null;
		
		try {
			
			result = shopDAO.getStates();
		}
		catch ( HibernateException e ) {
			
			logger.error(e);
			
			throw new Exception("Could not get list of states");
		}
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#persistFlorist(com.bloomnet.bom.mvc.businessobjects.WebAppShop)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void mergeFlorist( WebAppUser user, WebAppShop shop ) throws Exception {
		
		Shop mergeShop = shopDAO.getShopByShopId(shop.getShopId());
		City city = orderDAO.getCityByNameAndState(shop.getCity().getName(), shop.getSelectedState());
		Zip zip = orderDAO.getZipByCode(shop.getZip().getZipCode());
		List<Shopnetwork> networks = shopDAO.getShopNetworks(shop.getShopId());
		
		if(shop.getBmtShopCode() != null && !shop.getBmtShopCode().equals("")){
			Shopnetwork network = shopDAO.getBloomNetShopnetwork(shop.getShopId());
			if(network != null){
				network.setShopCode(shop.getBmtShopCode());
				if(shop.getOpenSunday() == 1)
					network.setOpenSunday(true);
				else
					network.setOpenSunday(false);
				if(shop.getHasWireService() == 1)
					network.setCommmethod(messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API ));
				else
					network.setCommmethod(messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE ));
				if(shop.getBmtActive() == 1){
					Shopnetworkstatus sns = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
					network.setShopnetworkstatus(sns);
				}else{
					Shopnetworkstatus sns = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_INACTIVE );
					network.setShopnetworkstatus(sns);
				}
				shopDAO.mergeShopNetwork(network);
			}else{
				Shopnetwork shopNetwork = new Shopnetwork();
				
				Network bmtNetwork = shopDAO.getNetwork( "BloomNet" );
				
				Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
				
				Commmethod commmethod = null;
				
				if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
				else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
				
				shopNetwork = new Shopnetwork(mergeShop, 
						bmtNetwork, 
                        shopnetworkstatus, 
                        user, 
                        commmethod, 
                        new Date() );
				
				if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
				else shopNetwork.setOpenSunday( false );
				
				shopNetwork.setShopCode( shop.getBmtShopCode() );
				
				shopDAO.persitShopnetwork( shopNetwork );
				
				mergeShop = UploadShopNetworkCoverage(networks, shopNetwork, user, mergeShop, zip.getZipCode(), shop.getCoveredZip());
			}
		}
		if(shop.getTfShopCode() != null && !shop.getTfShopCode().equals("")){
			Shopnetwork network = shopDAO.getTelefloraShopnetwork(shop.getShopId());
			if(network != null){
				network.setShopCode(shop.getTfShopCode());
				if(shop.getOpenSunday() == 1)
					network.setOpenSunday(true);
				else
					network.setOpenSunday(false);
				if(shop.getHasWireService() == 1)
					network.setCommmethod(messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API ));
				else
					network.setCommmethod(messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE ));
				if(shop.getTfActive() == 1){
					Shopnetworkstatus sns = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
					network.setShopnetworkstatus(sns);
				}else{
					Shopnetworkstatus sns = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_INACTIVE );
					network.setShopnetworkstatus(sns);
				}
				shopDAO.mergeShopNetwork(network);
			}else{
				Shopnetwork shopNetwork = new Shopnetwork();
				
				Network tfNetwork = shopDAO.getNetwork( "Teleflora" );
				
				Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
				
				Commmethod commmethod = null;
				
				if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
				else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
				
				shopNetwork = new Shopnetwork(mergeShop, 
						tfNetwork, 
                        shopnetworkstatus, 
                        user, 
                        commmethod, 
                        new Date() );
				
				if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
				else shopNetwork.setOpenSunday( false );
				
				shopNetwork.setShopCode( shop.getTfShopCode() );
				
				shopDAO.persitShopnetwork( shopNetwork );
				
				mergeShop = UploadShopNetworkCoverage(networks, shopNetwork, user, mergeShop, zip.getZipCode(), shop.getCoveredZip());
			}
		}
		
		networks = shopDAO.getShopNetworks(shop.getShopId());
		Set<Shopnetwork> currentNetworks = new HashSet<Shopnetwork>();
		currentNetworks.addAll(networks);
		
		shop.setShopnetworks(currentNetworks);
		mergeShop.setShopnetworks(shop.getShopnetworks());
		mergeShop.setZip( zip );
		mergeShop.setCity( city );
		mergeShop.setShopName( shop.getShopName() );
		mergeShop.setShopAddress1( shop.getShopAddress1() );
		mergeShop.setShopAddress2( shop.getShopAddress2() );
		mergeShop.setShopPhone( shop.getShopPhone() );
		mergeShop.setShopContact( shop.getShopContact() );
		mergeShop.setShopFax( shop.getShopFax() );
		mergeShop.setShop800( shop.getShop800() );
		mergeShop.setShopEmail( shop.getShopEmail() );
		shopDAO.mergeShop(mergeShop);
		
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#persistFlorist(com.bloomnet.bom.mvc.businessobjects.WebAppShop)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public Shop persistFlorist( WebAppUser user, WebAppShop shop ) throws Exception {

		validateAddress( shop );
		
		Shop m_shop = shop.getShop();
		
		String m_zipCoveredString = shop.getCoveredZip();
		
		String m_zipString = m_shop.getZip().getZipCode();
		
		try {
			
			User m_user = userDao.getUserById( user.getUserId() );

			m_shop.setUserByCreatedUserId( m_user );
			m_shop.setCreatedDate( new Date() );
			
			Shop existingShop = shopDAO.getShopByAddressAndPhone(m_shop.getShopAddress1(), m_shop.getShopPhone());
			if (existingShop == null){
				System.out.println("shop does not exist");
				
			}else{
				System.out.println("shop already exist: "+ m_shop.getShopAddress1() +"/ " + m_shop.getShopPhone());

			}
			if(existingShop == null) existingShop = shopDAO.getShopByCode(shop.getBmtShopCode());
			if(existingShop == null) existingShop = shopDAO.getShopByCode(shop.getTfShopCode());
			
			if(existingShop == null) shopDAO.persistShop( m_shop );
			else m_shop = existingShop;
			
			List<Shopnetwork> shopNetworks = shopDAO.getShopNetworks(m_shop.getShopId());
			
			if(shopNetworks.isEmpty()){
				
				System.out.println("shop network is empty");
				
				if(!shop.getBmtShopCode().equals("")){
					
					Shopnetwork shopNetwork = new Shopnetwork();
					
					Network network = shopDAO.getNetwork( "BloomNet" );
					
					Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
					
					Commmethod commmethod = null;
					
					if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
					else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
					
					shopNetwork = new Shopnetwork(m_shop, 
	                        network, 
	                        shopnetworkstatus, 
	                        m_user, 
	                        commmethod, 
	                        new Date() );
					
					if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
					else shopNetwork.setOpenSunday( false );
					
					shopNetwork.setShopCode( shop.getBmtShopCode() );
					
					shopDAO.persitShopnetwork( shopNetwork );
					
					m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				}
				
				if(!shop.getTfShopCode().equals("")){
					
					Shopnetwork shopNetwork = new Shopnetwork();
					
					Network network = shopDAO.getNetwork( "Teleflora" );
					
					Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
					
					Commmethod commmethod = null;
					
					if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
					else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
					
					shopNetwork = new Shopnetwork(m_shop, 
	                        network, 
	                        shopnetworkstatus, 
	                        m_user, 
	                        commmethod, 
	                        new Date() );
					
					if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
					else shopNetwork.setOpenSunday( false );
					
					shopNetwork.setShopCode( shop.getTfShopCode() );
					
					shopDAO.persitShopnetwork( shopNetwork );
					
					m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				}
				
				Shopnetwork shopNetwork = new Shopnetwork();
				
				Network network = shopDAO.getNetwork( "Other" );
				
				Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
				
				Commmethod commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
				
				shopNetwork = new Shopnetwork(m_shop, 
			                                   network, 
			                                   shopnetworkstatus, 
			                                   m_user, 
			                                   commmethod, 
			                                   new Date() );
			
				if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
				else shopNetwork.setOpenSunday( false );
				
				shopNetwork.setShopCode( "OTH"+m_shop.getShopId() );
				
				shopDAO.persitShopnetwork( shopNetwork );
				
				m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				
			}else{
				
				System.out.println("Existing shop network");
				
				boolean existingBMT = false;
				boolean existingTF = false;
				
				Shopnetwork bmt = null;
				Shopnetwork tf = null;
				
				for(Shopnetwork network : shopNetworks){
					if(network.getNetwork().getName().equals("BloomNet")){
						existingBMT = true;
						bmt = network;
					}
					if(network.getNetwork().getName().equals("Teleflora")){
						existingTF = true;
						tf = network;
					}
				}
				
				if(!shop.getBmtShopCode().equals("")){
					
					Shopnetwork shopNetwork = new Shopnetwork();
					
					if(!existingBMT){
						
						Network network = shopDAO.getNetwork( "BloomNet" );
						
						Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
						
						Commmethod commmethod = null;
						
						if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
						else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
						
						shopNetwork = new Shopnetwork(m_shop, 
		                        network, 
		                        shopnetworkstatus, 
		                        m_user, 
		                        commmethod, 
		                        new Date() );
						
						if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
						else shopNetwork.setOpenSunday( false );
						
						shopNetwork.setShopCode( shop.getBmtShopCode() );
						
						shopDAO.persitShopnetwork( shopNetwork );
						
					}else{
						
						shopNetwork = bmt;
						
					}
					
					m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				}
				
				if(!shop.getTfShopCode().equals("")){
					
					Shopnetwork shopNetwork = new Shopnetwork();
					
					if(!existingTF){
						
						Network network = shopDAO.getNetwork( "Teleflora" );
						
						Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
						
						Commmethod commmethod = null;
						
						if(shop.getHasWireService() == 1) commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_API );
						else commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
						
						shopNetwork = new Shopnetwork(m_shop, 
		                        network, 
		                        shopnetworkstatus, 
		                        m_user, 
		                        commmethod, 
		                        new Date() );
						
						if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
						else shopNetwork.setOpenSunday( false );
						
						shopNetwork.setShopCode( shop.getTfShopCode() );
						
						shopDAO.persitShopnetwork( shopNetwork );
						
					}else{
						
						shopNetwork = tf;
						
					}
					
					m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				}
				
				Shopnetwork shopNetwork = new Shopnetwork();
				
				Network network = shopDAO.getNetwork( "Other" );
				
				Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus( BOMConstants.SHOPNETWORKSTATUS_ACTIVE );
				
				Commmethod commmethod = messageDAO.getCommethodByDesc( BOMConstants.COMMMETHOD_PHONE );
				
				shopNetwork = new Shopnetwork(m_shop, 
			                                   network, 
			                                   shopnetworkstatus, 
			                                   m_user, 
			                                   commmethod, 
			                                   new Date() );
			
				if(shop.getOpenSunday() == 1) shopNetwork.setOpenSunday( true );
				else shopNetwork.setOpenSunday( false );
				
				shopNetwork.setShopCode( "OTH"+m_shop.getShopId() );
				
				shopDAO.persitShopnetwork( shopNetwork );
				
				m_shop = UploadShopNetworkCoverage(shopNetworks, shopNetwork, m_user, m_shop, m_zipString, m_zipCoveredString);
				
			}
			
		} catch (Exception e) {
		    
            logger.error(e);
            e.printStackTrace();
		    throw new Exception( "Shop "+shop.getShopName()+" was not saved: "+e.getMessage() );
		}
		
		return m_shop;
	}
	
	@Transactional( propagation = Propagation.REQUIRED )
	private Shop UploadShopNetworkCoverage(List<Shopnetwork> shopNetworks, Shopnetwork shopNetwork, User m_user, Shop m_shop, String m_zipString, String m_zipCoveredString){
		
		Set<Shopnetwork> myShopNetwork = new HashSet<Shopnetwork>();
		
		myShopNetwork.add(shopNetwork);
		
		for(int ii=0; ii<shopNetworks.size(); ++ii){
		
			myShopNetwork.add(shopNetworks.get(ii));
		}
		
		m_shop.setShopnetworks(myShopNetwork);
		
		Shopnetworkcoverage netCoverage = new Shopnetworkcoverage( m_user, shopNetwork, m_shop.getZip() );
		
		netCoverage.setCreatedDate( new Date() );
		
		shopDAO.persitShopnetworkcoverage(netCoverage);
		
	
		if(!(m_zipString == m_zipCoveredString)){
			Zip m_zipCovered = orderDAO.getZipByCode(m_zipCoveredString);

			Shopnetworkcoverage netCoverage2 = new Shopnetworkcoverage( m_user, shopNetwork, m_zipCovered );
			
			netCoverage2.setCreatedDate( new Date() );
			
			shopDAO.persitShopnetworkcoverage(netCoverage2);
		}
		
		return m_shop;

	}
	
	@Transactional( readOnly = true )
	private void validateAddress( WebAppShop shop ) throws AddressException {
		
		City m_city = new City();

		
		final State m_state = shopDAO.getState( Long.valueOf( shop.getSelectedState() ) );
		//final City  m_city  = shopDAO.getCity ( shop.getCity().getName() );
		
		try {
			m_city = orderDAO.getCityByNameAndState( shop.getCity().getName(),m_state.getShortName() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Zip   m_zip   = shopDAO.getZip  ( shop.getZip().getZipCode().replaceAll(" ", "") );
		
		boolean isInZip = false;
		
		for ( CityZipXref m_cz : m_city.getCityZipXrefs() ) {
			String cityZipCode = m_cz.getZip().getZipCode();
			String zipcode = m_zip.getZipCode();
			
			if ( cityZipCode.equalsIgnoreCase( zipcode ) ) {
				
				isInZip = true;
				break;
				
			}
		}
		String city_state = m_city.getState().getShortName();
		String selectedState = m_state.getShortName();
		if (! city_state.equals(selectedState)  ) {
			
			throw new AddressException( m_state, m_city );
		}
		
		if ( ! isInZip && m_zip.getZipCode().length() != 3 ) {
			
			throw new AddressException( m_zip, m_city );
		}
		else {
			
			shop.setCity(m_city);
			shop.getCity().setState(m_state);
			shop.setZip(m_zip);
		}
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ShopDataService#validateCityAndZip(java.lang.String, java.lang.String)
	 */
	@Transactional( readOnly = true )
	public Map<String, Object> validateCityAndZip( String cityName, String zipCode, String state ) throws AddressException {

		Map<String, Object> result = new HashMap<String, Object>();
		
		City m_city = new City();
		try {
			m_city = orderDAO.getCityByNameAndState( cityName,state );
		} catch (Exception e) {
			e.printStackTrace();
		}
		final Zip   m_zip   = shopDAO.getZip  ( zipCode.replaceAll(" ", "") );
		
		boolean isInZip = false;
		
		for ( CityZipXref m_cz : m_city.getCityZipXrefs() ) {
			
			if ( m_cz.getZip().getZipCode().equalsIgnoreCase( m_zip.getZipCode() ) ) {
				
				isInZip = true;
			}
		}
		
		if ( ! isInZip && m_zip.getZipCode().length() != 3 ) {
			
			throw new AddressException( m_zip, m_city );
		}
		else {
			
			result.put( "city", m_city );
			result.put( "zip",  m_zip);
		}
		
		return result;
	}


	@Override
	public List<Shopnetworkcoverage> getShopCoverage(String shopCode)
			throws Exception {
		
		List<Shopnetworkcoverage> results = new ArrayList<Shopnetworkcoverage>();

		Shop shop = shopDAO.getShopByCode(shopCode);
		if (shop!=null){
			List<Shopnetwork> shopnetworks = shopDAO.getShopNetworks(shop.getShopId());
			for(Shopnetwork shopnetwork: shopnetworks){
				List<Shopnetworkcoverage> coverages= shopDAO.getShopNetworkCoverage(shopnetwork);
				for(Shopnetworkcoverage coverage: coverages){
					results.add(coverage);
				}
			}

		}
		else{
			results=null;
		}
		return results;
	}
	
	public List<String> getZipCodesCoveredDB(String shopCode) throws Exception{
		
		List<String> results = new ArrayList<String>();
		
		List<Shopnetworkcoverage> coverages = getShopCoverage(shopCode);
		
		for (Shopnetworkcoverage coverage: coverages){
			
			results.add(coverage.getZip().getZipCode());
		}
		
		return results;
		
	}
	
	
	public List<String> getZipCodesCoveredMDI(String shopCode) throws Exception{
		
		
		List<String> results = new ArrayList<String>();
		
		SearchByShopCode searchShopCode = new SearchByShopCode();
		searchShopCode.setShopCode(shopCode);
		searchShopCode.setReturnNonDeliveryDates("true");
		searchShopCode.setReturnFulfilledZipCodes("true");
		searchShopCode.setReturnCommunicationCode("true");

		MemberDirectoryInterface mdi =  new MemberDirectoryInterface();
		SearchShopRequest searchshoprequest = new SearchShopRequest();
		MemberDirectorySearchOptions searchOptions = new MemberDirectorySearchOptions();
		searchshoprequest.setMemberDirectorySearchOptions(searchOptions);
		searchOptions.getMemberDirectorySearchOptionsGroup().add(searchShopCode);
		Security security = new Security();

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));


		searchshoprequest.setSecurity(security);

		mdi.setSearchShopRequest(searchshoprequest);

		String mdiXml = transformService.convertToMDIXML(mdi);

		String xmlEncoded = URLEncoder.encode(mdiXml, "UTF-8");
		String post = fsiProperties.getProperty("fsi.endpoint")+ BOMConstants.GET_MEMBER + xmlEncoded;

		String response = fsiClient.sendRequest(post,"GET_MEMBER");//TODO Need to check if shop is found

		MemberDirectoryInterface mdiResults = transformService.convertToJavaMDI(response);
		
		if (mdiResults.getSearchShopResponse().getErrors().getError().isEmpty()){

		com.bloomnet.bom.common.jaxb.mdi.Shop shop = mdiResults.getSearchShopResponse().getShops().getShop().get(0);
		ServicedZips serviceZips = shop.getServicedZips();
		if (serviceZips!=null){
			results = serviceZips.getZip();
		}

		}

		
		return results;

		
	}
}
