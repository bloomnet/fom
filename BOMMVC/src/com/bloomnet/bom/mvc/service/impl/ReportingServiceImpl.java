package com.bloomnet.bom.mvc.service.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.spring.VelocityEngineUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.OutstandingOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.WorkOverviewDailyDAO;
import com.bloomnet.bom.common.entity.Bomlatestdispositionview;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.BomorderviewV2Id;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.service.ReportingService;

@Service("reportingService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ReportingServiceImpl implements ReportingService {
	
    static Logger logger = Logger.getLogger( ReportingServiceImpl.class );
    
	// Injected DAO implementation
	@Autowired private OrderDAO orderDAO;
	
	@Autowired private BomorderviewV2DAO bomorderviewV2DAO;
	
	@Autowired protected FulfillmentSummaryDAO fulfillmentSummaryDAO;
	
	@Autowired protected WorkOverviewDailyDAO workOverviewDailyDAO;
	
	@Autowired MetricsSpreadSheetServiceImpl metricsSpreadSheetService;

	@Autowired private TransformService transformService;	

	
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected dependency
	@Autowired private JavaMailSender jmailSender;	
	
	@Autowired private VelocityEngine velocityEngine;
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getFSIOrdersForToday() throws Exception {

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersEnteredToday();

		List<BomorderviewV2> bmtfsiList = new ArrayList<BomorderviewV2>();

		//System.out.println("***************routed fsi*********");

		for(BomorderviewV2 order: orders){
			if (order!=null){
				BomorderviewV2Id orderId = order.getId();

				Byte route = orderId.getVirtualqueueId();
				//System.out.println("ordernum: " + order.getId() + "route: " + route);

				if(route==1){
					bmtfsiList.add(order);
				}
			}
		}


		return bmtfsiList;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getTFSIOrdersForToday() throws Exception {

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersEnteredToday();

		List<BomorderviewV2> tfsiList = new ArrayList<BomorderviewV2>();
		//System.out.println("***************routed TFSI*********");


		for(BomorderviewV2 order: orders){
			if (order!=null){

				BomorderviewV2Id orderId = order.getId();
				Byte route = orderId.getVirtualqueueId();
				//System.out.println("ordernum: " + order.getId() + "route: " + route);

				if(route==2){
					tfsiList.add(order);
				}
			}
		}


		return tfsiList;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getTLOOrdersForToday() throws Exception {

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersEnteredToday();

		List<BomorderviewV2> tloList = new ArrayList<BomorderviewV2>();

		//System.out.println("***************routed tlo*********");
		for(BomorderviewV2 order: orders){
			if (order!=null){

				BomorderviewV2Id orderId = order.getId();
				Byte route = orderId.getVirtualqueueId();
				//System.out.println("ordernum: " + order.getId() + "route: " + route);
				if((route==3)||(route==4)){
					tloList.add(order);
				}
			}
		}


		return tloList;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getOutstandingOrdersForToday() throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersForToday();

		return orders;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getOutstandingOrdersPriorToday() throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersPriorToday();
		
		return orders;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getShopOrdersForToday(long shopCode) throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getShopOrdersForToday(shopCode);

		return orders;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Bomorder> getStdOrders() throws Exception {
		

		List<Bomorder> orders = orderDAO.getOrdersEnteredToday(new Date());
		List<Bomorder> std = new ArrayList<Bomorder>();

		
		for(Bomorder order : orders){
			if (order!=null){

			String orderXml = order.getOrderXml();
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(orderXml);
			List<MessagesOnOrder> messagesOnOrder = fsi.getMessagesOnOrder();

			for ( MessagesOnOrder messages : messagesOnOrder ) {
				
				MessageOrder messageOnOrder = messages.getMessageOrder();
				String sendingShopCode = messageOnOrder.getSendingShopCode();
				String externalShopOrderNumber = messageOnOrder.getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber();
				if ( !externalShopOrderNumber.equals("10004"))			
				{
					if ((!sendingShopCode.equals(BOMConstants.FROM_YOU_FLOWERS))&&
							(!sendingShopCode.equals(BOMConstants.BLOOMS_TODAY))&&
							(!sendingShopCode.equals(BOMConstants.JUST_FLOWERS))) 
					{

								std.add(order);
							}
				}
				
			}
			}
		}
		
		
		return std;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Bomorder> getDirOnlineOrders() throws Exception {
		

		List<Bomorder> orders = orderDAO.getOrdersEnteredToday(new Date());
		List<Bomorder> dirOnline = new ArrayList<Bomorder>();

		
		for(Bomorder order : orders){
			if (order!=null){

			String orderXml = order.getOrderXml();
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(orderXml);
			List<MessagesOnOrder> messagesOnOrder = fsi.getMessagesOnOrder();

			for ( MessagesOnOrder messages : messagesOnOrder ) {
				
				MessageOrder messageOnOrder = messages.getMessageOrder();
				String externalShopOrderNumber = messageOnOrder.getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber();
				if (externalShopOrderNumber.equals("10004"))
				{
					dirOnline.add(order);
				}
				
			}
			}
		}
		

		
		return dirOnline;
	}
	
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public Map<String,Integer> getOrdersByOcassion(Map<String,String> orderOcassions) throws Exception {
		
		int funeral = 0;
		int anniversary = 0;
		int birthday = 0;
		int funeralPrev = 0;
		int anniversaryPrev = 0;
		int birthdayPrev = 0;
		int funeralFuture = 0;
		int anniversaryFuture = 0;
		int birthdayFuture = 0;
		int funeralToday = 0;
		int anniversaryToday = 0;
		int birthdayToday = 0;

		
		List<String> orderTypes = orderDAO.getOrderTypes(orderOcassions);
		
		for(int ii=0; ii<orderTypes.size(); ++ii){
			String temp = orderTypes.get(ii);
			String day = temp.split(",")[0];
			String ocassion = temp.split(",")[1];
			if(day.equals("previous")){
				
				if(ocassion.equals(BOMConstants.Funeral)){
					funeral++;
					funeralPrev++;
				}else if(ocassion.equals(BOMConstants.Anniversary)){
					anniversary++;
					anniversaryPrev++;
				}else if(ocassion.equals(BOMConstants.Birthday)){
					birthday++;
					birthdayPrev++;
				}
				
			}else if(day.equals("future")){
				
				if(ocassion.equals(BOMConstants.Funeral)){
					funeral++;
					funeralFuture++;
				}else if(ocassion.equals(BOMConstants.Anniversary)){
					anniversary++;
					anniversaryFuture++;
				}else if(ocassion.equals(BOMConstants.Birthday)){
					birthday++;
					birthdayFuture++;
				}
				
			}else if(day.equals("today")){
				
				if(ocassion.equals(BOMConstants.Funeral)){
					funeral++;
					funeralToday++;
				}else if(ocassion.equals(BOMConstants.Anniversary)){
					anniversary++;
					anniversaryToday++;
				}else if(ocassion.equals(BOMConstants.Birthday)){
					birthday++;
					birthdayToday++;
				}
				
			}
		}
		
		Map<String,Integer> results = new HashMap<String,Integer>();
		
		results.put("funeral", funeral);
		results.put("anniversary", anniversary);
		results.put("birthday", birthday);
		results.put("funeralPrev", funeralPrev);
		results.put("anniversaryPrev", anniversaryPrev);
		results.put("birthdayPrev", birthdayPrev);
		results.put("funeralFuture", funeralFuture);
		results.put("anniversaryFuture", anniversaryFuture);
		results.put("birthdayFuture", birthdayFuture);
		results.put("funeralToday", funeralToday);
		results.put("anniversaryToday", anniversaryToday);
		results.put("birthdayToday", birthdayToday);
		
		
		return results;
	}
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void sendSummaryReport() throws Exception {
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
	         public void prepare(MimeMessage mimeMessage) throws Exception {
	            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	            String fromEmail = bomProperties.getProperty(BOMConstants.EMAIL_FROM);	
				message.setFrom(fromEmail);
				message.setTo(bomProperties.getProperty(BOMConstants.EMAIL_RPT));	
				String date = DateUtil.EasternTime();
				message.setSubject(bomProperties.getProperty(BOMConstants.EMAIL_RPT_SUBJECT)+ "  " + date);
				
				String summaryRpt = getSummaryReportHtml();
				message.setText(summaryRpt,true);
	         }
	      };
	      jmailSender.send(preparator);

	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void sendOutstandingReport() throws Exception {
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
	         public void prepare(MimeMessage mimeMessage) throws Exception {
	            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	            String fromEmail = bomProperties.getProperty(BOMConstants.EMAIL_FROM);	
				message.setFrom(fromEmail);
				message.setTo("jpursoo@bloomnet.net");
				String date = DateUtil.toStringFormatTime(new Date());
				message.setSubject("Bloomnet Order Management Outstanding Report " + date);
				
				String summaryRpt = getOutstandingReportHtml();
				message.setText(summaryRpt,true);
	         }
	      };
	      jmailSender.send(preparator);

	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void sendMessageCountReport() throws Exception {
		 MimeMessagePreparator preparator = new MimeMessagePreparator() {
	         public void prepare(MimeMessage mimeMessage) throws Exception {
	            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
	            String fromEmail = bomProperties.getProperty(BOMConstants.EMAIL_FROM);	
				message.setFrom(fromEmail);
				message.setTo(bomProperties.getProperty(BOMConstants.EMAIL_RPT));	
				String date = DateUtil.EasternTime();
				message.setSubject(bomProperties.getProperty(BOMConstants.EMAIL_RPT_SUBJECT)+ "  " + date);
				
				String msgRpt = getMessageCountReportHtml();
				message.setText(msgRpt,true);
	         }
	      };
	      jmailSender.send(preparator);

	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<BomorderviewV2> getShopOrdersForTodayAccepted(long shop) throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getShopOrdersForToday(shop);
		
		List<BomorderviewV2> acceptList = new ArrayList<BomorderviewV2>();
		
		for(BomorderviewV2 order: orders){
			BomorderviewV2Id orderId = order.getId();
			
		
			//System.out.println("*********getShopOrdersForTodayAccepted************: " + shop);

			Byte status = orderId.getStsRoutingId();

			if( (!Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
					&&(!Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP)))
					&&(!Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF))) ){

				acceptList.add(order);
			}
		}
				
		return acceptList;

	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private List<BomorderviewV2> getShopOrdersForTodayRejected(long shop) throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getShopOrdersForToday(shop);
		
		List<BomorderviewV2> rejectList = new ArrayList<BomorderviewV2>();
		
		//System.out.println("*********getShopOrdersForTodayRejected************: " + shop);

		
		for(BomorderviewV2 order: orders){
			BomorderviewV2Id orderId = order.getId();
			
			Byte status = orderId.getStsRoutingId();
			if(Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
				{
				
				rejectList.add(order);
				
			}
		}
		
		
		
		return rejectList;
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private List<BomorderviewV2> getShopOrdersForTodayCanceled(long shop) throws Exception {
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getShopOrdersForToday(shop);
		
		List<BomorderviewV2> cancList = new ArrayList<BomorderviewV2>();
		
		//System.out.println("*********getShopOrdersForTodayCanceled************: " + shop);

		for(BomorderviewV2 order: orders){
			BomorderviewV2Id orderId = order.getId();
			
			Byte status = orderId.getStsRoutingId();
			if( Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP))
					||Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)) )
				{
				
				cancList.add(order);
			}
		}
		
		
		return cancList;
	}
	
	
	public List<BomorderviewV2> getOutstandingOrders(){
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrders();
		
		for(BomorderviewV2 order: orders){
			BomorderviewV2Id orderId = order.getId();
			
			String orderNumber = orderId.getParentOrderNumber();
			Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
		}
		
		//return orders;
		return null;
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private String getSummaryReportHtml(  ) throws Exception {
		
		//int numOrdersIgnore = Integer.parseInt(bomProperties.getProperty("num.ignore"));
        
        Map velocityMap = new HashMap();
        velocityMap.put( "totalbom", bomorderviewV2DAO.getOrdersEnteredToday().size() );
        velocityMap.put( "fsi", getFSIOrdersForToday().size() );
        velocityMap.put( "tfsi", getTFSIOrdersForToday().size() );
        velocityMap.put( "tlo", getTLOOrdersForToday().size() );
        velocityMap.put( "orderswaiting", bomorderviewV2DAO.getOutstandingOrders().size() );
        velocityMap.put( "orderswaitingtoday", getOutstandingOrdersForToday().size());
        velocityMap.put( "orderswaitingprevious", getOutstandingOrdersPriorToday().size() );
        velocityMap.put( "ordersbeingworked", orderDAO.getTLOOrdersBeingWorked().size() ); 
        velocityMap.put( "xprocessed", getShopOrdersForToday(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))).size() );
        velocityMap.put( "xaccepted", getShopOrdersForTodayAccepted(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))).size() );
        velocityMap.put( "xrejected", getShopOrdersForTodayRejected(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))).size() );
        velocityMap.put( "xcanceled", getShopOrdersForTodayCanceled(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))).size() );
        velocityMap.put( "stddir", getStdOrders().size() );
        velocityMap.put( "dironline", getDirOnlineOrders().size() );
        velocityMap.put( "xnum", getShopOrdersForToday(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))).size() );
        velocityMap.put( "cnum", getShopOrdersForToday(Long.parseLong(bomProperties.getProperty(BOMConstants.BLOOMS_TODAY_ID))).size() );
        velocityMap.put( "pnum", getShopOrdersForToday(Long.parseLong(bomProperties.getProperty(BOMConstants.JUST_FLOWERS_ID))).size() );



        return VelocityEngineUtils.mergeTemplateIntoString( velocityEngine, "bomsummary.html", "UTF-8", velocityMap );
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private String getOutstandingReportHtml(  ) throws Exception {
		
        
        Map velocityMap = new HashMap();
        
        List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrders();
        List<OutstandingOrderBean> beans= new ArrayList<OutstandingOrderBean>();

        
        for (BomorderviewV2 order: orders){
        	String orderNumber = order.getId().getParentOrderNumber();
        	Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
        	Bomlatestordernoteview orderNote = bomorderviewV2DAO.getOrderNoteView(orderNumber);
        	Bomlatestdispositionview disp = bomorderviewV2DAO.getDispositionView(orderNumber);

        	
        	String city = bomorder.getCity().getName();
        	String state = bomorder.getCity().getState().getShortName();
        	String zip = bomorder.getZip().getZipCode();
        	String user = "&nbsp;";
        	User userEntity = bomorder.getUser();
        	if (userEntity!=null){
        		user = userEntity.getUserName();
        	}
			String note = "&nbsp;";
			String timeOfNote = "&nbsp;";
			if (orderNote!=null){
				note = orderNote.getId().getNote();
				timeOfNote = DateUtil.toString(orderNote.getId().getTimeLogged());
			}
			
			String disposition = "&nbsp;";
			String dispositionText = "&nbsp;";
			String timeOfDisposition = "&nbsp;";
			
			if (disp!=null){
				disposition = disp.getId().getDisposition();
				dispositionText = disp.getId().getText();
				timeOfDisposition = DateUtil.toString(disp.getId().getTimeLogged());
			}
			
			
        	
        	OutstandingOrderBean bean = new OutstandingOrderBean(orderNumber, 
        			DateUtil.toString(order.getId().getParentOrderDate()), 
        			DateUtil.toString(order.getId().getDeliveryDate()), 
        			order.getId().getSendingShopCode(), city, state, zip, user, 
        			note, timeOfNote, disposition, dispositionText, timeOfDisposition);
        	
        	beans.add(bean);
        	
        }
        
        
        
        velocityMap.put( "orders", beans);
        

        return VelocityEngineUtils.mergeTemplateIntoString( velocityEngine, "bomoutstanding.html", "UTF-8", velocityMap );
    }
	
	@Deprecated
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private String getMessageCountReportHtml(  ) throws Exception {
		
		//String dateStr = DateUtil.todashFormatString(new Date());
		String dateStr = "05-01-2012";
		Date lastReportDate = DateUtil.toDateDashFormat(dateStr);
		Map<String, Integer> messages_man = orderDAO.getManualMessageCounts(lastReportDate );
		Map<String, Integer> messages_auto = orderDAO.getAutomatedMessageCounts(lastReportDate);

		System.out.println(DateUtil.previousDate(lastReportDate));
        
        Map velocityMap = new HashMap();
        velocityMap.put( "date", dateStr );
        velocityMap.put( "date", dateStr );
        velocityMap.put( "orders_man", bomorderviewV2DAO.getManualOrdersEnteredByDate(DateUtil.previousDate(lastReportDate)).size() );
        velocityMap.put( "orders_auto", bomorderviewV2DAO.getAutomatedOrdersEnteredByDate(DateUtil.previousDate(lastReportDate)).size() );
        velocityMap.put( "inqr_man", messages_man.get("inqr") );
        velocityMap.put( "resp_man", messages_man.get("resp") );
        velocityMap.put( "info_man", messages_man.get("info") );
        velocityMap.put( "rjct_man", messages_man.get("rjct") );
        velocityMap.put( "dlcf_man", messages_man.get("dlcf") );
        velocityMap.put( "pchg_man", messages_man.get("pchg"));
        velocityMap.put( "canc_man", messages_man.get("canc") );
        velocityMap.put( "conf_man", messages_man.get("conf") ); 
        velocityMap.put( "deni_man", messages_man.get("deni") );
        velocityMap.put( "inqr_auto", messages_auto.get("inqr") );
        velocityMap.put( "resp_auto", messages_auto.get("resp") );
        velocityMap.put( "info_auto", messages_auto.get("info") );
        velocityMap.put( "rjct_auto", messages_auto.get("rjct") );
        velocityMap.put( "dlcf_auto", messages_auto.get("dlcf") );
        velocityMap.put( "pchg_auto", messages_auto.get("pchg"));
        velocityMap.put( "canc_auto", messages_auto.get("canc") );
        velocityMap.put( "conf_auto", messages_auto.get("conf") ); 
        velocityMap.put( "deni_auto", messages_auto.get("deni") );

        

        /*
         * results.put("inqr", inqr);
  		results.put("resp", resp);
  		results.put("info", info);
  		results.put("rjct", rjct);
  		results.put("dlcf", dlcf);
  		results.put("pchg", pchg);
  		results.put("canc", canc);
  		results.put("conf", conf);
  		results.put("deni", deni);
         */


        return VelocityEngineUtils.mergeTemplateIntoString( velocityEngine, "bommessagecount.html", velocityMap );
    }

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDate(
			Date date) {
		
		List<FulfillmentSummaryDaily> fulfillmentSummary = new ArrayList<FulfillmentSummaryDaily>();

		fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryDailyByDate(date);
		
	/*	String selectedDate = DateUtil.toXmlNoTimeFormatString(date);
		String now = DateUtil.toXmlNoTimeFormatString(new Date());
		 
		if ((fulfillmentSummary.isEmpty()&&(!selectedDate.equals(now)))){
			Date first = DateUtil.getFirstOfMonth(date);
			Date next =DateUtil.getNextDate(date);
			String month = DateUtil.toXmlDateFormatString(first); 
			String begin = DateUtil.toXmlDateFormatString(date); 
			String end = DateUtil.toXmlDateFormatString(next); 

			System.out.println("callBomDailyMetricsSP for " + date);
			fulfillmentSummaryDAO.callBomDailyMetricsSP(month, begin, end);
			fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryDailyByDate(date);
			System.out.println("callsp size " + fulfillmentSummary.size());

		}*/

		return fulfillmentSummary;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDateRange(
			String date, String dateTo) throws  Exception {
		
		List<FulfillmentSummaryDaily> fulfillmentSummary = new ArrayList<FulfillmentSummaryDaily>();

		fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryDailyByDateRange(date, dateTo);
	

		return fulfillmentSummary;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<FulfillmentSummaryMtd> getFulfillmentSummaryMtdByDate(Date date) {

		 List<FulfillmentSummaryMtd> fulfillmentSummaryMtd = fulfillmentSummaryDAO.getFulfillmentSummaryMtdByDate(date);
		
		return fulfillmentSummaryMtd;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDate(Date date) {
		
		
		 List<WorkOverviewDaily> workOverviewDaily = workOverviewDailyDAO.getAutomatedWorkOverviewDailyByDate(date);

		return workOverviewDaily;
	}
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<WorkOverviewDaily> getTLOWorkOverviewDailyByDate(Date date) {
		
		
		 List<WorkOverviewDaily> workOverviewDaily = workOverviewDailyDAO.getTLOWorkOverviewDailyByDate(date);

		return workOverviewDaily;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDateRange(Date date,Date toDate) {
		
		
		 List<WorkOverviewDaily> workOverviewDaily = workOverviewDailyDAO.getAutomatedWorkOverviewDailyByDateRange(date, toDate);

		return workOverviewDaily;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<WorkOverviewDaily> getTLOWorkOverviewDailyByDateRange(Date date,Date toDate) {
		
		
		 List<WorkOverviewDaily> workOverviewDaily = workOverviewDailyDAO.getTLOWorkOverviewDailyByDateRange(date, toDate);

		return workOverviewDaily;
	}
	
	
	/**
	 * Generates metrics spreadsheet by date
	 * @throws Exception 
	 */
   @Override
   public void generateMetricsSpreadSheet(FulfillmentSummaryDaily summaryDaily, FulfillmentSummaryMtd summaryMtd, 
		   WorkOverviewDaily workDailyMan, WorkOverviewDaily workDailyAuto, String dateStr ) throws Exception{
	   
	   metricsSpreadSheetService.generateDailySpreadSheet(summaryDaily, summaryMtd, workDailyMan, workDailyAuto, dateStr);
	   
   }
   
   /**
	 * Generates metrics spreadsheet by date range
 * @throws Exception 
	 */
  @Override
  public void generateMetricsDRSpreadSheet(List<FulfillmentSummaryDaily> summaryDailyList, List<FulfillmentSummaryMtd> summaryMtdList, 
		   List<WorkOverviewDaily> workDailyManList, List<WorkOverviewDaily> workDailyAutoList,Date date, Date endDate ) throws Exception{
	   
	  metricsSpreadSheetService.generateDateRangeSpreadSheet(summaryDailyList, summaryMtdList, workDailyManList, workDailyAutoList, date, endDate);
	   
  }


	

}
