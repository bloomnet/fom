package com.bloomnet.bom.mvc.service.impl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.service.SpreadSheetService;

@Service("metricsSpreadSheetService")
public class MetricsSpreadSheetServiceImpl implements SpreadSheetService {

	private static final String[] subTitles = { "Delivery Date",
			"*Fulfillment %", "Total Orders", "Unassigned Orders",
			"Assigned Active Orders", "Rejected Orders", "Canceled Orders",
			"Delivery Confirmations" };

	private static final String[] subTitles2 = { "Delivery Month",
			"*Fulfillment %", "Total Orders", "Unassigned Orders",
			"Assigned Active Orders", "Rejected Orders", "Canceled Orders",
			"Delivery Confirmations" };

	private static final String[] subTitles3 = { "Date", "Orders", "Inqr",
			"Resp", "Info", "Rjct", "Dlcf", "Pchg", "Canc", "Conf", "Deni" };

	private final String dailyTitle = "Daily Summary";

	private final String monthlyTitle = "Monthly Summary";

	private final String tloTitle = "Manual Work Overview";

	private final String autoTitle = "Automated Work Overview";

	// Injected property
	@Autowired
	private Properties bomProperties;

	/**
	 * Generates metrics spreadsheet by date
	 */
	@Override
	public void generateDailySpreadSheet(FulfillmentSummaryDaily summaryDaily,
			FulfillmentSummaryMtd summaryMtd, WorkOverviewDaily workDailyMan,
			WorkOverviewDaily workDailyAuto, String dateStr) throws Exception{

		try {

			// create spreadsheet
			Workbook wb = new HSSFWorkbook();
			Sheet sheet = wb.createSheet("BOM Metrics");

			CreationHelper createHelper = wb.getCreationHelper();

			// create style
			CellStyle style = createCellStyle(wb, true);
			CellStyle dataStyle = createCellStyle(wb, false);

			// daily title summary
			createTitleRow(wb, sheet, style, dailyTitle, subTitles, 1, 2);

			// daily data row
			createDailyDataRow(wb, sheet, createHelper, dataStyle,
					summaryDaily, 3);
			createDailyPercentRow(wb, sheet, createHelper, style, summaryDaily,
					4);

			// monthly title summary
			createTitleRow(wb, sheet, style, monthlyTitle, subTitles2, 6, 7);

			// monthly data row
			createMonthlyDataRow(wb, sheet, createHelper, dataStyle,
					summaryMtd, 8);
			createMonthlyPercentRow(wb, sheet, createHelper, style, summaryMtd,
					9);

			createFootNote(sheet, 11);

			// tlo overview title
			createOverviewTitleRow(wb, sheet, style, tloTitle, subTitles3, 13,
					14);

			// tlo data row
			createworkOverviewDataRow(wb, sheet, createHelper, dataStyle,
					workDailyMan, 15);

			// auto overview title
			createOverviewTitleRow(wb, sheet, style, autoTitle, subTitles3, 17,
					18);

			// auto data row
			createworkOverviewDataRow(wb, sheet, createHelper, dataStyle,
					workDailyAuto, 19);

			// set column widths
			// sheet.autoSizeColumn(2);
			sheet.setColumnWidth(2, 3000);
			sheet.setColumnWidth(4, 3000);
			sheet.setColumnWidth(5, 2500);
			sheet.setColumnWidth(7, 3000);
			sheet.setColumnWidth(8, 3500);

			// file
			String rpt_dir = bomProperties.getProperty(BOMConstants.REPORT_DIR);
			FileOutputStream fileOut = new FileOutputStream(rpt_dir
					+ "BOM_Metrics_" + dateStr + ".xls");

			// write file
			wb.write(fileOut);
			fileOut.close();

			System.out.println("completed");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Generates metrics spreadsheet by date range
	 */
	@Override
	public void generateDateRangeSpreadSheet(
			List<FulfillmentSummaryDaily> summaryDailyList,
			List<FulfillmentSummaryMtd> summaryMtdList,
			List<WorkOverviewDaily> workDailyManList,
			List<WorkOverviewDaily> workDailyAutoList, Date date, Date endDate) throws Exception{

		try {

			// create spreadsheet
			Workbook wb = new HSSFWorkbook();
			Sheet sheet = wb.createSheet("BOM Metrics");

			CreationHelper createHelper = wb.getCreationHelper();

			// create style
			CellStyle style = createCellStyle(wb, true);
			CellStyle dataStyle = createCellStyle(wb, false);

			String fromDateStr = DateUtil.toXmlDateFormatString(date);
			String toDateStr = DateUtil.toXmlDateFormatString(endDate);

			String fileNameDate = new String();
			if (fromDateStr.equals(toDateStr)) {
				fileNameDate = fromDateStr;
			} else {
				fileNameDate = fromDateStr + "_thru_" + toDateStr;
			}

			int dailyTitleCount = 1;
			int dailysubCount = 2;
			int dailydataCount = 3;
			int dailyperCount = 4;
			int finalDailyCount = 5;

			for (FulfillmentSummaryDaily summaryDaily : summaryDailyList) {

				// daily title summary
				createTitleRow(wb, sheet, style, dailyTitle, subTitles,
						dailyTitleCount, dailysubCount);

				// daily data row
				createDailyDataRow(wb, sheet, createHelper, dataStyle,
						summaryDaily, dailydataCount);
				createDailyPercentRow(wb, sheet, createHelper, style,
						summaryDaily, dailyperCount);

				dailyTitleCount = dailyTitleCount + 5;
				dailysubCount = dailysubCount + 5;
				dailydataCount = dailydataCount + 5;
				dailyperCount = dailyperCount + 5;
				finalDailyCount = dailyperCount;

			}

			int mtdTitleCount = finalDailyCount + 2;
			int mtdsubCount = finalDailyCount + 3;
			int mtddataCount = finalDailyCount + 4;
			int mtdperCount = finalDailyCount + 5;
			int finalMtdCount;

			int mtdIndex = summaryMtdList.size() - 1;
			FulfillmentSummaryMtd summaryMtd = summaryMtdList.get(mtdIndex);

			// monthly title summary
			createTitleRow(wb, sheet, style, monthlyTitle, subTitles2,
					mtdTitleCount, mtdsubCount);

			// monthly data row
			createMonthlyDataRow(wb, sheet, createHelper, dataStyle,
					summaryMtd, mtddataCount);
			createMonthlyPercentRow(wb, sheet, createHelper, style, summaryMtd,
					mtdperCount);

			finalMtdCount = mtdperCount;

			createFootNote(sheet, finalMtdCount + 2);

			int overviewTitleCount = finalMtdCount + 4;
			int overviewsubCount = finalMtdCount + 5;
			int overviewdataCount = finalMtdCount + 6;
			int finalOverviewCount = overviewdataCount;

			// tlo overview title
			createOverviewTitleRow(wb, sheet, style, tloTitle, subTitles3,
					overviewTitleCount, overviewsubCount);

			for (WorkOverviewDaily workDailyMan : workDailyManList) {

				// tlo data row
				createworkOverviewDataRow(wb, sheet, createHelper, dataStyle,
						workDailyMan, finalOverviewCount);
				finalOverviewCount++;

			}
			int autoTitleCount = finalOverviewCount + 2;
			int autosubCount = finalOverviewCount + 3;
			int autodataCount = finalOverviewCount + 4;
			int finalAutoCount = autodataCount;

			// auto overview title
			createOverviewTitleRow(wb, sheet, style, autoTitle, subTitles3,
					autoTitleCount, autosubCount);

			for (WorkOverviewDaily workDailyAuto : workDailyAutoList) {

				// auto data row
				createworkOverviewDataRow(wb, sheet, createHelper, dataStyle,
						workDailyAuto, finalAutoCount);
				finalAutoCount++;

			}

			// set column widths
			// sheet.autoSizeColumn(2);
			sheet.setColumnWidth(2, 3000);
			sheet.setColumnWidth(4, 3000);
			sheet.setColumnWidth(5, 2500);
			sheet.setColumnWidth(7, 3000);
			sheet.setColumnWidth(8, 3500);

			// file
			String rpt_dir = bomProperties.getProperty(BOMConstants.REPORT_DIR);
			FileOutputStream fileOut = new FileOutputStream(rpt_dir
					+ "BOM_Metrics_" + fileNameDate + ".xls");

			// write file
			wb.write(fileOut);
			fileOut.close();

			System.out.println("completed");

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFootNote(Sheet sheet, int row) {

		Row dataRow = sheet.createRow((short) row);
		Cell cell0 = dataRow.createCell(1);
		cell0.setCellValue("* Less FYF Rejects and Cancels");

	}

	private void createDailyPercentRow(Workbook wb, Sheet sheet,
			CreationHelper createHelper, CellStyle style,
			FulfillmentSummaryDaily summaryDaily, int row) {

		Row dataRow = sheet.createRow((short) row);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"0.00%"));
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

		// unassigned orders
		double unassigned = (double) summaryDaily.getUnassignedOrders()
				/ (double) summaryDaily.getTotalOrders();
		if (Double.isNaN(unassigned)) {
			unassigned = 0;
		}

		Cell cell3 = dataRow.createCell(4);
		cell3.setCellValue(unassigned);
		cell3.setCellStyle(cellStyle);

		// assigned orders
		double assigned = (double) summaryDaily.getAssignedActiveOrders()
				/ (double) summaryDaily.getTotalOrders();
		if (Double.isNaN(assigned)) {
			assigned = 0;
		}

		Cell cell4 = dataRow.createCell(5);
		cell4.setCellValue(assigned);
		cell4.setCellStyle(cellStyle);

		// rjct
		double rjct = (double) summaryDaily.getRejectedOrders()
				/ (double) summaryDaily.getTotalOrders();
		if (Double.isNaN(rjct)) {
			rjct = 0;
		}

		Cell cell5 = dataRow.createCell(6);
		cell5.setCellValue(rjct);
		cell5.setCellStyle(cellStyle);

		// canc
		double canc = (double) summaryDaily.getCanceledOrders()
				/ (double) summaryDaily.getTotalOrders();
		if (Double.isNaN(canc)) {
			canc = 0;
		}

		Cell cell6 = dataRow.createCell(7);
		cell6.setCellValue(canc);
		cell6.setCellStyle(cellStyle);

		// dlcf
		double dlcf = (double) summaryDaily.getDeliveryConfirmations()
				/ (double) summaryDaily.getAssignedActiveOrders();
		if (Double.isNaN(dlcf)) {
			dlcf = 0;
		}

		Cell cell7 = dataRow.createCell(8);
		cell7.setCellValue(dlcf);
		cell7.setCellStyle(cellStyle);

	}

	private void createMonthlyPercentRow(Workbook wb, Sheet sheet,
			CreationHelper createHelper, CellStyle style,
			FulfillmentSummaryMtd summaryMtd, int row) {

		Row dataRow = sheet.createRow((short) row);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"0.00%"));
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

		// unassigned orders
		double unassigned = (double) summaryMtd.getUnassignedOrders()
				/ (double) summaryMtd.getTotalOrders();
		if (Double.isNaN(unassigned)) {
			unassigned = 0;
		}

		Cell cell3 = dataRow.createCell(4);
		cell3.setCellValue(unassigned);
		cell3.setCellStyle(cellStyle);

		// assigned orders
		double assigned = (double) summaryMtd.getAssignedActiveOrders()
				/ (double) summaryMtd.getTotalOrders();
		if (Double.isNaN(assigned)) {
			assigned = 0;
		}

		Cell cell4 = dataRow.createCell(5);
		cell4.setCellValue(assigned);
		cell4.setCellStyle(cellStyle);

		// rjct
		double rjct = (double) summaryMtd.getRejectedOrders()
				/ (double) summaryMtd.getTotalOrders();
		if (Double.isNaN(rjct)) {
			rjct = 0;
		}

		Cell cell5 = dataRow.createCell(6);
		cell5.setCellValue(rjct);
		cell5.setCellStyle(cellStyle);

		// canc
		double canc = (double) summaryMtd.getCanceledOrders()
				/ (double) summaryMtd.getTotalOrders();
		if (Double.isNaN(canc)) {
			canc = 0;
		}

		Cell cell6 = dataRow.createCell(7);
		cell6.setCellValue(canc);
		cell6.setCellStyle(cellStyle);

		// dlcf
		double dlcf = (double) summaryMtd.getDeliveryConfirmations()
				/ (double) summaryMtd.getAssignedActiveOrders();
		if (Double.isNaN(dlcf)) {
			dlcf = 0;
		}
		Cell cell7 = dataRow.createCell(8);
		cell7.setCellValue(dlcf);
		cell7.setCellStyle(cellStyle);

	}

	private void createworkOverviewDataRow(Workbook wb, Sheet sheet,
			CreationHelper createHelper, CellStyle style,
			WorkOverviewDaily workDailyMan, int row) {

		// date
		Row dataRow = sheet.createRow((short) row);
		Cell cell = dataRow.createCell(1);
		cell.setCellValue(workDailyMan.getDateSent());
		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setWrapText(true);
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"m/d/yy"));
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cell.setCellStyle(cellStyle);

		// orders
		// Row dataRow = sheet.createRow((short) row);
		Cell cell0 = dataRow.createCell(2);
		cell0.setCellValue(workDailyMan.getOrders());
		cell0.setCellStyle(style);

		// inqr
		Cell cell1 = dataRow.createCell(3);
		cell1.setCellValue(workDailyMan.getInqr());
		cell1.setCellStyle(style);

		// resp
		Cell cell2 = dataRow.createCell(4);
		cell2.setCellValue(workDailyMan.getResp());
		cell2.setCellStyle(style);

		// info
		Cell cell3 = dataRow.createCell(5);
		cell3.setCellValue(workDailyMan.getInfo());
		cell3.setCellStyle(style);

		// rjct
		Cell cell4 = dataRow.createCell(6);
		cell4.setCellValue(workDailyMan.getRjct());
		cell4.setCellStyle(style);

		// dlcf
		Cell cell5 = dataRow.createCell(7);
		cell5.setCellValue(workDailyMan.getDlcf());
		cell5.setCellStyle(style);

		// pchg
		Cell cell6 = dataRow.createCell(8);
		cell6.setCellValue(workDailyMan.getPchg());
		cell6.setCellStyle(style);

		// canc
		Cell cell7 = dataRow.createCell(9);
		cell7.setCellValue(workDailyMan.getCanc());
		cell7.setCellStyle(style);

		// conf
		Cell cell8 = dataRow.createCell(10);
		cell8.setCellValue(workDailyMan.getConf());
		cell8.setCellStyle(style);

		// deni
		Cell cell9 = dataRow.createCell(11);
		cell9.setCellValue(workDailyMan.getDeni());
		cell9.setCellStyle(style);

	}

	private void createMonthlyDataRow(Workbook wb, Sheet sheet,
			CreationHelper createHelper, CellStyle style,
			FulfillmentSummaryMtd summaryMtd, int row) {

		// delivery date
		Row dataRow2 = sheet.createRow((short) row);
		Cell cell8 = dataRow2.createCell(1);
		cell8.setCellValue(DateUtil.getMonthDescription(summaryMtd
				.getDeliveryMonth()));
		cell8.setCellStyle(style);

		// fulfillment %
		Cell cell9 = dataRow2.createCell(2);
		cell9.setCellValue(summaryMtd.getFulfillmentPercentage());
		cell9.setCellStyle(style);

		// total orders
		Cell cell10 = dataRow2.createCell(3);
		cell10.setCellValue(summaryMtd.getTotalOrders());
		cell10.setCellStyle(style);

		// unassigned orders
		Cell cell11 = dataRow2.createCell(4);
		cell11.setCellValue(summaryMtd.getUnassignedOrders());
		cell11.setCellStyle(style);

		// assigned orders
		Cell cell12 = dataRow2.createCell(5);
		cell12.setCellValue(summaryMtd.getAssignedActiveOrders());
		cell12.setCellStyle(style);

		// rjct
		Cell cell13 = dataRow2.createCell(6);
		cell13.setCellValue(summaryMtd.getRejectedOrders());
		cell13.setCellStyle(style);

		// canc
		Cell cell14 = dataRow2.createCell(7);
		cell14.setCellValue(summaryMtd.getCanceledOrders());
		cell14.setCellStyle(style);

		// dlcf
		Cell cell15 = dataRow2.createCell(8);
		cell15.setCellValue(summaryMtd.getDeliveryConfirmations());
		cell15.setCellStyle(style);

	}

	private void createDailyDataRow(Workbook wb, Sheet sheet,
			CreationHelper createHelper, CellStyle style,
			FulfillmentSummaryDaily summaryDaily, int row) {

		// delivery date
		Row dataRow = sheet.createRow((short) row);
		Cell cell0 = dataRow.createCell(1);
		cell0.setCellValue(summaryDaily.getDeliveryDate());
		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setWrapText(true);
		// CreationHelper createHelper = wb.getCreationHelper();
		cellStyle.setDataFormat(createHelper.createDataFormat().getFormat(
				"m/d/yy"));
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cell0.setCellStyle(cellStyle);

		// fulfillment %
		Cell cell1 = dataRow.createCell(2);
		cell1.setCellValue(summaryDaily.getFulfillmentPercentage());
		cell1.setCellStyle(style);

		// total orders
		Cell cell2 = dataRow.createCell(3);
		cell2.setCellValue(summaryDaily.getTotalOrders());
		cell2.setCellStyle(style);

		// unassigned orders
		Cell cell3 = dataRow.createCell(4);
		cell3.setCellValue(summaryDaily.getUnassignedOrders());
		cell3.setCellStyle(style);

		// assigned orders
		Cell cell4 = dataRow.createCell(5);
		cell4.setCellValue(summaryDaily.getAssignedActiveOrders());
		cell4.setCellStyle(style);

		// rjct
		Cell cell5 = dataRow.createCell(6);
		cell5.setCellValue(summaryDaily.getRejectedOrders());
		cell5.setCellStyle(style);

		// canc
		Cell cell6 = dataRow.createCell(7);
		cell6.setCellValue(summaryDaily.getCanceledOrders());
		cell6.setCellStyle(style);

		// dlcf
		Cell cell7 = dataRow.createCell(8);
		cell7.setCellValue(summaryDaily.getDeliveryConfirmations());
		cell7.setCellStyle(style);
	}

	private CellStyle createCellStyle(Workbook wb, boolean bold) {

		CellStyle style = wb.createCellStyle();
		style.setWrapText(true);
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		if (bold == true) {
			Font font = wb.createFont();
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			style.setFont(font);
		}

		return style;
	}

	@SuppressWarnings("deprecation")
	private void createTitleRow(Workbook wb, Sheet sheet, CellStyle style,
			String title, String[] subTitles, int titleRow, int subtitleRow) {

		// title row
		Row row = sheet.createRow((short) titleRow);
		Cell titleCell = row.createCell((short) 1);
		titleCell.setCellValue(title);
		titleCell.setCellStyle(style);

		// Merge title row
		sheet.addMergedRegion(new CellRangeAddress(titleRow, // first row
																// (0-based)
				titleRow, // last row (0-based)
				1, // first column (0-based)
				8 // last column (0-based)
		));
		Row subTitleRow = sheet.createRow((short) subtitleRow);
		subTitleRow.setHeightInPoints(43);

		int length = subTitles.length;
		for (int i = 0; i < length; i++) {
			Cell cell = subTitleRow.createCell(i + 1);
			cell.setCellValue(subTitles[i]);
			cell.setCellStyle(style);

		}

		// work around for adding border around merged cells
		Row borderRow = sheet.createRow((short) titleRow - 1);
		Cell bordercell = borderRow.createCell((short) 1);
		CellStyle cellstyle = wb.createCellStyle();
		cellstyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellstyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		bordercell.setCellValue("aaa");
		bordercell.setCellStyle(cellstyle);

		for (int i = 0; i < length; i++) {
			Cell cell = borderRow.createCell(i + 1);
			// cell.setCellValue("zyz");
			cell.setCellStyle(cellstyle);
		}

		Cell leftCell = row.createCell((short) 0);
		CellStyle leftstyle = wb.createCellStyle();
		leftstyle.setBorderRight(CellStyle.BORDER_THIN);
		leftstyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		leftCell.setCellStyle(leftstyle);

		Cell rightCell = row.createCell((short) length + 1);
		CellStyle rightstyle = wb.createCellStyle();
		rightstyle.setBorderLeft(CellStyle.BORDER_THIN);
		rightstyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		rightCell.setCellStyle(rightstyle);

	}

	@SuppressWarnings("deprecation")
	private void createOverviewTitleRow(Workbook wb, Sheet sheet,
			CellStyle style, String title, String[] subTitles, int titleRow,
			int subtitleRow) {

		// title row
		Row row = sheet.createRow((short) titleRow);
		Cell titleCell = row.createCell((short) 1);
		titleCell.setCellValue(title);
		titleCell.setCellStyle(style);

		// Merge title row
		sheet.addMergedRegion(new CellRangeAddress(titleRow, // first row
																// (0-based)
				titleRow, // last row (0-based)
				1, // first column (0-based)
				11 // last column (0-based)
		));
		Row subTitleRow = sheet.createRow((short) subtitleRow);

		int length = subTitles.length;
		for (int i = 0; i < length; i++) {
			Cell cell = subTitleRow.createCell(i + 1);
			cell.setCellValue(subTitles[i]);
			cell.setCellStyle(style);

		}

		// work around for adding border around merged cells
		Row borderRow = sheet.createRow((short) titleRow - 1);
		Cell bordercell = borderRow.createCell((short) 1);
		CellStyle cellstyle = wb.createCellStyle();
		cellstyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellstyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		bordercell.setCellValue("aaa");
		bordercell.setCellStyle(cellstyle);

		for (int i = 0; i < length; i++) {
			Cell cell = borderRow.createCell(i + 1);
			cell.setCellStyle(cellstyle);
		}

		Cell leftCell = row.createCell((short) 0);
		CellStyle leftstyle = wb.createCellStyle();
		leftstyle.setBorderRight(CellStyle.BORDER_THIN);
		leftstyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		leftCell.setCellStyle(leftstyle);

		Cell rightCell = row.createCell((short) length + 1);
		CellStyle rightstyle = wb.createCellStyle();
		rightstyle.setBorderLeft(CellStyle.BORDER_THIN);
		rightstyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		rightCell.setCellStyle(rightstyle);

	}

	

}
