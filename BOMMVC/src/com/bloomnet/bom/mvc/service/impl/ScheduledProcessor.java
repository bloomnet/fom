package com.bloomnet.bom.mvc.service.impl;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Component;

public class ScheduledProcessor {
	   private final AtomicInteger counter = new AtomicInteger();   
	  
	   public void process() {
	      System.out.println("processing next 10 at " + new Date());
	      for (int i = 0; i < 10; i++) {
	    	  System.out.println(counter.incrementAndGet());      
	         }   
	      }
	   }