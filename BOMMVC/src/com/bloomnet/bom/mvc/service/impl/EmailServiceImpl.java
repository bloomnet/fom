package com.bloomnet.bom.mvc.service.impl;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.util.OccassionUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.service.EmailService;

public class EmailServiceImpl implements EmailService {
	
	  // Define a static logger variable
    static Logger logger = Logger.getLogger( EmailServiceImpl.class );

	// Injected DAO implementation
	@Autowired private ShopDAO shopDAO;
	
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected dependency
	@Autowired private MailSender mailSender;
	
	@Autowired private TransformService transformService;	

	
	
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.impl.EmailService#sendEmail(java.lang.String, java.lang.String)
	 */
	@Override
	public void sendEmail(String order,  String email) {

		MessageOnOrderBean messageOnOrderBean = new MessageOnOrderBean();
		String fulfillingShopName = new String();
		long fulfillingShopId = 0;
		String notify =bomProperties.getProperty(BOMConstants.EMAIL_NOTIFY);

		
		try {


			ForeignSystemInterface fsi = transformService.convertToJavaFSI(order);

			MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

			String recievingShopcode = messageOrder.getReceivingShopCode();
			Shop receivingShop  = shopDAO.getShopByCode(recievingShopcode);
			Shop sendingShop    = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			Shop fulfillingShop = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());

			receivingShop.setShopEmail(email);
			fulfillingShop.setShopEmail(email);


			messageOnOrderBean = transformService.createBeanFromFSI(fsi,receivingShop, sendingShop, fulfillingShop);


			fulfillingShopName = messageOnOrderBean.getFulfillingShop().getShopName();
			fulfillingShopId = messageOnOrderBean.getFulfillingShop().getShopId();

			SimpleMailMessage message=new SimpleMailMessage();

			String fromEmail = new String();
			if (notify.equals("ON")){
				fromEmail = bomProperties.getProperty(BOMConstants.EMAIL_NOTIFY_FROM);
			}else{
				fromEmail = bomProperties.getProperty(BOMConstants.EMAIL_FROM);
			}
			String toEmail = messageOnOrderBean.getFulfillingShop().getShopEmail();
			String orderNumber = messageOnOrderBean.getOrderNumber();
			if (orderNumber==null){
				orderNumber = messageOnOrderBean.getBmtOrderNumber();
			}
			long sequenceNumber = messageOnOrderBean.getBmtSeqNumberOfOrder();
			String deliveryDate = messageOnOrderBean.getDeliveryDate();
			String fname = messageOnOrderBean.getRecipientFirstName();
			String lname = messageOnOrderBean.getRecipientLastName();
			String address = messageOnOrderBean.getRecipientAddress1();
			String city = messageOnOrderBean.getRecipientCity();
			String state = messageOnOrderBean.getRecipientState();
			String zip  = messageOnOrderBean.getRecipientZipCode();
			String phone = messageOnOrderBean.getRecipientPhoneNumber();
			String occassion = messageOnOrderBean.getOccasionId();
			if(!occassion.equals("") && occassion != null) 
				occassion = OccassionUtil.getOccassionDescription(occassion);
			String cardMessage = messageOnOrderBean.getCardMessage();
			String instructions = messageOnOrderBean.getSpecialInstruction();
			String totalCost =messageOnOrderBean.getTotalCost();
			String[] toEmailarray;
			toEmailarray = toEmail.split(",");
			System.out.println(toEmailarray.toString());
			message.setFrom(fromEmail);
			message.setTo(toEmailarray);

			//message.setTo(toEmail);
			//message.setTo("jpursoo@bloomnet.net");
			//message.setTo("pursuance805@yahoo.com");
			if (notify.equals("ON")){
				message.setSubject("Florists.com Order Received " + orderNumber);
				
			}else{
				message.setSubject("BloomNet Order Confirmation " + orderNumber);

			}

			String orderString = "Order Number: " + orderNumber+"\n"+ 
			"Sequence number: " + sequenceNumber+"\n"+ 
			"Delivery Date: " + deliveryDate+"\n"+ 

			"\nOrder Information:\n";
			for(OrderProductInfoDetails products:messageOnOrderBean.getProducts()){
				orderString = orderString+ "Qty " + products.getUnits()+"\n"+
				"Product Code: "+ products.getProductCode()+"\n"+
				"Product Description: "+ products.getProductDescription()+"\n"+
				"Amount "+ products.getCostOfSingleProduct()+"\n";
			}
			orderString  = orderString+"\nTotal Cost: "+ totalCost;
			orderString = orderString+"\nRecipient Information: \n"+
			fname +" "+ lname +"\n"+
			address +"\n"+
			city + ", " + state +"\n"+
			zip +"\n"+
			phone +"\n"+

			"\nOccasion: " + occassion +"\n"+
			"\nCard message: " + cardMessage +"\n"+
			"\nSpecial Instructions: " + instructions;

			message.setText(orderString);
			mailSender.send(message);
		} catch (MailException e) {
			logger.error("Unable to send email to shop: " + fulfillingShopName + "\nshop id: "
					+ fulfillingShopId + "\nto email address: "
					+messageOnOrderBean.getFulfillingShop().getShopEmail() 
					+ "reason: " + e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	


}
