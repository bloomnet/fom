package com.bloomnet.bom.mvc.service.impl;


import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Useractivity;
import com.bloomnet.bom.common.entity.Userrole;

import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.MessageTypeUtil;
import com.bloomnet.bom.common.util.PasswordHasher;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.utils.HibernateHelper;

@Service("bomService")
@Transactional(propagation = Propagation.REQUIRED)
public class BOMServiceImpl extends AbstractService  implements BomService {
	
	@Autowired private Properties bomProperties;
	
	@Autowired private Properties fsiProperties;
	
    @Autowired private MessageProducer messageProducer;
    
	@Autowired private OrderDAO orderDAO;
	@Autowired private UserDAO  userDAO;
	@Autowired private BomorderviewV2DAO bomorderviewV2DAO;

	@Autowired private TransformService transformService;	


	
	
	// Injected service
    @Autowired private ActivitiesService activitiesService;
    
	@Autowired private AgentService agentService;

	
    static Logger logger = Logger.getLogger( BOMServiceImpl.class );

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.impl.BomService#sendOrderToQueue(java.lang.String)
	 */
	@Override
	//@Transactional(propagation=Propagation.MANDATORY,rollbackFor=Exception.class)
    public void sendOrderToQueue( String orderNumber, String queue ) throws Exception {
		
		List<String> tloOrders  = new ArrayList<String>();
		tloOrders = messageProducer.BrowseQueue();
		
		boolean onTLO=tloOrders.contains(orderNumber);
		
		if ((onTLO)&&(queue.equals(BOMConstants.TLO_QUEUE))){
			throw new Exception("Order number " + orderNumber + " is already on the TLO queue");
		}

		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

		if (!(queue.equals(BOMConstants.FSI_QUEUE))
				&&!(queue.equals(BOMConstants.TFSI_QUEUE))
				&&!(queue.equals(BOMConstants.TLO_QUEUE)))
		{
			throw new Exception("Queue: '" + queue + "' is not a valid destination");
			
		}else{

			if (bomorder!=null){

				String orderXml =bomorder.getOrderXml();

				ForeignSystemInterface fsi = transformService.convertToJavaFSI( orderXml );

				MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

				String userName          = UserDAO.INTERNAL_USER;
				String sendingShopCode   = messageOrder.getSendingShopCode();
				int    messageTypeInt    = 0;
				String messageType       = MessageTypeUtil.getMessageDesc(messageTypeInt);

				String skillset = BOMConstants.SKILLSET_BASICAGENT;

				String status = bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED);
				String destination = queue;

				orderDAO.persistOrderactivityByUser( userName, BOMConstants.ACT_ROUTING, status, orderNumber, destination );
				long childId = 0;
				String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);
				if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
					childId = orderDAO.getCurrentFulfillingOrder(orderNumber).getBomorderId();
				}
				
				byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
				BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomorder.getBomorderId());
				bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
				bomorderSts.setStsRoutingId(Byte.parseByte(status));
				bomorderSts.setVirtualqueueId(virtualqueueId);
			
				orderDAO.updateBomorderSts(bomorderSts, childId);

				String deliveryDate = fsi.getMessagesOnOrder().get(0).getMessageOrder().getDeliveryDetails().getDeliveryDate();
				String city = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientCity();
				String zip  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientZipCode();
				String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
				String occasionCode  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
				messageProducer.produceMessage(destination, orderXml,  orderNumber, sendingShopCode, deliveryDate,
						city, zip,occasionCode, skillset,messageType,Byte.toString(BOMConstants.ORDER_TYPE_BMT),timeZone) ;

				logger.debug("Sending order number: "+orderNumber + " to TLO queue");

			}
			else{
				throw new Exception("order number: " + orderNumber + " not found");
			}

		}

    }
    
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.impl.BomService#sendOrderByFax(java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional(propagation=Propagation.MANDATORY,rollbackFor=Exception.class)
    public void sendOrderByFax( String orderNumber, String fax ) throws Exception {

    	String destination= BOMConstants.FAX_OUTBOUND_ORDER_QUEUE;
    	Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

    	String orderXml =bomorder.getOrderXml();

    	final ForeignSystemInterface fsi = transformService.convertToJavaFSI( orderXml );

    	final MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

    	String userName          = UserDAO.INTERNAL_USER;
    	String receivingShop     = messageOrder.getReceivingShopCode();
    	int    messageTypeInt    = 0;
    	String messageType       = MessageTypeUtil.getMessageDesc(messageTypeInt);

    	final String status = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);

    	if (messageType.equals(BOMConstants.ORDER_MESSAGE_DESC)){
    		orderDAO.persistOrderactivityByUser( userName, BOMConstants.ACT_ROUTING, status, orderNumber, destination );
    		long childId = 0;
    		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);
    		if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
    			childId = orderDAO.getCurrentFulfillingOrder(orderNumber).getBomorderId();
    		}
    		
    		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
    		
    		BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomorder.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(status));
			bomorderSts.setVirtualqueueId(virtualqueueId);
    		
    		orderDAO.updateBomorderSts(bomorderSts, childId);
    	}

    	String deliveryDate = fsi.getMessagesOnOrder().get(0).getMessageOrder().getDeliveryDetails().getDeliveryDate();
    	String occasionCode  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
    	
    	messageProducer.produceOutboundMessage(destination, orderXml, orderNumber, receivingShop, deliveryDate, occasionCode, fax);

    }


	


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void removeUserFromOrder(WebAppOrder order) throws Exception {
		
		String orderNumber = order.getBloomnetOrderNumber();
		
		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
		if (bomorder==null){
			throw new Exception("order number: " + orderNumber + " not found");
		}
		else{
		WebAppOrder wao =new WebAppOrder(bomorder);
		activitiesService.unlockOrder( wao );
		sendOrderToQueue(orderNumber, BOMConstants.TLO_QUEUE);
		}
		
	}

	
	
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void addOrder(WebAppOrder order) throws Exception {
		
		MessageOnOrderBean orderBean = order.getBean();
		
		ForeignSystemInterface fsi = createFSIOrderFromBean( orderBean,false );

		final String orderString   = transformService.convertToFSIXML( fsi );
		Bomorder bomorder  = agentService.createOrderEntity( orderBean, orderString );
		
		orderDAO.persistParentOrder(bomorder);
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateOrder(WebAppOrder order) throws Exception {
		orderDAO.updateOrder(order);
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getTLOOrdersBeingWorked() throws Exception {
		
		List<Bomorder> bomorders = orderDAO.getTLOOrdersBeingWorked();
		
		return bomorders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<WebAppUser> getTLOAgentsWorking() throws Exception {
		
		List<Bomorder> bomorders;
		List<WebAppUser> users= new ArrayList<WebAppUser>();
		String timeStr  = new String();

    
    	bomorders = getTLOOrdersBeingWorked();
    		
    	for (Bomorder bomorder: bomorders){
    		User user = bomorder.getUser();
    		WebAppUser wau = new WebAppUser(user);
    		String orderNumber = bomorder.getOrderNumber();
    		wau.setOrderNumber(orderNumber);
    		//
    		Orderactivity activity = orderDAO.getLatestOrderActivity(bomorder);
			ActRouting actRouting = activity.getActRouting();
			String queue = actRouting.getVirtualQueue();
			StsRouting stsRouting = actRouting.getStsRouting();
			stsRouting.getDescription();
			String status = actRouting.getStatus();
			if (status.equals("Actively being worked")&&(queue.equals(BOMConstants.TLO_QUEUE))){
				
				//get the difference between the time order is being worked to now
				// if time is greater than invalidSession time then unlock
				Date lock = activity.getCreatedDate();
				Date now = new Date();

				long lockedDuration = lock.getTime() - now.getTime();
				long time = lockedDuration * (-1)/ 1000;
				String seconds = Integer.toString((int)(time % 60));   
				String minutes = Integer.toString((int)((time % 3600) / 60));   
				String hours = Integer.toString((int)(time / 3600));  
				
				timeStr = hours + ":" + minutes + ":" + seconds;
				wau.setTimeInOrder(timeStr);
			}
				
    		//
    		users.add(wau);
    	}
		
		return users;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void sendOrderToFSIShop(WebAppOrder order) throws Exception {
		
		order.getBean();
		String userName          = UserDAO.INTERNAL_USER;
		agentService.sendOrderByFSI(order, userName);
		
	}


	
	@Override
	protected Properties getFsiProperties() throws Exception {
		return fsiProperties;
	}


	@Override
	protected ShopDAO getShopDAO() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	@Override
	public void updateUser(User user) throws Exception {
		
		userDAO.persistUser(user);
		
	}
	


	

	@Override
	public void updateUser(User user, User adminUser) throws Exception {
		
		User agent = userDAO.getUserByUserName(user.getUserName());
		/*user.setModifiedDate(DateUtil.toXmlDashFormatString(new Date()));
		user.setUserByModifiedUserId(adminUser);
		user.setUserByCreatedUserId(agent.getUserByCreatedUserId());
		user.setCreatedDate(agent.getCreatedDate());*/
		String userName = user.getUserName();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		String password = user.getPassword();
		String email =  user.getEmail();
		String active = user.getActive();


		agent.setUserName(userName);
		agent.setFirstName(firstName);
		agent.setLastName(lastName);
		agent.setPassword(password);
		agent.setEmail(email);
		agent.setModifiedDate(DateUtil.toXmlDashFormatString(new Date()));
		agent.setUserByModifiedUserId(adminUser);
		agent.setActive(active);
		
		updateUser(agent);
		
	}
	
	@Override
	public void addUserrole(Userrole userrole) throws Exception {
		
		userDAO.persistUserrole(userrole);
		
	}
	


	@Override
	public String encodePassword(String password)throws Exception {
		
		PasswordHasher encoder = new PasswordHasher();
		String hashedPass = encoder.hash(password);

		return hashedPass;
	}
	
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getWFRS(){

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersWaitingForResponse();
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();

		String inString = "";

		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gottenOrders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getOutstandingAutomatedOrders(){

		List<BomorderviewV2> orders = bomorderviewV2DAO.getFrozenAutomatedOrders();
		String inString  = "";
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();

		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gottenOrders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getOutstandingOrders(){
		
		long startTime = System.currentTimeMillis();

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersNoWfrs();
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();
		
		long stopTime = System.currentTimeMillis();
	      long elapsedTime = stopTime - startTime;
	      System.out.println(elapsedTime + "ms execution time for bomorderview_v2 query");
		
		String inString = "";
		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				startTime = System.currentTimeMillis();
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
				stopTime = System.currentTimeMillis();
				elapsedTime = stopTime - startTime;
				System.out.println(elapsedTime + "ms execution time for bomorder query");
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	      
			startTime = System.currentTimeMillis();
		if(gottenOrders != null && !gottenOrders.isEmpty()){
			sortList(gottenOrders);
		}
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime + "ms execution time for java based comparator sorting");
		return gottenOrders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<WebAppOrder> getMultipleOrders(List<String> specifiedOrders){

		List<Bomorder> orders = orderDAO.getMultipleOrdersByOrderIds(specifiedOrders);
		List<WebAppOrder> waos = new ArrayList<WebAppOrder>();
		for(Bomorder o : orders){
			WebAppOrder wao = new WebAppOrder(o);
			waos.add(wao);
		}

		if(orders != null && !orders.isEmpty()){
			sortList(orders);
		}
		return waos;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getPreviousOutstandingOrders(){

		List<BomorderviewV2> orders = bomorderviewV2DAO.getPreviousOutstandingOrdersNoWfrs();
		String inString = "";
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();

		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gottenOrders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getOutstandingOrdersByState(String state){
		
		long startTime = System.currentTimeMillis();

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersNoWfrs();
		
		long stopTime = System.currentTimeMillis();
	      long elapsedTime = stopTime - startTime;
	      System.out.println(elapsedTime + "ms execution time for bomorderview_v2 query");
		
		String inString = "";
		
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();
		List<Bomorder> returnResults = new ArrayList<Bomorder>();
		
		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				startTime = System.currentTimeMillis();
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
				stopTime = System.currentTimeMillis();
				elapsedTime = stopTime - startTime;
				System.out.println(elapsedTime + "ms execution time for bomorder query");
				startTime = System.currentTimeMillis();
				for(Bomorder o : gottenOrders){
					if(o.getCity().getState().getShortName().equalsIgnoreCase(state)){
						returnResults.add(o);
					}
				}
				stopTime = System.currentTimeMillis();
				elapsedTime = stopTime - startTime;
				System.out.println(elapsedTime + "ms execution time for converting bomorders to webapporder objects");
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	      
			startTime = System.currentTimeMillis();
		if(returnResults != null && !returnResults.isEmpty()){
			sortList(returnResults);
		}
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime + "ms execution time for java based comparator sorting");
		return returnResults;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getFutureOutstandingOrders(){

		List<BomorderviewV2> orders = bomorderviewV2DAO.getFutureOutstandingOrdersNoWfrs();
		String inString = "";
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();

		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return gottenOrders;
	}
	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Bomorder> getTodayOutstandingOrders(){

		List<BomorderviewV2> orders = bomorderviewV2DAO.getTodayOutstandingOrdersNoWfrs();
		String inString = "";
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();
		
		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
			}	

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gottenOrders;
	}
	
	

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getFrozenAutomatedOrders(){


		List<BomorderviewV2> orders = bomorderviewV2DAO.getFrozenAutomatedOrders();

		
		
		return orders;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public WebAppOrder viewOrderHistory(String orderNumber) throws Exception{
		WebAppOrder wao;

		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
		if (bomorder!=null){
			wao= new WebAppOrder(bomorder);
		}
		else{
			throw new Exception("order number: " + orderNumber + " not found");
		}
		return wao;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<Role> getRoles(User user){
		
		
		//User m_user = HibernateHelper.initializeAndUnproxy(user);
		
		//WebAppUser wau = new WebAppUser(m_user);
		
		List<Role> roles =   userDAO.getUserroleByUser(user);
		
		List<Role> results = HibernateHelper.initializeAndUnproxy(roles);
		for (Role role: results){
			System.out.println("Role: " + role.getDescription());
		}
			
		return results;

	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getTotalOrdersWorkedToday(long userid){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		orders = userDAO.getTotalOrdersWorkedToday(userid);
		
		return orders;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getTotalOrdersTouchedToday(long userid){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		orders = userDAO.getTotalOrdersTouchedToday(userid);
		
		return orders;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getTotalOrdersWorkedByDate(long userid, Date date){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		orders = userDAO.getTotalOrdersWorkedByDate(userid, date);
		
		return orders;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getTotalOrdersTouchedByDate(long userid, Date date){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		orders = userDAO.getTotalOrdersTouchedByDate(userid, date);
		
		return orders;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public List<BomorderviewV2> getTotalOrdersTouchedByDate(Date date){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		orders = userDAO.getTotalOrdersTouchedByDate( date);
		
		return orders;
		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Date getLastLogInTime(long userid){
		
		Uactivitytype activitytype = userDAO.getUserActivityType("Logged In");
		Date time = null;
		
		User user = userDAO.getUserById(userid);
		List<Useractivity> activities = userDAO.getLastUserActivity(user, activitytype);
		
		if ((activities!=null)&&(!activities.isEmpty())){
			Useractivity activity = activities.get(0);
			 time = activity.getCreatedDate();
		}
		
		return time;
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public String getTotalWorkTimeToday(long userId) {
		
		
		long totalTime = 0;
		
		String totalTimeToday = null;
		
		List<Long> workTimes = userDAO.getTotalWorkTimeToday(userId );
		
		if (!workTimes.isEmpty()){
		for (Long workTime:workTimes)
		{
			totalTime = totalTime + workTime;
			Integer.toString((int)(workTime % 60));   
			Integer.toString((int)((workTime % 3600) / 60));   
			Integer.toString((int)(workTime / 3600));
		
		System.out.println(workTime);
		
		}
		String seconds = Integer.toString((int)(totalTime % 60));   
		String minutes = Integer.toString((int)((totalTime % 3600) / 60));   
		String hours = Integer.toString((int)(totalTime / 3600));
		
		totalTimeToday = hours+":"+minutes+":"+seconds;
	
	System.out.println("total time : " +totalTime);
	System.out.println("total time : " +hours+":"+minutes+":"+seconds);
	}
	return totalTimeToday;

		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public String getTotalWorkTimeByDate(long userId, Date date) {
		
		
		long totalTime = 0;
		
		String totalTimeToday = null;
		
		List<Long> workTimes = userDAO.getTotalWorkTimeByDate(userId, date );
		
		if (!workTimes.isEmpty()){
		for (Long workTime:workTimes)
		{
			totalTime = totalTime + workTime;
			Integer.toString((int)(workTime % 60));   
			Integer.toString((int)((workTime % 3600) / 60));   
			Integer.toString((int)(workTime / 3600));
		
		System.out.println(workTime);
		
		}
		String seconds = Integer.toString((int)(totalTime % 60));   
		String minutes = Integer.toString((int)((totalTime % 3600) / 60));   
		String hours = Integer.toString((int)(totalTime / 3600));
		
		totalTimeToday = hours+":"+minutes+":"+seconds;
	
	System.out.println("total time : " +totalTime);
	System.out.println("total time : " +hours+":"+minutes+":"+seconds);
	}
	return totalTimeToday;

		
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public WebAppUser getSelectedUser(long userId){
		User user = userDAO.getUserById(userId);
		WebAppUser wau = new WebAppUser(user);
		return wau;
		
	}
	
	@Override
	public void sortList(List<Bomorder> gottenOrders){
		Collections.sort(gottenOrders, new Comparator<Bomorder>() {
			
	        public int compare(Bomorder o1, Bomorder o2) {
	            int compare =  o1.getCity().getState().getName().trim().compareToIgnoreCase(o2.getCity().getState().getName().trim());
	            if (compare != 0)
	                return compare;
	            compare = o1.getCity().getName().trim().compareToIgnoreCase(o2.getCity().getName().trim());
	            if (compare != 0)
	                return compare;
	            return o1.getZip().getZipCode().trim().compareToIgnoreCase(o2.getZip().getZipCode().trim());
	        }
	    });
		
	}


	@Override
	public List<Bomorder> getOutstandingMessages() {
		long startTime = System.currentTimeMillis();

		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingMessages();
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();
		
		long stopTime = System.currentTimeMillis();
	      long elapsedTime = stopTime - startTime;
	      System.out.println(elapsedTime + "ms execution time for bomorderview_v2 query");
		
		String inString = "";
		try {
			for(BomorderviewV2 orderView : orders){

				String orderId = String.valueOf(orderView.getId().getParentBomid());
				inString += orderId + ",";
			}
			
			if(inString.length() > 0){
				inString = inString.substring(0, inString.length() - 1);
				startTime = System.currentTimeMillis();
				gottenOrders = orderDAO.getOrdersByOrderIds(inString);
				stopTime = System.currentTimeMillis();
				elapsedTime = stopTime - startTime;
				System.out.println(elapsedTime + "ms execution time for bomorder query");
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	      
			startTime = System.currentTimeMillis();
		if(gottenOrders != null && !gottenOrders.isEmpty()){
			sortList(gottenOrders);
		}
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime + "ms execution time for java based comparator sorting");
		return gottenOrders;
	}


}
