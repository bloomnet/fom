package com.bloomnet.bom.mvc.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewDAO;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Bomorderview;
import com.bloomnet.bom.common.entity.BomorderviewId;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.BomorderviewV2Id;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.WebserviceMonitor;
import com.bloomnet.bom.common.jaxb.audit.AuditInterface;
import com.bloomnet.bom.common.jaxb.audit.AuditMessagesByDateRange;
import com.bloomnet.bom.common.jaxb.audit.AuditRequest;
import com.bloomnet.bom.common.jaxb.audit.AuditResponse;
import com.bloomnet.bom.common.jaxb.audit.AuditSearchOptions;
import com.bloomnet.bom.common.jaxb.audit.Message;
import com.bloomnet.bom.common.jaxb.audit.Security;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.SendGridEmailer;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.BOMTask;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.service.MessagingService;

@Service("bomTask")
@Transactional(propagation = Propagation.SUPPORTS)
public class BOMTaskImpl implements BOMTask {
	
	@Autowired private Properties bomProperties;
	
	@Autowired private Properties fsiProperties;
	
    @Autowired private MessageProducer messageProducer;
    
	@Autowired private OrderDAO orderDAO;
	@Autowired private MessageDAO messageDAO;
	@Autowired private ShopDAO shopDAO;


	
	//@Autowired private BomorderviewDAO bomorderviewDAO;
	
	@Autowired private BomorderviewV2DAO  bomorderviewV2DAO;
	
	@Autowired private MessagingService messagingService;

	
	// Injected service
    @Autowired private ActivitiesService activitiesService;
    
	@Autowired private TransformService transformService;	

    
    
    	
	@Autowired private BomService bomService;
	
    static Logger logger = Logger.getLogger( BOMTaskImpl.class );


    /**
	 *  every hour at :30
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public synchronized void releaseLockedOrdersTask() throws Exception {
		
			//20 minutes test       
			//long releaseOrderTime = 7200;
			long releaseOrderTime = Long.parseLong(bomProperties.getProperty("order.release"));
	    	String userName          = UserDAO.INTERNAL_USER;
	    	

	    	logger.info("Checking for orders that need to be unlocked");
			
			//get orders that have a user id associated with it
			List<Bomorder> orders = orderDAO.getTLOOrdersBeingWorked();
			
			//get the act_routing order activity of order being worked
			for (Bomorder order: orders){
				
				String orderNumber = order.getOrderNumber();
				
				Orderactivity activity = orderDAO.getLatestOrderActivity(order);
				ActRouting actRouting = activity.getActRouting();
				String queue = actRouting.getVirtualQueue();
				StsRouting stsRouting = actRouting.getStsRouting();
				String routingDesc = stsRouting.getDescription();
				String status = actRouting.getStatus();
				if (status.equals("Actively being worked")&&(queue.equals(BOMConstants.TLO_QUEUE))){
					
					//get the difference between the time order is being worked to now
					// if time is greater than invalidSession time then unlock
					Date lock = activity.getCreatedDate();
					Date now = new Date();
					
					long lockedDuration = lock.getTime() - now.getTime();
					long time = lockedDuration * (-1)/ 1000;
					String seconds = Integer.toString((int)(time % 60));   
					String minutes = Integer.toString((int)((time % 3600) / 60));   
					String hours = Integer.toString((int)(time / 3600));   
					
					String rseconds = Integer.toString((int)(releaseOrderTime % 60));   
					String rminutes = Integer.toString((int)((releaseOrderTime % 3600) / 60));   
					String rhours = Integer.toString((int)(releaseOrderTime / 3600));

					if (time > releaseOrderTime){
						logger.error("Order " + orderNumber + " is being unlocked automatically, has been locked for " + hours + ":" + minutes + ":" + seconds);
						activitiesService.unlockOrder(new WebAppOrder( order ));
						
						List<ActRouting> statusList = orderDAO.getAllOrderStatus(order);
						byte prevStatus = statusList.get(1).getStsRouting().getStsRoutingId();
						
						//get current status before changing status to being worked
						//byte currentStatus = orderDAO.getCurrentOrderStatus(order);

						String sentToShopStr = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);
						String rjctByBmtStr = bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT);
						String cancConfStr = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF);
						String acceptStr = bomProperties.getProperty(BOMConstants.ORDER_ACCEPTED_BY_SHOP);
						String dlouStr = bomProperties.getProperty(BOMConstants.ORDER_OUT_FOR_DELIVERY);
						String dlcaStr = bomProperties.getProperty(BOMConstants.ORDER_DELIVERY_ATTEMPTED);
						String dlcfStr = bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP);
						String wfrStr = bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP);
						String researchStr = bomProperties.getProperty(BOMConstants.ORDER_BEING_RESEARCHED);
						String outsideStr = bomProperties.getProperty(BOMConstants.ORDER_COMPLETED_OUTSIDE_BOM);
						String completeStr = bomProperties.getProperty(BOMConstants.ORDER_COMPLETED);


						if((prevStatus!=Byte.valueOf(sentToShopStr))
								&&(prevStatus!=Byte.valueOf(rjctByBmtStr))
								&&(prevStatus!=Byte.valueOf(cancConfStr))
								&&(prevStatus!=Byte.valueOf(acceptStr))
								&&(prevStatus!=Byte.valueOf(dlouStr))
								&&(prevStatus!=Byte.valueOf(dlcaStr))
								&&(prevStatus!=Byte.valueOf(dlcfStr))
								&&(prevStatus!=Byte.valueOf(wfrStr))
								&&(prevStatus!=Byte.valueOf(researchStr))
								&&(prevStatus!=Byte.valueOf(outsideStr))
								&&(prevStatus!=Byte.valueOf(completeStr)))

							
						{
							bomService.sendOrderToQueue(order.getOrderNumber(), BOMConstants.TLO_QUEUE);
						}else{
							orderDAO.persistOrderactivityByUser( userName, BOMConstants.ACT_ROUTING, Byte.toString(prevStatus), orderNumber, BOMConstants.TLO_QUEUE );

						}
					}
					
				}
				
			}
			
			
			
			
			
		

	}

	/**
	 *  every 2 hours at :45
	 */
	@Override
	public void auditOrdersTask() throws Exception {
		
		List<String> bloomLinkOrders = new ArrayList<String>();
		List<String> bomOrders = new ArrayList<String>();
		
		String startDate = DateUtil.toXmlNoTimeFormatString(new Date());
		String endDate = DateUtil.tomorrowsNoTimeDate();
		
		int numOfMessages=0;
		
		
		AuditInterface ai = new AuditInterface();
		AuditRequest auditRequest = new AuditRequest();
		Security security = new Security();
		AuditSearchOptions auditSearchOptions = new AuditSearchOptions();
		AuditMessagesByDateRange auditMessagesByDateRange = new AuditMessagesByDateRange();

		
		security .setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));
		
		auditRequest.setSecurity(security);
		auditRequest.setAuditSearchOptions(auditSearchOptions );
		auditMessagesByDateRange.setStartDate(startDate);
		auditMessagesByDateRange.setEndDate(endDate);
		auditMessagesByDateRange.setMessageDirection("INBOUND");
		
		auditSearchOptions.getAuditSearchOptionsGroup().add(auditMessagesByDateRange);

		
		ai.setAuditRequest(auditRequest);
		
		
		String xml = transformService.convertToAuditXML(ai);
		
		System.out.println(xml);
		
		AuditResponse auditResponse = sendMessageDirect(xml);
		
		if (auditResponse.getErrors().getError().size()==0){
			numOfMessages = auditResponse.getMessages().getMessage().size();

			System.out.println("num Of Messages entered Bloomlink today: " + numOfMessages);

			List<Message> messages = auditResponse.getMessages().getMessage();

			int numOfOrders=0;
		for(Message message: messages){
			logger.info("********BloomLink Order Audit********");
			if (message.getMessageType().equals(BOMConstants.ORDER_MESSAGE_TYPE)){
				logger.info(message.getBmtOrderNumber());
				bloomLinkOrders.add(message.getBmtOrderNumber());
				numOfOrders++;
				}

			} 
			System.out.println("num Of Orders entered Bloomlink today: " + numOfOrders);

			List<Bomorder> bomorders = orderDAO.getOrdersEnteredToday(new Date());
			System.out.println("Num of orders entered bom today: " +  bomorders.size());
			logger.info("********BOM Order Audit********");
			for( Bomorder bomorder: bomorders){
				logger.info(bomorder.getOrderNumber());
             	bomOrders.add(bomorder.getOrderNumber());

			}

			if (numOfOrders!=bomorders.size()){
				logger.error("number of orders entered in Bloomlink(" + numOfOrders +") is not equal to number of orders entered in FOM(" + bomorders.size() +")");
             	new SendGridEmailer("number of orders entered in Bloomlink(" + numOfOrders +") is not equal to number of orders entered in FOM(" + bomorders.size() +")");
				displayMissingOrders(bloomLinkOrders,bomOrders);
			}
			else{
				logger.debug("number of orders entered in Bloomlink(" + numOfOrders +") is equal to number of orders entered in FOM(" + bomorders.size() +")");
			}
		}
		else{
			 List<com.bloomnet.bom.common.jaxb.audit.Error> errors = auditResponse.getErrors().getError();

				for (com.bloomnet.bom.common.jaxb.audit.Error error : errors) {
					logger.debug(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.debug(error.getErrorMessage());
					//throw new Exception("BloomLink Exception: " +error.getErrorMessage());


				}
		}
	}

	
	@Override
	public void auditMessagesTask() throws Exception {
		
		List<String> bloomLinkMessages = new ArrayList<String>();
		List<String> bomMessages = new ArrayList<String>();
		
		String startDate = DateUtil.toXmlNoTimeFormatString(new Date());
		String endDate = DateUtil.tomorrowsNoTimeDate();
		
		int numOfMessages=0;
		
		
		AuditInterface ai = new AuditInterface();
		AuditRequest auditRequest = new AuditRequest();
		Security security = new Security();
		AuditSearchOptions auditSearchOptions = new AuditSearchOptions();
		AuditMessagesByDateRange auditMessagesByDateRange = new AuditMessagesByDateRange();

		
		security .setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));
		
		auditRequest.setSecurity(security);
		auditRequest.setAuditSearchOptions(auditSearchOptions );
		auditMessagesByDateRange.setStartDate(startDate);
		auditMessagesByDateRange.setEndDate(endDate);
		auditMessagesByDateRange.setMessageDirection("INBOUND");
		
		auditSearchOptions.getAuditSearchOptionsGroup().add(auditMessagesByDateRange);

		
		ai.setAuditRequest(auditRequest);
		

		String xml = transformService.convertToAuditXML(ai);
		
		System.out.println(xml);
		
		AuditResponse auditResponse = sendMessageDirect(xml);
		
		if (auditResponse.getErrors().getError().size()==0){
			numOfMessages = auditResponse.getMessages().getMessage().size();

			System.out.println("num Of Messages entered Bloomlink today: " + numOfMessages);

			List<Message> messages = auditResponse.getMessages().getMessage();

			int numOfNonOrders=0;
		for(Message message: messages){
			logger.info("********BloomLink Messages Audit********");
			if (!message.getMessageType().equals(BOMConstants.ORDER_MESSAGE_TYPE)){
				logger.info(message.getBmtOrderNumber());
				bloomLinkMessages.add(message.getBmtOrderNumber());
				numOfNonOrders++;
				}

			}
			System.out.println("num Of non Orders messages entered Bloomlink today: " + numOfNonOrders);

			 List<BloomNetMessage> messageslist = messageDAO.getMessagesEnteredToday(new Date());
			System.out.println("Num of orders entered bom today: " +  messageslist.size());
			logger.info("********BOM Order Audit********");
			for( BloomNetMessage bmtmessage: messageslist){
				logger.info(bmtmessage.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
             	bomMessages.add(bmtmessage.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());

			}

			if (numOfNonOrders!=messageslist.size()){
				logger.error("number of non order messages entered in Bloomlink(" + numOfNonOrders +") is not equal to number of messages entered in FOM(" + messageslist.size() +")");
             	new SendGridEmailer("number of non order messages entered in Bloomlink(" + numOfNonOrders +") is not equal to number of messages entered in FOM(" + messageslist.size() +")");
				displayMissingMessages(bloomLinkMessages,bomMessages);
			}
			else{
				logger.debug("number of orders entered in Bloomlink(" + numOfNonOrders +") is equal to number of orders entered in FOM(" + messageslist.size() +")");
			}
		}
		else{
			 List<com.bloomnet.bom.common.jaxb.audit.Error> errors = auditResponse.getErrors().getError();

				for (com.bloomnet.bom.common.jaxb.audit.Error error : errors) {
					logger.debug(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.debug(error.getErrorMessage());
					//throw new Exception("BloomLink Exception: " +error.getErrorMessage());


				}
		}
	}
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void wfrTask() throws Exception {
		
		//20 minutes test       
		//long releaseOrderTime = 7200;
		
		long releaseOrderTime = Long.parseLong(bomProperties.getProperty("wfr.release"));
    	String userName          = UserDAO.INTERNAL_USER;
    	
    	

    	logger.debug("Checking for orders Waiting for Response that need to be worked");
		
		//get orders that are waiting for response
		List<Bomorder> orders = orderDAO.getOrdersByStatus(BOMConstants.LOGACALL_WFR);
		
		for (Bomorder order: orders){
			
			String orderNumber = order.getOrderNumber();

			 String deliveryDate = DateUtil.toXmlNoTimeFormatString(order.getDeliveryDate());
			
	    	if (isSameDayOrder(deliveryDate)){
				logger.debug("Order " + orderNumber + " is a same day order ");
	    		releaseOrderTime = Long.parseLong(bomProperties.getProperty("wfr.sameday"));

	    	}

			Orderactivity activity = orderDAO.getLatestOrderActivity(order);
			ActRouting actRouting = activity.getActRouting();
			StsRouting stsRouting = actRouting.getStsRouting();
			String routingDesc = stsRouting.getDescription();
			String status = actRouting.getStatus();
				
				//get the difference between the time order was set to wfr to now
				// if time is greater than send order back to queue 
				Date lock = activity.getCreatedDate();
				Date now = new Date();
				
				long wfrDuration = lock.getTime() - now.getTime();
				long time = wfrDuration * (-1)/ 1000;
				String seconds = Integer.toString((int)(time % 60));   
				String minutes = Integer.toString((int)((time % 3600) / 60));   
				String hours = Integer.toString((int)(time / 3600));   
				
				String rseconds = Integer.toString((int)(releaseOrderTime % 60));   
				String rminutes = Integer.toString((int)((releaseOrderTime % 3600) / 60));   
				String rhours = Integer.toString((int)(releaseOrderTime / 3600));

				if (time > releaseOrderTime){
					logger.error("Order " + orderNumber + " has been waiting for response for " + hours + ":" + minutes + ":" + seconds +" and is now being sent back to be worked");
				
					bomService.sendOrderToQueue(order.getOrderNumber(), BOMConstants.TLO_QUEUE);
					
				}		
			
		}
		

	}

	/**
	 *  every  2 hours at :15
	 */
	@Override
	public synchronized void resendOrderTask() throws Exception {
		
		logger.debug("****resending orders****");
		long releaseOrderTime = 360;
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getFrozenAutomatedOrders();
		
		
		
		for(BomorderviewV2 order: orders){
			 BomorderviewV2Id orderId = order.getId();
			
			String status = orderId.getStatus();
			String childOrderNumber = orderId.getChildOrderNumber();

			if(childOrderNumber.equals("0") ){
				
			
				String route = orderId.getRoute();
				//if((route.equals("BMTFSI"))||(route.equals("TFSI"))){

					String orderNumber = orderId.getParentOrderNumber();
					Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

					Orderactivity activity = orderDAO.getLatestOrderActivity(bomorder);
					
					Date lock = activity.getCreatedDate();
					Date now = new Date();

					long lockedDuration = lock.getTime() - now.getTime();
					long time = lockedDuration * (-1)/ 1000;
					String seconds = Integer.toString((int)(time % 60));   
					String minutes = Integer.toString((int)((time % 3600) / 60));   
					String hours = Integer.toString((int)(time / 3600));   

					String rseconds = Integer.toString((int)(releaseOrderTime % 60));   
					String rminutes = Integer.toString((int)((releaseOrderTime % 3600) / 60));   
					String rhours = Integer.toString((int)(releaseOrderTime / 3600));

					if (time > releaseOrderTime){


						logger.error("Order " + orderNumber + " has not been processed for " + hours + ":" + minutes + ":" + seconds);
						bomService.sendOrderToQueue(orderNumber, route);

					}

				//}
			}
		}
		

	}
	
	private void displayMissingOrders(List<String> bloomLinkOrders, List<String> bomOrders) {
		
		List<String> missingOrder = new ArrayList<String>();
	for (String bloomLinkOrder: bloomLinkOrders){
		
		if (!bomOrders.contains(bloomLinkOrder)){
			missingOrder.add(bloomLinkOrder);
		}
		
		
	}
	System.out.println("Orders " + missingOrder.toString() + " are not in FOM");
	logger.error(missingOrder.size() + " Order(s) "+ missingOrder.toString() + " are not in FOM");
 	try {
		new SendGridEmailer(missingOrder.size() + " Order(s) "+ missingOrder.toString() + " are not in FOM");
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	}
	
	
	private void displayMissingMessages(List<String> bloomLinkMessages, List<String> bomMessages) {
		
		List<String> missingOrder = new ArrayList<String>();
	for (String bloomLinkMessage: bloomLinkMessages){
		
		if (!bomMessages.contains(bloomLinkMessage)){
			missingOrder.add(bloomLinkMessage);
		}
		
		
	}
	System.out.println("Orders " + missingOrder.toString() + " are not in FOM");
	logger.error(missingOrder.size() + " Order(s) "+ missingOrder.toString() + " are not in FOM");
	try {
		new SendGridEmailer(missingOrder.size() + " Order(s) "+ missingOrder.toString() + " are not in FOM");
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	
	
}
	
	/**
	 * Checks if order is a same day order
	 * 
	 * @param deliveryDate
	 * @return
	 */
	public boolean isSameDayOrder(String deliveryDate) {

		boolean sameDay = false;

		String today = DateUtil.toXmlNoTimeFormatString(new Date());

		if (today.equals(deliveryDate)) {
			sameDay = true;
		}

		return sameDay;

	}
	
private AuditResponse sendMessageDirect(String sendingOrderXML ) throws Exception {

		
		String dataEncoded = "";
		String post = "";
		String response = "";
		int numOfErrors = 0;

		AuditResponse auditResponse = new AuditResponse();


		try {

			dataEncoded = URLEncoder.encode(sendingOrderXML, "UTF-8");
			post = fsiProperties.getProperty("fsi.endpoint")
					+ BOMConstants.AUDIT_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("xml encoded: " + dataEncoded);
				logger.debug("sendMessage xml " + sendingOrderXML);
				logger.debug("audit " + post);

			}
			response = messagingService.sendRequest(post);

			if (response==null){
				throw new Exception("No response from BloomLink");
			}

			AuditInterface auditInterface = transformService.convertToJavaAudit(response);
			
			auditResponse = auditInterface.getAuditResponse();
			
			numOfErrors = auditResponse.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				 List<com.bloomnet.bom.common.jaxb.audit.Error> errors = auditInterface.getAuditResponse().getErrors().getError();

				for (com.bloomnet.bom.common.jaxb.audit.Error error : errors) {
					if (error.getErrorCode().equals("62")&&error.getDetailedErrorCode().equals("7004")){
						break;
					}
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());
					new SendGridEmailer(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());

					//throw new Exception("BloomLink Exception: " +error.getErrorMessage());


				}
			}

		}
		catch (UnsupportedEncodingException e) {

			logger.error(e);
			
		}

		return auditResponse;
	}
@Override
public void checkWebservicesTask(){
	
	try{
		checkBMT("BloomNet");
		checkTFSI("Teleflora");
	}catch(Exception e){
		e.printStackTrace();
		logger.error(e.getMessage());
	}
	
	
	
	
	
}

private void checkTFSI(String networkName)  {
	
	try{
	logger.debug("checking : " + networkName+ " network");

	Network network = shopDAO.getNetwork( networkName );
	WebserviceMonitor wsMonitor = shopDAO.getLastQuery( network);
	
	if (wsMonitor!=null){
	
	long releaseOrderTime = 360;
	Date lock = wsMonitor.getLastquery();
	Date now = new Date();

	long lockedDuration = lock.getTime() - now.getTime();
	long time = lockedDuration * (-1)/ 1000;
	String seconds = Integer.toString((int)(time % 60));   
	String minutes = Integer.toString((int)((time % 3600) / 60));   
	String hours = Integer.toString((int)(time / 3600));   

	String rseconds = Integer.toString((int)(releaseOrderTime % 60));   
	String rminutes = Integer.toString((int)((releaseOrderTime % 3600) / 60));   
	String rhours = Integer.toString((int)(releaseOrderTime / 3600));

	if (time > releaseOrderTime){

		logger.error(" Messages from " + networkName+ " has not been processed for " + hours + ":" + minutes + ":" + seconds);
		//new SendGridEmailer(" Messages from " + networkName+ " has not been processed for " + hours + ":" + minutes + ":" + seconds);

	}

	}else{
		logger.debug("No entries for " + networkName +" network" );
	}
	}catch(Exception e){
		System.out.println(e.getMessage());
	}
	
}

private void checkBMT(String networkName)   {
	
	try{
	logger.debug("checking : " + networkName+ " network");

	Network network = shopDAO.getNetwork( networkName );
	WebserviceMonitor wsMonitor = shopDAO.getLastQuery( network);
	
	if (wsMonitor!=null){
		System.out.println("****************hello");
	long releaseOrderTime = Long.parseLong(bomProperties.getProperty("bmt.lastquery"));
	Date lock = wsMonitor.getLastquery();
	Date now = new Date();
	System.out.println("****************hello");


	long lockedDuration = lock.getTime() - now.getTime();
	long time = lockedDuration * (-1)/ 1000;
	String seconds = Integer.toString((int)(time % 60));   
	String minutes = Integer.toString((int)((time % 3600) / 60));   
	String hours = Integer.toString((int)(time / 3600));   

	String rseconds = Integer.toString((int)(releaseOrderTime % 60));   
	String rminutes = Integer.toString((int)((releaseOrderTime % 3600) / 60));   
	String rhours = Integer.toString((int)(releaseOrderTime / 3600));
	
	System.out.println(" Messages from " + networkName+ " last processed  " + hours + ":" + minutes + ":" + seconds);
	System.out.println(" Messages from " + networkName+ " released  " + rhours + ":" + rminutes + ":" + rseconds);
	
	System.out.println("time: " + time+"\n"+ "releaseOrderTime: "+ releaseOrderTime);



	if (time > releaseOrderTime){

		logger.error(" Messages from " + networkName+ " has not been processed for " + hours + ":" + minutes + ":" + seconds);
		//new SendGridEmailer(" Messages from " + networkName+ " has not been processed for " + hours + ":" + minutes + ":" + seconds);
	}

	}else{
		logger.debug("No entries for " + networkName +" network" );
	}

	}catch(Exception e){
		System.out.println(e.getMessage());
	}
}
}
