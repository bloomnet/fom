/**
 * 
 */
package com.bloomnet.bom.mvc.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.FsiSecurityElementInterface;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.entity.Useractivity;
import com.bloomnet.bom.common.entity.Userrole;
import com.bloomnet.bom.common.util.PasswordHasher;
import com.bloomnet.bom.mvc.businessobjects.ApplicationView;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.businessobjects.WebAppUserInterface;
import com.bloomnet.bom.mvc.exceptions.UserNotFoundException;
import com.bloomnet.bom.mvc.exceptions.UserWrongPasswordException;
import com.bloomnet.bom.mvc.exceptions.WorkingShopNotFoundException;
import com.bloomnet.bom.mvc.service.AuthUserDetailsService;
import com.bloomnet.bom.mvc.service.BomService;

/**
 * An implementation of the authentication service.
 * 
 * This implementation will find a user(s) in the DB by email and
 * validate this user(s) by a password.
 * 
 * On success the user object will be populated with a list of views
 * which match the user roles.
 * 
 * View access settings are loaded from a configuration file.
 * 
 * @author Danil Svirchtchev
 */
@Service("authService")
@Transactional(propagation = Propagation.SUPPORTS)
public class DbUserauthenticator implements AuthUserDetailsService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( DbUserauthenticator.class );
	
    @Autowired private FsiSecurityElementInterface securityElement;
	@Autowired private Properties viewAccess;
	@Autowired private UserDAO userDao;
	@Autowired private ShopDAO shopDAO;	 
    @Autowired private BomService bomService;
    
    PasswordHasher passwordHasher = new PasswordHasher();
	
	public DbUserauthenticator(){}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AuthUserDetailsService#authenticate(org.springframework.security.core.userdetails.User)
	 */
	@Override
	@Transactional(readOnly = true)
	public WebAppUser authenticate(WebAppUserInterface formUser) throws HibernateException,
																	    UserNotFoundException,
																	    UserWrongPasswordException, 
																	    WorkingShopNotFoundException {

		User dbUser = userDao.getUserByUserName( formUser.getUserName() );
		
		if (dbUser == null) {
			
			throw new UserNotFoundException( formUser.getUserName() );
		}
		
		WebAppUser webAppUser = validateUser( formUser, dbUser );
		
		Shop shop = shopDAO.getShopByCode(securityElement.getShopcode());
		
		if ( shop == null ) {
			
			throw new WorkingShopNotFoundException( securityElement.getShopcode() );
		}
		
		return webAppUser;
	}

	
	/**
	 * Validates a WebAppUser.
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private WebAppUser validateUser( WebAppUserInterface formUser,
			                         UserInterface dbUser ) throws UserWrongPasswordException {
		if(dbUser.getUserId()!=1 &&
			dbUser.getUserId()!=2 &&
			dbUser.getUserId()!=3 &&
			(dbUser.getActive() == null || !dbUser.getActive().equals("N"))){
			
			String password = passwordHasher.hash(formUser.getPassword());
			
			if( password.equalsIgnoreCase(dbUser.getPassword())){
				
				HashMap<String, String> propMap = new HashMap<String, String>((Map) viewAccess);
			    Set<Map.Entry<String, String>> propSet;
			    propSet = propMap.entrySet();
				return (WebAppUser) populateUserViews(dbUser, propSet);
			}
			else {
				
				throw new UserWrongPasswordException();
			}
		}else {
			throw new UserWrongPasswordException();
		}
	}
	
	private WebAppUserInterface populateUserViews( UserInterface dbUser, 
										           Set<Map.Entry<String, String>> propSet) {
		
		Set<ApplicationView> allowedViews = new HashSet<ApplicationView>();
		Set<Userrole>        userRoles    = dbUser.getUserrolesForUserId();
		
	    for (Map.Entry<String, String> me : propSet) {
	    	
	    	ApplicationView view = new ApplicationView();
	    	view.setUrl(me.getKey());
	    	
	    	String[] roleNames = me.getValue().split(",");
	        
	    	for( int i = 0; i < roleNames.length; i++ ) {
	    		
	    		String definedRoleDescription = roleNames[i];
	    		
	    		for(Userrole userRole : userRoles){
	    			
	    			if(definedRoleDescription.equalsIgnoreCase(userRole.getRole().getDescription())){
	    				allowedViews.add(view);
	    			}
	    		}
	    	}
	    }
	    
	    // Construct the web application user
	    WebAppUser webAppUser = new WebAppUser();
	    webAppUser.initialize( dbUser );
	    webAppUser.setUserViews( allowedViews );
	    
		return webAppUser;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateUserDetails( UserInterface user, 
								   Uactivitytype activity ) throws HibernateException {
		
		userDao.persistUserActivity((User) user, activity);
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AuthUserDetailsService#getUserActivities(java.lang.Long)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Useractivity> getUserActivities(Long userId) throws HibernateException {
		
		List<Useractivity> activities = new ArrayList<Useractivity>();
		
		User dbUser = userDao.getUserById(userId);
		
		Set<Useractivity> userActivities = dbUser.getUseractivities();

		for( Useractivity actyvity : userActivities ){
			activities.add(actyvity);
		}
		
		return activities;
	}
}
