/**
 * 
 */
package com.bloomnet.bom.mvc.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteriaExtended;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.exceptions.SearchParameterNotSupportedException;
import com.bloomnet.bom.mvc.service.AdminService;

/**
 * @author Danil Svirchtchev
 *
 */
@Service("adminService")
@Transactional( propagation = Propagation.SUPPORTS )
public class AdminServiceImpl implements AdminService {
    
    // Define a static logger variable
    static Logger logger = Logger.getLogger( AdminServiceImpl.class );
    
    // Injected DAO implementations
    @Autowired private OrderDAO orderpDAO;
    
    private AdminServiceImpl(){}

    
    /* (non-Javadoc)
     * @see com.bloomnet.bom.mvc.service.AdminService#getAllOrders()
     */
	@Override
    @Transactional( readOnly = true )
    @Cache( usage = CacheConcurrencyStrategy.READ_ONLY )
    public List<WebAppOrder> getAllOrders() throws Exception {

        return convert( orderpDAO.getAllParentOrders(), true );
    }

    
    /* (non-Javadoc)
     * @see com.bloomnet.bom.mvc.service.AdminService#getAllOrders()
     */
    @Override
    @Transactional( readOnly = true )
    @Cache( usage = CacheConcurrencyStrategy.READ_ONLY )
	public List<WebAppOrder> getOrders( List<Bomorder> ordersToShow, int startIndex, int pageSize ) throws Exception {
		
    	List<WebAppOrder> results = new ArrayList<WebAppOrder>();
    	
    	List<Bomorder> subResults = new ArrayList<Bomorder>();
    	
    	
    	for(int ii=startIndex; ii<pageSize; ++ii){
    		try{
    			subResults.add(ordersToShow.get(ii));
    		}catch(Exception ee){}
    	}
    	
    	results.addAll(convert( subResults, false ));
    	
		return results;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AdminService#getOrders(java.util.Map)
	 */
	@Override
	@Transactional( readOnly = true )
	public List<Bomorder> getManagerOrders( Map<String, String> searchParams ) throws SearchParameterNotSupportedException, 
																				  Exception {
		
		List<WebAppOrder> result = new ArrayList<WebAppOrder>();
    	
		
		// validate the parameters map first
		validateCriterias(searchParams);
		
		List<Bomorder> orders = orderpDAO.managerSearch(searchParams);
		List<Bomorder> parentorders = new ArrayList<Bomorder>();

		
		if (orders!=null){
		for (Bomorder order:orders){
			if (order!=null){
				long bomorderid = order.getBomorderId();
				long parentorderid = order.getParentorderId();
				if (bomorderid==parentorderid){
					parentorders.add(order);
				}
				
			}
		}
		
			
		}
		//result = convert(parentorders, true);
		
		
		return parentorders;
	}
	
	@Override
	@Transactional( readOnly = true )
	@Cache( usage = CacheConcurrencyStrategy.NONE )
	public List<Bomorder> getOrders( Map<String, String> searchParams ) throws SearchParameterNotSupportedException, 
																				  Exception {

		List<Bomorder> result = new ArrayList<Bomorder>();
		
		// validate the parameters map first
		validateCriterias(searchParams);
		
		// return orders with matching order number
		if ( searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_NUMBER ) ) {
			
			final String orderNumber = searchParams.get( SearchCriteriaExtended.KEY_ORDER_NUMBER );
			List<Bomorder> orders = new ArrayList<Bomorder>();
			Bomorder order = orderpDAO.getParentOrderByOrderNumber( orderNumber );
			if(order != null && order.getOrderNumber() != null)
				result.add(order);
		}
		
		// return orders with matching zip code
		if ( searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_ZIPCODE ) ) {
			
			final String zipCode = searchParams.get( SearchCriteriaExtended.KEY_ORDER_ZIPCODE );
			//result.addAll( convert( orderpDAO.getOrdersByZipCode( zipCode );
		}
		
		// return orders with matching order delivery date
		if ( searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_DATE ) ) {
			
			// TODO: Add date validation
			final String date = searchParams.get( SearchCriteriaExtended.KEY_ORDER_DATE );
			
			// TODO: Fix hardcoded value
	        SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yy" );
	        // we will now try to parse the string into date form
	        try {
	            
	            result = new ArrayList<Bomorder>();
	        	Date testDate = sdf.parse(date);
	            
	        	result = orderpDAO.getOrdersByDate( testDate );
	            
	        }
	        catch ( ParseException e ) {
	            
	            throw new Exception( "Please enter date using MM/dd/yy format" );
	        }
		}
		
		return result;
	}
	
	
	/**
	 * @param orders
	 * @return
	 * @throws Exception
	 */
	private List<WebAppOrder> convert( List<Bomorder> orders, boolean reverse ) throws Exception {
		
		List<WebAppOrder> result = new ArrayList<WebAppOrder>();

        for ( Bomorder entity : orders ) {
            
            result.add( new WebAppOrder( entity ) );
        }
        
        Collections.sort ( result, OrderDAO.ORDER_BY_DELIVERY_DATE );
        if(reverse) Collections.reverse( result );
        
        return result;
	}
	
	
	/**
	 * A search criteria parameters check, in case the form backing object is changed.
	 * 
	 * @param searchParams
	 * @throws SearchParameterNotSupportedException
	 */
	private void validateCriterias( Map<String, String> searchParams ) throws SearchParameterNotSupportedException {
		
		StringBuffer notSupportedSearchFields = new StringBuffer();
		
		for ( Iterator<Entry<String, String>> it = searchParams.entrySet().iterator(); it.hasNext(); ) {
			
		    Entry<String, String> entry = (Entry<String, String>)it.next();
		    
		    String key = entry.getKey();
		    
		    if ( ( key.equals( SearchCriteriaExtended.KEY_ORDER_DATE ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_TO_DATE ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_FULFILLING_SHOP ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_NUMBER ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_OCCASION ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_SENDING_SHOP ) ||
		    	   key.equals( SearchCriteriaExtended.KEY_ORDER_ZIPCODE ) ) == false ) {
		    	
		    	notSupportedSearchFields.append( key + " " );
		    }
		}
		
		if ( notSupportedSearchFields.length() > 0 ) {
			
			throw new SearchParameterNotSupportedException( notSupportedSearchFields.toString() );
		}
	}
}
