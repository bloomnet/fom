package com.bloomnet.bom.mvc.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.mvc.AbstractController;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.bloomnet.bom.common.entity.Files;
import com.bloomnet.bom.common.dao.FilesDAO;

import java.util.List;

public class FilesService  {
	
	@Autowired private FilesDAO filesDAO;

    String query = null;

    /**
     * find
     */
    public Files find(int id) {

        try {
      
        	Files files = filesDAO.find(id);
        	
        	return files;
          
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * listAll
     */
   /* public List<Files> listAll() {
        query = "select id, filename, notes, type from files";

        try{
            List<Files> files = getJdbcTemplate().query(query, new BeanPropertyRowMapper(Files.class));

            return files;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }*/

    /**
     * save
     */
    /*public void save(final Files file) {
        query = "insert into files (filename, notes, type, file) values (?, ?, ?, ?)";

        try {
            synchronized(this) {
                getJdbcTemplate().update(new PreparedStatementCreator() {

                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                        PreparedStatement statement = con.prepareStatement(query);
                        statement.setString(1, file.getFilename());
                        statement.setString(2, file.getNotes());
                        statement.setString(3, file.getType());
                        statement.setBytes(4, file.getFile());
                        return statement;
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
*/
    /**
     * delete
     */
   /* public void delete(int id) {
        query = "delete from files where id = ?";

        try {
            getJdbcTemplate().update(query, new Object[] {id});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/
}
