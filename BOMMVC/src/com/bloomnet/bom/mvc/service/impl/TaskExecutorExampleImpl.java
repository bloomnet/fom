package com.bloomnet.bom.mvc.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.bloomnet.bom.mvc.service.TaskExecutorExample;
public class TaskExecutorExampleImpl implements TaskExecutorExample {
	
	 // Define a static logger variable
    static Logger logger = Logger.getLogger( TaskExecutorExampleImpl.class );
	
	//private TaskExecutor taskExecutor;
	
/*	public TaskExecutorExampleImpl(TaskExecutor taskExecutor) {
	    this.taskExecutor = taskExecutor;
	  }


	
	public void printMessages() {
		logger.debug("inside printMessages()");
	    for(int i = 0; i < 25; i++) {
	      taskExecutor.execute(new MessagePrinterTask("Message" + i));
	    }
	}*/

	
	private class MessagePrinterTask implements Runnable {

	    private String message;

	    public MessagePrinterTask(String message) {
	      this.message = message;
	    }

	    public void run() {
	      logger.debug(message);
	    }

	  }

	@Override
	public void printMessages() {
		// TODO Auto-generated method stub
		
	}


}
