/**
 * 
 */
package com.bloomnet.bom.mvc.service.impl;

import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.CallDispDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.PaymentDAO;
import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.PaymentInterface;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder.OrderNote;
import com.bloomnet.bom.mvc.service.ActivitiesService;

/**
 * Service for handeling activities on an order
 * 
 * @author Danil Svirchtchev
 *
 */
@Service("activitiesService")
public class ActivitiesServiceImpl extends AbstractService implements ActivitiesService {
	

	// Define a static logger variable
    static Logger logger = Logger.getLogger( ActivitiesServiceImpl.class );
    
	
	// Injected property
	@Autowired private Properties bomProperties;
	@Autowired private Properties fsiProperties;
	
	
	// Injected DAO implementations
	@Autowired private OrderDAO    orderDAO;
	@Autowired private PaymentDAO  paymentDAO;
	@Autowired private RoutingDAO  routingDAO;
	@Autowired private ShopDAO     shopDAO;
	@Autowired private CallDispDAO callDispDAO;
	
	@Autowired private TransformService transformService;	

	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#lockOrder(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.mvc.businessobjects.WebAppUser)
	 * 
	 * TODO: complete implementation as needed for carrying over nested objects
	 * 
	 */
	@Override
	public synchronized WebAppOrder lockOrder( WebAppOrder order, UserInterface user ) throws Exception {
		
		Bomorder o = orderDAO.getOrderByOrderId(order.getBomorderId());
		User userTemp = o.getUser();
		long userId = 0;
		if(userTemp != null){
			userId = userTemp.getUserId();
		}
		if( userId > 0 && userId != user.getUserId()){
			return null;
		}
		
		final PaymentInterface pi = order;

		order.setUser( (User) user );

		return new WebAppOrder ( orderDAO.updateOrder( order.getEntity() ), pi );
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#unlockOrder(com.bloomnet.bom.mvc.businessobjects.WebAppOrder)
	 * 
	 * TODO: complete implementation as needed for carrying over nested objects
	 * 
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public WebAppOrder unlockOrder( WebAppOrder order ) throws Exception {
		
		final PaymentInterface pi = order;
		
		order.setUser( null );

		return new WebAppOrder ( orderDAO.updateOrder( order.getEntity() ), pi );
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#persistActivity_Routing(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.mvc.businessobjects.WebAppUser)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void persistActivityRouting( WebAppOrder order, UserInterface user, String status, String destination ) throws Exception {

		if ( order.getBomorderId() != null && user != null && status != null && destination != null ) {
			
			// create order activity
			Oactivitytype actType   = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_ROUTING );
			StsRouting    actStatus = routingDAO.getStatusById( bomProperties.getProperty( status ) );
			
			Orderactivity activity = new Orderactivity( actType );
			activity.setBomorder( order );
			activity.setUser( (User) user );
	
			order.setOrderactivity( activity );
			order.setStsRouting( actStatus );
			order.setVirtualQueue( destination );
			
			routingDAO.persistRouting( order );
		}
		else {
			
			throw new Exception( "Can not save activity without order, user, status and destination" );
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#persistActivityRouting(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.common.entity.UserInterface, java.lang.String, java.lang.String, boolean)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void persistActivityRouting( WebAppOrder order, 
			                            UserInterface user, 
			                            String status, 
			                            String destination, 
			                            boolean updateChildOrders ) throws Exception {

		if( updateChildOrders ) {
			
			for( Bomorder bo : order.getBomorders() ) {
				
				persistActivityRouting( new WebAppOrder( bo ), user, status, destination );
			}
		}
		else {
			
			persistActivityRouting( order, user, status, destination );
		}
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#persistActivityPayment(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.common.entity.UserInterface, java.lang.String)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void persistActivityPayment( WebAppOrder order, UserInterface user, String status ) throws Exception {

		if ( order.getBomorderId() != null && user != null && status != null ) {
			
			// create order activity
			Oactivitytype m_type   = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_PAYMENT );
			StsPayment    m_status = paymentDAO.getStatusById( bomProperties.getProperty( status ) );
			
			Orderactivity activity = new Orderactivity( m_type );
			activity.setBomorder( order );
			activity.setUser( (User) user );
	
			order.setOrderactivity( activity );
			order.setStsPayment( m_status );
			
			paymentDAO.persistPayment( order );
		}
		else {
			
			throw new Exception( "Can not save activity without order, user and status" );
		}
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#persistActivityAudit(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.common.entity.UserInterface)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void persistActivityAudit( WebAppOrder order, UserInterface user ) throws Exception {
		
		if ( order.getBomorderId() != null && user != null ) {
			
			// create order activity
			Oactivitytype actType = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_AUDIT );
			
			Orderactivity activity = new Orderactivity( actType );
			activity.setBomorder( order );
			activity.setUser( (User) user );
			
			order.setOrderactivity( activity );
			
			orderDAO.persistOrderHistory( order );
			
			ForeignSystemInterface fsi = createFSIOrderFromBean( order.getBean(),false );
			
			String containsPerishables = "";
			String pickupCode = "";
			try{
				containsPerishables = order.getEntity().getOrderXml().split("<containsPerishables>")[1].split("</containsPerishables>")[0];
			}catch(Exception ee){}
			try {
				pickupCode = order.getEntity().getOrderXml().split("<pickupCode>")[1].split("</pickupCode>")[0];
			}catch(Exception ee){}
			
			fsi.getMessagesOnOrder().get(0).getMessageOrder().setContainsPerishables(containsPerishables);
			fsi.getMessagesOnOrder().get(0).getMessageOrder().setPickupCode(pickupCode);

			final String orderXml = transformService.convertToFSIXML( fsi );
			
			order.setOrderXml( orderXml );
			
			if( order.getBean().getNewDeliveryDate() != null )
				order.setDeliveryDate( order.getBean().getNewDeliveryDate() );
			
			if( order.getBean().getNewCity() != null )
				order.setCity( order.getBean().getNewCity() );
			
			if( order.getBean().getNewZip() != null )
				order.setZip( order.getBean().getNewZip() );

			order = lockOrder( order, user );
		}
		else {
			
			throw new Exception( "Can not save activity without order and user" );
		}
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.ActivitiesService#persistActivityOrderNote(com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.common.entity.UserInterface)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void persistActivityOrderNote( WebAppOrder order, UserInterface user ) throws Exception {
		
		if ( order.getBomorderId() != null && user != null ) {
			
			// create order activity
			Oactivitytype actType = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_LOGACALL );
			Calldisp      m_disp  = callDispDAO.getCallDispByDesc( BOMConstants.LOGACALL_ORDER_NOTE );
			
			Orderactivity activity = new Orderactivity( actType );
			activity.setBomorder( order );
			activity.setUser( (User) user );
			
			order.setOrderactivity( activity );
			order.setCalldisp(m_disp);
			
			orderDAO.persistOrderHistory( order );

			activity.setActLogacall( callDispDAO.persistCall( order ) );
			order.getOrderactivities().add( activity );
			
			OrderNote newNote = order.new OrderNote(null,null,null);
			final Date createDate = activity.getCreatedDate();

			newNote.setText(order.getLogAcallText());
			newNote.setCreatorName(activity.getUser().getUserName());
			newNote.setCreateDate(createDate);
			order.getNotes().add(newNote);
			
			order.setLogAcallText("");
		}
		else {
			
			throw new Exception( "Can not save activity without order and user" );
		}
	}


	@Override
	protected Properties getFsiProperties() {
		return fsiProperties;
	}	
	@Override
	protected ShopDAO getShopDAO() {
		return shopDAO;
	}
}
