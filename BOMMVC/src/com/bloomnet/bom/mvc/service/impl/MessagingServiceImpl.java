package com.bloomnet.bom.mvc.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActAudit;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.Error;
import com.bloomnet.bom.common.jaxb.fsi.Errors;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlca;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlou;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrderRelated;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.RedeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.MessageFactory;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.MessageOnOrderException;
import com.bloomnet.bom.mvc.exceptions.MessageTypeException;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.MessagingService;


/**
 * @author BloomNet Technologies
 *
 */
@Service("messagingService")
@Transactional( propagation = Propagation.SUPPORTS )
public class MessagingServiceImpl implements MessagingService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( MessagingServiceImpl.class );
    
    private MessageFactory msgFactory = MessageFactory.getInstance();
    
	static final String INTERNAL_USER = "FSI";
	
	// Injected DAO implementation
		@Autowired private UserDAO userDAO;	
    
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected property
	@Autowired private Properties fsiProperties;
        
	// Injected DAO implementations
	@Autowired private ShopDAO shopDAO;
	@Autowired private OrderDAO orderDAO;
	@Autowired private MessageDAO messageDAO;
	@Autowired private AgentService agentService;
	@Autowired private ActivitiesService activitiesService;
	@Autowired private TransformService transformService;	


	
	// Private constructor
	private MessagingServiceImpl() {}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.MessagingService#sendNewMessageToShop(com.bloomnet.bom.mvc.businessobjects.WebAppUser, com.bloomnet.bom.mvc.businessobjects.WebAppOrder, com.bloomnet.bom.mvc.businessobjects.WebAppActMessage)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void sendNewMessageToShop( WebAppUser user, WebAppOrder order, WebAppActMessage inMessage ) throws Exception {
		
		BloomNetMessage m_message = buildMessageObject( inMessage );

		// get the shop code
		Long sendingShopId = null;
		if(order.getSelectedMessage() != null){
			sendingShopId = order.getSelectedMessage().getShopBySendingShopId().getShopId();
		}
		else {
			sendingShopId = order.getShopBySendingShopId().getShopId();
		}
		//final String receivingShopCode = shopDAO.getShopCodeFromDefaultShopnetworks( sendingShopId );
		Shopnetwork BMTShopNetwork = shopDAO.getBloomNetShopnetwork(sendingShopId);
		Shopnetwork originalSendingShop = shopDAO.getBloomNetShopnetwork(order.getShopBySendingShopId().getShopId());
		//final String receivingShopCode = BMTShopNetwork.getShopCode();
		 String receivingShopCode = new String();
		 String fulfillingShopCode = new String();
		
		 if(originalSendingShop.getShopCode().equals(BOMConstants.WEBSITE_ORDER)){
			 fulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(order.getBloomnetOrderNumber());
			 receivingShopCode = fulfillingShopCode;
		  }
		 else{
			 receivingShopCode = BMTShopNetwork.getShopCode();

		 }
		// create security element
		Security security = new Security();
		security.setPassword( user.getSecurityElement().getPassword() );
		security.setShopCode( user.getSecurityElement().getShopcode() );
		security.setUsername( user.getSecurityElement().getUsername() );
		
		// create the identifiers element
		GeneralIdentifiers gi = new GeneralIdentifiers();
		if (originalSendingShop.getShopCode().equals(BOMConstants.WEBSITE_ORDER)){
			if (order.getOutboundOrderNumber()!=null){
			gi.setBmtOrderNumber( order.getOutboundOrderNumber() );
			}else{
				gi.setBmtOrderNumber( order.getBloomnetOrderNumber() );
			}
			Bomorder outboundOrder = orderDAO.getOrderByOrderNumber(order.getOutboundOrderNumber());
			gi.setBmtSeqNumberOfOrder( outboundOrder.getBmtOrderSequenceNumber() );
		}else{
			if (order.getBloomnetOrderNumber()==null){
				gi.setBmtOrderNumber( order.getBean().getOrderNumber() );

			}else{
				gi.setBmtOrderNumber( order.getBloomnetOrderNumber() );
			}
			if (order.getBean().getBmtSeqNumberOfOrder()==0){
				gi.setBmtSeqNumberOfOrder( order.getBmtOrderSequenceNumber() );

			}else{
				gi.setBmtSeqNumberOfOrder( String.valueOf( order.getBean().getBmtSeqNumberOfOrder() ) );
			}
		}
		gi.setExternalShopMessageNumber( BOMConstants.EXTERNAL_SHOP_NUMBER ); 

		
		Identifiers identifiers = new Identifiers();
		identifiers.setGeneralIdentifiers(gi);

		// set the meaaseg fields
		m_message.setMessageCreateTimestamp( DateUtil.toXmlFormatString( new Date() ) );
		m_message.setSendingShopCode( user.getSecurityElement().getShopcode() );
		m_message.setReceivingShopCode( receivingShopCode );
		 if(originalSendingShop.getShopCode().equals(BOMConstants.WEBSITE_ORDER)){
				m_message.setFulfillingShopCode( fulfillingShopCode );
		 }else{
		m_message.setFulfillingShopCode( user.getSecurityElement().getShopcode() );
		 }
		m_message.setIdentifiers( identifiers );
		m_message.setSystemType( BOMConstants.SYSTEM_TYPE );
		
		// prepare xml
		ForeignSystemInterface fsi = new ForeignSystemInterface();
		fsi.setSecurity(security);

		String xml = transformService.createForeignSystemInterface(inMessage.getMessageTypeId().toUpperCase(), m_message);
		//populateMessage( m_message, inMessage.getMessageTypeId() );
		//fsi.getMessagesOnOrder().add( messagesOnOrder  );
		
		//String xml = Transform.convertToFSIXML(fsi);
		
		final Long userId = user.getUserId();
		String status = bomProperties.getProperty(BOMConstants.MESSAGE_SENT_TO_SHOP);
		final String commmethod = BOMConstants.COMMMETHOD_PHONE;
		
		sendMessageandSave( userId,
							xml, 
							inMessage.getMessageTypeId(), 
				            status,
				            commmethod,
				            inMessage.getMessageText(),
				            m_message.getSendingShopCode(),
				            m_message.getReceivingShopCode());
		
		if (m_message.getMessageType().equals(BOMConstants.RJCT_MESSAGE_TYPE) ){
			status = bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT);
			order.setStatus("Rejected by BloomNet");
		}
		
		else if (m_message.getMessageType().equals(BOMConstants.DLCF_MESSAGE_TYPE) ){
			status = bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP);
			order.setStatus("Delivered by Shop");
		}
		else if (m_message.getMessageType().equals(BOMConstants.CONF_MESSAGE_TYPE) ){
			status = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF);
			order.setStatus("Cancel Confirmed by BloomNet");
		}
		else if (m_message.getMessageType().equals(BOMConstants.DENI_MESSAGE_TYPE) ){
			status = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_DENI);
			order.setStatus("Cancel Denied by BloomNet");
		}
		else if (m_message.getMessageType().equals(BOMConstants.DLOU_MESSAGE_TYPE) ){
			status = bomProperties.getProperty(BOMConstants.ORDER_OUT_FOR_DELIVERY);
			order.setStatus("Out for Delivery by Shop");
		}else if (m_message.getMessageType().equals(BOMConstants.PCHG_MESSAGE_TYPE) ){
			
			String orderNumber = order.getOrderNumber();
			Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
			String oldPrice = String.valueOf(parentOrder.getPrice());
			String oldXml = parentOrder.getOrderXml();
			
			//update price in order xml
			String price = inMessage.getNewPrice();
			String updatedXml = parentOrder.getOrderXml().replaceAll(oldPrice, price);
			parentOrder.setOrderXml(updatedXml);
			parentOrder.setPrice(Double.parseDouble(price));
			order.setPrice(Double.parseDouble(price));
			order.setOrderXml(updatedXml);
			
			updateOrderPrice(orderNumber, oldPrice,oldXml,parentOrder);
			
		}
		
		if ((m_message.getMessageType().equals(BOMConstants.RJCT_MESSAGE_TYPE) )
			||(m_message.getMessageType().equals(BOMConstants.DLCF_MESSAGE_TYPE) )
			||(m_message.getMessageType().equals(BOMConstants.CONF_MESSAGE_TYPE) )
			||(m_message.getMessageType().equals(BOMConstants.DENI_MESSAGE_TYPE) )
			||(m_message.getMessageType().equals(BOMConstants.DLOU_MESSAGE_TYPE) )){
						

			orderDAO.persistOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING, status, order.getOrderNumber(), BOMConstants.TLO_QUEUE);
			
			String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getOrderNumber());

			long childId = 0;
			if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
				Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
				orderDAO.persistChildOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING,  status, childOrder.getOrderNumber(), BOMConstants.TLO_QUEUE);

				childId = childOrder.getBomorderId();
			}
			
			byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE);
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(status));
			bomorderSts.setVirtualqueueId(virtualqueueId);
		
			orderDAO.updateBomorderSts(bomorderSts, childId);
			activitiesService.unlockOrder(order);
			order.setComplete(true);
			
			List<String> messages = new ArrayList<String>();
			messages.add("Order [" + order.getBean().getBmtOrderNumber()+"] is completed");
			
			order.setMessages(messages);
		}
		updateParentMessage( inMessage );
	}
	

	@Transactional( propagation = Propagation.REQUIRED )
	private void updateParentMessage( WebAppActMessage inMessage ) throws Exception {
		
		if ( inMessage.getOriginalMessage() != null ) {
			logger.debug("updateParentMessage, messageTypeid: " + inMessage.getMessageTypeId());
		
			// update the status of the original message
			final String status = bomProperties.getProperty(BOMConstants.MESSAGE_WORK_COMPLETED);
	
			messageDAO.updateMessageStatus( inMessage.getOriginalMessage().getOrderActivityId(), status );
		}
	}
	

	public BloomNetMessage buildMessageObject( WebAppActMessage inMessage ) throws MessageTypeException {
		
		String messageType = null;
		BloomNetMessage m_message = null; 
		
		try {
			
			messageType = inMessage.getMessageTypeId();
			m_message = msgFactory.createMessageObjectFromMessageType( inMessage.getMessageTypeId() );
			
			if ( m_message.getClass() == MessageDlcf.class  ) {
			    
			    ( ( MessageDlcf ) m_message ).setDateOrderDelivered( inMessage.getDateOrderDeliveredParsed() );
			    ( ( MessageDlcf ) m_message ).setSignature( inMessage.getSignature() );
			}
			else if ( m_message.getClass() == MessageDlou.class  ) {
			    
			    ( ( MessageDlou ) m_message ).setLoadedDate( inMessage.getDateOut() );
			}
			
			else if ( m_message.getClass() == MessageDlca.class ){
				String reason = new String();
				 
				MessageDlca dlca=   (  MessageDlca ) m_message;
				String messageText =   dlca.getMessageText();
				String  deliveryDate = parseDeliveryDate(messageText);
				( ( MessageDlca ) m_message ).setDeliveryAttemptedDate(deliveryDate);
				( ( MessageDlca ) m_message ).setMessageText(dlca.getMessageText());
				( ( MessageDlca ) m_message ).setSystemType(BOMConstants.SYSTEM_TYPE);
				
				RedeliveryDetails redeliveryDetails = new RedeliveryDetails();
				
				reason = dlca.getReasonForNonDelivery();

				 if(reason.equals("Need Additional Information")){
					 ( ( MessageDlca ) m_message ).setReasonForNonDelivery(BOMConstants.DLCA_NEED_INFO);
				
					 String notes = parseNotesAdditionalInfo(messageText);
					 redeliveryDetails.setNotes(notes);

				 }
				 else  if(reason.equals("Recipient Not Available")){
					 boolean b = messageText.indexOf("Will attempt Delivery") >0;
					 boolean c = messageText.indexOf("Will attempt delivery when contacted by recipient") >0;

						 if (b){
							 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
							 String redeliverydate = parseRedeliveryDate(messageText);
							 redeliveryDetails.setDeliveryDate(redeliverydate);
						 }
						 else if (c){
							 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT_CONT);
							 String notes = parseNotes(messageText);
							 redeliveryDetails.setNotes(notes);

						 }
					 
					 else{
						 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 }
						 ( ( MessageDlca ) m_message ).setReasonForNonDelivery(BOMConstants.DLCA_NOT_AVAIL);
				 }
				 else if(reason.equals("Incorrect Delivery Date")){
					 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 ( ( MessageDlca ) m_message ).setReasonForNonDelivery(BOMConstants.DLCA_INC_DATE);
					 String redeliverydate = parseRedeliveryDate(messageText);
					 redeliveryDetails.setDeliveryDate(redeliverydate);
				 }
				 else if(reason.equals("Other")){
					 boolean b = messageText.indexOf("Will attempt Delivery") >0;
					 boolean c = messageText.indexOf("Will attempt delivery when contacted by recipient") >0;

						 if (b){
							 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
							 String redeliverydate = parseRedeliveryDate(messageText);
							 redeliveryDetails.setDeliveryDate(redeliverydate);
							 }
						 else if (c){
							 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT_CONT);
							 String notes = parseNotes(messageText);
							 redeliveryDetails.setNotes(notes);

						 }
					 
					 else{
						 ( ( MessageDlca ) m_message ).setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 }
						 ( ( MessageDlca ) m_message ).setReasonForNonDelivery(BOMConstants.DLCA_NOT_AVAIL);
				 }
				 
				 ( ( MessageDlca ) m_message ).setRedeliveryDetails(redeliveryDetails);
				
			

			}
			else if ( m_message instanceof MessageOrderRelated ) {
			    
			    ( ( MessageOrderRelated ) m_message ).setMessageText( inMessage.getMessageText() );	
			     if ( messageType.equalsIgnoreCase(BOMConstants.PCHG_MESSAGE_DESC)  ) {
				    
				    ( ( MessagePchg ) m_message ).setPrice( inMessage.getNewPrice() );
				}
			}
		}
		catch ( Exception e ) {
			
			logger.error(e);
			throw new MessageTypeException( messageType );
		}

		return m_message;
	}
	

	@Transactional( propagation = Propagation.REQUIRED )
	private void sendMessageandSave( Long userId,
									 String xml, 
			                         String messageTypeDesc, 
			                         String status,
			                         String commmethod,
			                         String messageText,
			                         String sendingShopCode,
			                         String receivingShopCode ) throws Exception {
		
		GeneralIdentifiers identifiers = sendMessageToBloomlink(xml);
		
		logger.debug("about to persistMessageAndActivity" + messageTypeDesc);
	
		messageDAO.persistMessageAndActivity( userId,
										      identifiers, 
										      xml, 
										      messageTypeDesc, 
										      status, 
										      commmethod, 
										      sendingShopCode, 
										      receivingShopCode, 
										      messageText );
	}
	
	
	private  MessagesOnOrder populateMessage( final BloomNetMessage msg, 
			                                       final String messageType ) throws MessageTypeException {
		
		
		MessagesOnOrder messageOnOrder = null;

		List<BloomNetMessage> messagesList= new ArrayList<BloomNetMessage>();
		messagesList.add( msg );

        Class<MessagesOnOrder> c = MessagesOnOrder.class;

        @SuppressWarnings("rawtypes")
		Class[]  parameterTypes = new Class[] { List.class };
        Object[] arguments      = new Object[] { messagesList };
        
        Method[] methods = c.getMethods();
        
        try {
        	
        	messageOnOrder = c.newInstance();
            
            for ( int i = 0; i < methods.length; i++ ) {
                
                if ( methods[i].getName().indexOf( "set" ) == 0 && 
                     methods[i].getName().indexOf( messageType ) > 0 ) {
                    
                    Method setMessage = c.getMethod( methods[i].getName(), parameterTypes );
                    setMessage.invoke ( messageOnOrder, arguments );
                }
            }
            
            messageOnOrder.setMessageCount(1);
        } 
        catch ( Exception e ) { 
            
        	throw new MessageTypeException( messageType );
		}
       
		return messageOnOrder;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.MessagingService#getMessagesOnOrder(com.bloomnet.bom.mvc.businessobjects.WebAppUser, com.bloomnet.bom.mvc.businessobjects.WebAppOrder)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public List<ActMessage> getMessagesOnOrder( WebAppUser user, WebAppOrder order ) throws Exception,
	  																					    MessageOnOrderException {
		
    	List<ActMessage> result = new LinkedList<ActMessage>();
    	
    	Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
    	
    	if(childOrder != null && childOrder.getOrderNumber().startsWith("BMT"))
    		order.setOutboundOrderNumber(childOrder.getOrderNumber());
		 
    	Long sendingShopId = order.getShopBySendingShopId().getShopId();
		Shopnetwork BMTShopNetwork = shopDAO.getBloomNetShopnetwork(sendingShopId);


    	final Byte targetStatus = Byte.valueOf( bomProperties.getProperty( BOMConstants.MESSAGE_TO_BE_WORKED ) );
    	final Byte targetStatus2 = Byte.valueOf( bomProperties.getProperty( BOMConstants.MESSAGE_BEING_WORKED ) );
    	
		List<Orderactivity> orderActivities = orderDAO.getOrderActivities(order);

    	if(BMTShopNetwork.getShopCode().equals(BOMConstants.WEBSITE_ORDER)&&(childOrder.getOrderactivities()!=null)){
    		
    		List<Orderactivity> activities = new LinkedList<Orderactivity>();
			
			activities.addAll( childOrder.getOrderactivities() );
			
			Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse(activities);
			
    		ActRouting currentStatus = null;
			
			for( Orderactivity activity : activities ) {
					
				final ActRouting m_routing = activity.getActRouting();
					
				if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))) {
					
					currentStatus = m_routing;
					break;
					
				}
			}
	   		 if(!currentStatus.getStatus().equals("Rejected by Shop") && !currentStatus.getStatus().equals("Cancel Confirmed by BloomNet")){
	    		orderActivities = orderDAO.getOrderActivities(childOrder);
	   		 }


    	}else{
    		 orderActivities = orderDAO.getOrderActivities(order);

    	}
		//List<Orderactivity> orderActivities = orderDAO.getOrderActivities(order);
		Collections.sort ( orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
		Collections.reverse( orderActivities );
    	
		for( Orderactivity activity : orderActivities ) {
					
			if ( activity.getActMessage() != null  && 
				 (targetStatus.equals(activity.getActMessage().getStsMessage().getStsMessageId()) || targetStatus2.equals(activity.getActMessage().getStsMessage().getStsMessageId())) ) {
				
				result.add( activity.getActMessage() );
				
				final String status = bomProperties.getProperty(BOMConstants.MESSAGE_BEING_WORKED);
				messageDAO.updateMessageStatus(activity.getActMessage(), status);
				
			}
			
		}
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.MessagingService#updateMessageStatus(com.bloomnet.bom.common.entity.ActMessage, java.lang.String)
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void updateMessageStatus( User user, ActMessage message, String status) throws Exception {

		// set the user
		message.getOrderactivity().setUser( user );
		
		messageDAO.updateMessageStatus( message, status );
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GeneralIdentifiers sendMessageToBloomlink(String sendingOrderXML ) throws Exception {

		Properties props  = new Properties();
		GeneralIdentifiers identifiers = new GeneralIdentifiers();


		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}

		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}

		Transform.PROPERTIES = props;



		String dataEncoded = "";
		String post = "";
		String response = "";
		String orderNumber = "";

		try {

			dataEncoded = URLEncoder.encode(sendingOrderXML, "UTF-8");
			post = props.getProperty("fsi.endpoint")
					+ BOMConstants.POST_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("xml encoded: " + dataEncoded);
				logger.debug("sendMessage xml " + sendingOrderXML);
				logger.debug("post " + post);

			}
			response = sendRequest(post);

			if (response==null){
				throw new Exception("No response from BloomLink");
			}

			ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(response);
			orderNumber = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
			String seqNumberOfOrder = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder();
			String seqNumberOfMessage = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			identifiers.setBmtOrderNumber( orderNumber );
			identifiers.setBmtSeqNumberOfOrder( seqNumberOfOrder );
			identifiers.setBmtSeqNumberOfMessage( seqNumberOfMessage );

			logger.debug("Child order number returned from BloomLink: " + orderNumber);

			int numOfErrors = 0;
			numOfErrors = foreignSystemInterface.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = foreignSystemInterface.getErrors().getError();

				for (Error error : errors) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(error.getErrorCode() + "|"
							+ error.getDetailedErrorCode() + "|"
							+ error.getErrorMessage());
					logger.error(error.getErrorMessage());

				}
				
			}

			Errors messageAckBErrors = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getErrors();

			List<Error> messageAckBErrorList = messageAckBErrors.getError();

			for (Error messageAckBError : messageAckBErrorList) {
				logger.error("recieved error acknowledgement from BloomLink");
				logger.error(messageAckBError.getErrorCode() + "|"
						+ messageAckBError.getDetailedErrorCode() + "|"
						+ messageAckBError.getErrorMessage());
				//throw new Exception("BloomLink Exception: " +error.getErrorMessage());
				logger.error(messageAckBError.getErrorMessage());
				throw new Exception("BloomLink Exception: " +messageAckBError.getErrorMessage());

			}

		}
		catch (UnsupportedEncodingException e) {

			logger.error(e);
			throw new Exception(e);
		}

		return identifiers;
	}
	
	/**
	 * Send message to BloomLink
	 *
	 * @param outgoingMessage
	 * @return
	 */
	@Override
	public String sendRequest(String outgoingMessage) {

		String response = null;
		HttpURLConnection conn = null;

		try {

			conn = get_connection(outgoingMessage);
			conn.connect();
			response = handleResponse(conn);
		}
		catch (IOException e) {
			logger.error(e);
			System.err.println(e);
		}
		catch (NullPointerException e) {
			logger.error(e);
			System.err.println(e);
		}
		finally{
			conn.disconnect();

		}

		return response;
	}


	/**
	 * get httpUrlConnection
	 *
	 * @param url_string
	 * @return
	 */
	protected HttpURLConnection get_connection(String url_string) {
		HttpURLConnection conn = null;
		String verb = BOMConstants.POST_VERB;
		try {
			URL url = new URL(url_string);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(verb);
		} catch (MalformedURLException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
		return conn;
	}


	/**
	 * Handle response from BloomLink
	 *
	 * @param conn
	 * @return
	 */
	protected String handleResponse(HttpURLConnection conn) {
		String xml = "";

		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));

			String next = null;
			while ((next = reader.readLine()) != null)
				xml += next;
			if (logger.isDebugEnabled()) {
				logger.debug("RESPONSE MESSAGE\n The raw XML:\n" + xml);
			}
		} catch (IOException e) {
			logger.error(e);
			e.printStackTrace();
		}

		return xml;

	}

	@Override
	public BloomNetMessage buildMessageObject( ActMessage inMessage )  {
		
		String messageType = null;
		BloomNetMessage m_message = null; 

		
		try {
			
			messageType = inMessage.getMessagetype().getShortDesc();
			m_message = msgFactory.createMessageObjectFromMessageType( messageType );
			
			String messageXml= inMessage.getMessageXml();
			
			ForeignSystemInterface messageFsi = transformService.convertToJavaFSI( messageXml  );
			MessagesOnOrder messagesOnOrder = messageFsi.getMessagesOnOrder().get(0);
			
			if ( m_message.getClass() == MessageDlcf.class  ) {
				
				
			    ( ( MessageDlcf ) m_message ).setDateOrderDelivered( messagesOnOrder.getMessageDlcf().get(0).getDateOrderDelivered() );
			    ( ( MessageDlcf ) m_message ).setSignature( messagesOnOrder.getMessageDlcf().get(0).getSignature() );
			}
			else if ( m_message.getClass() == MessageDlou.class  ) {
			    
			    ( ( MessageDlou ) m_message ).setLoadedDate( messagesOnOrder.getMessageDlou().get(0).getLoadedDate() );
			}
			else if ( m_message instanceof MessageOrderRelated ) {
			    
			    ( ( MessageOrderRelated ) m_message ).setMessageText( inMessage.getMessageText() );	
			}
		}
		catch ( Exception e ) {
			
			logger.error(e);
			logger.error( "Message type ["+messageType+"] is not supported" );
		}

		return m_message;
	}
	/*
	private ActMessage convertMessageXmlToActMessage( MessageOnOrderBean orderBean, String  messageXml, int messageType ) throws Exception {

		//int messageType = orderBean.getMessageType();
		ForeignSystemInterface messageFsi = Transform.convertToJavaFSI( messageXml );
		MessagesOnOrder messagesOnOrder = messageFsi.getMessagesOnOrder().get(0);


		ActMessage message = new ActMessage();
		String bmtMessageSequenceNumber;

		if (messageType == Integer.parseInt(BOMConstants.DLCF_MESSAGE_TYPE)) {

			MessageDlcf dlcf = messagesOnOrder.getMessageDlcf().get(0);
			orderBean.setDateOrderDelivered(dlcf.getDateOrderDelivered());
			orderBean.setMessageType(messageType);

		} else if (messageType == Integer.parseInt(BOMConstants.DLOU_MESSAGE_TYPE)) {

			MessageDlou dlou = messagesOnOrder.getMessageDlou().get(0);
			orderBean.setLoadedDate(dlou.getLoadedDate());
			orderBean.setMessageType(messageType);


		} else if (messageType == Integer.parseInt(BOMConstants.PCHG_MESSAGE_TYPE)) {

			MessagePchg pchg = messagesOnOrder.getMessagePchg().get(0);
			bmtMessageSequenceNumber = pchg.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			message.setMessageText(pchg.getMessageText() );
			message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);



		} else if (messageType == Integer.parseInt(BOMConstants.CANC_MESSAGE_TYPE)) {

			MessageCanc canc = messagesOnOrder.getMessageCanc().get(0);
			bmtMessageSequenceNumber = canc.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			message.setMessageText(canc.getMessageText() );
			message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);


		} else if (messageType == Integer.parseInt(BOMConstants.CONF_MESSAGE_TYPE)) {

			MessageConf conf = messagesOnOrder.getMessageConf().get(0);
			orderBean.setMessageText(conf.getMessageText());
			orderBean.setMessageType(messageType);


		} else if (messageType == Integer.parseInt(BOMConstants.DENI_MESSAGE_TYPE)) {

			MessageDeni deni = messagesOnOrder.getMessageDeni().get(0);
			orderBean.setMessageText(deni.getMessageText());
			orderBean.setMessageType(messageType);


		} else if (messageType == Integer.parseInt(BOMConstants.INFO_MESSAGE_TYPE)) {

			MessageInfo info = messagesOnOrder.getMessageInfo().get(0);

			bmtMessageSequenceNumber = info.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			message.setMessageText(info.getMessageText() );
			message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);

		} else if (messageType == Integer.parseInt(BOMConstants.INQR_MESSAGE_TYPE)) {

			MessageInqr inqr = messagesOnOrder.getMessageInqr().get(0);
			bmtMessageSequenceNumber = inqr.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			message.setMessageText(inqr.getMessageText() );
			message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);

		} else if (messageType == Integer.parseInt(BOMConstants.RESP_MESSAGE_TYPE)) {

			MessageResp resp = messagesOnOrder.getMessageResp().get(0);
			bmtMessageSequenceNumber = resp.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			message.setMessageText(resp.getMessageText() );
			message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);


		} else if (messageType == Integer.parseInt(BOMConstants.RJCT_MESSAGE_TYPE)) {

			MessageRjct rjct = messagesOnOrder.getMessageRjct().get(0);
			orderBean.setMessageText(rjct.getMessageText());
			orderBean.setMessageType(messageType);

		}

		Shop sendingShop = orderBean.getSendingShop();
	    Shop receivingShop = orderBean.getReceivingShop();

	    String messageTypeDesc = MessageTypeUtil.getMessageDesc(messageType);


	    //byte status  = Byte.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_BEING_WORKED));
	    byte status  = 2;

		StsMessage stsMessage = messageDAO.getStsById(status);

	    Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_MESSAGE);
		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		User user = userDAO.getUserByUserName( UserDAO.INTERNAL_USER );
		orderactivity.setActMessage(message);
		orderactivity.setUser(user);
		List<Bomorder> orders = orderDAO.getOrdersByOrderNumber(orderBean.getOrderNumber());
		Long bomorderId = orders.get(0).getBomorderId();
		Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
		orderactivity.setBomorder(bomorder);
		orderactivity.setCreatedDate(new Date());

		Messagetype messagetype = messageDAO.getMessageTypeByShortDesc(messageTypeDesc);

		String commmethodDesc = BOMConstants.COMMMETHOD_PHONE;
		final Commmethod commmethod= messageDAO.getCommethodByDesc(commmethodDesc );

		byte messageFormat = BOMConstants.ORDER_TYPE_BMT; // messageFormat is equivalent to orderTYpe in bomorder

		message.setOrderactivity(orderactivity);
		message.setMessagetype(messagetype);
		message.setCommmethod(commmethod);
		message.setMessageXml(messageXml );
		message.setShopByReceivingShopId(receivingShop);
		message.setShopBySendingShopId(sendingShop);
		message.setMessageFormat(messageFormat );
		message.setStsMessage(stsMessage );
		message.setMessageXml(messageXml );


		// set status to actively being worked
		messageDAO.persistMessage(message);


		return message;
	}
*/
	
	private static String parseDeliveryDate(String messageText) {
		String formattedStr  = new String();
		
		int deliveryIndex = messageText.indexOf("Delivery Attempted on ");
		int enddeliveryIndex = messageText.indexOf(",");
		String deliveryDate = messageText.substring(deliveryIndex+22, enddeliveryIndex-1);
		 System.out.println(deliveryDate);
		 Date formatted = DateUtil.toDateDashTimeFormat(deliveryDate);
		 System.out.println(formatted);
		  formattedStr = DateUtil.toXml24FormatString(formatted);
		 System.out.println(formattedStr);
		 
		 return formattedStr;
	}


	private static String parseRedeliveryDate(String messageText) {
		
		String formattedStr = new String();
		
		 int deliveryIndex = messageText.indexOf("Will attempt Delivery");
		 String deliveryDate = messageText.substring(deliveryIndex+23);
		 System.out.println(deliveryDate);
		 Date formatted = DateUtil.toDateDashFormat(deliveryDate);
		 System.out.println(formatted);
		 formattedStr = DateUtil.toXmlFormatString(formatted);
		 System.out.println(formattedStr);
		
		return formattedStr;
	}
	
private static String parseNotes(String messageText) {
		
		String notes = new String();
		
		 int notesIndex = messageText.indexOf("Will attempt delivery when contacted by recipient");
		 notes = messageText.substring(notesIndex+53);
		 
		 return notes;
	}
	
	private static String parseNotesAdditionalInfo(String messageText) {
		
		String notes = new String();
		
		 int notesIndex = messageText.indexOf(",");
		 notes = messageText.substring(notesIndex+1);
		 
		 return notes;
	}
	
	@Override
	@Transactional( propagation = Propagation.REQUIRED )
	public void showAllMessages( WebAppUser user, WebAppOrder order ) throws Exception, MessageOnOrderException {
    	
    	
		List<Orderactivity> orderActivities = orderDAO.getOrderActivities(order);
    	
		for( Orderactivity activity : orderActivities ) {
					
			if ( activity.getActMessage() != null ) {
				if(String.valueOf(activity.getActMessage().getMessagetype().getMessageTypeId()).equals("6")){// This needs to be changes to a property later. Currently the BOM Contants are broken. CANC is listed as "3" instead of "6".
					final String status = bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED);
					messageDAO.updateMessageStatus(activity.getActMessage(), status);
				}
				
			}
			
		}

	}
	
	public void updateOrderPrice( String orderNumber, String oldPrice, String oldXml, Bomorder parentOrder ) throws Exception {
		
		
		// create order activity
		Oactivitytype actType = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_AUDIT );
		Bomorder order = orderDAO.getOrderByOrderNumber(orderNumber);
		User user  = userDAO.getUserByUserName(INTERNAL_USER);
		Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);

		
		Orderactivity activity = new Orderactivity( actType );
		activity.setBomorder( order );
		activity.setUser( user );
		activity.setCreatedDate(new Date());
		
		 long zipId = order.getZip().getZipId();
         long cityId  = order.getCity().getCityId();

					
		ActAudit actAudit = new ActAudit(activity,
										oldXml,
										order.getDeliveryDate(),
										Double.parseDouble(oldPrice), 
										zipId, 
										cityId );
					
											
		orderDAO.persistOrderHistory( actAudit );
		
		
		orderDAO.updateOrder(parentOrder);
		childOrder.setPrice(Double.valueOf(parentOrder.getPrice()));
		orderDAO.updateOrder(childOrder);

	
}
	


}
