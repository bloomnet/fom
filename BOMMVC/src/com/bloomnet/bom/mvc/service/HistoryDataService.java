package com.bloomnet.bom.mvc.service;

import java.util.List;
import java.util.Map;

import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Orderactivity;

public interface HistoryDataService {
	
	List<Orderactivity> getOrderHistory(String orderNumber) throws Exception;
	
	Map<Bomorder,List<Orderactivity>> getOrderAndActivities( String orderNumber ) throws Exception;

	Map<Bomorder, List<Orderactivity>> getMasterOrderAndActivities(String orderNumber) throws Exception;

	Map<Bomorder, List<Orderactivity>> getMasterOrderAndMessages(String orderNumber) throws Exception;
}
