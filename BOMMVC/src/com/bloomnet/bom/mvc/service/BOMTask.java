package com.bloomnet.bom.mvc.service;

public interface BOMTask {

	void releaseLockedOrdersTask() throws Exception;

	void auditOrdersTask() throws Exception;

	void wfrTask() throws Exception;

	void resendOrderTask() throws Exception;

	void auditMessagesTask() throws Exception;

	void checkWebservicesTask();

}
