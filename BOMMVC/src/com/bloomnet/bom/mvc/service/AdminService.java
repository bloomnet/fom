package com.bloomnet.bom.mvc.service;

import java.util.List;
import java.util.Map;

import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.exceptions.SearchParameterNotSupportedException;

public interface AdminService {
	
	// TODO: Move this final field to properties
	public static final int DEFAULT_RAWS_NUM = 14;
    
    List<WebAppOrder> getAllOrders() 
	throws Exception;
    
    List<WebAppOrder> getOrders( List<Bomorder> ordersToShow, int startIndex, int pageSize  ) 
	throws Exception;
    
    List<Bomorder> getOrders( Map<String, String> searchParams ) 
	throws 
	SearchParameterNotSupportedException,
	Exception;

	List<Bomorder> getManagerOrders(Map<String, String> searchParams)
			throws SearchParameterNotSupportedException, Exception;
}
