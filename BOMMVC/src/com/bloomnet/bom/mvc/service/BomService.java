package com.bloomnet.bom.mvc.service;

import java.util.Date;
import java.util.List;

import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Userrole;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;



public interface BomService {

	 void sendOrderToQueue(String orderNumber, String queue) throws Exception;

	 void sendOrderByFax(String orderNumber, String fax)
			throws Exception;
	 
	 void removeUserFromOrder(WebAppOrder order) throws Exception;
	 
	 void addOrder(WebAppOrder order) throws Exception;
	 
	 void updateOrder(WebAppOrder order) throws Exception;
	 
	 List<Bomorder> getTLOOrdersBeingWorked() throws Exception;
	 
	 void sendOrderToFSIShop(WebAppOrder order) throws Exception;
	      
     void updateUser(User user, User adminUser) throws Exception;
     
     String encodePassword(String password)throws Exception;

     List<Bomorder> getWFRS();

	void addUserrole(Userrole userrole) throws Exception;

	WebAppOrder viewOrderHistory(String orderNumber) throws Exception;

	List<WebAppUser> getTLOAgentsWorking() throws Exception;

	List<Bomorder> getOutstandingOrders();

	List<BomorderviewV2> getFrozenAutomatedOrders();

	void updateUser(User user) throws Exception;

	List<Role> getRoles(User user);

	List<Bomorder> getPreviousOutstandingOrders();

	List<Bomorder> getFutureOutstandingOrders();

	List<Bomorder> getTodayOutstandingOrders();

	List<BomorderviewV2> getTotalOrdersWorkedToday(long userid);

	List<BomorderviewV2> getTotalOrdersTouchedToday(long userid);

	Date getLastLogInTime(long userid);

	String getTotalWorkTimeToday(long userId);

	List<BomorderviewV2> getTotalOrdersTouchedByDate(long userid, Date date);

	List<BomorderviewV2> getTotalOrdersWorkedByDate(long userid, Date date);

	String getTotalWorkTimeByDate(long userId, Date date);

	WebAppUser getSelectedUser(long userId);

	List<BomorderviewV2> getTotalOrdersTouchedByDate(Date date);

	List<Bomorder> getOutstandingAutomatedOrders();
	
	List<WebAppOrder> getMultipleOrders(List<String> specifiedOrders);

	List<Bomorder> getOutstandingOrdersByState(String state);
	
	void sortList(List<Bomorder> gottenOrders);

	List<Bomorder> getOutstandingMessages();

	
}