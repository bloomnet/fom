package com.bloomnet.bom.mvc.service;

import java.util.List;

import com.bloomnet.bom.common.jaxb.recipe.RecipeInfo;

public interface RecipeService {
	
	List<RecipeInfo>  getRecipeByProductCode(String productCode) throws Exception;
	
	List<RecipeInfo>  getRecipeByOrderNumber(String orderNumber) throws Exception;
	
	List<RecipeInfo>  getAllRecipeInfo() throws Exception;

}
