package com.bloomnet.bom.mvc.service;

import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.mdi.Shop;

public interface AutomatedAgentService {

	public  Shop searchForShops(MessageOrder order,int count) throws Exception;
	
	public void handleOrder(String orderXML, String orderNumber, String orderType) throws Exception;

	public void sendOrderToShop( String message, String orderNumber, String fulfillingShop, String deliveryDate, String occasionCode );
}