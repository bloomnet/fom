package com.bloomnet.bom.mvc.service;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

public interface ActivitiesService {

	/**
	 * Locks an order by associating it with a user
	 * 
	 * @see com.bloomnet.bom.common.dao.OrderDAO#updateOrder(com.bloomnet.bom.common.entity.Bomorder)
	 * 
	 * @param order - {@link WebAppOrder} A new instance of a wrapper object backed by the order entity
	 * @param user - {@link UserInterface}
	 * @throws Exception
	 */
	WebAppOrder lockOrder( WebAppOrder order, UserInterface user ) throws Exception;
	
	
	/**
	 * Unlocks an order by removing a user from the DB record
	 * 
	 * @see com.bloomnet.bom.common.dao.OrderDAO#updateOrder(com.bloomnet.bom.common.entity.Bomorder)
	 * 
	 * @param order - {@link WebAppOrder} A new instance of a wrapper object backed by the order entity
	 * @throws Exception
	 */
	WebAppOrder unlockOrder( WebAppOrder order ) throws Exception;
	
	
	/**
	 * Store an order activity of type routing
	 * 
	 * @param order - {@link WebAppOrder}
	 * @param user - {@link UserInterface}
	 * @param status - Routing status from {@link BOMConstants}
	 * @param destination - MQ destination
	 * @throws Exception
	 */
	void persistActivityRouting( WebAppOrder order, UserInterface user, String status, String destination ) throws Exception;
	
	
	/**
	 * Store an order activity of type routing, with given status and destination.
	 * If updateChildOrders flag is true an activity record will be created per child order.
	 * 
	 * @param order - {@link WebAppOrder}
	 * @param user - {@link UserInterface}
	 * @param status - Routing status from {@link BOMConstants}
	 * @param destination - MQ destination
	 * @param updateChildOrders - If true, the orders collection will be used to record activities.
	 * 
	 * @throws Exception
	 */
	void persistActivityRouting( WebAppOrder order, 
			                     UserInterface user, 
			                     String status, 
			                     String destination,
			                     boolean updateChildOrders ) throws Exception;
	
	
	/**
	 * Store an order activity of type payment, with given status
	 * 
	 * @param order - {@link WebAppOrder}
	 * @param user - {@link UserInterface}
	 * @param status - Payment status from {@link BOMConstants}
	 * 
	 * @throws Exception - Exception object
	 */
	void persistActivityPayment( WebAppOrder order, UserInterface user, String status ) throws Exception;
	
	
	/**
	 * Store an order activity of type "Order Note"
	 * 
	 * @param order - {@link WebAppOrder}
	 * @param user - {@link UserInterface}
	 * 
	 * @throws Exception - Exception object
	 */
	void persistActivityOrderNote( WebAppOrder order, UserInterface user ) throws Exception;
	
	
	/**
	 * @param order - {@link WebAppOrder}
	 * @param user - {@link UserInterface}
	 * @throws Exception - Exception object
	 */
	void persistActivityAudit( WebAppOrder order, UserInterface user ) throws Exception;
}
