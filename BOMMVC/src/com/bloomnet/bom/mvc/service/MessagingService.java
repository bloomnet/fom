package com.bloomnet.bom.mvc.service;

import java.util.List;

import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.MessageOnOrderException;
import com.bloomnet.bom.mvc.exceptions.MessageTypeException;

public interface MessagingService {

	/**
	 * A service method used to send out and persist an order message.
	 * 
	 * The incoming message is a type of {@link ActMessage} object, converted to
	 * XML and added to an order.
	 * 
	 * 
	 * @param user
	 * @param order
	 * @param message
	 * @throws Exception
	 */
	void sendNewMessageToShop( WebAppUser user, WebAppOrder order, 
			                   WebAppActMessage message ) throws Exception;
	
	
	/**
	 * A service method which returns a list of {@link ActMessage} objects associated with an order.
	 * 
	 * When messages are red from the data base the status on the message will be changed to 
	 * "Actively Being Worked".
	 * 
	 * The messages being returned are in status "To Be Worked"
	 * 
	 * @param user
	 * @param order
	 * @return
	 * @throws MessageOnOrderException
	 * @throws Exception
	 */
	List<ActMessage> getMessagesOnOrder ( WebAppUser user, 
										  WebAppOrder order ) throws MessageOnOrderException, 
																	 Exception;
	
	
	void updateMessageStatus( User user, ActMessage message, String status ) throws Exception;


	GeneralIdentifiers sendMessageToBloomlink(String orderXML)
			throws Exception;


	String sendRequest(String message);


	BloomNetMessage buildMessageObject(ActMessage inMessage);


	void showAllMessages(WebAppUser user, WebAppOrder order)
			throws Exception, MessageOnOrderException;
}
