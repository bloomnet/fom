package com.bloomnet.bom.mvc.service;

public interface EmailService {

	public abstract void sendEmail(String order, String email);

}