package com.bloomnet.bom.mvc.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface FaxService {

	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public abstract void SendFax(String order, String shopCode, String fax)
			throws Exception;

}