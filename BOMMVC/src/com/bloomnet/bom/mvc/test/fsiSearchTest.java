package com.bloomnet.bom.mvc.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectorySearchOptions;
import com.bloomnet.bom.common.jaxb.mdi.SearchByAvailabilityDate;
import com.bloomnet.bom.common.jaxb.mdi.SearchShopRequest;
import com.bloomnet.bom.common.jaxb.mdi.Security;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;

public class fsiSearchTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String city  = "carle place";
		String zip="11550";
		String state="NY";
		String deliveryDate="12/10/2011";
		TransformService transform = new Transform();

		
		SearchByAvailabilityDate searchbyDate = new SearchByAvailabilityDate();
		
		
		searchbyDate.setAvailabilityDate(deliveryDate);
		//searchbyDate.setCity(city);
		//searchbyDate.setState(state);
		searchbyDate.setZipCode(zip);
		
		MemberDirectoryInterface mdi =  new MemberDirectoryInterface();
		SearchShopRequest searchshoprequest = new SearchShopRequest();
		MemberDirectorySearchOptions searchOptions = new MemberDirectorySearchOptions() ;
		
		searchOptions.getMemberDirectorySearchOptionsGroup().add(searchbyDate);
		
searchshoprequest.setMemberDirectorySearchOptions(searchOptions);

Security security = new Security();

security.setPassword("qa");
security.setShopCode("Q8830000");
security.setUsername("Q883");

searchshoprequest.setSecurity(security);
		
		mdi.setSearchShopRequest(searchshoprequest);
		
		
		
		
		String mdiXml = transform.convertToMDIXML(mdi);
		
		String dataEncoded = new String();
		try {
			dataEncoded = URLEncoder.encode(mdiXml, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String post = "http://10.180.1.220:9081/fsiv2/processor"+ BOMConstants.GET_MEMBER + dataEncoded;
		
		String shop = sendRequest(post);
		
		
		MemberDirectoryInterface results = transform.convertToJavaMDI(shop);

	}
	
	
	private static String sendRequest(String outgoingMessage){
		
		String response = null;
		
		  try {
	            
	            HttpURLConnection conn = get_connection(outgoingMessage);
	            conn.connect();
	            response = handleResponse(conn);
	        }
	        catch(IOException e) { System.err.println(e); }
	        catch(NullPointerException e) { System.err.println(e); }
		  return response;
	  }



private static HttpURLConnection get_connection(String url_string) {
	HttpURLConnection conn = null;
	String verb=BOMConstants.POST_VERB;
	try {
		URL url = new URL(url_string);
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(verb);
	} catch (MalformedURLException e) {
		System.err.println(e);
	} catch (IOException e) {
		System.err.println(e);
	}
	return conn;
}


private static String handleResponse(HttpURLConnection conn) {
	String xml = "";

		
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
		
		String next = null;
		while ((next = reader.readLine()) != null)
			xml += next;
		System.out.println("RESPONSE MESSAGE\n The raw XML:\n" + xml);
						
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
	return xml;


}


}
