package com.bloomnet.bom.mvc.fax;

import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER;
import com.bloomnet.bom.mvc.fax.soap.query.ActionResult;
import com.bloomnet.bom.mvc.fax.soap.query.Attachments;
import com.bloomnet.bom.mvc.fax.soap.query.QueryRequest;
import com.bloomnet.bom.mvc.fax.soap.query.QueryResult;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.query.UpdateParameters;
import com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE;
import com.bloomnet.bom.mvc.fax.soap.query.Var;
import com.bloomnet.bom.mvc.fax.soap.session.BindingResult;
import com.bloomnet.bom.mvc.fax.soap.session.LoginResult;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Attachment;
import com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Transport;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFile;

class RecvFax
{
	//////////////////////////////////////////////////////////////////////
	// STEP #1 : Global Initializations
	//////////////////////////////////////////////////////////////////////
	// Sample parameters
	String m_Username = "mYus3r";	                        // Session username
	String m_Password = "mYpassw0rd";						// Session password

	// Web Service URL, change with the required server url
	String m_WebServiceUrl = "https://as1.ondemand.esker.com:443/EDPWS/EDPWS.dll?Handler=Default&Version=1.0";
	
	static public void main(String[] args)
	{
		RecvFax sample1 = new RecvFax();
		sample1.Run();
	}

	// Helper method to allocate and fill in Variable objects.
	private static Var CreateValue(String AttributeName, String AttributeValue)
	{
		Var var = new Var();
		var.setAttribute(AttributeName);
		var.setSimpleValue(AttributeValue);
		var.setType(VAR_TYPE.TYPE_STRING);
		return var;
	}

	public void Run()
	{
		//////////////////////////////////////////////////////////////////////
		// STEP #2 : Initialization + Authentication
		//////////////////////////////////////////////////////////////////////

		System.out.println("Retrieving bindings");

		SessionServiceSoap_BindingStub session = null;
		try
		{
			SessionServiceLocator locator = new SessionServiceLocator();
			locator.setEndpointAddress("SessionServiceSoap", m_WebServiceUrl);            
			session = (SessionServiceSoap_BindingStub) locator.getSessionServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Retrieve the bindings on the Application Server (locations of the web services)
		BindingResult bindings = null;
		try
		{
			bindings = session.getBindings(m_Username);
		}
		catch(java.rmi.RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		System.out.println("Binding = " + bindings.getSessionServiceLocation());


		// Now uses the returned URL with our session object, in case the Application Server redirected us.
		session._setProperty(SessionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSessionServiceLocation());

		System.out.println("Authenticating session");

		// Authenticate the user on this session object to retrieve a sessionID
		LoginResult login = null;
		try
		{
			login = session.login(m_Username, m_Password);

			// Set the returned session header "SessionHeaderValue" as a submission session header.
			session.setHeader(session.getResponseHeader(new SessionServiceLocator().getServiceName().getNamespaceURI(), "SessionHeaderValue"));
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// This sessionID is an impersonation token representing the logged on user
		// You can use it with other Web Services objects, until you call Logout (which releases the
		// current sessionID and it's associated resources), or until the session times out (default is 10
		// minutes on the Application Server).
		System.out.println("SessionID = " + login.getSessionID());

		//////////////////////////////////////////////////////////////////////
		// STEP #3 : Fax retrieving
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a QueryService object.
		QueryServiceSoap_BindingStub query = null;
		try
		{
			query = (QueryServiceSoap_BindingStub) new QueryServiceLocator().getQueryServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service URL with the location retrieved above with GetBindings()
		query._setProperty(QueryServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getQueryServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.query.SessionHeader q_header = new com.bloomnet.bom.mvc.fax.soap.query.SessionHeader();
		q_header.setSessionID(login.getSessionID());
		query.setHeader(
			new QueryServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			q_header);

		// Build a request on the newly submitted fax transport using its unique identifier
		// We also specify the variables (attributes) we want to retrieve.
		QueryRequest request = new QueryRequest();
		request.setNItems(5);
		request.setAttributes("CompletionDateTime");
		// These variables are documented on the page
		// http://doc.esker.com/eskerondemand/cv/en/webservices/index.asp?page=References/Common/RecipientTypes.html
		request.setFilter("(&(RecipientType=FGFaxIn)(State=100)(!(Identifier=RETRIEVED)))");
		request.setSortOrder("");

		QueryResult qresult = null;

		System.out.println("Checking for new inbound faxes...");

		try
		{
			// Ask the Application Server
			qresult = query.queryFirst(request);
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		for(int i = 0; i < qresult.getNTransports(); i++)
		{
			int uniqueID = qresult.getTransports()[i].getTransportID();

			System.out.println("Fax received #" + uniqueID);

			Attachments atts = null;
			try
			{
				atts = query.queryAttachments(uniqueID, ATTACHMENTS_FILTER.FILTER_CONVERTED, com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE.MODE_INLINED);
			}
			catch(RemoteException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}

			// The final fax image is known to be the first converted attachment of the first available attachments, 
			// so retrieve this one.
			if(atts.getNAttachments() > 0 && atts.getAttachments()[0].getNConvertedAttachments() > 0)
			{
				com.bloomnet.bom.mvc.fax.soap.query.WSFile faxImage = atts.getAttachments()[0].getConvertedAttachments()[0];
				byte[] content = faxImage.getContent();
				if(content != null)
				{
					System.out.println("Fax image data retrieve, size = " + content.length);

					try
					{
						FileOutputStream stream = new FileOutputStream(uniqueID + ".tif");
						stream.write(content, 0, content.length);
						stream.close();
					}
					catch(FileNotFoundException ex)
					{
						System.out.println("An unexpected error has occurred." + ex.getMessage());
					}
					catch(IOException ex)
					{
						System.out.println("An unexpected error has occurred." + ex.getMessage());
					}
				}
				else
				{
					System.out.println("Error !! no valid attachments found");
				}
			}
			else
			{
				System.out.println("Error !! no valid attachments found");
			}

			// Now set the inbound fax as "retrieved"
			Var[] vars = new Var[1];
			vars[0] = CreateValue("Identifier", "RETRIEVED");

			UpdateParameters prms = new UpdateParameters();
			prms.setVars(vars);

			ActionResult r = null;
			try
			{
				// These variables are documented on the page
				// http://doc.esker.com/eskerondemand/cv/en/webservices/index.asp?page=References/Common/RecipientTypes.html
				r = query.update("(&(RecipientType=FGFaxIn)(msn=" + uniqueID + "))", prms);
			}
			catch(RemoteException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}
			if(r.getNFailed() > 0)
			{
				System.out.println("failed to update item #" + uniqueID + ", error = " + r.getErrorReason()[0]);
			}
			else
			{
				System.out.println("successfully updated item #" + uniqueID);
			}
		}

		//////////////////////////////////////////////////////////////////////
		// STEP #4 : Release the session and its allocated resources
		//////////////////////////////////////////////////////////////////////

		System.out.println("Press <enter> to quit...");
		try
		{
			System.in.read();
		}
		catch(IOException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// As soon as you call Logout(), the files allocated on the server during this session won't be available
		// any more, so keep in mind that former urls are now useless...

		System.out.println("Releasing session and server files");

		try
		{
			session.logout();
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}
	}
}


