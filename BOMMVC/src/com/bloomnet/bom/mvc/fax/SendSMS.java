package com.bloomnet.bom.mvc.fax;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.bloomnet.bom.mvc.fax.soap.query.QueryRequest;
import com.bloomnet.bom.mvc.fax.soap.query.QueryResult;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.session.BindingResult;
import com.bloomnet.bom.mvc.fax.soap.session.LoginResult;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Transport;
import com.bloomnet.bom.mvc.fax.soap.submission.VAR_TYPE;
import com.bloomnet.bom.mvc.fax.soap.submission.Var;

public class SendSMS
{
	//////////////////////////////////////////////////////////////////////
	// STEP #1 : Global Initializations
	//////////////////////////////////////////////////////////////////////
	// Sample parameters
	String m_Username = "mYus3r";	                        // Session username
	String m_Password = "mYpassw0rd";						// Session password

	// Web Service URL, change with the required server url
	String m_WebServiceUrl = "https://as1.ondemand.esker.com:443/EDPWS/EDPWS.dll?Handler=Default&Version=1.0";
	int m_PollingInterval = 15000;		// check SMS status every 15 seconds

	// Helper method to allocate and fill in Variable objects.
	private static Var CreateValue(String AttributeName, String AttributeValue)
	{
		Var var = new Var();
		var.setAttribute(AttributeName);
		var.setSimpleValue(AttributeValue);
		var.setType(VAR_TYPE.TYPE_STRING);
		return var;
	}

	public void Run()
	{
		//////////////////////////////////////////////////////////////////////
		// STEP #2 : Initialization + Authentication
		//////////////////////////////////////////////////////////////////////

		System.out.println("Retrieving bindings");

		SessionServiceSoap_BindingStub session = null;
		try
		{
			SessionServiceLocator locator = new SessionServiceLocator();
			locator.setEndpointAddress("SessionServiceSoap", m_WebServiceUrl);
			session = (SessionServiceSoap_BindingStub) locator.getSessionServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Retrieve the bindings on the Application Server (locations of the web services)
		BindingResult bindings = null;
		try
		{
			bindings = session.getBindings(m_Username);
		}
		catch(java.rmi.RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		System.out.println("Binding = " + bindings.getSessionServiceLocation());


		// Now uses the returned URL with our session object, in case the Application Server redirected us.
		session._setProperty(SessionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSessionServiceLocation());

		System.out.println("Authenticating session");

		// Authenticate the user on this session object to retrieve a sessionID
		LoginResult login = null;
		try
		{
			login = session.login(m_Username, m_Password);

			// Set the returned session header "SessionHeaderValue" as a submission session header.
			session.setHeader(session.getResponseHeader(new SessionServiceLocator().getServiceName().getNamespaceURI(), "SessionHeaderValue"));
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// This sessionID is an impersonation token representing the logged on user
		// You can use it with other Web Services objects, until you call Logout (which releases the
		// current sessionID and it's associated resources), or until the session times out (default is 10
		// minutes on the Application Server).
		System.out.println("SessionID = " + login.getSessionID());


		//////////////////////////////////////////////////////////////////////
		// STEP #3 : Simple SMS submission
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a SubmissionService object.
		SubmissionServiceSoap_BindingStub submission = null;
		try
		{
			submission = (SubmissionServiceSoap_BindingStub) new SubmissionServiceLocator().getSubmissionServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service url with the location retrieved above with GetBindings()
		submission._setProperty(SubmissionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSubmissionServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader s_header = new com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader();
		s_header.setSessionID(login.getSessionID());
		submission.setHeader(
			new SubmissionServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			s_header);


		System.out.println("Sending SMS Request");

		// Now allocate a transport with transportName = "SMS"
		Transport transport = new Transport();
		transport.setRecipientType("");
		transport.setTransportIndex("");
		transport.setTransportName("SMS");

		// Specifies SMS variables (see documentation for their definitions)
		Var[] vars = new Var[4];
		vars[0] = CreateValue("Subject", "Sample SMS");
		vars[1] = CreateValue("FromName", "John DOE");
		vars[2] = CreateValue("SMSNumber", "+33672335425");
		vars[3] = CreateValue("Message", "Everything's OK");

		transport.setVars(vars);

		// Submit the complete transport description to the Application Server
		SubmissionResult result = null;
		try
		{
			result = submission.submitTransport(transport);
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		System.out.println("Request submitted with transportID " + result.getTransportID());


		//////////////////////////////////////////////////////////////////////
		// STEP #4 : SMS tracking
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a QueryService object.
		QueryServiceSoap_BindingStub query = null;
		try
		{
			query = (QueryServiceSoap_BindingStub) new QueryServiceLocator().getQueryServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service URL with the location retrieved above with GetBindings()
		query._setProperty(QueryServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getQueryServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.query.SessionHeader q_header = new com.bloomnet.bom.mvc.fax.soap.query.SessionHeader();
		q_header.setSessionID(login.getSessionID());
		query.setHeader(
			new QueryServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			q_header);

		// Build a request on the newly submitted sms transport using its unique identifier
		// We also specify the variables (attributes) we want to retrieve.
		QueryRequest request = new QueryRequest();
		request.setNItems(1);
		request.setAttributes("State,ShortStatus,CompletionDateTime");
		// These variables are documented on the page
		// http://doc.esker.com/eskerondemand/cv/en/webservices/index.asp?page=References/Common/RecipientTypes.html
		request.setFilter("(&(msn=" + result.getTransportID() + ")(TransportName=Sms))");
		request.setSortOrder("");

		QueryResult qresult = null;

		System.out.println("Checking for your sms status...");

		int state = 0;
		String status = "";
		String date = "";

		while(true)
		{
			try
			{
				// Ask the Application Server
				qresult = query.queryFirst(request);
			}
			catch(RemoteException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}

			if(qresult.getNTransports() == 1)
			{
				// Hopefully, we found it
				// Parse the returned variables
				com.bloomnet.bom.mvc.fax.soap.query.Transport transport2 = qresult.getTransports()[0];
				for(int iVar = 0; iVar < transport2.getNVars(); iVar++)
				{
					com.bloomnet.bom.mvc.fax.soap.query.Var var = transport2.getVars()[iVar];
					String attribute = var.getAttribute().toLowerCase();
					if(attribute.compareTo("state") == 0)
					{
						state = Integer.parseInt(var.getSimpleValue());
					}
					else if(attribute.compareTo("shortstatus") == 0)
					{
						status = var.getSimpleValue();
					}
					else if(attribute.compareTo("completiondatetime") == 0)
					{
						date = var.getSimpleValue();
					}
				}

				if(state >= 100)
				{
					break;
				}

				System.out.println("SMS pending...");
			}
			else
			{
				System.out.println("Error !! SMS not found in database");
				return;
			}

			// Wait 15 seconds, then try again...
			try
			{
				Thread.sleep(m_PollingInterval);
			}
			catch(InterruptedException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}
		}

		if(state == 100)
		{
			System.out.println("SMS successfully sent at " + date);
		}
		else
		{
			System.out.println("SMS failed at + " + date + ", reason: " + status);
		}


		//////////////////////////////////////////////////////////////////////
		// STEP #5 : Release the session and its allocated resources
		//////////////////////////////////////////////////////////////////////

		System.out.println("Press <enter> to quit...");
		try
		{
			System.in.read();
		}
		catch(IOException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// As soon as you call Logout(), the files allocated on the server during this session won't be available
		// any more, so keep in mind that former urls are now useless...

		System.out.println("Releasing session and server files");

		try
		{
			session.logout();
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}
	}
}
