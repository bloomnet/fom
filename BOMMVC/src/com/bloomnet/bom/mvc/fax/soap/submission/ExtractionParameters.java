/**
 * ExtractionParameters.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class ExtractionParameters  implements java.io.Serializable {
    private int nItems;

    private boolean fullPreviewMode;

    private com.bloomnet.bom.mvc.fax.soap.submission.ATTACHMENTS_FILTER attachmentFilter;

    private com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode;

    private boolean includeSubNodes;

    private java.lang.String startIndex;

    public ExtractionParameters() {
    }

    public ExtractionParameters(
           int nItems,
           boolean fullPreviewMode,
           com.bloomnet.bom.mvc.fax.soap.submission.ATTACHMENTS_FILTER attachmentFilter,
           com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode,
           boolean includeSubNodes,
           java.lang.String startIndex) {
           this.nItems = nItems;
           this.fullPreviewMode = fullPreviewMode;
           this.attachmentFilter = attachmentFilter;
           this.outputFileMode = outputFileMode;
           this.includeSubNodes = includeSubNodes;
           this.startIndex = startIndex;
    }


    /**
     * Gets the nItems value for this ExtractionParameters.
     * 
     * @return nItems
     */
    public int getNItems() {
        return nItems;
    }


    /**
     * Sets the nItems value for this ExtractionParameters.
     * 
     * @param nItems
     */
    public void setNItems(int nItems) {
        this.nItems = nItems;
    }


    /**
     * Gets the fullPreviewMode value for this ExtractionParameters.
     * 
     * @return fullPreviewMode
     */
    public boolean isFullPreviewMode() {
        return fullPreviewMode;
    }


    /**
     * Sets the fullPreviewMode value for this ExtractionParameters.
     * 
     * @param fullPreviewMode
     */
    public void setFullPreviewMode(boolean fullPreviewMode) {
        this.fullPreviewMode = fullPreviewMode;
    }


    /**
     * Gets the attachmentFilter value for this ExtractionParameters.
     * 
     * @return attachmentFilter
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.ATTACHMENTS_FILTER getAttachmentFilter() {
        return attachmentFilter;
    }


    /**
     * Sets the attachmentFilter value for this ExtractionParameters.
     * 
     * @param attachmentFilter
     */
    public void setAttachmentFilter(com.bloomnet.bom.mvc.fax.soap.submission.ATTACHMENTS_FILTER attachmentFilter) {
        this.attachmentFilter = attachmentFilter;
    }


    /**
     * Gets the outputFileMode value for this ExtractionParameters.
     * 
     * @return outputFileMode
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE getOutputFileMode() {
        return outputFileMode;
    }


    /**
     * Sets the outputFileMode value for this ExtractionParameters.
     * 
     * @param outputFileMode
     */
    public void setOutputFileMode(com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode) {
        this.outputFileMode = outputFileMode;
    }


    /**
     * Gets the includeSubNodes value for this ExtractionParameters.
     * 
     * @return includeSubNodes
     */
    public boolean isIncludeSubNodes() {
        return includeSubNodes;
    }


    /**
     * Sets the includeSubNodes value for this ExtractionParameters.
     * 
     * @param includeSubNodes
     */
    public void setIncludeSubNodes(boolean includeSubNodes) {
        this.includeSubNodes = includeSubNodes;
    }


    /**
     * Gets the startIndex value for this ExtractionParameters.
     * 
     * @return startIndex
     */
    public java.lang.String getStartIndex() {
        return startIndex;
    }


    /**
     * Sets the startIndex value for this ExtractionParameters.
     * 
     * @param startIndex
     */
    public void setStartIndex(java.lang.String startIndex) {
        this.startIndex = startIndex;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtractionParameters)) return false;
        ExtractionParameters other = (ExtractionParameters) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nItems == other.getNItems() &&
            this.fullPreviewMode == other.isFullPreviewMode() &&
            ((this.attachmentFilter==null && other.getAttachmentFilter()==null) || 
             (this.attachmentFilter!=null &&
              this.attachmentFilter.equals(other.getAttachmentFilter()))) &&
            ((this.outputFileMode==null && other.getOutputFileMode()==null) || 
             (this.outputFileMode!=null &&
              this.outputFileMode.equals(other.getOutputFileMode()))) &&
            this.includeSubNodes == other.isIncludeSubNodes() &&
            ((this.startIndex==null && other.getStartIndex()==null) || 
             (this.startIndex!=null &&
              this.startIndex.equals(other.getStartIndex())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNItems();
        _hashCode += (isFullPreviewMode() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getAttachmentFilter() != null) {
            _hashCode += getAttachmentFilter().hashCode();
        }
        if (getOutputFileMode() != null) {
            _hashCode += getOutputFileMode().hashCode();
        }
        _hashCode += (isIncludeSubNodes() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getStartIndex() != null) {
            _hashCode += getStartIndex().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtractionParameters.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionParameters"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NItems");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fullPreviewMode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "fullPreviewMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachmentFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "attachmentFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "ATTACHMENTS_FILTER"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputFileMode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "outputFileMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFILE_MODE"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeSubNodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "includeSubNodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "startIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
