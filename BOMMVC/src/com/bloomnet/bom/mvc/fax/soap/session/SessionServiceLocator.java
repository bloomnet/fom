/**
 * SessionServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.session;

public class SessionServiceLocator extends org.apache.axis.client.Service implements com.bloomnet.bom.mvc.fax.soap.session.SessionService {

    public SessionServiceLocator() {
    }


    public SessionServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SessionServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SessionServiceSoap
    private java.lang.String SessionServiceSoap_address = "https://as1.ondemand.esker.com:443/EDPWS/EDPWS.dll?Handler=Default&Version=1.0";

    public java.lang.String getSessionServiceSoapAddress() {
        return SessionServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SessionServiceSoapWSDDServiceName = "SessionServiceSoap";

    public java.lang.String getSessionServiceSoapWSDDServiceName() {
        return SessionServiceSoapWSDDServiceName;
    }

    public void setSessionServiceSoapWSDDServiceName(java.lang.String name) {
        SessionServiceSoapWSDDServiceName = name;
    }

    public com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_PortType getSessionServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SessionServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSessionServiceSoap(endpoint);
    }

    public com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_PortType getSessionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub _stub = new com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getSessionServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSessionServiceSoapEndpointAddress(java.lang.String address) {
        SessionServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub _stub = new com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub(new java.net.URL(SessionServiceSoap_address), this);
                _stub.setPortName(getSessionServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SessionServiceSoap".equals(inputPortName)) {
            return getSessionServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:SessionService", "SessionService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:SessionService", "SessionServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SessionServiceSoap".equals(portName)) {
            setSessionServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
