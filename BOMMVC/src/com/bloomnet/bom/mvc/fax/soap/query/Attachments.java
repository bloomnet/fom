/**
 * Attachments.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class Attachments  implements java.io.Serializable {
    private int nAttachments;

    private com.bloomnet.bom.mvc.fax.soap.query.Attachment[] attachments;

    public Attachments() {
    }

    public Attachments(
           int nAttachments,
           com.bloomnet.bom.mvc.fax.soap.query.Attachment[] attachments) {
           this.nAttachments = nAttachments;
           this.attachments = attachments;
    }


    /**
     * Gets the nAttachments value for this Attachments.
     * 
     * @return nAttachments
     */
    public int getNAttachments() {
        return nAttachments;
    }


    /**
     * Sets the nAttachments value for this Attachments.
     * 
     * @param nAttachments
     */
    public void setNAttachments(int nAttachments) {
        this.nAttachments = nAttachments;
    }


    /**
     * Gets the attachments value for this Attachments.
     * 
     * @return attachments
     */
    public com.bloomnet.bom.mvc.fax.soap.query.Attachment[] getAttachments() {
        return attachments;
    }


    /**
     * Sets the attachments value for this Attachments.
     * 
     * @param attachments
     */
    public void setAttachments(com.bloomnet.bom.mvc.fax.soap.query.Attachment[] attachments) {
        this.attachments = attachments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Attachments)) return false;
        Attachments other = (Attachments) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nAttachments == other.getNAttachments() &&
            ((this.attachments==null && other.getAttachments()==null) || 
             (this.attachments!=null &&
              java.util.Arrays.equals(this.attachments, other.getAttachments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNAttachments();
        if (getAttachments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Attachments.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Attachments"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAttachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nAttachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "attachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Attachment"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "Attachment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
