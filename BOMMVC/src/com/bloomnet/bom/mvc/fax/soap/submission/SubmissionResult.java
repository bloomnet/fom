/**
 * SubmissionResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class SubmissionResult  implements java.io.Serializable {
    private java.lang.String submissionID;

    private int transportID;

    public SubmissionResult() {
    }

    public SubmissionResult(
           java.lang.String submissionID,
           int transportID) {
           this.submissionID = submissionID;
           this.transportID = transportID;
    }


    /**
     * Gets the submissionID value for this SubmissionResult.
     * 
     * @return submissionID
     */
    public java.lang.String getSubmissionID() {
        return submissionID;
    }


    /**
     * Sets the submissionID value for this SubmissionResult.
     * 
     * @param submissionID
     */
    public void setSubmissionID(java.lang.String submissionID) {
        this.submissionID = submissionID;
    }


    /**
     * Gets the transportID value for this SubmissionResult.
     * 
     * @return transportID
     */
    public int getTransportID() {
        return transportID;
    }


    /**
     * Sets the transportID value for this SubmissionResult.
     * 
     * @param transportID
     */
    public void setTransportID(int transportID) {
        this.transportID = transportID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmissionResult)) return false;
        SubmissionResult other = (SubmissionResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.submissionID==null && other.getSubmissionID()==null) || 
             (this.submissionID!=null &&
              this.submissionID.equals(other.getSubmissionID()))) &&
            this.transportID == other.getTransportID();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubmissionID() != null) {
            _hashCode += getSubmissionID().hashCode();
        }
        _hashCode += getTransportID();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmissionResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "submissionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "transportID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
