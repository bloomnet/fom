/**
 * ConversionResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class ConversionResult  implements java.io.Serializable {
    private com.bloomnet.bom.mvc.fax.soap.submission.WSFile convertedFile;

    public ConversionResult() {
    }

    public ConversionResult(
           com.bloomnet.bom.mvc.fax.soap.submission.WSFile convertedFile) {
           this.convertedFile = convertedFile;
    }


    /**
     * Gets the convertedFile value for this ConversionResult.
     * 
     * @return convertedFile
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile getConvertedFile() {
        return convertedFile;
    }


    /**
     * Sets the convertedFile value for this ConversionResult.
     * 
     * @param convertedFile
     */
    public void setConvertedFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile convertedFile) {
        this.convertedFile = convertedFile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConversionResult)) return false;
        ConversionResult other = (ConversionResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.convertedFile==null && other.getConvertedFile()==null) || 
             (this.convertedFile!=null &&
              this.convertedFile.equals(other.getConvertedFile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConvertedFile() != null) {
            _hashCode += getConvertedFile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConversionResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "ConversionResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("convertedFile");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "convertedFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
