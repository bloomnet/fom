/**
 * Attachment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class Attachment  implements java.io.Serializable {
    private java.lang.String inputFormat;

    private java.lang.String outputFormat;

    private java.lang.String stylesheet;

    private java.lang.String outputName;

    private com.bloomnet.bom.mvc.fax.soap.submission.WSFile sourceAttachment;

    private int nConvertedAttachments;

    private com.bloomnet.bom.mvc.fax.soap.submission.WSFile[] convertedAttachments;

    public Attachment() {
    }

    public Attachment(
           java.lang.String inputFormat,
           java.lang.String outputFormat,
           java.lang.String stylesheet,
           java.lang.String outputName,
           com.bloomnet.bom.mvc.fax.soap.submission.WSFile sourceAttachment,
           int nConvertedAttachments,
           com.bloomnet.bom.mvc.fax.soap.submission.WSFile[] convertedAttachments) {
           this.inputFormat = inputFormat;
           this.outputFormat = outputFormat;
           this.stylesheet = stylesheet;
           this.outputName = outputName;
           this.sourceAttachment = sourceAttachment;
           this.nConvertedAttachments = nConvertedAttachments;
           this.convertedAttachments = convertedAttachments;
    }


    /**
     * Gets the inputFormat value for this Attachment.
     * 
     * @return inputFormat
     */
    public java.lang.String getInputFormat() {
        return inputFormat;
    }


    /**
     * Sets the inputFormat value for this Attachment.
     * 
     * @param inputFormat
     */
    public void setInputFormat(java.lang.String inputFormat) {
        this.inputFormat = inputFormat;
    }


    /**
     * Gets the outputFormat value for this Attachment.
     * 
     * @return outputFormat
     */
    public java.lang.String getOutputFormat() {
        return outputFormat;
    }


    /**
     * Sets the outputFormat value for this Attachment.
     * 
     * @param outputFormat
     */
    public void setOutputFormat(java.lang.String outputFormat) {
        this.outputFormat = outputFormat;
    }


    /**
     * Gets the stylesheet value for this Attachment.
     * 
     * @return stylesheet
     */
    public java.lang.String getStylesheet() {
        return stylesheet;
    }


    /**
     * Sets the stylesheet value for this Attachment.
     * 
     * @param stylesheet
     */
    public void setStylesheet(java.lang.String stylesheet) {
        this.stylesheet = stylesheet;
    }


    /**
     * Gets the outputName value for this Attachment.
     * 
     * @return outputName
     */
    public java.lang.String getOutputName() {
        return outputName;
    }


    /**
     * Sets the outputName value for this Attachment.
     * 
     * @param outputName
     */
    public void setOutputName(java.lang.String outputName) {
        this.outputName = outputName;
    }


    /**
     * Gets the sourceAttachment value for this Attachment.
     * 
     * @return sourceAttachment
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile getSourceAttachment() {
        return sourceAttachment;
    }


    /**
     * Sets the sourceAttachment value for this Attachment.
     * 
     * @param sourceAttachment
     */
    public void setSourceAttachment(com.bloomnet.bom.mvc.fax.soap.submission.WSFile sourceAttachment) {
        this.sourceAttachment = sourceAttachment;
    }


    /**
     * Gets the nConvertedAttachments value for this Attachment.
     * 
     * @return nConvertedAttachments
     */
    public int getNConvertedAttachments() {
        return nConvertedAttachments;
    }


    /**
     * Sets the nConvertedAttachments value for this Attachment.
     * 
     * @param nConvertedAttachments
     */
    public void setNConvertedAttachments(int nConvertedAttachments) {
        this.nConvertedAttachments = nConvertedAttachments;
    }


    /**
     * Gets the convertedAttachments value for this Attachment.
     * 
     * @return convertedAttachments
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile[] getConvertedAttachments() {
        return convertedAttachments;
    }


    /**
     * Sets the convertedAttachments value for this Attachment.
     * 
     * @param convertedAttachments
     */
    public void setConvertedAttachments(com.bloomnet.bom.mvc.fax.soap.submission.WSFile[] convertedAttachments) {
        this.convertedAttachments = convertedAttachments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Attachment)) return false;
        Attachment other = (Attachment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputFormat==null && other.getInputFormat()==null) || 
             (this.inputFormat!=null &&
              this.inputFormat.equals(other.getInputFormat()))) &&
            ((this.outputFormat==null && other.getOutputFormat()==null) || 
             (this.outputFormat!=null &&
              this.outputFormat.equals(other.getOutputFormat()))) &&
            ((this.stylesheet==null && other.getStylesheet()==null) || 
             (this.stylesheet!=null &&
              this.stylesheet.equals(other.getStylesheet()))) &&
            ((this.outputName==null && other.getOutputName()==null) || 
             (this.outputName!=null &&
              this.outputName.equals(other.getOutputName()))) &&
            ((this.sourceAttachment==null && other.getSourceAttachment()==null) || 
             (this.sourceAttachment!=null &&
              this.sourceAttachment.equals(other.getSourceAttachment()))) &&
            this.nConvertedAttachments == other.getNConvertedAttachments() &&
            ((this.convertedAttachments==null && other.getConvertedAttachments()==null) || 
             (this.convertedAttachments!=null &&
              java.util.Arrays.equals(this.convertedAttachments, other.getConvertedAttachments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputFormat() != null) {
            _hashCode += getInputFormat().hashCode();
        }
        if (getOutputFormat() != null) {
            _hashCode += getOutputFormat().hashCode();
        }
        if (getStylesheet() != null) {
            _hashCode += getStylesheet().hashCode();
        }
        if (getOutputName() != null) {
            _hashCode += getOutputName().hashCode();
        }
        if (getSourceAttachment() != null) {
            _hashCode += getSourceAttachment().hashCode();
        }
        _hashCode += getNConvertedAttachments();
        if (getConvertedAttachments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConvertedAttachments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConvertedAttachments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Attachment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "Attachment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "inputFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "outputFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stylesheet");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "stylesheet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "outputName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceAttachment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "sourceAttachment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NConvertedAttachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nConvertedAttachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("convertedAttachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "convertedAttachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
