/**
 * ConversionParameters.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class ConversionParameters  implements java.io.Serializable {
    private java.lang.String inputType;

    private java.lang.String outputType;

    private java.lang.String customParameters;

    private com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode;

    public ConversionParameters() {
    }

    public ConversionParameters(
           java.lang.String inputType,
           java.lang.String outputType,
           java.lang.String customParameters,
           com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode) {
           this.inputType = inputType;
           this.outputType = outputType;
           this.customParameters = customParameters;
           this.outputFileMode = outputFileMode;
    }


    /**
     * Gets the inputType value for this ConversionParameters.
     * 
     * @return inputType
     */
    public java.lang.String getInputType() {
        return inputType;
    }


    /**
     * Sets the inputType value for this ConversionParameters.
     * 
     * @param inputType
     */
    public void setInputType(java.lang.String inputType) {
        this.inputType = inputType;
    }


    /**
     * Gets the outputType value for this ConversionParameters.
     * 
     * @return outputType
     */
    public java.lang.String getOutputType() {
        return outputType;
    }


    /**
     * Sets the outputType value for this ConversionParameters.
     * 
     * @param outputType
     */
    public void setOutputType(java.lang.String outputType) {
        this.outputType = outputType;
    }


    /**
     * Gets the customParameters value for this ConversionParameters.
     * 
     * @return customParameters
     */
    public java.lang.String getCustomParameters() {
        return customParameters;
    }


    /**
     * Sets the customParameters value for this ConversionParameters.
     * 
     * @param customParameters
     */
    public void setCustomParameters(java.lang.String customParameters) {
        this.customParameters = customParameters;
    }


    /**
     * Gets the outputFileMode value for this ConversionParameters.
     * 
     * @return outputFileMode
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE getOutputFileMode() {
        return outputFileMode;
    }


    /**
     * Sets the outputFileMode value for this ConversionParameters.
     * 
     * @param outputFileMode
     */
    public void setOutputFileMode(com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE outputFileMode) {
        this.outputFileMode = outputFileMode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConversionParameters)) return false;
        ConversionParameters other = (ConversionParameters) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputType==null && other.getInputType()==null) || 
             (this.inputType!=null &&
              this.inputType.equals(other.getInputType()))) &&
            ((this.outputType==null && other.getOutputType()==null) || 
             (this.outputType!=null &&
              this.outputType.equals(other.getOutputType()))) &&
            ((this.customParameters==null && other.getCustomParameters()==null) || 
             (this.customParameters!=null &&
              this.customParameters.equals(other.getCustomParameters()))) &&
            ((this.outputFileMode==null && other.getOutputFileMode()==null) || 
             (this.outputFileMode!=null &&
              this.outputFileMode.equals(other.getOutputFileMode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputType() != null) {
            _hashCode += getInputType().hashCode();
        }
        if (getOutputType() != null) {
            _hashCode += getOutputType().hashCode();
        }
        if (getCustomParameters() != null) {
            _hashCode += getCustomParameters().hashCode();
        }
        if (getOutputFileMode() != null) {
            _hashCode += getOutputFileMode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConversionParameters.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "ConversionParameters"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "inputType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "outputType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "customParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputFileMode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "outputFileMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFILE_MODE"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
