/**
 * XMLDescription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class XMLDescription  implements java.io.Serializable {
    private com.bloomnet.bom.mvc.fax.soap.submission.WSFile xmlFile;

    private int nAttachments;

    private com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments;

    public XMLDescription() {
    }

    public XMLDescription(
           com.bloomnet.bom.mvc.fax.soap.submission.WSFile xmlFile,
           int nAttachments,
           com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments) {
           this.xmlFile = xmlFile;
           this.nAttachments = nAttachments;
           this.attachments = attachments;
    }


    /**
     * Gets the xmlFile value for this XMLDescription.
     * 
     * @return xmlFile
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile getXmlFile() {
        return xmlFile;
    }


    /**
     * Sets the xmlFile value for this XMLDescription.
     * 
     * @param xmlFile
     */
    public void setXmlFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile xmlFile) {
        this.xmlFile = xmlFile;
    }


    /**
     * Gets the nAttachments value for this XMLDescription.
     * 
     * @return nAttachments
     */
    public int getNAttachments() {
        return nAttachments;
    }


    /**
     * Sets the nAttachments value for this XMLDescription.
     * 
     * @param nAttachments
     */
    public void setNAttachments(int nAttachments) {
        this.nAttachments = nAttachments;
    }


    /**
     * Gets the attachments value for this XMLDescription.
     * 
     * @return attachments
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] getAttachments() {
        return attachments;
    }


    /**
     * Sets the attachments value for this XMLDescription.
     * 
     * @param attachments
     */
    public void setAttachments(com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments) {
        this.attachments = attachments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof XMLDescription)) return false;
        XMLDescription other = (XMLDescription) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.xmlFile==null && other.getXmlFile()==null) || 
             (this.xmlFile!=null &&
              this.xmlFile.equals(other.getXmlFile()))) &&
            this.nAttachments == other.getNAttachments() &&
            ((this.attachments==null && other.getAttachments()==null) || 
             (this.attachments!=null &&
              java.util.Arrays.equals(this.attachments, other.getAttachments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getXmlFile() != null) {
            _hashCode += getXmlFile().hashCode();
        }
        _hashCode += getNAttachments();
        if (getAttachments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(XMLDescription.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "XMLDescription"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("xmlFile");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "xmlFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAttachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nAttachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "attachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "Attachment"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:SubmissionService", "Attachment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
