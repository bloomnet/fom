/**
 * SessionServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.session;

public interface SessionServiceSoap_PortType extends java.rmi.Remote {
    public com.bloomnet.bom.mvc.fax.soap.session.BindingResult getBindings(java.lang.String reserved) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.session.LoginResult login(java.lang.String userName, java.lang.String password) throws java.rmi.RemoteException;
    public void logout() throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.session.ServiceInformation getServiceInformation(java.lang.String language) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.session.SessionInformation getSessionInformation() throws java.rmi.RemoteException;
}
