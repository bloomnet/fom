/**
 * Transport.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class Transport  implements java.io.Serializable {
    private int transportID;

    private java.lang.String transportName;

    private java.lang.String recipientType;

    private int state;

    private int nVars;

    private com.bloomnet.bom.mvc.fax.soap.query.Var[] vars;

    private int nSubnodes;

    private com.bloomnet.bom.mvc.fax.soap.query.SubNode[] subnodes;

    public Transport() {
    }

    public Transport(
           int transportID,
           java.lang.String transportName,
           java.lang.String recipientType,
           int state,
           int nVars,
           com.bloomnet.bom.mvc.fax.soap.query.Var[] vars,
           int nSubnodes,
           com.bloomnet.bom.mvc.fax.soap.query.SubNode[] subnodes) {
           this.transportID = transportID;
           this.transportName = transportName;
           this.recipientType = recipientType;
           this.state = state;
           this.nVars = nVars;
           this.vars = vars;
           this.nSubnodes = nSubnodes;
           this.subnodes = subnodes;
    }


    /**
     * Gets the transportID value for this Transport.
     * 
     * @return transportID
     */
    public int getTransportID() {
        return transportID;
    }


    /**
     * Sets the transportID value for this Transport.
     * 
     * @param transportID
     */
    public void setTransportID(int transportID) {
        this.transportID = transportID;
    }


    /**
     * Gets the transportName value for this Transport.
     * 
     * @return transportName
     */
    public java.lang.String getTransportName() {
        return transportName;
    }


    /**
     * Sets the transportName value for this Transport.
     * 
     * @param transportName
     */
    public void setTransportName(java.lang.String transportName) {
        this.transportName = transportName;
    }


    /**
     * Gets the recipientType value for this Transport.
     * 
     * @return recipientType
     */
    public java.lang.String getRecipientType() {
        return recipientType;
    }


    /**
     * Sets the recipientType value for this Transport.
     * 
     * @param recipientType
     */
    public void setRecipientType(java.lang.String recipientType) {
        this.recipientType = recipientType;
    }


    /**
     * Gets the state value for this Transport.
     * 
     * @return state
     */
    public int getState() {
        return state;
    }


    /**
     * Sets the state value for this Transport.
     * 
     * @param state
     */
    public void setState(int state) {
        this.state = state;
    }


    /**
     * Gets the nVars value for this Transport.
     * 
     * @return nVars
     */
    public int getNVars() {
        return nVars;
    }


    /**
     * Sets the nVars value for this Transport.
     * 
     * @param nVars
     */
    public void setNVars(int nVars) {
        this.nVars = nVars;
    }


    /**
     * Gets the vars value for this Transport.
     * 
     * @return vars
     */
    public com.bloomnet.bom.mvc.fax.soap.query.Var[] getVars() {
        return vars;
    }


    /**
     * Sets the vars value for this Transport.
     * 
     * @param vars
     */
    public void setVars(com.bloomnet.bom.mvc.fax.soap.query.Var[] vars) {
        this.vars = vars;
    }


    /**
     * Gets the nSubnodes value for this Transport.
     * 
     * @return nSubnodes
     */
    public int getNSubnodes() {
        return nSubnodes;
    }


    /**
     * Sets the nSubnodes value for this Transport.
     * 
     * @param nSubnodes
     */
    public void setNSubnodes(int nSubnodes) {
        this.nSubnodes = nSubnodes;
    }


    /**
     * Gets the subnodes value for this Transport.
     * 
     * @return subnodes
     */
    public com.bloomnet.bom.mvc.fax.soap.query.SubNode[] getSubnodes() {
        return subnodes;
    }


    /**
     * Sets the subnodes value for this Transport.
     * 
     * @param subnodes
     */
    public void setSubnodes(com.bloomnet.bom.mvc.fax.soap.query.SubNode[] subnodes) {
        this.subnodes = subnodes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Transport)) return false;
        Transport other = (Transport) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.transportID == other.getTransportID() &&
            ((this.transportName==null && other.getTransportName()==null) || 
             (this.transportName!=null &&
              this.transportName.equals(other.getTransportName()))) &&
            ((this.recipientType==null && other.getRecipientType()==null) || 
             (this.recipientType!=null &&
              this.recipientType.equals(other.getRecipientType()))) &&
            this.state == other.getState() &&
            this.nVars == other.getNVars() &&
            ((this.vars==null && other.getVars()==null) || 
             (this.vars!=null &&
              java.util.Arrays.equals(this.vars, other.getVars()))) &&
            this.nSubnodes == other.getNSubnodes() &&
            ((this.subnodes==null && other.getSubnodes()==null) || 
             (this.subnodes!=null &&
              java.util.Arrays.equals(this.subnodes, other.getSubnodes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getTransportID();
        if (getTransportName() != null) {
            _hashCode += getTransportName().hashCode();
        }
        if (getRecipientType() != null) {
            _hashCode += getRecipientType().hashCode();
        }
        _hashCode += getState();
        _hashCode += getNVars();
        if (getVars() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVars());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVars(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getNSubnodes();
        if (getSubnodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubnodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubnodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Transport.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Transport"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "transportID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "transportName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipientType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "recipientType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NVars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nVars"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "vars"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSubnodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nSubnodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subnodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "subnodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "SubNode"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "SubNode"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
