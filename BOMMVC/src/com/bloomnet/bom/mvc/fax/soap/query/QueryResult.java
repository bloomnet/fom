/**
 * QueryResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class QueryResult  implements java.io.Serializable {
    private boolean noMoreItems;

    private int nTransports;

    private com.bloomnet.bom.mvc.fax.soap.query.Transport[] transports;

    public QueryResult() {
    }

    public QueryResult(
           boolean noMoreItems,
           int nTransports,
           com.bloomnet.bom.mvc.fax.soap.query.Transport[] transports) {
           this.noMoreItems = noMoreItems;
           this.nTransports = nTransports;
           this.transports = transports;
    }


    /**
     * Gets the noMoreItems value for this QueryResult.
     * 
     * @return noMoreItems
     */
    public boolean isNoMoreItems() {
        return noMoreItems;
    }


    /**
     * Sets the noMoreItems value for this QueryResult.
     * 
     * @param noMoreItems
     */
    public void setNoMoreItems(boolean noMoreItems) {
        this.noMoreItems = noMoreItems;
    }


    /**
     * Gets the nTransports value for this QueryResult.
     * 
     * @return nTransports
     */
    public int getNTransports() {
        return nTransports;
    }


    /**
     * Sets the nTransports value for this QueryResult.
     * 
     * @param nTransports
     */
    public void setNTransports(int nTransports) {
        this.nTransports = nTransports;
    }


    /**
     * Gets the transports value for this QueryResult.
     * 
     * @return transports
     */
    public com.bloomnet.bom.mvc.fax.soap.query.Transport[] getTransports() {
        return transports;
    }


    /**
     * Sets the transports value for this QueryResult.
     * 
     * @param transports
     */
    public void setTransports(com.bloomnet.bom.mvc.fax.soap.query.Transport[] transports) {
        this.transports = transports;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryResult)) return false;
        QueryResult other = (QueryResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.noMoreItems == other.isNoMoreItems() &&
            this.nTransports == other.getNTransports() &&
            ((this.transports==null && other.getTransports()==null) || 
             (this.transports!=null &&
              java.util.Arrays.equals(this.transports, other.getTransports())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isNoMoreItems() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getNTransports();
        if (getTransports() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransports());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransports(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueryResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "QueryResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noMoreItems");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "noMoreItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NTransports");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nTransports"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transports");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "transports"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Transport"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "Transport"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
