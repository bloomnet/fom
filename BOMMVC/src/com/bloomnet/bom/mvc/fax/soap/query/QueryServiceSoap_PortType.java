/**
 * QueryServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public interface QueryServiceSoap_PortType extends java.rmi.Remote {
    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryFirst(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryLast(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryNext(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryPrevious(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult queryAttributes(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.Attachments queryAttachments(int transportID, com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER eFilter, com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE eMode) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult queryStatistics(java.lang.String filter) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult delete(java.lang.String identifier) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult cancel(java.lang.String identifier) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult resubmit(java.lang.String identifier, com.bloomnet.bom.mvc.fax.soap.query.ResubmitParameters params) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult update(java.lang.String identifier, com.bloomnet.bom.mvc.fax.soap.query.UpdateParameters params) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult approve(java.lang.String identifier, java.lang.String reason) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult reject(java.lang.String identifier, java.lang.String reason) throws java.rmi.RemoteException;
    public byte[] downloadFile(com.bloomnet.bom.mvc.fax.soap.query.WSFile wsFile) throws java.rmi.RemoteException;
}
