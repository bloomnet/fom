/**
 * SubmissionServiceSoap_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class SubmissionServiceSoap_BindingStub extends org.apache.axis.client.Stub implements com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[13];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Submit");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "subject"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "document"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "BusinessData"), com.bloomnet.bom.mvc.fax.soap.submission.BusinessData.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "rules"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "BusinessRules"), com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SubmitTransport");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "transport"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "Transport"), com.bloomnet.bom.mvc.fax.soap.submission.Transport.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SubmitXML");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "xml"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "XMLDescription"), com.bloomnet.bom.mvc.fax.soap.submission.XMLDescription.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResults"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ExtractFirst");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "document"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "BusinessData"), com.bloomnet.bom.mvc.fax.soap.submission.BusinessData.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "rules"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "BusinessRules"), com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionParameters"), com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ExtractNext");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionParameters"), com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ConvertFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "inputFile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"), com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "ConversionParameters"), com.bloomnet.bom.mvc.fax.soap.submission.ConversionParameters.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "ConversionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UploadFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "fileContent"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"), byte[].class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "name"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DownloadFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "wsFile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"), com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        oper.setReturnClass(byte[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RegisterResource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "resource"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"), com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE"), com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "published"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "overwritePrevious"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ListResources");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE"), com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "published"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "Resources"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.Resources.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeleteResource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "resourceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE"), com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "published"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RetrieveResource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "resourceName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "type"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE"), com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "published"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), boolean.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "eMode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "WSFILE_MODE"), com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UploadFileAppend");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "fileContent"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"), byte[].class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:SubmissionService", "destWSFile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"), com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:SubmissionService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

    }

    public SubmissionServiceSoap_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public SubmissionServiceSoap_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public SubmissionServiceSoap_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Attachment>convertedAttachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.WSFile[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "WSFile");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "WSFile");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">BusinessData>attachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Attachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">BusinessData>externalVars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">ExtractionResult>transports");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Transport[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Transport");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Transport");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Resources>resources");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">SubmissionResults>transportIDs");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">SubNode>subNodes");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">SubNode>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Transport>attachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Attachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Transport>subnodes");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.SubNode[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "SubNode");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "SubNode");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Transport>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Var>multipleDoubleValues");
            cachedSerQNames.add(qName);
            cls = double[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "double");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Var>multipleLongValues");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">Var>multipleStringValues");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", ">XMLDescription>attachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Attachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            qName2 = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Attachment");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Attachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ATTACHMENTS_FILTER");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ATTACHMENTS_FILTER.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "BusinessData");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.BusinessData.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "BusinessRules");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ConversionParameters");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ConversionParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ConversionResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionHeader");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ExtractionHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionParameters");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Resources");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Resources.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "SessionHeader");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResults");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "SubNode");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.SubNode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Transport");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Transport.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "Var");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.Var.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "VAR_TYPE");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.VAR_TYPE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "WSFile");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "WSFILE_MODE");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:SubmissionService", "XMLDescription");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.submission.XMLDescription.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult submit(java.lang.String subject, com.bloomnet.bom.mvc.fax.soap.submission.BusinessData document, com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules rules) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Submit");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "Submit"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subject, document, rules});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult submitTransport(com.bloomnet.bom.mvc.fax.soap.submission.Transport transport) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#SubmitTransport");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "SubmitTransport"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transport});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults submitXML(com.bloomnet.bom.mvc.fax.soap.submission.XMLDescription xml) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#SubmitXML");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "SubmitXML"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {xml});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult extractFirst(com.bloomnet.bom.mvc.fax.soap.submission.BusinessData document, com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules rules, com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#ExtractFirst");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractFirst"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {document, rules, params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult extractNext(com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#ExtractNext");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractNext"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult convertFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile inputFile, com.bloomnet.bom.mvc.fax.soap.submission.ConversionParameters params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#ConvertFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "ConvertFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {inputFile, params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile uploadFile(byte[] fileContent, java.lang.String name) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#UploadFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "UploadFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {fileContent, name});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public byte[] downloadFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile wsFile) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#DownloadFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "DownloadFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {wsFile});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (byte[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (byte[]) org.apache.axis.utils.JavaUtils.convert(_resp, byte[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void registerResource(com.bloomnet.bom.mvc.fax.soap.submission.WSFile resource, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published, boolean overwritePrevious) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#RegisterResource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "RegisterResource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resource, type, new java.lang.Boolean(published), new java.lang.Boolean(overwritePrevious)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.Resources listResources(com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#ListResources");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "ListResources"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {type, new java.lang.Boolean(published)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.Resources) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.Resources) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.Resources.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void deleteResource(java.lang.String resourceName, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#DeleteResource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "DeleteResource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resourceName, type, new java.lang.Boolean(published)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile retrieveResource(java.lang.String resourceName, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published, com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE eMode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#RetrieveResource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "RetrieveResource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resourceName, type, new java.lang.Boolean(published), eMode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile uploadFileAppend(byte[] fileContent, com.bloomnet.bom.mvc.fax.soap.submission.WSFile destWSFile) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#UploadFileAppend");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:SubmissionService", "UploadFileAppend"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {fileContent, destWSFile});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.submission.WSFile) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.submission.WSFile.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
