/**
 * SubmissionServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public interface SubmissionServiceSoap_PortType extends java.rmi.Remote {
    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult submit(java.lang.String subject, com.bloomnet.bom.mvc.fax.soap.submission.BusinessData document, com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules rules) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult submitTransport(com.bloomnet.bom.mvc.fax.soap.submission.Transport transport) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResults submitXML(com.bloomnet.bom.mvc.fax.soap.submission.XMLDescription xml) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult extractFirst(com.bloomnet.bom.mvc.fax.soap.submission.BusinessData document, com.bloomnet.bom.mvc.fax.soap.submission.BusinessRules rules, com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters params) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.ExtractionResult extractNext(com.bloomnet.bom.mvc.fax.soap.submission.ExtractionParameters params) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.ConversionResult convertFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile inputFile, com.bloomnet.bom.mvc.fax.soap.submission.ConversionParameters params) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile uploadFile(byte[] fileContent, java.lang.String name) throws java.rmi.RemoteException;
    public byte[] downloadFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile wsFile) throws java.rmi.RemoteException;
    public void registerResource(com.bloomnet.bom.mvc.fax.soap.submission.WSFile resource, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published, boolean overwritePrevious) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.Resources listResources(com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published) throws java.rmi.RemoteException;
    public void deleteResource(java.lang.String resourceName, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile retrieveResource(java.lang.String resourceName, com.bloomnet.bom.mvc.fax.soap.submission.RESOURCE_TYPE type, boolean published, com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE eMode) throws java.rmi.RemoteException;
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile uploadFileAppend(byte[] fileContent, com.bloomnet.bom.mvc.fax.soap.submission.WSFile destWSFile) throws java.rmi.RemoteException;
}
