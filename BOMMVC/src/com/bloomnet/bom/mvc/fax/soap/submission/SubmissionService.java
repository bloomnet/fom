/**
 * SubmissionService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public interface SubmissionService extends javax.xml.rpc.Service {
    public java.lang.String getSubmissionServiceSoapAddress();

    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_PortType getSubmissionServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_PortType getSubmissionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
