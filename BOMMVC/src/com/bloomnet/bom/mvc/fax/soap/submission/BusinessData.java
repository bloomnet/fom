/**
 * BusinessData.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class BusinessData  implements java.io.Serializable {
    private com.bloomnet.bom.mvc.fax.soap.submission.WSFile file;

    private int nExternalVars;

    private com.bloomnet.bom.mvc.fax.soap.submission.Var[] externalVars;

    private int nAttachments;

    private com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments;

    public BusinessData() {
    }

    public BusinessData(
           com.bloomnet.bom.mvc.fax.soap.submission.WSFile file,
           int nExternalVars,
           com.bloomnet.bom.mvc.fax.soap.submission.Var[] externalVars,
           int nAttachments,
           com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments) {
           this.file = file;
           this.nExternalVars = nExternalVars;
           this.externalVars = externalVars;
           this.nAttachments = nAttachments;
           this.attachments = attachments;
    }


    /**
     * Gets the file value for this BusinessData.
     * 
     * @return file
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.WSFile getFile() {
        return file;
    }


    /**
     * Sets the file value for this BusinessData.
     * 
     * @param file
     */
    public void setFile(com.bloomnet.bom.mvc.fax.soap.submission.WSFile file) {
        this.file = file;
    }


    /**
     * Gets the nExternalVars value for this BusinessData.
     * 
     * @return nExternalVars
     */
    public int getNExternalVars() {
        return nExternalVars;
    }


    /**
     * Sets the nExternalVars value for this BusinessData.
     * 
     * @param nExternalVars
     */
    public void setNExternalVars(int nExternalVars) {
        this.nExternalVars = nExternalVars;
    }


    /**
     * Gets the externalVars value for this BusinessData.
     * 
     * @return externalVars
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.Var[] getExternalVars() {
        return externalVars;
    }


    /**
     * Sets the externalVars value for this BusinessData.
     * 
     * @param externalVars
     */
    public void setExternalVars(com.bloomnet.bom.mvc.fax.soap.submission.Var[] externalVars) {
        this.externalVars = externalVars;
    }


    /**
     * Gets the nAttachments value for this BusinessData.
     * 
     * @return nAttachments
     */
    public int getNAttachments() {
        return nAttachments;
    }


    /**
     * Sets the nAttachments value for this BusinessData.
     * 
     * @param nAttachments
     */
    public void setNAttachments(int nAttachments) {
        this.nAttachments = nAttachments;
    }


    /**
     * Gets the attachments value for this BusinessData.
     * 
     * @return attachments
     */
    public com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] getAttachments() {
        return attachments;
    }


    /**
     * Sets the attachments value for this BusinessData.
     * 
     * @param attachments
     */
    public void setAttachments(com.bloomnet.bom.mvc.fax.soap.submission.Attachment[] attachments) {
        this.attachments = attachments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BusinessData)) return false;
        BusinessData other = (BusinessData) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.file==null && other.getFile()==null) || 
             (this.file!=null &&
              this.file.equals(other.getFile()))) &&
            this.nExternalVars == other.getNExternalVars() &&
            ((this.externalVars==null && other.getExternalVars()==null) || 
             (this.externalVars!=null &&
              java.util.Arrays.equals(this.externalVars, other.getExternalVars()))) &&
            this.nAttachments == other.getNAttachments() &&
            ((this.attachments==null && other.getAttachments()==null) || 
             (this.attachments!=null &&
              java.util.Arrays.equals(this.attachments, other.getAttachments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFile() != null) {
            _hashCode += getFile().hashCode();
        }
        _hashCode += getNExternalVars();
        if (getExternalVars() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExternalVars());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExternalVars(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getNAttachments();
        if (getAttachments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BusinessData.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "BusinessData"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("file");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "file"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFile"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NExternalVars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nExternalVars"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("externalVars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "externalVars"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "Var"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:SubmissionService", "Var"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAttachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nAttachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "attachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "Attachment"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:SubmissionService", "Attachment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
