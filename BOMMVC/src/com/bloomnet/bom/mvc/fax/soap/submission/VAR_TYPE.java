/**
 * VAR_TYPE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class VAR_TYPE implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected VAR_TYPE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _TYPE_STRING = "TYPE_STRING";
    public static final java.lang.String _TYPE_DATETIME = "TYPE_DATETIME";
    public static final java.lang.String _TYPE_DOUBLE = "TYPE_DOUBLE";
    public static final java.lang.String _TYPE_INTEGER = "TYPE_INTEGER";
    public static final VAR_TYPE TYPE_STRING = new VAR_TYPE(_TYPE_STRING);
    public static final VAR_TYPE TYPE_DATETIME = new VAR_TYPE(_TYPE_DATETIME);
    public static final VAR_TYPE TYPE_DOUBLE = new VAR_TYPE(_TYPE_DOUBLE);
    public static final VAR_TYPE TYPE_INTEGER = new VAR_TYPE(_TYPE_INTEGER);
    public java.lang.String getValue() { return _value_;}
    public static VAR_TYPE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        VAR_TYPE enumeration = (VAR_TYPE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static VAR_TYPE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VAR_TYPE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "VAR_TYPE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
