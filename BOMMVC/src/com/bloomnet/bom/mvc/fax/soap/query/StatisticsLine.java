/**
 * StatisticsLine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class StatisticsLine  implements java.io.Serializable {
    private int nStates;

    private int[] states;

    private int[] counts;

    public StatisticsLine() {
    }

    public StatisticsLine(
           int nStates,
           int[] states,
           int[] counts) {
           this.nStates = nStates;
           this.states = states;
           this.counts = counts;
    }


    /**
     * Gets the nStates value for this StatisticsLine.
     * 
     * @return nStates
     */
    public int getNStates() {
        return nStates;
    }


    /**
     * Sets the nStates value for this StatisticsLine.
     * 
     * @param nStates
     */
    public void setNStates(int nStates) {
        this.nStates = nStates;
    }


    /**
     * Gets the states value for this StatisticsLine.
     * 
     * @return states
     */
    public int[] getStates() {
        return states;
    }


    /**
     * Sets the states value for this StatisticsLine.
     * 
     * @param states
     */
    public void setStates(int[] states) {
        this.states = states;
    }


    /**
     * Gets the counts value for this StatisticsLine.
     * 
     * @return counts
     */
    public int[] getCounts() {
        return counts;
    }


    /**
     * Sets the counts value for this StatisticsLine.
     * 
     * @param counts
     */
    public void setCounts(int[] counts) {
        this.counts = counts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatisticsLine)) return false;
        StatisticsLine other = (StatisticsLine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nStates == other.getNStates() &&
            ((this.states==null && other.getStates()==null) || 
             (this.states!=null &&
              java.util.Arrays.equals(this.states, other.getStates()))) &&
            ((this.counts==null && other.getCounts()==null) || 
             (this.counts!=null &&
              java.util.Arrays.equals(this.counts, other.getCounts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNStates();
        if (getStates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatisticsLine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NStates");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nStates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("states");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "states"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("counts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "counts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "int"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
