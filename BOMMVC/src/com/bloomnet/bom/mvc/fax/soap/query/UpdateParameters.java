/**
 * UpdateParameters.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class UpdateParameters  implements java.io.Serializable {
    private int nVars;

    private com.bloomnet.bom.mvc.fax.soap.query.Var[] vars;

    public UpdateParameters() {
    }

    public UpdateParameters(
           int nVars,
           com.bloomnet.bom.mvc.fax.soap.query.Var[] vars) {
           this.nVars = nVars;
           this.vars = vars;
    }


    /**
     * Gets the nVars value for this UpdateParameters.
     * 
     * @return nVars
     */
    public int getNVars() {
        return nVars;
    }


    /**
     * Sets the nVars value for this UpdateParameters.
     * 
     * @param nVars
     */
    public void setNVars(int nVars) {
        this.nVars = nVars;
    }


    /**
     * Gets the vars value for this UpdateParameters.
     * 
     * @return vars
     */
    public com.bloomnet.bom.mvc.fax.soap.query.Var[] getVars() {
        return vars;
    }


    /**
     * Sets the vars value for this UpdateParameters.
     * 
     * @param vars
     */
    public void setVars(com.bloomnet.bom.mvc.fax.soap.query.Var[] vars) {
        this.vars = vars;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateParameters)) return false;
        UpdateParameters other = (UpdateParameters) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nVars == other.getNVars() &&
            ((this.vars==null && other.getVars()==null) || 
             (this.vars!=null &&
              java.util.Arrays.equals(this.vars, other.getVars())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNVars();
        if (getVars() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVars());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVars(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateParameters.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "UpdateParameters"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NVars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nVars"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "vars"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
