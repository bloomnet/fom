/**
 * QueryServiceSoap_BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class QueryServiceSoap_BindingStub extends org.apache.axis.client.Stub implements com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_PortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[14];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryFirst");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"), com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "QueryResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryLast");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"), com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "QueryResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryNext");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"), com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "QueryResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryPrevious");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"), com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "QueryResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryAttributes");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"), com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "QueryAttributesResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryAttachments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "transportID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "eFilter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "ATTACHMENTS_FILTER"), com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "eMode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "WSFILE_MODE"), com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "Attachments"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.Attachments.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("QueryStatistics");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "filter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "StatisticsResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Delete");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Cancel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Resubmit");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "ResubmitParameters"), com.bloomnet.bom.mvc.fax.soap.query.ResubmitParameters.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Update");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "params"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "UpdateParameters"), com.bloomnet.bom.mvc.fax.soap.query.UpdateParameters.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Approve");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "reason"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Reject");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "identifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "reason"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        oper.setReturnClass(com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DownloadFile");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:QueryService", "wsFile"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:QueryService", "WSFile"), com.bloomnet.bom.mvc.fax.soap.query.WSFile.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        oper.setReturnClass(byte[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:QueryService", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

    }

    public QueryServiceSoap_BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public QueryServiceSoap_BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public QueryServiceSoap_BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:QueryService", ">ActionResult>errorReason");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">ActionResult>transportIDs");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Attachment>convertedAttachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.WSFile[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "WSFile");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "WSFile");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Attachments>attachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Attachment[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Attachment");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Attachment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">QueryAttributesResult>attributes");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">QueryResult>transports");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Transport[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Transport");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Transport");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">ResubmitParameters>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">StatisticsLine>counts");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">StatisticsLine>states");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">StatisticsResult>typeContent");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">StatisticsResult>typeName");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">SubNode>subNodes");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">SubNode>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Transport>subnodes");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.SubNode[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "SubNode");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "SubNode");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Transport>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">UpdateParameters>vars");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Var[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:QueryService", "Var");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "Var");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Var>multipleDoubleValues");
            cachedSerQNames.add(qName);
            cls = double[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "double");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Var>multipleLongValues");
            cachedSerQNames.add(qName);
            cls = int[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "int");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", ">Var>multipleStringValues");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:QueryService", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:QueryService", "ActionResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "Attachment");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Attachment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "Attachments");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Attachments.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "ATTACHMENTS_FILTER");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "QueryAttributesResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "QueryHeader");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.QueryHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "QueryOptions");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.QueryOptions.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "QueryRequest");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "QueryResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "ResubmitParameters");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.ResubmitParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "SessionHeader");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.SessionHeader.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "StatisticsResult");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "SubNode");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.SubNode.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "Transport");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Transport.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "UpdateParameters");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.UpdateParameters.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "Var");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.Var.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "VAR_TYPE");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "WSFile");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.WSFile.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:QueryService", "WSFILE_MODE");
            cachedSerQNames.add(qName);
            cls = com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryFirst(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryFirst");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryFirst"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryLast(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryLast");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryLast"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryNext(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryNext");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryNext"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.QueryResult queryPrevious(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryPrevious");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryPrevious"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.QueryResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult queryAttributes(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryAttributes");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryAttributes"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.QueryAttributesResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.Attachments queryAttachments(int transportID, com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER eFilter, com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE eMode) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryAttachments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryAttachments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {new java.lang.Integer(transportID), eFilter, eMode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.Attachments) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.Attachments) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.Attachments.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult queryStatistics(java.lang.String filter) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#QueryStatistics");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "QueryStatistics"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {filter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.StatisticsResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult delete(java.lang.String identifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Delete");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Delete"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult cancel(java.lang.String identifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Cancel");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Cancel"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult resubmit(java.lang.String identifier, com.bloomnet.bom.mvc.fax.soap.query.ResubmitParameters params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Resubmit");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Resubmit"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier, params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult update(java.lang.String identifier, com.bloomnet.bom.mvc.fax.soap.query.UpdateParameters params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Update");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Update"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier, params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult approve(java.lang.String identifier, java.lang.String reason) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Approve");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Approve"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier, reason});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.bloomnet.bom.mvc.fax.soap.query.ActionResult reject(java.lang.String identifier, java.lang.String reason) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#Reject");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "Reject"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {identifier, reason});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.bloomnet.bom.mvc.fax.soap.query.ActionResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.bloomnet.bom.mvc.fax.soap.query.ActionResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public byte[] downloadFile(com.bloomnet.bom.mvc.fax.soap.query.WSFile wsFile) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("#DownloadFile");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("urn:QueryService", "DownloadFile"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {wsFile});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (byte[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (byte[]) org.apache.axis.utils.JavaUtils.convert(_resp, byte[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
