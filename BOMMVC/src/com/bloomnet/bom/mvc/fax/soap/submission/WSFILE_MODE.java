/**
 * WSFILE_MODE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class WSFILE_MODE implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected WSFILE_MODE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _MODE_UNDEFINED = "MODE_UNDEFINED";
    public static final java.lang.String _MODE_ON_SERVER = "MODE_ON_SERVER";
    public static final java.lang.String _MODE_INLINED = "MODE_INLINED";
    public static final WSFILE_MODE MODE_UNDEFINED = new WSFILE_MODE(_MODE_UNDEFINED);
    public static final WSFILE_MODE MODE_ON_SERVER = new WSFILE_MODE(_MODE_ON_SERVER);
    public static final WSFILE_MODE MODE_INLINED = new WSFILE_MODE(_MODE_INLINED);
    public java.lang.String getValue() { return _value_;}
    public static WSFILE_MODE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        WSFILE_MODE enumeration = (WSFILE_MODE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static WSFILE_MODE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSFILE_MODE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "WSFILE_MODE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
