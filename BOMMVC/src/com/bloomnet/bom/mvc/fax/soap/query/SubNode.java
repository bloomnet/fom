/**
 * SubNode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class SubNode  implements java.io.Serializable {
    private java.lang.String name;

    private java.lang.String relativeName;

    private int nSubnodes;

    private java.lang.String[] subNodes;

    private int nVars;

    private com.bloomnet.bom.mvc.fax.soap.query.Var[] vars;

    public SubNode() {
    }

    public SubNode(
           java.lang.String name,
           java.lang.String relativeName,
           int nSubnodes,
           java.lang.String[] subNodes,
           int nVars,
           com.bloomnet.bom.mvc.fax.soap.query.Var[] vars) {
           this.name = name;
           this.relativeName = relativeName;
           this.nSubnodes = nSubnodes;
           this.subNodes = subNodes;
           this.nVars = nVars;
           this.vars = vars;
    }


    /**
     * Gets the name value for this SubNode.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this SubNode.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the relativeName value for this SubNode.
     * 
     * @return relativeName
     */
    public java.lang.String getRelativeName() {
        return relativeName;
    }


    /**
     * Sets the relativeName value for this SubNode.
     * 
     * @param relativeName
     */
    public void setRelativeName(java.lang.String relativeName) {
        this.relativeName = relativeName;
    }


    /**
     * Gets the nSubnodes value for this SubNode.
     * 
     * @return nSubnodes
     */
    public int getNSubnodes() {
        return nSubnodes;
    }


    /**
     * Sets the nSubnodes value for this SubNode.
     * 
     * @param nSubnodes
     */
    public void setNSubnodes(int nSubnodes) {
        this.nSubnodes = nSubnodes;
    }


    /**
     * Gets the subNodes value for this SubNode.
     * 
     * @return subNodes
     */
    public java.lang.String[] getSubNodes() {
        return subNodes;
    }


    /**
     * Sets the subNodes value for this SubNode.
     * 
     * @param subNodes
     */
    public void setSubNodes(java.lang.String[] subNodes) {
        this.subNodes = subNodes;
    }


    /**
     * Gets the nVars value for this SubNode.
     * 
     * @return nVars
     */
    public int getNVars() {
        return nVars;
    }


    /**
     * Sets the nVars value for this SubNode.
     * 
     * @param nVars
     */
    public void setNVars(int nVars) {
        this.nVars = nVars;
    }


    /**
     * Gets the vars value for this SubNode.
     * 
     * @return vars
     */
    public com.bloomnet.bom.mvc.fax.soap.query.Var[] getVars() {
        return vars;
    }


    /**
     * Sets the vars value for this SubNode.
     * 
     * @param vars
     */
    public void setVars(com.bloomnet.bom.mvc.fax.soap.query.Var[] vars) {
        this.vars = vars;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubNode)) return false;
        SubNode other = (SubNode) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.relativeName==null && other.getRelativeName()==null) || 
             (this.relativeName!=null &&
              this.relativeName.equals(other.getRelativeName()))) &&
            this.nSubnodes == other.getNSubnodes() &&
            ((this.subNodes==null && other.getSubNodes()==null) || 
             (this.subNodes!=null &&
              java.util.Arrays.equals(this.subNodes, other.getSubNodes()))) &&
            this.nVars == other.getNVars() &&
            ((this.vars==null && other.getVars()==null) || 
             (this.vars!=null &&
              java.util.Arrays.equals(this.vars, other.getVars())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getRelativeName() != null) {
            _hashCode += getRelativeName().hashCode();
        }
        _hashCode += getNSubnodes();
        if (getSubNodes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubNodes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubNodes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += getNVars();
        if (getVars() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVars());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVars(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubNode.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "SubNode"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relativeName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "relativeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSubnodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nSubnodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subNodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "subNodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NVars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nVars"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vars");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "vars"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
