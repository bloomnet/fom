package com.bloomnet.bom.mvc.fax;

import java.io.IOException;
import java.util.Properties;

public class class1
{
	public static void SetNillable(org.apache.axis.description.TypeDesc td)
	{
		org.apache.axis.description.FieldDesc[] fields = td.getFields();
		for(int i=0; i<fields.length; i++)
			((org.apache.axis.description.ElementDesc)fields[i]).setNillable(true);
	}

	static public void main(String[] args)
	{
		
	/*	Properties systemProps = System.getProperties();
		systemProps.put("proxySet", "true");
		systemProps.put("proxyHost", "iarraywb");
		systemProps.put("proxyPort", "8080");*/
		// if you want to use a proxy server, change this
//		System.setProperty("http.proxyHost", "iarraywb");	// Hostname of proxy server
//		System.setProperty("http.proxyPort", "8080");				// Port on server of proxy
//		System.setProperty ("http.proxyUser", "someone");			// Optional username for proxy authentication
//		System.setProperty ("http.proxyPassword", "password");		// Optional proxy server password

		// Warning / Axis 1.4 compatibility: those items are nillable, even if the WSDL doesn't say so.
		SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.WSFile.getTypeDesc());
		SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.Transport.getTypeDesc());
		SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.Attachment.getTypeDesc());
		SetNillable(com.bloomnet.bom.mvc.fax.soap.query.WSFile.getTypeDesc());
		SetNillable(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.getTypeDesc());

		while(true)
		{

			System.out.println("<1> SendFax Sample");
			System.out.println("<2> SendSMTP Sample");
			System.out.println("<3> SendMOD Sample");
			System.out.println("<4> SendSMS Sample");
			System.out.println("<5> RecvFax Sample");
			System.out.println("<Q> Quit");

			try
			{
				int key = System.in.read();

				// ignore further keyboard events
				System.in.skip(System.in.available());

				switch(key)
				{
					case '1':
						SendFax sample1 = new SendFax();
					//	sample1.Run();
						return;
					case '2':
						SendSMTP sample2 = new SendSMTP();
						sample2.Run();
						return;
					case '3':
						SendMailOnDemand sample3 = new SendMailOnDemand();
						sample3.Run();
						return;
					case '4':
						SendSMS sample4 = new SendSMS();
						sample4.Run();
						return;
					case '5':
						RecvFax sample5 = new RecvFax();
						sample5.Run();
						return;
					case 'q':
					case 'Q':
						return;
					default:
						break;
				}
			}
			catch(IOException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
			}
		}
	}
}