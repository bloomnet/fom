package com.bloomnet.bom.mvc.fax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.bloomnet.bom.mvc.fax.soap.query.QueryRequest;
import com.bloomnet.bom.mvc.fax.soap.query.QueryResult;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.session.BindingResult;
import com.bloomnet.bom.mvc.fax.soap.session.LoginResult;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Attachment;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Transport;
import com.bloomnet.bom.mvc.fax.soap.submission.VAR_TYPE;
import com.bloomnet.bom.mvc.fax.soap.submission.Var;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFile;

class SendSMTP
{
	//////////////////////////////////////////////////////////////////////
	// STEP #1 : Global Initializations
	//////////////////////////////////////////////////////////////////////
	// Sample parameters
	String m_Username = "mYus3r";	                        // Session username
	String m_Password = "mYpassw0rd";						// Session password
	String m_EMailAttachment1 = "data\\azur1023.txt";				// the first attachment file

	// Web Service URL, change with the required server url
	String m_WebServiceUrl = "https://as1.ondemand.esker.com:443/EDPWS/EDPWS.dll?Handler=Default&Version=1.0";
	int m_PollingInterval = 15000;					            // check Email status every 15 seconds

	// Method used to read data from a file and store them in a Web Service file object.
	private static WSFile ReadFile(String filename)
	{
		WSFile wsFile = new WSFile();
		wsFile.setMode(WSFILE_MODE.MODE_INLINED);
		wsFile.setName(shortFileName(filename));

		try
		{
			FileInputStream stream = new FileInputStream(filename);
			byte[] data = new byte[(int) stream.available()];
			stream.read(data, 0, data.length);
			wsFile.setContent(data);
			stream.close();
		}
		catch(FileNotFoundException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
		}
		catch(IOException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
		}

		return wsFile;
	}

	// Helper method to allocate and fill in Variable objects.
	private static Var CreateValue(String AttributeName, String AttributeValue)
	{
		Var var = new Var();
		var.setAttribute(AttributeName);
		var.setSimpleValue(AttributeValue);
		var.setType(VAR_TYPE.TYPE_STRING);
		return var;
	}

	// Helper method to extract the short file name from a full file path
	public static String shortFileName(String filename)
	{
		int i = filename.lastIndexOf("\\");
		if(i < 0)
		{
			return filename;
		}
		return filename.substring(i + 1);
	}

	public void Run()
	{
		//////////////////////////////////////////////////////////////////////
		// STEP #2 : Initialization + Authentication
		//////////////////////////////////////////////////////////////////////

		System.out.println("Retrieving bindings");

		SessionServiceSoap_BindingStub session = null;
		try
		{
			SessionServiceLocator locator = new SessionServiceLocator();
			locator.setEndpointAddress("SessionServiceSoap", m_WebServiceUrl);
			session = (SessionServiceSoap_BindingStub) locator.getSessionServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Retrieve the bindings on the Application Server (locations of the web services)
		BindingResult bindings = null;
		try
		{
			bindings = session.getBindings(m_Username);
		}
		catch(java.rmi.RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		System.out.println("Binding = " + bindings.getSessionServiceLocation());


		// Now uses the returned URL with our session object, in case the Application Server redirected us.
		session._setProperty(SessionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSessionServiceLocation());

		System.out.println("Authenticating session");

		// Authenticate the user on this session object to retrieve a sessionID
		LoginResult login = null;
		try
		{
			login = session.login(m_Username, m_Password);

			// Set the returned session header "SessionHeaderValue" as a submission session header.
			session.setHeader(session.getResponseHeader(new SessionServiceLocator().getServiceName().getNamespaceURI(), "SessionHeaderValue"));
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// This sessionID is an impersonation token representing the logged on user
		// You can use it with other Web Services objects, until you call Logout (which releases the
		// current sessionID and it's associated resources), or until the session times out (default is 10
		// minutes on the Application Server).
		System.out.println("SessionID = " + login.getSessionID());


		//////////////////////////////////////////////////////////////////////
		// STEP #3 : Simple Email submission
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a SubmissionService object.
		SubmissionServiceSoap_BindingStub submission = null;
		try
		{
			submission = (SubmissionServiceSoap_BindingStub) new SubmissionServiceLocator().getSubmissionServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service url with the location retrieved above with GetBindings()
		submission._setProperty(SubmissionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSubmissionServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader s_header = new com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader();
		s_header.setSessionID(login.getSessionID());
		submission.setHeader(
			new SubmissionServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			s_header);

		System.out.println("Sending Email Request");

		// Now allocate a transport with transportName = "Mail"
		Transport transport = new Transport();
		transport.setRecipientType("");
		transport.setTransportIndex("");
		transport.setTransportName("Mail");

		// Specifies Email variables (see documentation for their definitions)
		Var[] vars = new Var[8];
		vars[0] = CreateValue("Subject", "Sample eMail");
		vars[1] = CreateValue("EmailAddress", "customer@someplace.net");
		vars[2] = CreateValue("Message", "This is a sample eMail, including one attachment");
		vars[3] = CreateValue("FromName", "John DOE");
		vars[4] = CreateValue("FromCompany", "Dummy Inc.");
		vars[5] = CreateValue("FromAddress", "salesdep@dummy.com");
		vars[6] = CreateValue("ToName", "Jay TOUCHAMPS");
		vars[7] = CreateValue("ToCompany", "Touchamps SA");

		transport.setVars(vars);

		// Specify a text attachment to append to the email.
		// The attachment content is inlined in the transport description
		// We also request the attachment to be converted to pdf format and named 'YourInformations.pdf' 
		// before being sent
		WSFile attachedFile = ReadFile(m_EMailAttachment1);
		attachedFile.setMode(WSFILE_MODE.MODE_INLINED);
		attachedFile.setName(m_EMailAttachment1);

		Attachment[] attachments = new Attachment[1];
		attachments[0] = new Attachment();
		attachments[0].setSourceAttachment(attachedFile);
		attachments[0].setInputFormat("");
		attachments[0].setOutputFormat(".pdf");
		attachments[0].setOutputName("YourInformations.pdf");
		attachments[0].setStylesheet("");

		transport.setAttachments(attachments);

		// Submit the complete transport description to the Application Server
		SubmissionResult result = null;
		try
		{
			result = submission.submitTransport(transport);
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		System.out.println("Request submitted with transportID " + result.getTransportID());


		//////////////////////////////////////////////////////////////////////
		// STEP #4 : Email tracking
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a QueryService object.
		QueryServiceSoap_BindingStub query = null;
		try
		{
			query = (QueryServiceSoap_BindingStub) new QueryServiceLocator().getQueryServiceSoap();
		}
		catch(ServiceException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service URL with the location retrieved above with GetBindings()
		query._setProperty(QueryServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getQueryServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.query.SessionHeader q_header = new com.bloomnet.bom.mvc.fax.soap.query.SessionHeader();
		q_header.setSessionID(login.getSessionID());
		query.setHeader(
			new QueryServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			q_header);

		// Build a request on the newly submitted Email transport using its unique identifier
		// We also specify the variables (attributes) we want to retrieve.
		QueryRequest request = new QueryRequest();
		request.setNItems(1);
		request.setAttributes("State,ShortStatus,CompletionDateTime");
		// These variables are documented on the page
		// http://doc.esker.com/eskerondemand/cv/en/webservices/index.asp?page=References/Common/RecipientTypes.html
		request.setFilter("(&(msn=" + result.getTransportID() + ")(TransportName=Mail))");
		request.setSortOrder("");

		QueryResult qresult = null;

		System.out.println("Checking for your Email status...");

		int state = 0;
		String status = "";
		String date = "";

		while(true)
		{
			try
			{
				// Ask the Application Server
				qresult = query.queryFirst(request);
			}
			catch(RemoteException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}

			if(qresult.getNTransports() == 1)
			{
				// Hopefully, we found it
				// Parse the returned variables
				com.bloomnet.bom.mvc.fax.soap.query.Transport transport2 = qresult.getTransports()[0];
				for(int iVar = 0; iVar < transport2.getNVars(); iVar++)
				{
					com.bloomnet.bom.mvc.fax.soap.query.Var var = transport2.getVars()[iVar];
					String attribute = var.getAttribute().toLowerCase();
					if(attribute.compareTo("state") == 0)
					{
						state = Integer.parseInt(var.getSimpleValue());
					}
					else if(attribute.compareTo("shortstatus") == 0)
					{
						status = var.getSimpleValue();
					}
					else if(attribute.compareTo("completiondatetime") == 0)
					{
						date = var.getSimpleValue();
					}
				}

				if(state >= 100)
				{
					break;
				}

				System.out.println("Email pending...");
			}
			else
			{
				System.out.println("Error !! Email not found in database");
				return;
			}

			// Wait 15 seconds, then try again...
			try
			{
				Thread.sleep(m_PollingInterval);
			}
			catch(InterruptedException ex)
			{
				System.out.println("An unexpected error has occurred." + ex.getMessage());
				return;
			}
		}

		// If the Email is successfully sent (state=100), retrieves the final Email image
		// for remote display (using a web browser), and also download the image data for local processing
		if(state == 100)
		{
			System.out.println("eMail successfully sent at " + date);
		}
		else
		{
			System.out.println("eMail failed at + " + date + ", reason: " + status);
		}


		//////////////////////////////////////////////////////////////////////
		// STEP #5 : Release the session and its allocated resources
		//////////////////////////////////////////////////////////////////////

		System.out.println("Press <enter> to quit...");
		try
		{
			System.in.read();
		}
		catch(IOException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// As soon as you call Logout(), the files allocated on the server during this session won't be available
		// any more, so keep in mind that former urls are now useless...

		System.out.println("Releasing session and server files");

		try
		{
			session.logout();
		}
		catch(RemoteException ex)
		{
			System.out.println("An unexpected error has occurred." + ex.getMessage());
			return;
		}
	}
}


