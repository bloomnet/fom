
package com.bloomnet.bom.mvc.fax;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.swing.text.MaskFormatter;

import org.springframework.beans.factory.annotation.Autowired;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.OccassionUtil;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class FaxTemplate {


	protected  BaseFont bf;

	// Injected property
	@Autowired private Properties bomProperties;


	public void createPdf(MessageOnOrderBean messageOnOrderBean, String shopCode)
	throws IOException, DocumentException {


		String filename = bomProperties.getProperty(BOMConstants.FAX_DIR);
		
		String orderNumber = messageOnOrderBean.getOrderNumber();
		if (orderNumber==null){
			orderNumber = messageOnOrderBean.getBmtOrderNumber();
		}

		filename = filename + "BMTOrder_" + orderNumber + ".pdf";
		File file = new File(filename); 


		bf = BaseFont.createFont();
		Font f = new Font(bf,10);
		f.setColor(BaseColor.WHITE);

		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(file));
		// step 3
		document.open();
		// step 4


		document.add(generalTitle());
		document.add(generalTable(messageOnOrderBean,f));


		document.add(fulfillingShopTable(messageOnOrderBean, shopCode,f));

		document.add(orderDetailsTable(messageOnOrderBean,f));

		document.add(recipientTable(messageOnOrderBean,f));


		// step 5
		document.close();
	}

	public PdfPTable generalTitle() throws DocumentException, IOException {
		PdfPTable table = new PdfPTable(4);
		table.setSpacingBefore(5);
		table.setSpacingAfter(5);
		PdfPCell title;
		PdfPCell blank1 = new PdfPCell(new Phrase(" "));

		BaseFont baseFont = BaseFont.createFont();
		Font f = new Font(baseFont,12);



		title = new PdfPCell(new Phrase("BloomNet Commitment to Coverage",f));
		title.setBorder(0);
		title.setColspan(4);
		title.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(title);
		//blank separation
		blank1.setColspan(4);
		blank1.setBorder(0);
		table.addCell(blank1);
		//table.addCell(blank1);


		return table;
	}
	/**
	 * Creates our first table
	 * @param messageOnOrderBean 
	 * @return our first table
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	public PdfPTable generalTable(MessageOnOrderBean messageOnOrderBean,Font f) throws DocumentException, IOException {




		PdfPTable table = new PdfPTable(4);
		table.setSpacingBefore(10);
		table.setSpacingAfter(10);
		PdfPCell title;
		PdfPCell blank1 = new PdfPCell(new Phrase(" "));
		blank1.setBorder(0);
		BaseColor backgroundColor = BaseColor.DARK_GRAY;





		PdfPCell orderInfoHeader;
		PdfPCell orderInfoVals;
		PdfPCell seqno =  new PdfPCell(new Phrase("Sequence Number",f));
		PdfPCell deliverDate = new PdfPCell(new Phrase("Delivery Date",f));
		PdfPCell received = new PdfPCell(new Phrase("Received on",f));
		seqno.setBackgroundColor(backgroundColor);
		deliverDate.setBackgroundColor(backgroundColor);
		received.setBackgroundColor(backgroundColor);

		//order info header
		orderInfoHeader = new PdfPCell(new Phrase("Order Number",f));
		orderInfoHeader.setColspan(1);
		orderInfoHeader.setBackgroundColor(backgroundColor);

		table.addCell(orderInfoHeader);
		table.addCell(seqno);
		table.addCell(deliverDate);
		table.addCell(received);

		//order info values
		orderInfoVals = new PdfPCell(new Phrase(messageOnOrderBean.getBmtOrderNumber()));
		orderInfoVals.setColspan(1);
		table.addCell(orderInfoVals);
		table.addCell(Long.toString(messageOnOrderBean.getBmtSeqNumberOfOrder()));
		table.addCell(messageOnOrderBean.getDeliveryDate());
		table.addCell(DateUtil.toString(messageOnOrderBean.getCaptureDate()));
		table.addCell(blank1); 

		return table;
	}

	public PdfPTable fulfillingShopTable(MessageOnOrderBean messageOnOrderBean,String shopCode, Font f) throws DocumentException, IOException {

		PdfPTable table = new PdfPTable(4);
		table.setSpacingBefore(5);
		table.setSpacingAfter(5);
		// the cell object
		PdfPCell title;
		PdfPCell blank1 = new PdfPCell(new Phrase(" "));
		BaseColor backgroundColor = BaseColor.DARK_GRAY;
		BaseColor backgroundColor2 = BaseColor.LIGHT_GRAY;


		BaseFont baseFont = BaseFont.createFont();
		Font f2 = new Font(baseFont,12);
		f2.setColor(BaseColor.BLACK);




		PdfPCell fulfillShop;

		Shop shop = messageOnOrderBean.getFulfillingShop();

		table.setSpacingBefore(5);
		table.setSpacingAfter(5);


		PdfPTable table2 = new PdfPTable(4);

		PdfPCell parentCell = new PdfPCell();
		parentCell.setColspan(4);
		parentCell.setRowspan(4);

		String firstName = messageOnOrderBean.getRecipientFirstName();
		String lastName = messageOnOrderBean.getRecipientLastName();



		//Recipient 
		PdfPCell recipientInfo = new PdfPCell(new Phrase("Florist Information",f));
		recipientInfo.setColspan(4);
		recipientInfo.setBackgroundColor(backgroundColor);
		table.addCell(recipientInfo);


		//table.addCell(parentCell);




		//shop name       
		PdfPCell shopname = new PdfPCell(new Phrase(shop.getShopName()));
		shopname.setColspan(4);
		shopname.setRowspan(1);
		shopname.setBorder(0);
		table2.addCell(shopname);

		PdfPCell shopcodeCell = new PdfPCell(new Phrase(shopCode));
		shopcodeCell.setColspan(4);
		shopcodeCell.setRowspan(1);
		shopcodeCell.setBorder(0);
		table2.addCell(shopcodeCell);

		//shop address
		PdfPCell raddress = new PdfPCell(new Phrase(shop.getShopAddress1()));
		raddress.setColspan(4);
		raddress.setRowspan(1);
		raddress.setBorder(0);
		table2.addCell(raddress);


		String country = messageOnOrderBean.getRecipientCountryCode();
		if (country==null){
			country = " ";
		}
		PdfPCell address = new PdfPCell(new Phrase(messageOnOrderBean.getRecipientCity()+ " " + messageOnOrderBean.getRecipientState()+"," + country + " "+ messageOnOrderBean.getRecipientZipCode()));
		address.setColspan(4);
		address.setRowspan(1);
		address.setBorder(0);
		table2.addCell(address);

		PdfPCell shopphone = new PdfPCell(new Phrase("Phone: " +formatPhoneNumberFormated(shop.getShopPhone())));
		shopphone.setColspan(4);
		shopphone.setRowspan(1);
		shopphone.setBorder(0);
		table2.addCell(shopphone);

		PdfPCell shopfax = new PdfPCell(new Phrase("Fax: " +formatPhoneNumberFormated(shop.getShopFax())));
		shopfax.setColspan(4);
		shopfax.setRowspan(1);
		shopfax.setBorder(0);
		table2.addCell(shopfax);


		PdfPCell innerTable = new PdfPCell(table2);
		innerTable.setColspan(4);
		table.addCell(innerTable); 


		return table;
	}


	public PdfPTable orderDetailsTable(MessageOnOrderBean messageOnOrderBean, Font f) throws DocumentException, IOException {

		PdfPTable table = new PdfPTable(4);
		table.setSpacingBefore(5);
		table.setSpacingAfter(5);
		// the cell object
		PdfPCell blank1 = new PdfPCell(new Phrase(" "));
		BaseColor backgroundColor = BaseColor.DARK_GRAY;
		BaseColor backgroundColor2 = BaseColor.LIGHT_GRAY;

		BaseFont baseFont = BaseFont.createFont();
		Font f2 = new Font(baseFont,12);
		f2.setColor(BaseColor.BLACK);




		//order details title
		PdfPCell orderDetailsTitle = new PdfPCell(new Phrase("Order Details",f));
		orderDetailsTitle.setColspan(4);
		orderDetailsTitle.setBackgroundColor(backgroundColor);
		table.addCell(orderDetailsTitle);

		//order details categories 
		PdfPCell orderDetailsCat = new PdfPCell(new Phrase("Quantity",f2));
		PdfPCell code = new PdfPCell(new Phrase("Code",f2));
		PdfPCell description = new PdfPCell(new Phrase("Description",f2));
		PdfPCell price = new PdfPCell(new Phrase("Price",f2));


		orderDetailsCat.setColspan(1);
		orderDetailsCat.setBackgroundColor(backgroundColor2);
		code.setBackgroundColor(backgroundColor2);
		description.setBackgroundColor(backgroundColor2);
		price.setBackgroundColor(backgroundColor2);

		table.addCell(orderDetailsCat);
		table.addCell(code);
		table.addCell(description);
		table.addCell(price);

		List<OrderProductInfoDetails> products = messageOnOrderBean.getProducts();

		for (OrderProductInfoDetails product:products){
			//place in for loop
			//order details  
			PdfPCell orderDetails = new PdfPCell(new Phrase(product.getUnits()));
			orderDetails.setColspan(1);
			table.addCell(Integer.toString(product.getUnits()));
			table.addCell(product.getProductCode());
			table.addCell(product.getProductDescription());
			table.addCell(product.getCostOfSingleProduct());

		}

		//order total
		PdfPCell orderTotal = new PdfPCell(new Phrase("Total " + messageOnOrderBean.getTotalCost()));
		orderTotal.setColspan(4);
		table.addCell(orderTotal);
		blank1.setColspan(4);
		blank1.setBorder(0);
		table.addCell(blank1);








		return table;
	}

	public PdfPTable recipientTable(MessageOnOrderBean messageOnOrderBean, Font f) {
		// a table with three columns
		PdfPTable table = new PdfPTable(4);
		table.setSpacingBefore(5);
		table.setSpacingAfter(5);
		// the cell object
		PdfPCell title;
		PdfPCell blank1 = new PdfPCell(new Phrase(" "));
		BaseColor backgroundColor = BaseColor.DARK_GRAY;

		PdfPTable table2 = new PdfPTable(4);

		PdfPCell parentCell = new PdfPCell();
		parentCell.setColspan(4);
		parentCell.setRowspan(4);

		String firstName = messageOnOrderBean.getRecipientFirstName();
		String lastName = messageOnOrderBean.getRecipientLastName();



		//Recipient 
		PdfPCell recipientInfo = new PdfPCell(new Phrase("Recipient Information",f));
		recipientInfo.setColspan(4);
		recipientInfo.setBackgroundColor(backgroundColor);
		table.addCell(recipientInfo);


		//table.addCell(parentCell);




		//recipient name       
		PdfPCell rname = new PdfPCell(new Phrase(firstName + " " + lastName));
		rname.setColspan(4);
		rname.setRowspan(1);
		rname.setBorder(0);
		table2.addCell(rname);

		//recipient address
		PdfPCell raddress = new PdfPCell(new Phrase(messageOnOrderBean.getRecipientAddress1()));
		raddress.setColspan(4);
		raddress.setRowspan(1);
		raddress.setBorder(0);
		table2.addCell(raddress);


		String country = messageOnOrderBean.getRecipientCountryCode();
		if (country==null){
			country = " ";
		}
		PdfPCell address = new PdfPCell(new Phrase(messageOnOrderBean.getRecipientCity()+ " " + messageOnOrderBean.getRecipientState()+"," + country + " "+ messageOnOrderBean.getRecipientZipCode()));
		address.setColspan(4);
		address.setRowspan(1);
		address.setBorder(0);
		table2.addCell(address);

		PdfPCell rphone = new PdfPCell(new Phrase("Phone: " + messageOnOrderBean.getRecipientPhoneNumberFormated()));
		rphone.setColspan(4);
		rphone.setRowspan(1);
		rphone.setBorder(0);
		table2.addCell(rphone);


		PdfPCell innerTable = new PdfPCell(table2);
		innerTable.setColspan(4);
		table.addCell(innerTable);

		//blank separation
		blank1.setColspan(4);

		blank1.setBorder(0);
		table.addCell(blank1);

		String occasion = OccassionUtil.getOccassionDescription(messageOnOrderBean.getOccasionId());

		//Occassion
		PdfPCell occassion = new PdfPCell(new Phrase("Occasion",f));
		occassion.setBackgroundColor(backgroundColor);
		occassion.setColspan(1);
		occassion.setRowspan(1);
		table.addCell(occassion);

		PdfPCell occassionVal = new PdfPCell(new Phrase(occasion));
		occassionVal.setColspan(3);
		occassionVal.setRowspan(1);
		table.addCell(occassionVal);

		//card message
		PdfPCell card = new PdfPCell(new Phrase("Card Message",f));
		card.setBackgroundColor(backgroundColor);
		card.setColspan(1);
		card.setRowspan(1);
		table.addCell(card);

		PdfPCell cardVal = new PdfPCell(new Phrase(messageOnOrderBean.getCardMessage()));
		cardVal.setColspan(3);
		cardVal.setRowspan(1);
		table.addCell(cardVal);

		//instructions
		PdfPCell instructions = new PdfPCell(new Phrase("Special Instructions",f));
		instructions.setBackgroundColor(backgroundColor);
		instructions.setColspan(1);
		instructions.setRowspan(1);
		table.addCell(instructions);

		PdfPCell instructionsVal = new PdfPCell(new Phrase(messageOnOrderBean.getSpecialInstruction()));
		instructionsVal.setColspan(3);
		instructionsVal.setRowspan(1);
		table.addCell(instructionsVal);


		return table;
	}

	private  String formatPhoneNumberFormated(String m_shopPhone) {

		int phonelength = m_shopPhone.length();
		System.out.println(m_shopPhone.length());

		m_shopPhone = m_shopPhone.replaceAll("\\s+", "");

		phonelength = m_shopPhone.length();
		System.out.println(m_shopPhone.length());

		if ( m_shopPhone != null && ! m_shopPhone.equals("") ) {

			try {

				MaskFormatter mf = new MaskFormatter();

				if ( m_shopPhone.length() == 10 ) {

					mf.setMask( "(AAA) AAA-AAAA" );
					mf.setValueContainsLiteralCharacters(false);
				}
				else if ( m_shopPhone.length() == 7 ) {

					mf.setMask( "AAA-AAAA" );
					mf.setValueContainsLiteralCharacters(false);
				}

				m_shopPhone = mf.valueToString( m_shopPhone );
			}
			catch ( Exception e ) {
				// do nothing
			}
		}

		return m_shopPhone;
	}





}
