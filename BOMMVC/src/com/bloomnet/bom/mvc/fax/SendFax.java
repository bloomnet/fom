package com.bloomnet.bom.mvc.fax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Properties;

import javax.xml.rpc.ServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.mvc.fax.soap.query.ATTACHMENTS_FILTER;
import com.bloomnet.bom.mvc.fax.soap.query.Attachments;
import com.bloomnet.bom.mvc.fax.soap.query.QueryRequest;
import com.bloomnet.bom.mvc.fax.soap.query.QueryResult;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.session.BindingResult;
import com.bloomnet.bom.mvc.fax.soap.session.LoginResult;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Attachment;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionResult;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceLocator;
import com.bloomnet.bom.mvc.fax.soap.submission.SubmissionServiceSoap_BindingStub;
import com.bloomnet.bom.mvc.fax.soap.submission.Transport;
import com.bloomnet.bom.mvc.fax.soap.submission.VAR_TYPE;
import com.bloomnet.bom.mvc.fax.soap.submission.Var;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFILE_MODE;
import com.bloomnet.bom.mvc.fax.soap.submission.WSFile;

public class SendFax
{
	
	 // Define a static logger variable
    static Logger logger = Logger.getLogger( SendFax.class );
    
	// Injected property
	@Autowired private Properties bomProperties;
	

	
	//////////////////////////////////////////////////////////////////////
	// STEP #1 : Global Initializations
	//////////////////////////////////////////////////////////////////////

	String m_Username = new String();	                        // Session username
	String m_Password = new String();						// S
	String m_FaxAttachment1 = new String();

	// Web Service URL, change with the required server url
	String m_WebServiceUrl = "https://as1.ondemand.esker.com:443/EDPWS/EDPWS.dll?Handler=Default&Version=1.0";
	int m_PollingInterval = 15000;					// check fax status every 15 seconds

	// Method used to read data from a file and store them in a Web Service file object.
	private static WSFile ReadFile(String filename)
	{
		WSFile wsFile = new WSFile();
		wsFile.setMode(WSFILE_MODE.MODE_INLINED);
		wsFile.setName(shortFileName(filename));

		try
		{
			FileInputStream stream = new FileInputStream(filename);
			byte[] data = new byte[(int) stream.available()];
			stream.read(data, 0, data.length);
			wsFile.setContent(data);
			stream.close();
		}
		catch(FileNotFoundException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
		}
		catch(IOException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
		}

		return wsFile;
	}

	// Helper method to allocate and fill in Variable objects.
	private static Var CreateValue(String AttributeName, String AttributeValue)
	{
		Var var = new Var();
		var.setAttribute(AttributeName);
		var.setSimpleValue(AttributeValue);
		var.setType(VAR_TYPE.TYPE_STRING);
		return var;
	}

	// Helper method to extract the short file name from a full file path
	public static String shortFileName(String filename)
	{
		int i = filename.lastIndexOf("\\");
		if(i < 0)
		{
			return filename;
		}
		return filename.substring(i + 1);
	}

	public void Run(MessageOnOrderBean orderBean) throws Exception
	{
	    String filename = bomProperties.getProperty(BOMConstants.FAX_DIR);

	    String orderNumber = orderBean.getOrderNumber();
		if (orderNumber==null){
			orderNumber = orderBean.getBmtOrderNumber();
		}
	     
		m_Username = bomProperties.getProperty(BOMConstants.FAX_USERNAME);	                        // Session username
		m_Password = bomProperties.getProperty(BOMConstants.FAX_PASSWORD);       
		m_FaxAttachment1 = filename + "BMTOrder_" +orderNumber+ ".pdf"; // the first attachment file


		//////////////////////////////////////////////////////////////////////
		// STEP #2 : Initialization + Authentication
		//////////////////////////////////////////////////////////////////////

		logger.debug("Retrieving bindings");

		SessionServiceSoap_BindingStub session = null;
		try
		{
			SessionServiceLocator locator = new SessionServiceLocator();
			locator.setEndpointAddress("SessionServiceSoap", m_WebServiceUrl);
			session = (SessionServiceSoap_BindingStub) locator.getSessionServiceSoap();
		}
		catch(ServiceException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Retrieve the bindings on the Application Server (locations of the web services)
		BindingResult bindings = null;
		try
		{
			bindings = session.getBindings(m_Username);
		}
		catch(java.rmi.RemoteException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		logger.debug("Binding = " + bindings.getSessionServiceLocation());


		// Now uses the returned URL with our session object, in case the Application Server redirected us.
		session._setProperty(SessionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSessionServiceLocation());

		logger.debug("Authenticating session");

		// Authenticate the user on this session object to retrieve a sessionID
		LoginResult login = null;
		try
		{
			login = session.login(m_Username, m_Password);

			// Set the returned session header "SessionHeaderValue" as a submission session header.
			session.setHeader(session.getResponseHeader(new SessionServiceLocator().getServiceName().getNamespaceURI(), "SessionHeaderValue"));
		}
		catch(RemoteException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// This sessionID is an impersonation token representing the logged on user
		// You can use it with other Web Services objects, until you call Logout (which releases the
		// current sessionID and it's associated resources), or until the session times out (default is 10
		// minutes on the Application Server).
		logger.debug("SessionID = " + login.getSessionID());


		//////////////////////////////////////////////////////////////////////
		// STEP #3 : Simple fax submission
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a SubmissionService object.
		SubmissionServiceSoap_BindingStub submission = null;
		try
		{
			submission = (SubmissionServiceSoap_BindingStub) new SubmissionServiceLocator().getSubmissionServiceSoap();
		}
		catch(ServiceException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service url with the location retrieved above with GetBindings()
		submission._setProperty(SubmissionServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getSubmissionServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		submission.setHeader(
			new SubmissionServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			new com.bloomnet.bom.mvc.fax.soap.submission.SessionHeader(login.getSessionID()));

	
	logger.debug("Sending Fax Request");

		// Now allocate a transport with transportName = "Fax"
		Transport transport = new Transport();

		// Set transport variables
		transport.setRecipientType("");
		transport.setTransportIndex("");
		transport.setTransportName("Fax");

		// Specifies fax variables (see documentation for their definitions)
		Var[] vars = new Var[9];
		vars[0] = CreateValue("Subject", "Commitment to Coverage Order " + orderNumber);
		vars[1] = CreateValue("FaxNumber", "+1"+orderBean.getFulfillingShop().getShopFax());
		vars[2] = CreateValue("Message", "BloomNet Order " + orderNumber);
		vars[3] = CreateValue("FromName", "BloomNet");
		vars[4] = CreateValue("FromCompany", "BloomNet");
		vars[5] = CreateValue("FromFax", bomProperties.getProperty(BOMConstants.FAX_NUMBER));
		vars[6] = CreateValue("ToName", orderBean.getFulfillingShop().getShopContact());
		vars[7] = CreateValue("ToCompany", orderBean.getFulfillingShop().getShopName());
	
		transport.setVars(vars);

		// Specify a pdf attachment to append to the fax.
		// The attachment content is inlined in the transport description
	    Attachment[] attachments = new Attachment[1];
		attachments[0] = new Attachment();
		attachments[0].setSourceAttachment(ReadFile(m_FaxAttachment1));
		attachments[0].setInputFormat("");
		attachments[0].setOutputFormat("");
		attachments[0].setOutputName(m_FaxAttachment1);
		attachments[0].setStylesheet("");

	
		
	transport.setAttachments(attachments);

		// Submit the complete transport description to the Application Server
		SubmissionResult result = null;
		try
		{
			result = submission.submitTransport(transport);
		}
		catch(RemoteException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		logger.debug("Request submitted with transportID " + result.getTransportID());


		//////////////////////////////////////////////////////////////////////
		// STEP #4 : Fax tracking
		//////////////////////////////////////////////////////////////////////

		// Creating and initializing a QueryService object.
		QueryServiceSoap_BindingStub query = null;
		try
		{
			query = (QueryServiceSoap_BindingStub) new QueryServiceLocator().getQueryServiceSoap();
		}
		catch(ServiceException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}

		// Set the service URL with the location retrieved above with GetBindings()
		query._setProperty(QueryServiceSoap_BindingStub.ENDPOINT_ADDRESS_PROPERTY, bindings.getQueryServiceLocation());

		// Set the sessionID with the one retrieved above with Login()
		// Every action performed on this object will now use the authenticated context created in step 1
		com.bloomnet.bom.mvc.fax.soap.query.SessionHeader q_header = new com.bloomnet.bom.mvc.fax.soap.query.SessionHeader();
		q_header.setSessionID(login.getSessionID());
		query.setHeader(
			new QueryServiceLocator().getServiceName().getNamespaceURI(),
			"SessionHeaderValue",
			q_header);

		// Build a request on the newly submitted fax transport using its unique identifier
		// We also specify the variables (attributes) we want to retrieve.
		QueryRequest request = new QueryRequest();
		request.setNItems(1);
		request.setAttributes("State,ShortStatus,CompletionDateTime");
		// These variables are documented on the page
		// http://doc.esker.com/eskerondemand/cv/en/webservices/index.asp?page=References/Common/RecipientTypes.html
		request.setFilter("(&(msn=" + result.getTransportID() + ")(TransportName=Fax))");
		request.setSortOrder("");

		QueryResult qresult = null;

		logger.debug("Checking for your fax status...");

		int state = 0;
		String status = "";
		String date = "";

		while(true)
		{
			try
			{
				// Ask the Application Server
				qresult = query.queryFirst(request);
			}
			catch(RemoteException ex)
			{
				logger.error("An unexpected error has occurred." + ex.getMessage());
				return;
			}

			if(qresult.getNTransports() == 1)
			{
				// Hopefully, we found it
				// Parse the returned variables
				com.bloomnet.bom.mvc.fax.soap.query.Transport transport2 = qresult.getTransports()[0];
				for(int iVar = 0; iVar < transport2.getNVars(); iVar++)
				{
					com.bloomnet.bom.mvc.fax.soap.query.Var var = transport2.getVars()[iVar];
					String attribute = var.getAttribute().toLowerCase();
					if(attribute.compareTo("state") == 0)
					{
						state = Integer.parseInt(var.getSimpleValue());
					}
					else if(attribute.compareTo("shortstatus") == 0)
					{
						status = var.getSimpleValue();
					}
					else if(attribute.compareTo("completiondatetime") == 0)
					{
						date = var.getSimpleValue();
					}
				}

				if(state >= 100)
				{
					break;
				}

				logger.debug("Fax pending...");
			}
			else
			{
				logger.error("Error !! Fax not found in database");
				return;
			}

			// Wait 15 seconds, then try again...
			try
			{
				Thread.sleep(m_PollingInterval);
			}
			catch(InterruptedException ex)
			{
				logger.error("An unexpected error has occurred." + ex.getMessage());
				return;
			}
		}

		// If the fax is successfully sent (state=100), retrieves the final fax image
		// for remote display (using a web browser), and also download the image data for local processing
		if(state == 100)
		{
			logger.debug("Fax successfully sent at " + new Date());

			logger.debug("Sent order: "+orderBean.getOrderNumber());

			Attachments atts = null;
			try
			{
				atts = query.queryAttachments(
					result.getTransportID(),
					ATTACHMENTS_FILTER.FILTER_CONVERTED,
					com.bloomnet.bom.mvc.fax.soap.query.WSFILE_MODE.MODE_ON_SERVER);
			}
			catch(RemoteException ex)
			{
				logger.error("An unexpected error has occurred." + ex.getMessage());
				return;
			}

			// The final fax image is known to be the first converted attachment of the last available attachments,
			// so retrieve this one.
			if(atts.getNAttachments() > 0 && atts.getAttachments()[atts.getNAttachments() - 1].getNConvertedAttachments() > 0)
			{
				com.bloomnet.bom.mvc.fax.soap.query.WSFile faxImage = atts.getAttachments()[atts.getNAttachments() - 1].getConvertedAttachments()[0];

				logger.debug("Fax image available at " + faxImage.getUrl());

				// Download the file for local use
				byte[] imagedata = null;
				try
				{
					imagedata = query.downloadFile(faxImage);
				}
				catch(RemoteException ex)
				{
					logger.error("An unexpected error has occurred." + ex.getMessage());
					return;
				}

				logger.debug("Fax image data retrieve, size = " + imagedata.length);
			}
			else
			{
				logger.error("Error !! no valid attachments found");
			}
		}
		else
		{
			logger.error("Fax failed at + " + date + ", reason: " + status);
		}


		//////////////////////////////////////////////////////////////////////
		// STEP #5 : Release the session and its allocated resources
		//////////////////////////////////////////////////////////////////////

	

		// As soon as you call Logout(), the files allocated on the server during this session won't be available
		// any more, so keep in mind that former urls are now useless...

		logger.debug("Releasing session and server files");

		try
		{
			session.logout();
		}
		catch(RemoteException ex)
		{
			logger.error("An unexpected error has occurred." + ex.getMessage());
			return;
		}
	}
}


