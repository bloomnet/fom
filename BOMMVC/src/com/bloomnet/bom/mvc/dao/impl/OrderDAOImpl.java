package com.bloomnet.bom.mvc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActAudit;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.AuditInterface;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.CityZipXref;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Country;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Paymenttype;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Virtualqueue;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteriaExtended;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;

/**
 * Implementation of a DAO for {@link com.bloomnet.bom.common.dao.OrderDAO}
 * 
 * @author Danil Svirchtchev
 */
@Transactional
@Repository("orderDAO")
public class OrderDAOImpl implements OrderDAO {
	
	@Autowired private SessionFactory sessionFactory;
	
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected DAO implementation
	@Autowired private RoutingDAO routingDAO;
	
	// Injected DAO implementation
	@Autowired private UserDAO userDAO;	
	
	// Injected DAO implementation
	@Autowired private ShopDAO shopDAO;	
	
	@Autowired private TransformService transformService;	


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrderByOrderId(java.lang.String)
	 */
	@Override
	public Bomorder getOrderByOrderId( Long orderID ) throws HibernateException {
		return (Bomorder) sessionFactory.getCurrentSession().get( Bomorder.class, orderID );
	}

	
	
	@Override
	public Bomorder getParentOrderByOrderNumber(String orderNumber) throws HibernateException {
		
		Bomorder result = null;
		List<Bomorder> orders = getOrdersByOrderNumber( orderNumber ) ;
       
		for ( Bomorder m_order : orders) {
			
			Long pid = m_order.getParentorderId();
			
			result = getOrderByOrderId(pid);
				
			
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByOrderNumber(java.lang.String)
	 */
	@Override
	public List<Bomorder> getOrdersByOrderNumber( String orderNumber ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("orderNumber"), orderNumber));
		cq.orderBy(cb.asc(root.get("bomorderId")));

		List<Bomorder> m_orders = session.createQuery(cq).getResultList();
		
        
		
		return m_orders;
	}
	
	@Override
	public List<Bomorder> getMultipleOrdersByOrderIds(List<String> orderIds ) throws HibernateException {
		
		List<Bomorder> results = new ArrayList<Bomorder>();
		for(String s : orderIds){
			Bomorder order = getOrderByOrderId(Long.valueOf(s));
			results.add(order);
		}
		
		return results;
	}

	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByAgent(java.lang.String)
	 */
	@Override
	public List<Bomorder> getOrdersByAgent( String userName, String stsOrderId ) throws HibernateException {
				
		List<Bomorder> results = new ArrayList<Bomorder>();
		
		final User m_user = userDAO.getUserByUserName(userName);

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
		cq.where(cb.equal(root.get("user"), m_user));
        
		List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
			
		if ( stsOrderId != null ) {
			
			final byte m_id = Byte.parseByte(stsOrderId);
			
			for ( Orderactivity m_oactivity : m_orderActivities ) {
				
				final ActRouting m_actRouting = m_oactivity.getActRouting();
				
				if ( m_actRouting != null && 
					 m_actRouting.getStsRouting()!= null && 
					 m_actRouting.getStsRouting().getStsRoutingId().equals(m_id) ) {
					
					results.add(m_oactivity.getBomorder());
				}
			}
		}
		
		
		return results;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getMostRecentOrderByAgent(java.lang.Long, java.lang.String)
	 */
	@Override
	public Bomorder getMostRecentOrderByAgent ( Long userId, String status  ) throws HibernateException {
		
		Bomorder result = null;
		
		final User user = userDAO.getUserById(userId);
		final StsRouting routingStatus = routingDAO.getStatusById( bomProperties.getProperty( status ) );
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("user"), user));
        
		List<Bomorder> orders = session.createQuery(cq).getResultList();
		
		List<Orderactivity> activities = new LinkedList<Orderactivity>();
		
		if ( ! orders.isEmpty() ) {

			for ( Bomorder m_order : orders ) {
			
				activities.addAll( m_order.getOrderactivities() );
			}
			
			Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
		}
			
		for( Orderactivity activity : activities ) {
				
			final ActRouting m_routing = activity.getActRouting();
			final ActMessage m_message = activity.getActMessage();
				
			if ( ( m_routing != null && routingStatus.getDescription().equals( m_routing.getStatus() ) ) ||
				 ( m_message != null ) ) {
						
					result = activity.getBomorder();
			}
		}
		
		return result;
	}



	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByWorkFlowStatus(java.lang.String)
	 */
	@Override
	public List<Bomorder> getOrdersByStatus( String status ) throws HibernateException {


		List<Bomorder> result = new ArrayList<Bomorder>();
		
		List<Bomorder> orders = getAllParentOrders();
		
		Session session = sessionFactory.getCurrentSession();
		
		for (Bomorder order : orders){
			

			
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
	        
	        Root<Orderactivity> root = cq.from(Orderactivity.class);
	        cq.select(root);
			cq.where(cb.equal(root.get("bomorder"), order));

			List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
			
			
			if ( ! m_orderActivities.isEmpty() ) {
				
				Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse( m_orderActivities );
				
			//	for(Orderactivity m_oactivity : m_orderActivities){
				
				Orderactivity m_oactivity  = m_orderActivities.get(0);
				
				if( m_oactivity != null ) {
					
					final ActRouting m_actRouting = m_oactivity.getActRouting();
					
					if ( m_actRouting != null && 
						 m_actRouting.getStsRouting().getDescription().equals(status) ) {
							
						result.add(order);
					}
				}
			//	}
			}
			
		}	
	;
		
	return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByDate(java.util.Date)
	 */
	@Override
	public List<Bomorder> getOrdersByDate( Date date ) throws HibernateException {
		
		Date nextMidnight = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.and(cb.between(root.get("deliveryDate"), date, nextMidnight), cb.equal(root.get("ParentOrder_ID"), root.get("BOMOrder_ID"))));
		List<Bomorder> orders = session.createQuery(cq).getResultList();
		
        
		
		return orders;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByToday(java.util.Date)
	 */
	@Override
	public List<Bomorder> getOrdersByToday( ) throws HibernateException {
		
		
		Date todayMidnight = new DateTime().toDateMidnight().toDate();
		Date nextMidnight  = new DateTime(todayMidnight).plusHours(24).minusSeconds(1).toDate();
		
		List<Bomorder> results = new ArrayList<Bomorder>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.between(root.get("deliveryDate"), todayMidnight, nextMidnight));
		List<Bomorder> orders = session.createQuery(cq).getResultList();


		
		for(Bomorder order : orders){
			if(order.getBomorderId().equals(order.getParentorderId()))
				results.add(order);
		}
						
		return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersByTommorrow(java.util.Date)
	 */
	@Override
	public List<Bomorder> getOrdersByTommorrow() throws HibernateException {

		Date tomorrowMidnight = new DateTime().toDateMidnight().plusDays(1).toDate();
		Date nextMidnight     = new DateTime(tomorrowMidnight).plusHours(24).toDateMidnight().toDate();
		
		List<Bomorder> results = new ArrayList<Bomorder>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.between(root.get("deliveryDate"), tomorrowMidnight, nextMidnight));
		List<Bomorder> orders = session.createQuery(cq).getResultList();

		
		for(Bomorder order : orders){
			if(order.getBomorderId().equals(order.getParentorderId()))
				results.add(order);		}
		
						
		return results;
	}


	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getRelatedOrders(java.lang.String)
	 */
	@Override
	public List<Bomorder> getRelatedOrders(String orderID) throws HibernateException {
		throw new HibernateException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrderHistory(java.lang.String)
	 */
	@Override
	@SuppressWarnings({ "null" })
	public List<Orderactivity> getOrderHistory( String orderNumber ) throws HibernateException {
		
		List<Orderactivity> m_orderActivities = new LinkedList<Orderactivity>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("orderNumber"), orderNumber));
		List<Bomorder> m_orders = session.createQuery(cq).getResultList();
		
		if (m_orders!=null||!m_orders.isEmpty()){
			
			for ( Bomorder m_order : m_orders ) {
				if (m_order.getOrderactivities()!=null){
				m_orderActivities.addAll( m_order.getOrderactivities() );
				}
			}

			Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				}
		
						
		return m_orderActivities;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersToBeWorked()
	 */
	@Override
	public List<Bomorder> getOrdersToBeWorked() throws HibernateException {
		throw new HibernateException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#deleteOrder(java.lang.String)
	 */
	@Override
	public String deleteOrder(Bomorder order) throws HibernateException {

		final String result = order.getOrderNumber();
		sessionFactory.getCurrentSession().delete(order);
		return result;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getZipByCode(java.lang.String)
	 */
	@Override
	public Zip getZipByCode( String zip ) throws HibernateException {
		
		String newZip = zip.replaceAll("\\s+","");
		
		Zip zipEntity = null;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Zip> cq = cb.createQuery(Zip.class);
        
        Root<Zip> root = cq.from(Zip.class);
        cq.select(root);
		cq.where(cb.equal(root.get("zipCode"), newZip));
		List<Zip>  zipList = session.createQuery(cq).getResultList();

		if (!zipList.isEmpty()){
			zipEntity = zipList.get(0);
		}
						
		return zipEntity;
	}
	@Override
	@SuppressWarnings("unchecked")
	@Deprecated
	public Zip getSimilarZipByCode( String zip ) throws HibernateException {
		
		Zip zipcode = new Zip();
		 List<Zip> zipList = (List<Zip>) sessionFactory.getCurrentSession()
								   .createCriteria( Zip.class )
								   .add( Restrictions.ilike( "zipCode", zip.substring(0, 4),MatchMode.START ) )
								   .list();
		if (!zipList.isEmpty()){
			zipcode = zipList.get(0);
			return zipcode;
		}
		else{
			List<Zip> zipList2 = (List<Zip>) sessionFactory.getCurrentSession()
			   .createCriteria( Zip.class )
			   .add( Restrictions.ilike( "zipCode", zip.substring(0, 3),MatchMode.START ) )
			   .list();
			
			zipcode = zipList2.get(0);
			
		}
		
		return zipcode;
		
	}
	
	@Override
	public Zip getZipByCityAndState(String city, String stateName) throws HibernateException {
		
		Zip zipEntity = new Zip();
		City cityEntity = getCityByNameAndState(city, stateName);

		if (cityEntity!=null){

			for ( CityZipXref cityZip : cityEntity.getCityZipXrefs() ) {

				Zip zip = cityZip.getZip();
				City cityRef = cityZip.getCity();
				if(cityRef.getCityId()==cityEntity.getCityId()){
					zipEntity = zip;
					return zipEntity;
				}

			}
		}

		return zipEntity;
	}
	
	@Override
	public City getCityByZip(String zipCode, String recipientCity) throws Exception{
		
		City city = new City();
		
		Zip zip = getZipByCode(zipCode);
		
		if (zip!=null){
			
		for ( CityZipXref cityZip :zip.getCityZipXrefs()) {
			Zip zipRef = cityZip.getZip();
			City cityRef = cityZip.getCity();
		     if(zipRef.getZipId() == zip.getZipId()){
		    	 city = cityRef;
		    	 return city;
		     }
		}
	}
		
		return city;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getPaymentTypeById(byte)
	 */ 
	@Override
	public Paymenttype getPaymentTypeById(byte id) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Paymenttype> cq = cb.createQuery(Paymenttype.class);
        
        Root<Paymenttype> root = cq.from(Paymenttype.class);
        cq.select(root);
		cq.where(cb.equal(root.get("paymentTypeId"), id));
		Paymenttype paymentType = session.createQuery(cq).uniqueResult();
		
		
		
		return paymentType;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getPaymentStatusById(byte)
	 */
	@Override
	public StsPayment getPaymentStatusById(byte id) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<StsPayment> cq = cb.createQuery(StsPayment.class);
        
        Root<StsPayment> root = cq.from(StsPayment.class);
        cq.select(root);
		cq.where(cb.equal(root.get("stsPaymentId"), id));
		StsPayment stsPayment = session.createQuery(cq).uniqueResult();
		
		
		
		return stsPayment;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getCommmethodById(byte)
	 */
	@Override
	@Transactional
	public Commmethod getCommmethodById(byte id) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Commmethod> cq = cb.createQuery(Commmethod.class);
        
        Root<Commmethod> root = cq.from(Commmethod.class);
        cq.select(root);
		cq.where(cb.equal(root.get("commMethodId"), id));
		Commmethod commMethod = session.createQuery(cq).uniqueResult();
		
		

		
		return commMethod;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getLastOrderSequenceNumber()
	 */
	@Override
	@Transactional
	public String getLastOrderSequenceNumber() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("orderType"), BOMConstants.ORDER_TYPE_TEL));
		cq.orderBy(cb.desc(root.get("bomorderId")));
		List<Bomorder> bo = session.createQuery(cq).setMaxResults(1000).getResultList();
									     
		if(bo != null){
			for(int ii=0; ii<bo.size(); ++ii){
				if(bo.get(ii).getBmtOrderSequenceNumber() != null && !bo.get(ii).getOrderNumber().startsWith("BMT")){
					if(Integer.valueOf(bo.get(ii).getBmtOrderSequenceNumber()) > 99999) continue;
					else if(bo.get(ii).getBmtOrderSequenceNumber().equals("99999")) return "0";
					else return bo.get(ii).getBmtOrderSequenceNumber();
				}

						}
			return "0";
		
		}
		return "0";
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistParentOrder(com.bloomnet.bom.common.entity.Bomorder)
	 */
	@Override
	public Bomorder persistParentOrder(Bomorder order) throws HibernateException {
		
		sessionFactory.getCurrentSession().persist(order);
		order.setParentorderId(order.getBomorderId());
		sessionFactory.getCurrentSession().saveOrUpdate(order);
		return order;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistChildOrder(com.bloomnet.bom.common.entity.Bomorder)
	 */
	@Override
	public Bomorder persistChildOrder( Bomorder order, String parentOrderNumber ) throws HibernateException {
		
		final Bomorder parentOrder = getOrderByOrderNumber(parentOrderNumber);
		order.setParentorderId(parentOrder.getBomorderId());
		sessionFactory.getCurrentSession().saveOrUpdate(order);
		return order;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistOrderHistory(com.bloomnet.bom.common.entity.AuditInterface)
	 */
	@Override
	public void persistOrderHistory( AuditInterface actAudit ) throws HibernateException {
		
		final ActAudit entity = new ActAudit( actAudit.getOrderactivity(), 
											  actAudit.getOrderXml(), 
											  actAudit.getDeliveryDate(), 
											  actAudit.getPrice(), 
											  actAudit.getZipId(), 
											  actAudit.getCityId() );

		sessionFactory.getCurrentSession().persist( entity );
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#updateOrder(com.bloomnet.bom.common.entity.Bomorder)
	 */
	@Override
	public Bomorder updateOrder( Bomorder order ) throws HibernateException {
		
		final Object result;
		
		if( order.getClass() == WebAppOrder.class ) {
		
			result = sessionFactory.getCurrentSession().merge(((WebAppOrder)order).getEntity());
		}
		else {
			
			result = sessionFactory.getCurrentSession().merge( order );
		}

		sessionFactory.getCurrentSession().flush();
		
		return (Bomorder) result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#updateChildOrder(com.bloomnet.bom.common.entity.Bomorder)
	 */
	@Override
	public String updateChildOrder(Bomorder order, String parentOrderNumber) throws HibernateException {
		
		final Bomorder parentOrder = getOrderByOrderNumber(parentOrderNumber);
		order.setParentorderId(parentOrder.getBomorderId());
		sessionFactory.getCurrentSession().merge(order);
		return parentOrderNumber;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getActivityTypeByDesc(java.lang.String)
	 */
	@Override
	public Oactivitytype getActivityTypeByDesc(String desc) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Oactivitytype> cq = cb.createQuery(Oactivitytype.class);
        
        Root<Oactivitytype> root = cq.from(Oactivitytype.class);
        cq.select(root);
		cq.where(cb.equal(root.get("description"), desc));
		Oactivitytype oat = session.createQuery(cq).uniqueResult();
		
		return oat;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getActRouting(java.lang.String)
	 */
	@Override
	public List<ActRouting> getActRouting( String orderNumber ) throws HibernateException {
		
		List<ActRouting> result = new ArrayList<ActRouting>();
		
		for( Orderactivity m_orderActivity : getOrderHistory( orderNumber ) ) {
			
			if ( m_orderActivity.getActRouting() != null ) {
				result.add( m_orderActivity.getActRouting() );
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getCurrentFulfillingShopCode(java.lang.String)
	 */
	@Override
	public String getCurrentFulfillingShopCode(String orderNumber) throws HibernateException {
		
		String fulfillingShopCode = new String();
		String[] BLOOMNET_NETWORK_NAMES  = {"BMT","BloomNet"};
		String[] TELEFLORA_NETWORK_NAMES = {"Teleflora"};
		String[] networkNames = BLOOMNET_NETWORK_NAMES;//default to bmt

		Bomorder order = getOrderByOrderNumber(orderNumber);

		
		if (order==null){
			throw new HibernateException("No order found with order Number: " + orderNumber);
		}
		
		Long parentOrderId = order.getParentorderId();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("parentorderId"), parentOrderId));
		cq.orderBy(cb.desc(root.get("createdDate")));
		Bomorder currentChild = session.createQuery(cq).setMaxResults(1).uniqueResult();
			
		//get current receiving shop
		Shop currentChildReceivingShop = currentChild.getShopByReceivingShopId();
		byte orderType = currentChild.getOrderType();
		Shop shop = currentChildReceivingShop;
		Long shopId = shop.getShopId();

		//get network id based on order type
		if (orderType==BOMConstants.ORDER_TYPE_BMT){
			networkNames  = BLOOMNET_NETWORK_NAMES;
		}
		else if (orderType==BOMConstants.ORDER_TYPE_TEL){
			networkNames  = TELEFLORA_NETWORK_NAMES;
		}
		
		//get shopcode of current receiving shop based on network
		List<Shopnetwork> shopNetworks = shopDAO.getShopnetworks(shopId, networkNames);
				
		if (!shopNetworks.isEmpty()){
		
			for ( Shopnetwork shopNetwork: shopNetworks){
			
				fulfillingShopCode = shopNetwork.getShopCode();
				return fulfillingShopCode;
			
			}
		}
			
		return fulfillingShopCode;

	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrdersCompletedByAgent(com.bloomnet.bom.common.entity.User)
	 */
	@Override
	public List<Bomorder> getOrdersCompletedByAgent(User user) throws HibernateException {
		throw new HibernateException("Not implemented");
	}

	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getTFShopsAlreadySentTo(Java.util.Long)
	 */
	@Override
	public List<Shop> getTFShopsAlreadySentTo(long parentOrderId) throws HibernateException{
		
		List<Shop> shops = new ArrayList<Shop>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("parentorderId"), parentOrderId));
		List<Bomorder> orders = session.createQuery(cq).getResultList();

		for(Bomorder order : orders){
			shops.add(order.getShopByReceivingShopId());
		}
		return shops;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrderByBMTSeqNumber(Java.util.Long)
	 */
	@Override
	public Bomorder getOrderByBMTSeqNumber(long bmtSeqNum) throws HibernateException{
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("bmtOrderSequenceNumber"), String.valueOf(bmtSeqNum)));
		Bomorder bomOrder = session.createQuery(cq).uniqueResult();
		
		return bomOrder;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getTFOrderByBMTSeqNumber(Java.util.Long)
	 */
	@Override
	public Bomorder getTFOrderByBMTSeqNumber(long bmtSeqNum) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("bmtOrderSequenceNumber"), String.valueOf(bmtSeqNum)));
		List<Bomorder> bomOrder = session.createQuery(cq).getResultList();
		int latestIndex = bomOrder.size() - 1;

		return bomOrder.get(latestIndex);
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOriginalSendingShopCode(java.lang.String)
	 */
	@Override
	public String getOriginalSendingShopCode( String orderNumber ) throws HibernateException {
		
		final Bomorder order = getParentOrderByOrderNumber(orderNumber);
		
		if ( order == null ){
			
			throw new HibernateException("Order "+orderNumber+ " found in database");
		}

		final Long shopId = order.getShopBySendingShopId().getShopId();
		
		final String originalShopCode = shopDAO.getBloomNetShopnetwork( shopId ).getShopCode();
		
		return originalShopCode;
	}



	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistOrderactivityByUser(java.lang.String, java.lang.String, byte, java.lang.String, java.lang.String)
	 */
	//@Override
	public void persistOrderactivityByUser( final String userName,
											final String activityRoutingDescription, 
											final String status, 
											final String orderNumber,
										    final String destination) throws Exception {
		
		final User user = userDAO.getUserByUserName(userName);

		if( user == null ){
			
			throw new Exception("User "+userName+" is not valid");
		}
		else {
			
			Oactivitytype activitytype = getActivityTypeByDesc(activityRoutingDescription);
			StsRouting    stsRouting   = routingDAO.getStatusById(status);
			System.out.println("persistOrderactivityByUser, orderNumber: "+ orderNumber + "user: "+userName);
			Bomorder	  order        = getParentOrderByOrderNumber(orderNumber);
			System.out.println("persistOrderactivityByUser, order: "+ order.getOrderNumber());

			// set up order activity
			Orderactivity orderActivity = new Orderactivity(activitytype);
			orderActivity.setBomorder(order);
			orderActivity.setUser(user);
			orderActivity.setCreatedDate( new Date() );
			
			ActRouting actRouting =  new ActRouting();
			actRouting.setStsRouting(stsRouting);
			actRouting.setVirtualQueue(destination);
			actRouting.setOrderactivity(orderActivity);
			
			orderActivity.setActRouting(actRouting);
			
			sessionFactory.getCurrentSession().persist(actRouting);
			sessionFactory.getCurrentSession().persist(orderActivity);
			
		
		}
	}
	
	@Override
	public void persistChildOrderactivityByUser( final String userName,
			final String activityRoutingDescription, 
			final String status, 
			final String orderNumber,
			final String destination) throws Exception {

		final User user = userDAO.getUserByUserName(userName);

		if( user == null ){

			throw new Exception("User "+userName+" is not valid");
		}
		else {

			Oactivitytype activitytype = getActivityTypeByDesc(activityRoutingDescription);
			StsRouting    stsRouting   = routingDAO.getStatusById(status);
			Bomorder	  order        = getOrderByOrderNumber(orderNumber);

			// set up order activity
			Orderactivity orderActivity = new Orderactivity(activitytype);
			orderActivity.setBomorder(order);
			orderActivity.setUser(user);
			orderActivity.setCreatedDate( new Date() );

			ActRouting actRouting =  new ActRouting();
			actRouting.setStsRouting(stsRouting);
			actRouting.setVirtualQueue(destination);
			actRouting.setOrderactivity(orderActivity);

			orderActivity.setActRouting(actRouting);

			sessionFactory.getCurrentSession().persist(actRouting);
			sessionFactory.getCurrentSession().persist(orderActivity);
		
		}
	}

	

	
	
	@Override
	@Transactional
	public City getCityByNameAndState( String city, String stateName ) throws HibernateException {
		
		State state = getStateByName(stateName);
		if(state != null){

			Session session = sessionFactory.getCurrentSession();
			
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<City> cq = cb.createQuery(City.class);
	        
	        Root<City> root = cq.from(City.class);
	        cq.select(root);
			cq.where(cb.and(cb.equal(root.get("name"), city),cb.equal(root.get("state"), state)));
			List<City> results = session.createQuery(cq).getResultList();
		
			City result = new City();
			if(results != null && results.size() > 0){
				result = results.get(0);
			}else{
				return new City();
			}
			
			
			return result;
		}
		
		
		return new City();
	}
	
	public State getStateByName( String state ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<State> cq = cb.createQuery(State.class);
        
        Root<State> root = cq.from(State.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shortName"), state));
        State result = session.createQuery(cq).uniqueResult();
		
		return result;
	}
	
	public State getStateByNameAndCountry( String state, Country country) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<State> cq = cb.createQuery(State.class);
        
        Root<State> root = cq.from(State.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("shortName"), state),cb.equal(root.get("country"), country)));
        State result = session.createQuery(cq).uniqueResult();
		
		return result;
	}
	
	public Country getCountryByCode( String country ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Country> cq = cb.createQuery(Country.class);
        
        Root<Country> root = cq.from(Country.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shortName"), country));
        Country result = session.createQuery(cq).uniqueResult();
		
		return result;
	}
	
	public State getStateByCountry( String countryCode ) throws HibernateException {
		
		Country country = getCountryByCode(countryCode);
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<State> cq = cb.createQuery(State.class);
        
        Root<State> root = cq.from(State.class);
        cq.select(root);
        cq.where(cb.equal(root.get("State"), country));
        State result = session.createQuery(cq).uniqueResult();
		
		return result;
	}

	@Override
	public List<Paymenttype> getPaymentTypes() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Paymenttype> cq = cb.createQuery(Paymenttype.class);
        
        cq.from(Paymenttype.class);
        List<Paymenttype> paymenttypes = session.createQuery(cq).getResultList();

		return paymenttypes;
	}

	@Override
	public boolean isUniqueOrderNumber(String orderNumber) throws Exception {
	
		boolean isNew = true;
		
		Bomorder existing = getOrderByOrderNumber(orderNumber);
		
		if(existing != null) return false;
		
		return isNew;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrderByOrderNumber(java.lang.String)
	 */
	@Override
	public Bomorder getOrderByOrderNumber(String orderNumber) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
        cq.where(cb.equal(root.get("orderNumber"), orderNumber));
        cq.orderBy(cb.desc(root.get("createdDate")));
        Bomorder result = session.createQuery(cq).setMaxResults(1).uniqueResult();
		
		return result;
	}
	
	public Bomorder getCurrentFulfillingOrder(String orderNumber) throws HibernateException {
		
		Bomorder order = getOrderByOrderNumber(orderNumber);
		
		if (order==null){
			throw new HibernateException("No order found with order Number: " + orderNumber);
		}
		
		Long parentOrderId = order.getParentorderId();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
        cq.where(cb.equal(root.get("parentorderId"), parentOrderId));
        cq.orderBy(cb.desc(root.get("createdDate")));
        Bomorder result = session.createQuery(cq).setMaxResults(1).uniqueResult();
		
		return result;
	}

	@Override
	public Bomorder getOrderByOrderActivityId(long orderActivityId)
			throws Exception {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.equal(root.get("orderActivityId"), orderActivityId));
        Orderactivity orderActivity = session.createQuery(cq).uniqueResult();
		
		Long bomorderID = orderActivity.getBomorder().getBomorderId();
		
		Bomorder order  = getOrderByOrderId(bomorderID);
		
		
		return order;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getOrderactivities(java.lang.Long)
	 */
	@Override
	public List<Orderactivity> getOrderActivities( Bomorder order ) throws HibernateException {
		
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.equal(root.get("bomorder"), order));
        List<Orderactivity> result = session.createQuery(cq).getResultList();
		
		return result;
	}

	@Override
	public boolean isUniqueSeqNumberOrder(String bmtSeqNumberOfOrder)
			throws Exception {
		
		boolean isNew = true;
		
		Bomorder existing = getOrderByBMTSeqNumber(Long.valueOf(bmtSeqNumberOfOrder));
		
		if(existing != null) return false;
		
		
		return isNew;
	}

    /* (non-Javadoc)
     * @see com.bloomnet.bom.common.dao.OrderDAO#getAllOrders()
     */
	@Override
    @SuppressWarnings("unchecked")
    public List<Bomorder> getAllOrders() throws HibernateException {
		
		Query<Bomorder> q = sessionFactory.getCurrentSession().createQuery("from Bomorder");
		
        return q.list();
    }
	
    /* (non-Javadoc)
     * @see com.bloomnet.bom.common.dao.OrderDAO#getAllOrders()
     */
	@Override
    @SuppressWarnings("unchecked")
    public List<Bomorder> getAllOrders( int startIndex, int pageSize ) throws HibernateException {
		
		Query<Bomorder> q = sessionFactory.getCurrentSession().createQuery("from Bomorder b order by b.deliveryDate");
		
		q.setFirstResult(startIndex);
		q.setMaxResults(pageSize);
		
		sessionFactory.getCurrentSession().flush();
				
		return q.list();
    }
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getAllOrdersCount()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getAllOrdersCount() throws HibernateException {
		
		Query<Long> q = sessionFactory.getCurrentSession().createQuery("select count(*) from Bomorder");
		
		Long result = ( ( Long ) q.uniqueResult() ).longValue();

		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistCity(java.lang.String, java.lang.String)
	 */
	@Override
	public City persistCity(String cityName, String stateName, String countryName) throws HibernateException {
		
		User user = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
		String createdDate = DateUtil.toXmlFormatString(new Date());

		Country country = getCountryByName(countryName);
		
		if (country==null){
			if (countryName.equals("US")){
				country = getCountryByName("USA");
			}
			else{
				country = new Country(user, countryName, countryName, createdDate) ;
				sessionFactory.getCurrentSession().persist(country);
			}
			
		}
		
		State state = getStateByName(stateName);
		if(state == null){
			state = persistState(user,country,stateName);
		}
		
		City cityEntity = new City(user, 
								   state, 
								   cityName, 
								   createdDate );
		
		sessionFactory.getCurrentSession().persist(cityEntity );
		
		
		return cityEntity;
	}
	
	@Override
	public City persistIntCity(String cityName, String stateName, String countryCode) throws HibernateException {
		
		User user = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
		String createdDate = DateUtil.toXmlFormatString(new Date());

	
		State state = getStateByCountry(countryCode);
		
		Country country = getCountryByCode(countryCode);
		if (state==null){
			if (country==null){
				if (countryCode.equals("US")){
					country = getCountryByName("USA");
				}
				else{
					country = new Country(user, countryCode, countryCode, createdDate) ;
					sessionFactory.getCurrentSession().persist(country);
				}
			}
			
			state = persistState(user,country,countryCode);
		}
		
		
		City cityEntity = new City(user, 
								   state, 
								   cityName, 
								   createdDate );
		
		sessionFactory.getCurrentSession().persist(cityEntity );
		
		return cityEntity;
	}
	
	public void persistCityCoverage(City city) throws HibernateException{
		
		User user = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
		
		try{
			
			Zip zipSave = getZipByCode("00000");
			CityZipXref xref = new CityZipXref(user,zipSave,city, new Date());
			sessionFactory.getCurrentSession().persist(xref);
			sessionFactory.getCurrentSession().flush();

		}catch(Exception ee){
			ee.printStackTrace();
		}
		return;
	}
	

	private State persistState(User user, Country country, String stateName) {
		
		
		State state = new State(user,country,stateName, stateName, new Date() );
		sessionFactory.getCurrentSession().persist(state );

		return state;
	}



	private Country getCountryByName(String countryName) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Country> cq = cb.createQuery(Country.class);
        
        Root<Country> root = cq.from(Country.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shortName"), countryName));
        Country result = session.createQuery(cq).uniqueResult();
		
		return result;
	}



	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#persistZip(java.lang.String)
	 */
	@Override
	public Zip persistZip(String zipCode)throws HibernateException {
	
		Zip zip = new Zip();
		
		User user = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
		String newZip = zipCode.replaceAll("\\s+","");
	
		zip.setZipCode(newZip);
		zip.setUserByCreatedUserId(user);
		zip.setCreatedDate(new Date());
		zip.setModifiedDate(new Date());
		zip.setTimeZone(BOMConstants.EAST_TIME);
		
		sessionFactory.getCurrentSession().persist(zip);

		return zip;
	}
	
	public byte getCurrentOrderStatus(Bomorder bomorder)throws HibernateException{
		
	byte result = 0;
	
	Session session = sessionFactory.getCurrentSession();
	
	CriteriaBuilder cb = session.getCriteriaBuilder();
	CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
    
    Root<Orderactivity> root = cq.from(Orderactivity.class);
    cq.select(root);
    cq.where(cb.equal(root.get("bomorder"), bomorder));
    List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
	
		if ( ! m_orderActivities.isEmpty() ) {
			
			Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse( m_orderActivities );
			
			for(Orderactivity m_oactivity : m_orderActivities){
			
			if( m_oactivity != null ) {
				
				final ActRouting m_actRouting = m_oactivity.getActRouting();
				
				if ( m_actRouting != null && 
					 m_actRouting.getStsRouting()!= null ) {
						
					result = m_actRouting.getStsRouting().getStsRoutingId();
					return result;
				}
			}
			}
		}
		
		return result;
	}
	
	
	public ActRouting getCurrentOrderStatusBeforeBeingWorked(Bomorder bomorder)throws HibernateException{
		
		ActRouting result = null;
		
		
		List<Orderactivity> activities = new LinkedList<Orderactivity>();
		
		activities.addAll( bomorder.getOrderactivities() );
		
		Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
		Collections.reverse(activities);
		
			
		for( Orderactivity activity : activities ) {
				
			final ActRouting m_routing = activity.getActRouting();
				
			if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))) {
				
				result =  m_routing;
				return result;
				
			}
		}
		return result;
	}
	
	
	
	@Override
	public List<ActRouting> getAllOrderStatus(Bomorder bomorder)throws HibernateException{
		
		
		List<ActRouting> routings = new ArrayList<ActRouting>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.equal(root.get("bomorder"), bomorder));
        List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
		
			if ( ! m_orderActivities.isEmpty() ) {
				
				Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse( m_orderActivities );
				
				for(Orderactivity m_oactivity : m_orderActivities){
				
				if( m_oactivity != null ) {
					
					final ActRouting m_actRouting = m_oactivity.getActRouting();
					
					if ( m_actRouting != null && 
						 m_actRouting.getStsRouting()!= null ) {
							
						routings.add(m_actRouting);
					}
				}
				}
			}
			
			return routings;
		}
	
	
	
	@SuppressWarnings({ })
	public Orderactivity getLatestOrderActivity(Bomorder bomorder)throws HibernateException{
		
		Orderactivity result = new Orderactivity();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.equal(root.get("bomorder"), bomorder));
        List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
		
			if ( ! m_orderActivities.isEmpty() ) {
				
				Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse( m_orderActivities );
				
				for(Orderactivity m_oactivity : m_orderActivities){
				
				if( m_oactivity != null ) {
					
					final ActRouting m_actRouting = m_oactivity.getActRouting();
					
					if ( m_actRouting != null && 
						 m_actRouting.getStsRouting()!= null ) {
							
						result = m_oactivity;
						return result;
					}
				}
				}
			}
			
			return result;
		}
	
	
	
	
	@Override
	public List<Bomorder> getOrdersForToday( ) throws HibernateException {
		 		
		List<Bomorder> result = new ArrayList<Bomorder>();
		List<Bomorder> orders = getOrdersByToday();
		
		for (Bomorder order: orders){
			if(order.getBomorderId().equals(order.getParentorderId())){
				result.add(order);
			}
			
		}
	 
		return result;
	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Bomorder> getOrdersEnteredToday( Date today ) throws HibernateException {
		
		List<Bomorder> result = new ArrayList<Bomorder>();
		List<Bomorder> bomorders = new ArrayList<Bomorder>();
		
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date todayDate = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.tomorrowsNoTimeDate();
		Date tomorrow = DateUtil.toDate(tomorrowStr);

		String str = "from Bomorder where createddate >= :today and createddate < :tomorrow";   
		Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(str); 
		query.setParameter("today", todayDate);
		query.setParameter("tomorrow", tomorrow);

		bomorders = query.list();
		for( Bomorder bomorder: bomorders){
			long bomorderId = bomorder.getBomorderId();
			long parentorderId =bomorder.getParentorderId();
			if ( bomorderId ==parentorderId ){
				result.add(bomorder);
			}
		}

		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.OrderDAO#getAllOrdersCount()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getAllParentOrdersCount() throws HibernateException {
		
		Query<Long> q = sessionFactory.getCurrentSession().createQuery("select count(*) from Bomorder where bomorderId in (select distinct parentorderId from Bomorder) ");
		
		Long result = ( ( Long ) q.uniqueResult() ).longValue();

		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Bomorder> getAllParentOrders() throws HibernateException {
		
		Query<Bomorder> q = sessionFactory.getCurrentSession().createQuery("from Bomorder where bomorderId in (select distinct parentorderId from Bomorder) order by DeliveryDate desc");
		
        return q.list();
    }
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Bomorder> getAllParentOrders( int startIndex, int pageSize ) throws HibernateException {
		
	Query<Bomorder> q = sessionFactory.getCurrentSession().createQuery("from Bomorder where bomorderId in (select distinct parentorderId from Bomorder) order by DeliveryDate desc");
		
		q.setFirstResult(startIndex);
		q.setMaxResults(pageSize);
		
		sessionFactory.getCurrentSession().flush();
				
		return q.list();
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Bomorder> getTLOOrdersBeingWorked() throws HibernateException {
		
	List<Bomorder> bomorders = new ArrayList<Bomorder>();
		
		String str = "from Bomorder where user is not null";   
		Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(str); 
	

		bomorders = query.list();
		System.out.println("*******orders being worked*********");

		
		for(Bomorder order: bomorders){
			if (order!=null){
				String ordernum = order.getOrderNumber();
				long shopid = order.getShopBySendingShopId().getShopId();
				System.out.println("ordernum: "+ ordernum + ", shopid: " + shopid);
				
				

			}
		}
		
		
		
		
		return bomorders;
		
	}
	
	@Override
	public List<Bomorder> getOrdersForTommorrow(  ) throws HibernateException {
		 		
		List<Bomorder> result = new ArrayList<Bomorder>();
		List<Bomorder> orders = getOrdersByTommorrow();
		
		for (Bomorder order: orders){
			if(order.getBomorderId().equals(order.getParentorderId())){
				result.add(order);
			}
			
		}
	 
		return result;
	
	}
	
	@Override
	public List<Bomorder> getOrdersByDate( String dateStr ) throws HibernateException {
		
		List<Bomorder> result = new ArrayList<Bomorder>();
		List<Bomorder> orders = getAllParentOrders();
		
		for (Bomorder order: orders){
			Date date = order.getDeliveryDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (dateStr.equals(cdate)){
				result.add(order);
			}
			
		}
	 
		return result;
	
	}

	@Override
	public boolean isStateValid(String state) throws HibernateException {
		
		State stateEntity = getStateByName( state );
		
		if (stateEntity==null){
			return false;
		}
		else {
			return true;
		}
	}
	
	@Override
	public boolean isCountryValid(String countryCode) throws HibernateException {
		
		Country countryEntity = getCountryByCode( countryCode );
		
		if (countryEntity==null){
			return false;
		}
		else {
			return true;
		}
	}
	

	@Override
	public boolean isOrderLocked(String orderNumber) throws HibernateException {
		
		boolean isLocked = true;
		
		Bomorder order = getOrderByOrderNumber(orderNumber);
		
		if (order.getUser()==null){
			isLocked = false;
		}
		
		
		
		return isLocked;
	}

	@Override
	public String persistParentOrderAndActivity(Bomorder bomorder,
												String userName, 
												String activityRoutingDescription, 
												String status, 
												String destination) throws Exception{
		
		String orderNumber = bomorder.getOrderNumber();
		
		Bomorder parentOrder = persistParentOrder(bomorder);
		persistOrderactivityByUser(userName, activityRoutingDescription, status, orderNumber, destination);
		
		Date createdDate = bomorder.getCreatedDate();
		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
		BomorderSts bomorderSts = new BomorderSts(parentOrder.getBomorderId(), 
									createdDate.getTime()/1000L,
									Byte.parseByte(status), 
									virtualqueueId , 
									null);
		persistBomorderSts(bomorderSts, 0);
		
		sessionFactory.getCurrentSession().flush();

		
		return orderNumber;
	}
	
	@Override
	public String persistChildOrderAndActivity(Bomorder bomorder, 
											    String parentOrderNumber,
											    String userName, 
												String activityRoutingDescription, 
												String status, 
												String destination) throws Exception{
		
		String orderNumber = bomorder.getOrderNumber();
		
		Bomorder childOrder = persistChildOrder(bomorder, parentOrderNumber);
		Long childId = childOrder.getBomorderId();
		Long parentId = getOrderByOrderNumber(parentOrderNumber).getBomorderId();
		persistChildOrderactivityByUser(userName, activityRoutingDescription, status, orderNumber, destination);
		persistOrderactivityByUser(userName, activityRoutingDescription, status, parentOrderNumber, destination);

		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
		BomorderSts bomorderSts = getBomorderStsOrderId(parentId);
		bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
		bomorderSts.setStsRoutingId(Byte.parseByte(status));
		bomorderSts.setVirtualqueueId(virtualqueueId);
	
		updateBomorderSts(bomorderSts, childId);
		
	
		return orderNumber;
	}
	
	/**
	 * persist bomorder sts
	 * when childId is zero there is not active child
	 * @param bomorderSts
	 * @param childId
	 * @return
	 * @throws HibernateException
	 */
	@Override
	public BomorderSts persistBomorderSts(BomorderSts bomorderSts, long childId)throws HibernateException {
		
		if (childId>0){
			bomorderSts.setActivechildId(childId);
		}
		
		sessionFactory.getCurrentSession().persist(bomorderSts);

		return bomorderSts;
	}
	
	@Override
	public BomorderSts updateBomorderSts(BomorderSts bomorderSts, long childId)throws HibernateException {
		
		if (childId>0){
			bomorderSts.setActivechildId(childId);
		}
		sessionFactory.getCurrentSession().merge(bomorderSts);

		return bomorderSts;
	}
	
	
	@Override
	public BomorderSts getBomorderStsOrderId( Long orderID ) throws HibernateException {
		BomorderSts bomorderSts = new BomorderSts();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<BomorderSts> cq = cb.createQuery(BomorderSts.class);
        
        Root<BomorderSts> root = cq.from(BomorderSts.class);
        cq.select(root);
        cq.where(cb.equal(root.get("bomorderId"), orderID));
        List<BomorderSts> list = session.createQuery(cq).getResultList();
		 
		 bomorderSts = list.get(0);
		 
		 return bomorderSts;
	}
	
	@Override
	public Map<String,String> getAllStates() throws HibernateException{
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<State> cq = cb.createQuery(State.class);
        
        Root<State> root = cq.from(State.class);
        cq.select(root);
        cq.orderBy(cb.asc(root.get("shortName")));
        List<State> states = session.createQuery(cq).getResultList();
		
		
		Map<String,String> results = new LinkedHashMap<String,String>();
		
		for(int ii=0; ii<states.size(); ++ii){
				results.put(states.get(ii).getShortName(),states.get(ii).getName());
		}
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getOrderTypes(Map<String,String> orderOcassions) throws HibernateException{
		

		List <Bomorder> bomOrders = new ArrayList<Bomorder>();
		
		List<String> results = new ArrayList<String>();
		String orders = "";
		
		for(String key : orderOcassions.keySet()){
			orders += key + ",";
		}
		
		if(orders.length() > 0) {

			orders = orders.substring(0, orders.length()-1);

			String str = "from Bomorder where OrderNumber in ("+orders+")";   
			Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(str); 

			bomOrders = query.list();

			for(Bomorder order : bomOrders){

				String orderXml = order.getOrderXml();
				ForeignSystemInterface fsi = transformService.convertToJavaFSI(orderXml);
				List<MessagesOnOrder> messagesOnOrder = fsi.getMessagesOnOrder();

				for ( MessagesOnOrder messages : messagesOnOrder ) {

					MessageOrder messageOnOrder = messages.getMessageOrder();
					String occassion = messageOnOrder.getOrderDetails().getOccasionCode();
					String temp = orderOcassions.get(order.getOrderNumber());
					temp += "," + occassion;
					results.add(temp);
				}
			}
		}
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Bomorder> managerSearch( Map<String, String> searchParams ) throws Exception {
		
		List<Bomorder> results  = new ArrayList<Bomorder>();
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from Bomorder where ");
			
			boolean orderNumberEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_NUMBER );
			boolean zipEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_ZIPCODE);
			boolean fromDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_DATE);
			boolean toDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_TO_DATE);
			boolean sendingShopEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_SENDING_SHOP );
			boolean fulfillingShopEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_FULFILLING_SHOP );
			boolean occasionEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_OCCASION );

			
			String orderNumber = searchParams.get( SearchCriteriaExtended.KEY_ORDER_NUMBER );
			String zipCode = searchParams.get( SearchCriteriaExtended.KEY_ORDER_ZIPCODE );
			String sendingShopStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_SENDING_SHOP );
			String fulfillingShopStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_FULFILLING_SHOP );
			String dateStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_DATE );
			String dateToStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_TO_DATE );
			String occassion = searchParams.get( SearchCriteriaExtended.KEY_ORDER_OCCASION );
			
			Shop sendingShop = new Shop();
			Shop fulfillingShop = new Shop();

			
			long zipid = 0;
			long sendingShopid = 0;
			long fulfillingShopid = 0;
			int count =0;
			
			 SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" );
			    // we will now try to parse the string into date form
			 Date date = new Date();
			 Date nextMidnight = new Date();
			 Date dateTo = new Date();
			 if (dateStr!=null){
			 date = sdf.parse(dateStr);
 
			  nextMidnight = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
			 }
			 if(dateToStr!=null){
				dateTo= sdf.parse(dateToStr);
			 }

			
			// return orders with matching order number
			if ( orderNumberEntered ) {
				
				if(count==0){
					sql.append(" orderNumber = :ordernumber");
				}
				else{
			sql.append(" and orderNumber = :ordernumber");
				}
				count ++;

			}
			
			// return orders with matching zip code
			if ( zipEntered ) {
				
				Zip zip = getZipByCode(zipCode);
				zipid = zip.getZipId();
				if (zipid!=0){
					if(count==0){
						sql.append(" zip_id = :zipid");
					}
					else{
					sql.append(" and zip_id = :zipid");
					}

				}
				count ++;
				
			}
			
			// return orders with matching sending shop
			if ( sendingShopEntered ) {
				
				sendingShop = shopDAO.getShopByCode(sendingShopStr);
				sendingShopid = sendingShop.getShopId();
				if (sendingShopid!=0){
					if(count==0){
						sql.append(" shopBySendingShopId = :sendingShop");
					}
					else{
					sql.append(" and shopBySendingShopId = :sendingShop");
					}

				}
				count ++;
				
			}
			
			// return orders with matching fulfilling shop
			if ( fulfillingShopEntered ) {
				
				fulfillingShop = shopDAO.getShopByCode(fulfillingShopStr);
				fulfillingShopid = fulfillingShop.getShopId();
				if (fulfillingShopid!=0){
					if(count==0){
						sql.append(" shopByReceivingShopId = :fulfillingShop");
					}
					else{
					sql.append(" and shopByReceivingShopId = :fulfillingShop");
					}

				}
				count ++;
				
			}
			
			// return orders with matching order delivery date
			if (( fromDateEntered )&& (!toDateEntered)) {
				
			    	if(count==0){
			        	sql.append(" deliverydate >= :date and deliverydate < :nextday ");
					}
					else{
			    	sql.append(" and deliverydate >= :date and deliverydate < :nextday ");
					}
					count ++;
			   
			}
			
			if (( toDateEntered )&& (fromDateEntered)) {
				
			    	if(count==0){
			        	sql.append(" deliverydate >= :date and deliverydate < :date2   ");
					}
					else{
			    	sql.append(" and deliverydate >= :date and deliverydate < :date2  ");
					}
					count ++;

			}
			
			//perhaps it will have to be combined with at least from date
			
			System.out.println(sql.toString());
			Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(sql.toString()); 
			
			
			if( orderNumberEntered ){
				query.setParameter("ordernumber", orderNumber);
			}
			if ( zipEntered ){
				query.setParameter("zipid", zipid);
			}
			if ( sendingShopEntered ){
				query.setParameter("sendingShop", sendingShop);
			}
			if (fulfillingShopEntered ){
				query.setParameter("fulfillingShop", fulfillingShop);
			}
			if (( fromDateEntered )&& (!toDateEntered)) {
				query.setParameter("date", date);
				query.setParameter("nextday", nextMidnight);

			}
			if (( toDateEntered )&& (fromDateEntered)) {
				query.setParameter("date", date);
				query.setParameter("date2", dateTo);
			}
			
			results = query.list();
			
			if (occasionEntered){
				
				List<Bomorder> occasionList = new ArrayList<Bomorder>();
				occasionList = getOrdersByOcassion(results,occassion);
				return occasionList;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BomorderviewV2> managerSearch2( Map<String, String> searchParams ) throws Exception {
		
		List<BomorderviewV2> results  = new ArrayList<BomorderviewV2>();
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("from BomorderviewV2 where ");
			
			boolean orderNumberEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_NUMBER );
			boolean zipEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_ZIPCODE);
			boolean fromDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_DATE);
			boolean toDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_TO_DATE);
			boolean sendingShopEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_SENDING_SHOP );
			boolean fulfillingShopEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_FULFILLING_SHOP );
			boolean occasionEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_OCCASION );

			
			String orderNumber = searchParams.get( SearchCriteriaExtended.KEY_ORDER_NUMBER );
			String zipCode = searchParams.get( SearchCriteriaExtended.KEY_ORDER_ZIPCODE );
			String sendingShopStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_SENDING_SHOP );
			String fulfillingShopStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_FULFILLING_SHOP );
			String dateStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_DATE );
			String dateToStr = searchParams.get( SearchCriteriaExtended.KEY_ORDER_TO_DATE );
			String occassion = searchParams.get( SearchCriteriaExtended.KEY_ORDER_OCCASION );
			
			Shop sendingShop = new Shop();
			Shop fulfillingShop = new Shop();

			
			long zipid = 0;
			long sendingShopid = 0;
			long fulfillingShopid = 0;
			int count =0;
			
			 SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" );
			    // we will now try to parse the string into date form
			 Date date = new Date();
			 Date nextMidnight = new Date();
			 Date dateTo = new Date();
			     
			 if (dateStr!=null){
			 date = sdf.parse(dateStr);
 
			  nextMidnight = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
			 }
			 if(dateToStr!=null){
				dateTo= sdf.parse(dateToStr);
			 }

			
			// return orders with matching order number
			if ( orderNumberEntered ) {
				
				if(count==0){
					sql.append(" parentOrderDate = :ordernumber");
				}
				else{
			sql.append(" and parentOrderDate = :ordernumber");
				}
				count ++;

			}
			
			// return orders with matching zip code
			if ( zipEntered ) {
				
				Zip zip = getZipByCode(zipCode);
				zipid = zip.getZipId();
				if (zipid!=0){
					if(count==0){
						sql.append(" zip_id = :zipid");
					}
					else{
					sql.append(" and zip_id = :zipid");
					}

				}
				count ++;
				
			}
			
			// return orders with matching sending shop
			if ( sendingShopEntered ) {
				
				sendingShop = shopDAO.getShopByCode(sendingShopStr);
				sendingShopid = sendingShop.getShopId();
				if (sendingShopid!=0){
					if(count==0){
						sql.append(" SendingShop_ID = :sendingShop");
					}
					else{
					sql.append(" and SendingShop_ID = :sendingShop");
					}

				}
				count ++;
				
			}
			
			// return orders with matching fulfilling shop
			if ( fulfillingShopEntered ) {
				
				fulfillingShop = shopDAO.getShopByCode(fulfillingShopStr);
				fulfillingShopid = fulfillingShop.getShopId();
				if (fulfillingShopid!=0){
					if(count==0){
						sql.append(" ReceivingShop_ID = :fulfillingShop");
					}
					else{
					sql.append(" and ReceivingShop_ID = :fulfillingShop");
					}

				}
				count ++;
				
			}
			
			// return orders with matching order delivery date
			if (( fromDateEntered )&& (!toDateEntered)) {
				
			    	if(count==0){
			        	sql.append(" DeliveryDate >= :date and DeliveryDate < :nextday ");
					}
					else{
			    	sql.append(" and DeliveryDate >= :date and DeliveryDate < :nextday ");
					}
					count ++;
			   
			}
			
			if (( toDateEntered )&& (fromDateEntered)) {
				
			    	if(count==0){
			        	sql.append(" DeliveryDate >= :date and DeliveryDate < :date2   ");
					}
					else{
			    	sql.append(" and DeliveryDate >= :date and DeliveryDate < :date2  ");
					}
					count ++;

			}
			
			//perhaps it will have to be combined with at least from date
			
			System.out.println(sql.toString());
			Query<BomorderviewV2> query = sessionFactory.getCurrentSession().createQuery(sql.toString()); 
			
			
			if( orderNumberEntered ){
				query.setParameter("ordernumber", orderNumber);
			}
			/*if ( zipEntered ){
				query.setParameter("zipid", zipid);
			}*/
			if ( sendingShopEntered ){
				query.setParameter("sendingShop", sendingShop);
			}
			if (fulfillingShopEntered ){
				query.setParameter("fulfillingShop", fulfillingShop);
			}
			if (( fromDateEntered )&& (!toDateEntered)) {
				query.setParameter("date", date);
				query.setParameter("nextday", nextMidnight);

			}
			if (( toDateEntered )&& (fromDateEntered)) {
				query.setParameter("date", date);
				query.setParameter("date2", dateTo);
			}
			
			results = query.list();
			
			if (occasionEntered){
				
				List<BomorderviewV2> occasionResults = new ArrayList<BomorderviewV2>();

				occasionResults = getOrdersViewByOcassion(results,occassion);
				
				return occasionResults;
				
			}
			if ( zipEntered ){
				List<BomorderviewV2> zipResults = getOrdersViewByZip(results, zipid);
				
				return zipResults;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
	
	private List<BomorderviewV2>  getOrdersViewByZip(List<BomorderviewV2> results, long zipid) {
		return null;
	}



	private List<BomorderviewV2> getOrdersViewByOcassion(List<BomorderviewV2> orders,String occassion) throws Exception {

		List<BomorderviewV2> occasionList = new ArrayList<BomorderviewV2>();

		List<Bomorder> bomorders = convertBomorderViewV2sToBomorders(orders);
		List<Bomorder> results = getOrdersByOcassion(bomorders, occassion) ;
		
		occasionList = convertBomordersToBomorderViewV2s(results);

		
		return occasionList;
	}



	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public List<Bomorder> getOrdersByOcassion(List<Bomorder> orders, String ocassionId) throws Exception {
		

		List<Bomorder> occasionList = new ArrayList<Bomorder>();

		
		for(Bomorder order : orders){
			
			String orderXml = order.getOrderXml();
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(orderXml);
			List<MessagesOnOrder> messagesOnOrder = fsi.getMessagesOnOrder();

			for ( MessagesOnOrder messages : messagesOnOrder ) {
				
				MessageOrder messageOnOrder = messages.getMessageOrder();
				String occassion = messageOnOrder.getOrderDetails().getOccasionCode();
				if ( occassion.equals(ocassionId))			
				{					
					occasionList.add(order);
							
				}
				
			}
		}
		
		
		return occasionList;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String,Integer> getManualMessageCounts(Date lastReportDate) {
		
		
		List<Object> objs = new ArrayList<Object>();
	    Map<String,Integer> results = new HashMap<String,Integer>();
		
		
		int inqr = 0;
		int resp = 0;
		int info = 0;
		int rjct = 0;
		int dlcf = 0;
		int pchg = 0;
		int canc = 0;
		int conf = 0;
		int deni = 0;

		/*String sql = "from Orderactivity as oa " +
		" LEFT OUTER JOIN oa.actMessage as ar " +
		" INNER JOIN ar.messagetype as mt " +
		" where oa.oactivitytype = 3" + 
		" and ( oa.user > 9 OR ar.commmethod = 2 )" +
		" and oa.createdDate > :lastReportDate ";*/

		String sqlstr = "select *" +
		" from orderactivity oa" + 
		" LEFT OUTER JOIN act_message ar ON oa.OrderActivity_ID = ar.OrderActivity_ID" +
		" INNER JOIN messagetype mt on ar.MessageType_ID = mt.MessageType_ID "+
		" where oa.OActivityType_ID = 3" +
		" and ( oa.User_ID > 9 OR ar.commmethod_id = 2 ) "+
		" and oa.CreatedDate > :lastReportDate" ;
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sqlstr)
		//.addEntity("messagetype", Messagetype.class)
		.addEntity("orderactivity", Orderactivity.class)

		.setParameter("lastReportDate", lastReportDate);

		objs = query.list();
		

		for( Object obj: objs){
			if (obj!=null){
				
				Orderactivity activity = (Orderactivity)obj;
				
				Messagetype msgType = activity.getActMessage().getMessagetype();
				
				
				if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.INQR_MESSAGE_DESC)){
					inqr++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RESP_MESSAGE_DESC)){
					resp++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RESP_MESSAGE_DESC)){
					resp++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.INFO_MESSAGE_DESC)){
					info++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RJCT_MESSAGE_DESC)){
					rjct++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.DLCF_MESSAGE_DESC)){
					dlcf++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.PCHG_MESSAGE_DESC)){
					pchg++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.CANC_MESSAGE_DESC)){
					canc++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.CONF_MESSAGE_DESC)){
					conf++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.DENI_MESSAGE_DESC)){
					deni++;
				}

			}

		}


		results.put("inqr", inqr);
		results.put("resp", resp);
		results.put("info", info);
		results.put("rjct", rjct);
		results.put("dlcf", dlcf);
		results.put("pchg", pchg);
		results.put("canc", canc);
		results.put("conf", conf);
		results.put("deni", deni);
		System.out.println("**************getManualMessageCounts************** " + objs.size());

		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Integer> getAutomatedMessageCounts(Date lastReportDate) {
		
		List<Orderactivity> objs = new ArrayList<Orderactivity>();
	    Map<String,Integer> results = new HashMap<String,Integer>();
		
		
		int inqr = 0;
		int resp = 0;
		int info = 0;
		int rjct = 0;
		int dlcf = 0;
		int pchg = 0;
		int canc = 0;
		int conf = 0;
		int deni = 0;

	
		String sqlstr = "select *" +
		" from orderactivity oa" + 
		" LEFT OUTER JOIN act_message ar ON oa.OrderActivity_ID = ar.OrderActivity_ID" +
		" INNER JOIN messagetype mt on ar.MessageType_ID = mt.MessageType_ID "+
		" where oa.OActivityType_ID = 3" +
		" and ( oa.User_ID <= 9 AND ar.commmethod_id = 2 ) "+
		" and oa.CreatedDate > :lastReportDate" ;
		
		//                     " and ( oa.User_ID <= 9 AND ar.commmethod = 2 ) "+

		Query<Orderactivity> query = sessionFactory.getCurrentSession().createSQLQuery(sqlstr)
		.addEntity("messagetype", Messagetype.class)
		.setParameter("lastReportDate", lastReportDate);

		objs = query.list();

		for( Object obj: objs){
			if (obj!=null){
				
				Messagetype msgType = (Messagetype)obj;
				
				if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.INQR_MESSAGE_DESC)){
					inqr++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RESP_MESSAGE_DESC)){
					resp++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RESP_MESSAGE_DESC)){
					resp++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.INFO_MESSAGE_DESC)){
					info++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.RJCT_MESSAGE_DESC)){
					rjct++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.DLCF_MESSAGE_DESC)){
					dlcf++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.PCHG_MESSAGE_DESC)){
					pchg++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.CANC_MESSAGE_DESC)){
					canc++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.CONF_MESSAGE_DESC)){
					conf++;
				}
				else if (msgType.getShortDesc().equalsIgnoreCase(BOMConstants.DENI_MESSAGE_DESC)){
					deni++;
				}

			}

		}
  		
  		results.put("inqr", inqr);
  		results.put("resp", resp);
  		results.put("info", info);
  		results.put("rjct", rjct);
  		results.put("dlcf", dlcf);
  		results.put("pchg", pchg);
  		results.put("canc", canc);
  		results.put("conf", conf);
  		results.put("deni", deni);
  		
		System.out.println("**************getAutomatedMessageCounts************** " + results.size());

		return results;
	}
	
	@Override
	public Map<String,Byte> getAllQueues() throws HibernateException{
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Virtualqueue> cq = cb.createQuery(Virtualqueue.class);
        
        Root<Virtualqueue> root = cq.from(Virtualqueue.class);
        cq.select(root);
		cq.orderBy(cb.asc(root.get("description")));
		List<Virtualqueue> queues = session.createQuery(cq).getResultList();
		
		Map<String,Byte> results = new LinkedHashMap<String,Byte>();
		
		for(int i=0; i<queues.size(); ++i){
				results.put(queues.get(i).getDescription(),queues.get(i).getVirtualqueueId());
		}
		
		return results;
	}
	
	
	@Override
	public Map<String,Byte> getAllStatuses() throws HibernateException{
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<StsRouting> cq = cb.createQuery(StsRouting.class);
        
        Root<StsRouting> root = cq.from(StsRouting.class);
        cq.select(root);
		cq.orderBy(cb.asc(root.get("description")));
		List<StsRouting> sts = session.createQuery(cq).getResultList();
		
		Map<String,Byte> results = new LinkedHashMap<String,Byte>();
		
		for(int i=0; i<sts.size(); ++i){
				results.put(sts.get(i).getDescription(),sts.get(i).getStsRoutingId());
		}
		
		return results;
	}
	
	List<Bomorder> convertBomorderViewV2sToBomorders(List<BomorderviewV2> orders){

		List<Bomorder> bomorders = new ArrayList<Bomorder>();
		
		for (BomorderviewV2 order:orders){
			if (order.getId()!=null){
				
				String orderNumber = order.getId().getParentOrderNumber();
				Bomorder bomorder = getOrderByOrderNumber(orderNumber );
				bomorders.add(bomorder);
			}
		}
		return bomorders;
	}
	
	public List<BomorderviewV2> convertBomordersToBomorderViewV2s(List<Bomorder> bomorders){
		
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		
		for (Bomorder bomorder:bomorders){
			if (bomorder!=null){
				BomorderviewV2 order = getBomorderviewV2ByOrderNumber(bomorder.getOrderNumber());
				orders.add(order);
			}
		}
		
		return orders;
		
	}
	
	@Override
	@Transactional
	public BomorderviewV2 getBomorderviewV2ByOrderNumber(String orderNumber) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<BomorderviewV2> cq = cb.createQuery(BomorderviewV2.class);
        
        Root<BomorderviewV2> root = cq.from(BomorderviewV2.class);
        cq.select(root);
        cq.where(cb.equal(root.get("ParentOrderNumber"), orderNumber));
		BomorderviewV2 result = session.createQuery(cq).uniqueResult();
		
		return result;
		
	}
	
	@Override
	@Transactional
	public Map<String,String> getLastOrderAndSequenceNumberTest() throws HibernateException {
		
		Map<String,String> results = new LinkedHashMap<String,String>();

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
        cq.where(cb.equal(root.get("orderType"), BOMConstants.ORDER_TYPE_BMT));
        cq.orderBy(cb.desc(root.get("bomorderId")));
        List<Bomorder> bomorders = session.createQuery(cq).setMaxResults(1000).getResultList();
		
									     
		if(bomorders != null){
			for(Bomorder bomorder:bomorders){
				long parentid = bomorder.getParentorderId();
				long orderid = bomorder.getBomorderId();
				if(parentid==orderid){
					results.put(bomorder.getOrderNumber(),bomorder.getBmtOrderSequenceNumber());
					return results;

				}
			}
		}
		return results;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@Transactional
public List<WebAppShop> searchShops( Map<String, String> searchParams ) throws Exception {
		
		List<ArrayList> results  = new ArrayList();
		List<WebAppShop> wasList = new ArrayList<WebAppShop>();

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT S.Shop_ID, S.ShopName,S.ShopAddress1,c.Name as city ,st.name as state, Z.Zip_Code ,S.ShopPhone, sn.ShopCode,SN.Network_ID,SN.ShopNetworkStatus_ID,SN.CommMethod_ID");
            sql.append(" FROM shopnetworkcoverage SNC");
            sql.append(" INNER JOIN shopnetwork SN ON SNC.ShopNetwork_ID = SN.ShopNetwork_ID");
            sql.append(" INNER JOIN Zip Z ON SNC.Zip_ID = Z.Zip_ID");
            sql.append(" INNER JOIN shop S ON SN.Shop_ID = S.Shop_ID");
            sql.append(" INNER JOIN city c ON c.City_ID = S.City_ID");
            sql.append(" INNER JOIN state st ON c.State_ID = st.State_ID");
            sql.append(" WHERE ");
            			
			String shopCode = null;
			String shopName =  null;
			String shopPhone =  null;
			String coveredZip =  null;
			
			boolean hasShopCode = false;
    		boolean hasShopName = false;
    		boolean hasShopPhone = false;
    		boolean hasCoveredZip = false;
			
			
			Iterator iterator = searchParams.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry mapEntry = (Map.Entry)iterator.next();
				if (mapEntry.getKey().equals("shopCode")){
					String [] shopCodeArray = (String[])mapEntry.getValue();
					shopCode=shopCodeArray[0];
					if (!shopCode.trim().isEmpty()){
						hasShopCode=true;
					}
				}
				if (mapEntry.getKey().equals("shopName")){
					String [] shopNameArray = (String[])mapEntry.getValue();
					shopName=shopNameArray[0];
					if (!shopName.trim().isEmpty()){
						hasShopName=true;
					}
				}
				if (mapEntry.getKey().equals("shopPhone")){
					String [] shopPhoneArray = (String[])mapEntry.getValue();
					shopPhone=shopPhoneArray[0];
					if (!shopPhone.trim().isEmpty()){
						hasShopPhone=true;
					}
				}
				if (mapEntry.getKey().equals("coveredZip")){
					String [] coveredZipArray = (String[])mapEntry.getValue();
					coveredZip=coveredZipArray[0];
					if (!coveredZip.trim().isEmpty()){
						hasCoveredZip=true;
					}
				}
			
			}
			
			
			
			

			
			
			
			int count=0;

			
			if ( hasShopCode ) {
				if(count==0){
					sql.append(" sn.ShopCode ='"+shopCode+"'");
				}
				else{
					sql.append(" and sn.ShopCode ='"+shopCode+"'");
				}
				count ++;
			}
			if ( hasShopName ) {
				if(count==0){
					sql.append(" S.ShopName = '"+shopName+"'");
				}
				else{
					sql.append(" and S.ShopName ='"+shopName+"'");
				}
				count ++;
				}
			if ( hasShopPhone ) {
				if(count==0){
					sql.append(" S.ShopPhone ='"+shopPhone+"'");
				}
				else{
					sql.append(" and S.ShopPhone ='"+shopPhone+"'");
				}
				count ++;			}
			if ( hasCoveredZip ) {
				if(count==0){
					sql.append(" Z.Zip_Code ='"+coveredZip+"'");
				}
				else{
					sql.append(" and Z.Zip_Code ='"+coveredZip+"'");
				}
				count ++;			}
			
			
			
			System.out.println(sql.toString());
			Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString()); 
			
			
			
			results =(ArrayList) query.getResultList();
			for(Object result:results){
		    	WebAppShop was = new WebAppShop();
		    	Object st[] = (Object[]) result;
		    	was.setShopCode(st[7].toString());
		    	was.setShopPhone(st[6].toString());
		    	was.setCoveredZip(st[5].toString());
		    	was.setShopName(st[1].toString());
		    	/*was.setShopCode(result.get(1).toString());
		    	was.setShopPhone(result.get(6).toString());
		    	was.setCoveredZip(result.get(5).toString());
		    	was.setShopName(result.get(1).toString());*/
		    	
		    	wasList.add(was);
			}

			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return wasList;
	}



	@Override
	public List<Bomorder> getOrdersInProgress(Long userId) {
		
		final User user = userDAO.getUserById(userId);
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
        cq.where(cb.equal(root.get("user"), user));
        List<Bomorder> results = session.createQuery(cq).getResultList();
		
		return results;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<Bomorder> getOrdersByOrderIds(String inString) {
		
		String str = "from Bomorder where BOMOrder_ID in ("+inString+")";   
		Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(str);

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Bomorder> getOrdersByOrderNumbersAndState(String inString, String state) {
		
		String str = "from Bomorder where OrderNumber in ("+inString+") and city.state.shortName = :state";   
		Query<Bomorder> query = sessionFactory.getCurrentSession().createQuery(str);
		query.setParameter("state", state);

		return query.getResultList();
	}
 
	
}
