package com.bloomnet.bom.mvc.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.FilesDAO;
import com.bloomnet.bom.common.entity.Files;

@Transactional
@Repository("filesDAO")
public class FilesDAOImpl implements FilesDAO {

	@Autowired private SessionFactory sessionFactory;
	
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.dao.impl.FilesDAO#find(int)
	 */
    @Override
	public Files find(int id) {
		return (Files) sessionFactory.getCurrentSession().get( Files.class, id );

    }
}
