package com.bloomnet.bom.mvc.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewDAO;
import com.bloomnet.bom.common.entity.Bomlatestdispositionview;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
import com.bloomnet.bom.common.entity.Bomorderview;
import com.bloomnet.bom.common.entity.BomorderviewId;
import com.bloomnet.bom.common.util.DateUtil;

@Transactional
@Repository("bomorderviewDAO")
public class BomorderviewDAOImpl implements BomorderviewDAO {
	
	@Autowired private SessionFactory sessionFactory;
	
    static Logger logger = Logger.getLogger( Bomorderview.class );


    @Override
	public List<Bomorderview> getAllOrders() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<Bomorderview> cq = session.getCriteriaBuilder().createQuery(Bomorderview.class);
        cq.from(Bomorderview.class);
        
        List<Bomorderview> orders = session.createQuery(cq).getResultList();
        
        
		
		return orders;
	}

	/*
	 * compares dates as dates
	 * (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.BomorderviewDAO#getOrdersForToday()
	 */
	@Override
	@Deprecated
	public List<Bomorderview> getOrdersForToday(){
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());

		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (todayStr.equals(cdate)){
				result.add(order);
			}
			}
			
		}

		return result;
	}
	
	@Override
	public List<Bomorderview> getOrderByDeliveryDate(Date date){
		
		String todayStr = DateUtil.toXmlNoTimeFormatString(date);

		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date delDate = order.getId().getDeliveryDate();
			String ddate = DateUtil.toXmlNoTimeFormatString(delDate);
			if (todayStr.equals(ddate)){
				result.add(order);
			}
			}
			
		}

		

		return result;
	}
	
	@Override
	@Deprecated
	public List<Bomorderview> getOrdersEnteredToday(){
		//Date todayMidnightDate = new DateTime().toDateMidnight().toDate();
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());


		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date date = order.getId().getParentOrderDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (todayStr.equals(cdate)){
				result.add(order);
			}
			}
			
		}

		

		return result;
	}
	
	@Override
	public List<Bomorderview> getOrdersEnteredByDate(Date date){
		
		String todayStr = DateUtil.toXmlNoTimeFormatString(date);


		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date parentDate = order.getId().getParentOrderDate();
			String pdate = DateUtil.toXmlNoTimeFormatString(parentDate);
			if (todayStr.equals(pdate)){
				result.add(order);
			}
			}
			
		}

		

		return result;
	}
	
	/*
	 * compares dates as string
	 * (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.BomorderviewDAO#getOrdersForTommorrow()
	 */
	@Override
	public List<Bomorderview> getOrdersForTommorrow(){
		String tommorrowStr = DateUtil.tomorrowsNoTimeDate();
 		
		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();
		
		for (Bomorderview order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (tommorrowStr.equals(cdate)){
				result.add(order);
			}
			}
			
		}
	 
		return result;
	}
	
	@Override
	public List<Bomorderview> getOutstandingOrders() {
		
		List<Bomorderview> orders = getAllOrders();
		
		List<Bomorderview> oustanding = new ArrayList<Bomorderview>();
		
				
		for(Bomorderview order: orders){
			if (order!=null){
				BomorderviewId orderId = order.getId();

				String status = orderId.getStatus();
				String childOrderNumber = orderId.getChildOrderNumber();
				String sendingshop = orderId.getSendingShopCode();


				if( (status.equals("To be worked"))||(status.equals("Actively being worked"))
						||(status.equals("Waiting for Response")) ||(status.equals("Waiting for Time Zone"))
						&&(childOrderNumber.isEmpty())
						&&(!sendingshop.equals(BOMConstants.FROM_YOU_FLOWERS))){

					oustanding.add(order);
				}

			}
		}
		
		
		return oustanding;
	}

	@Override
	@Deprecated
	public List<Bomorderview> getOutstandingOrdersForToday() {
		
		List<Bomorderview> orders = getOrderByDeliveryDate(new Date());
		
		List<Bomorderview> todaysOrders = new ArrayList<Bomorderview>();
		
		for(Bomorderview order: orders){
			if (order!=null){

				BomorderviewId orderId = order.getId();

				String status = orderId.getStatus();
				String childOrderNumber = orderId.getChildOrderNumber();

				if( (status.equals("To be worked"))||(status.equals("Actively being worked"))
						&&(childOrderNumber.isEmpty()) ){

					todaysOrders.add(order);
				}

			}
		}
		
		
		return todaysOrders;
	}
	
	@Override
	public List<Bomorderview> getOutstandingOrdersByDate(Date date) {
		
		List<Bomorderview> orders = getOrderByDeliveryDate(date);
		
		List<Bomorderview> todaysOrders = new ArrayList<Bomorderview>();
		
		for(Bomorderview order: orders){
			if (order!=null){

				BomorderviewId orderId = order.getId();

				String status = orderId.getStatus();
				String childOrderNumber = orderId.getChildOrderNumber();
				String sendingshop = orderId.getSendingShopCode();


				if( (status.equals("To be worked"))||(status.equals("Actively being worked"))
						||(status.equals("Waiting for Response")) ||(status.equals("Waiting for Time Zone"))
						&&(childOrderNumber.isEmpty())
						&&(!sendingshop.equals(BOMConstants.FROM_YOU_FLOWERS)) ){

					todaysOrders.add(order);
				}

			}
		}
		
		
		return todaysOrders;
	}
	
	
	@Override
	public List<Bomorderview> getOrdersPastDue(){
		
	
		Date todayMidnightDate = new DateTime().toDateMidnight().toDate();

		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			int eq = date.compareTo(todayMidnightDate);
			if (eq<0){
				result.add(order);
			}
			}
		}
	 
		return result;
	}
	
	@Override
	public List<Bomorderview> getFutureOrders(){
		
	
		Date todayMidnightDate = new DateTime().toDateMidnight().toDate();

		List<Bomorderview> result = new ArrayList<Bomorderview>();
		List<Bomorderview> orders = getAllOrders();

		for (Bomorderview order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			int eq = date.compareTo(todayMidnightDate);
			if (eq>0){
				result.add(order);
			}
			}
		}
	 
		return result;
	}

	@Override
	public List<Bomorderview> getOutstandingOrdersPriorToday(){
		
		List<Bomorderview> orders = getOrdersPastDue();
		
		List<Bomorderview> priorOrders = new ArrayList<Bomorderview>();
		
		for(Bomorderview order: orders){
			if (order!=null){

			BomorderviewId orderId = order.getId();
			
			String status = orderId.getStatus();
			String childOrderNumber = orderId.getChildOrderNumber();
			String sendingshop = orderId.getSendingShopCode();


			if( (status.equals("To be worked"))||(status.equals("Actively being worked"))
					&&(childOrderNumber.isEmpty()) 
					&&(!sendingshop.equals(BOMConstants.FROM_YOU_FLOWERS))){
				
				priorOrders.add(order);
			}
			}
			
		}
		
		
		return priorOrders;
	}

	@Override
	@Deprecated
	public List<Bomorderview> getShopOrdersForToday(String shopCode) {
		
		List<Bomorderview> orders = getOrdersEnteredByDate(new Date());
		
		List<Bomorderview> shopOrders = new ArrayList<Bomorderview>();
		
		for(Bomorderview order: orders){
			if (order!=null){

				BomorderviewId orderId = order.getId();

				String sendingshop = orderId.getSendingShopCode();

				if( (sendingshop.equals(shopCode)) ){
					shopOrders.add(order);
				}

			}
		}
		
		
		return shopOrders;
	}
	
	@Override
	public List<Bomorderview> getShopOrdersByDate(String shopCode, Date date) {
		
		List<Bomorderview> orders = getOrdersEnteredByDate(date);
		
		List<Bomorderview> shopOrders = new ArrayList<Bomorderview>();
		
		for(Bomorderview order: orders){
			if (order!=null){

				BomorderviewId orderId = order.getId();

				String sendingshop = orderId.getSendingShopCode();

				if( (sendingshop.equals(shopCode)) ){
					shopOrders.add(order);
				}

			}
		}
		
		
		return shopOrders;
	}
	

	@Override
	public List<Bomlatestordernoteview> getAllOrderNoteView() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<Bomlatestordernoteview> cq = session.getCriteriaBuilder().createQuery(Bomlatestordernoteview.class);
        cq.from(Bomlatestordernoteview.class);
        
        List<Bomlatestordernoteview> orderNotes = session.createQuery(cq).getResultList();
		
        
		
		return orderNotes;
	}
	
	@Override
	public Bomlatestordernoteview getOrderNoteView(String orderNumber) {
		
		List<Bomlatestordernoteview> orderNotes = getAllOrderNoteView();
		
		for(Bomlatestordernoteview orderNote: orderNotes){
			if(orderNote.getId().getParentOrderNumber().equals(orderNumber)){
				return orderNote;
			}
		}
		return null;
	}


	@Override
	public List<Bomlatestdispositionview> getAllDispositionview() {
		
		Session session = sessionFactory.getCurrentSession();
       
		CriteriaQuery<Bomlatestdispositionview> cq = session.getCriteriaBuilder().createQuery(Bomlatestdispositionview.class);
        cq.from(Bomlatestdispositionview.class);
        
        List<Bomlatestdispositionview> categories = session.createQuery(cq).getResultList();
		
        
		
		return categories;
	}

	@Override
	public Bomlatestdispositionview getDispositionView(String orderNumber) {
		
		List<Bomlatestdispositionview> disps = getAllDispositionview();
		
		for(Bomlatestdispositionview disp: disps){
			if(disp.getId().getParentOrderNumber().equals(orderNumber)){
				return disp;
			}
		}
		return null;
	}




}
