package com.bloomnet.bom.mvc.dao.impl;

import java.util.Date;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.BusinessLogicDAO;
import com.bloomnet.bom.common.entity.Businesslogic;

@Transactional
@Repository("businessLogicDAO")
public class BusinessLogicDAOImpl implements BusinessLogicDAO {

	@Autowired private SessionFactory sessionFactory;

	//@Override
	public Businesslogic getCurrentQueueRoute() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Businesslogic> cq = cb.createQuery(Businesslogic.class);
		Root<Businesslogic> root = cq.from(Businesslogic.class);
		cq.select(root);
        cq.orderBy(cb.asc(root.get("createdDate")));
        
        Businesslogic businesslog = session.createQuery(cq).uniqueResult();
		
        
        
		return businesslog;
	}

	@Override
	public long setCurrentQueueRoute(Businesslogic businessLogic) {

		long result=businessLogic.getBusinessLogicId();
		sessionFactory.getCurrentSession().saveOrUpdate(businessLogic);
		return result;
	}

	@Override
	public boolean isDefaultRoute() {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Businesslogic> cq = cb.createQuery(Businesslogic.class);
		Root<Businesslogic> root = cq.from(Businesslogic.class);
		cq.select(root);
        cq.orderBy(cb.asc(root.get("createdDate")));
        
        Businesslogic businesslog = session.createQuery(cq).uniqueResult();
		
		
		long id = businesslog.getBusinessLogicId();
		
		
		
		if (id!=1){
		return false;
		}
		else{
			return true;
		}
	}

	@Override
	public long setRouteToDefault() {
		
		long id =1;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Businesslogic> cq = cb.createQuery(Businesslogic.class);
		Root<Businesslogic> root = cq.from(Businesslogic.class);
		cq.select(root);
		cq.where(cb.equal(root.get("businessLogicId"), id));
        
        Businesslogic businessLogic = session.createQuery(cq).uniqueResult();	
		
		businessLogic.setCreatedDate(new Date());
		
		long result=businessLogic.getBusinessLogicId();
		sessionFactory.getCurrentSession().saveOrUpdate(businessLogic);
		
		
		
		return result;
	}
}
