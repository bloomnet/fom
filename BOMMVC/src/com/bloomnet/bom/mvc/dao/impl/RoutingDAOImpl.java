/**
 * 
 */
package com.bloomnet.bom.mvc.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.RoutingInterface;
import com.bloomnet.bom.common.entity.StsRouting;

/**
 * @author Danil Svirchtchev
 *
 */
@Transactional
@Repository("routingDAO")
public class RoutingDAOImpl implements RoutingDAO {
	
	@Autowired private SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.RoutingDAO#getStatusById(byte)
	 */
	@Override
	public StsRouting getStatusById( byte id ) throws HibernateException {
		return (StsRouting) sessionFactory.getCurrentSession().get(StsRouting.class, id);
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.RoutingDAO#getStatusById(java.lang.String)
	 */
	@Override
	public StsRouting getStatusById(String id) throws HibernateException  {
		final byte m_id = Byte.parseByte(id);
		return getStatusById( m_id );
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.RoutingDAO#getVirtualQueue(java.lang.String)
	 */
	@Override
	public String getVirtualQueue(String orderNumber) throws HibernateException {
		// TODO: Need more implementation details
		throw new HibernateException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.RoutingDAO#persistRouting(com.bloomnet.bom.common.entity.ActRouting)
	 */
	@Override
	public String persistRouting( ActRouting route ) throws HibernateException {
		final String result = route.getStsRouting().getDescription();
		sessionFactory.getCurrentSession().saveOrUpdate(route);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.RoutingDAO#persistRouting(com.bloomnet.bom.common.entity.RoutingInterface)
	 */
	@Override
	public void persistRouting( RoutingInterface route ) throws HibernateException {
		
		final ActRouting entity = new ActRouting( route.getOrderactivity(),
												  route.getStsRouting(),
												  route.getVirtualQueue() );
		
		persistRouting( entity );
	}
}
