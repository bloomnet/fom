package com.bloomnet.bom.mvc.dao.impl;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.hibernate.query.Query;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.util.DateUtil;

@Transactional
@Repository("fulfillmentSummaryDAO")
public class FulfillmentSummaryDAOImpl implements FulfillmentSummaryDAO {

	@Autowired private SessionFactory sessionFactory;
	
	@Override
	@SuppressWarnings({ })
	public FulfillmentSummaryDaily getFulfillmentSummaryDaily()
			throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<FulfillmentSummaryDaily> cq = session.getCriteriaBuilder().createQuery(FulfillmentSummaryDaily.class);
        cq.from(FulfillmentSummaryDaily.class);
        
        List<FulfillmentSummaryDaily> fulfillmentSummaryDaily = session.createQuery(cq).getResultList();
		
        
        
		return fulfillmentSummaryDaily.get(0);
	}

	@Override	
	@SuppressWarnings({ })
	public FulfillmentSummaryMtd getFulfillmentSummaryMtd()
			throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();

		CriteriaQuery<FulfillmentSummaryMtd> cq = session.getCriteriaBuilder().createQuery(FulfillmentSummaryMtd.class);
        cq.from(FulfillmentSummaryMtd.class);
        
        List<FulfillmentSummaryMtd> fulfillmentSummaryMtd = session.createQuery(cq).getResultList();
        
        
		
		return fulfillmentSummaryMtd.get(0);
	}

	@Override
	public List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDate(Date date)
			throws HibernateException {
		
		Date nextMidnight  = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<FulfillmentSummaryDaily> cq = cb.createQuery(FulfillmentSummaryDaily.class);

        Root<FulfillmentSummaryDaily> root = cq.from(FulfillmentSummaryDaily.class);
        cq.select(root);
		cq.where(cb.between(root.get("deliveryDate"), date, nextMidnight));
        
        List<FulfillmentSummaryDaily> fulfillmentSummaryDaily = session.createQuery(cq).getResultList();
		
        
		
		return fulfillmentSummaryDaily;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDateRange(String dateStr, String dateToStr)
			throws  Exception {
		
		List<FulfillmentSummaryDaily> results = new ArrayList<FulfillmentSummaryDaily>();
		StringBuffer sql = new StringBuffer();
		sql.append("from FulfillmentSummaryDaily where ");
		
	/*	boolean fromDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_DATE);
		boolean toDateEntered = searchParams.containsKey( SearchCriteriaExtended.KEY_ORDER_TO_DATE);
		*/
		boolean fromDateEntered = ((dateStr != null) && !dateStr.equals(""));

		boolean toDateEntered = ((dateToStr != null) && !dateToStr.equals(""));

		
		int count =0;
		
		 SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy" );
		    // we will now try to parse the string into date form
		 Date date = new Date();
		 Date nextMidnight = new Date();
		 Date dateTo = new Date();
		     
		// String nextDay = new String();
		 if (dateStr!=null){
		 date = sdf.parse(dateStr);

		  nextMidnight = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		//  nextDay = DateUtil.toString(nextMidnight);
		 }
		 if((dateToStr!=null)&&(!dateToStr.trim().isEmpty())){
			dateTo= sdf.parse(dateToStr);
		 }
		
		if (( fromDateEntered )&& (!toDateEntered)) {
			
		    	if(count==0){
		        	sql.append(" deliveryDate >= :date and deliveryDate < :nextday ");
				}
				else{
		    	sql.append(" and deliveryDate >= :date and deliveryDate < :nextday ");
				}
				count ++;
		   
		}
		
		if (( toDateEntered )&& (fromDateEntered)) {
			
		    	if(count==0){
		        	sql.append(" deliveryDate >= :date and deliveryDate < :date2   ");
				}
				else{
		    	sql.append(" and deliveryDate >= :date and deliveryDate < :date2  ");
				}
				count ++;

		}
		
		System.out.println(sql.toString());
		
		Session session = sessionFactory.getCurrentSession();
		Query<FulfillmentSummaryDaily> query = session.createQuery(sql.toString());
		
		if (( fromDateEntered )&& (!toDateEntered)) {
			query.setParameter("date", date);
			query.setParameter("nextday", nextMidnight);

		}
		if (( toDateEntered )&& (fromDateEntered)) {
			query.setParameter("date", date);
			query.setParameter("date2", dateTo);
		}
		
		results = query.getResultList();
		
		
		
		return results;
	}

	@Override
	@SuppressWarnings({ })
	public List<FulfillmentSummaryMtd> getFulfillmentSummaryMtdByDate(Date date)
			throws HibernateException {
		
		String dateStr = DateUtil.toString(date);
		StringTokenizer st = new StringTokenizer(dateStr, "/"); 
		int month=0;
		int day=0;
		int year=0;
		
		List<String> dateParsed = new ArrayList<String>();
		while(st.hasMoreTokens()) { 
			dateParsed.add(st.nextToken());
		}
		month = Integer.parseInt(dateParsed.get(0));
		day = Integer.parseInt(dateParsed.get(1));
		year = Integer.parseInt(dateParsed.get(2));

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<FulfillmentSummaryMtd> cq = cb.createQuery(FulfillmentSummaryMtd.class);

        Root<FulfillmentSummaryMtd> root = cq.from(FulfillmentSummaryMtd.class);
        cq.select(root);
		cq.where(cb.and(cb.equal(root.get("deliveryMonth"), month), cb.equal(root.get("reportDay"), day),cb.equal(root.get("deliveryYear"), year)));

        
        List<FulfillmentSummaryMtd> fulfillmentSummaryMtd = session.createQuery(cq).getResultList();
		
        
        
		return fulfillmentSummaryMtd;
	}
	
	@Override
	public void callBomDailyMetricsSP(final String month, final String beginDate, final String endDate){
		
		/*Query query = sessionFactory.getCurrentSession().createSQLQuery(
		"CALL test_sp( )")
		.addEntity(Commmethod.class);
		
		List<Commmethod> result = (ArrayList<Commmethod>) query.list();
		for(Commmethod method: result){
			System.out.println(method.getDescription());
		}
*/
		
		try {
			sessionFactory.getCurrentSession().doWork(
				    new Work() {
				    	@Override
				    	public void execute(java.sql.Connection connection) throws SQLException 
				        { 
				    		CallableStatement callableStatement = connection.prepareCall("{CALL bom_daily_metrics_sp(?,?,?)}");
				        	callableStatement.setString(1, month);
							callableStatement.setString(2, beginDate);
							callableStatement.setString(3, endDate);
							callableStatement.execute();
				        }

				    }
				); 


		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		/*Query query = sessionFactory.getCurrentSession().createSQLQuery(
		"CALL bom_daily_metrics_sp(:DeliveryDate1,:DeliveryDate2 ,:DeliveryDate3 )")
		.addEntity(FulfillmentSummaryDaily.class)
		.setParameter("DeliveryDate1", month)
		.setParameter("DeliveryDate2", beginDate)
		.setParameter("DeliveryDate3", endDate);
		
		List<FulfillmentSummaryDaily> result = (ArrayList<FulfillmentSummaryDaily>) query.list();
		for(FulfillmentSummaryDaily daily: result){
			System.out.println(daily.getTotalOrders());
		}*/
		
		
		
		
		
		
		
		/*Query query = sessionFactory.getCurrentSession().createSQLQuery(
		"CALL bom_daily_metrics_sp(:DeliveryDate1,:DeliveryDate2 ,:DeliveryDate3 )")
		.setParameter("DeliveryDate1", month)
		.setParameter("DeliveryDate2", beginDate)
		.setParameter("DeliveryDate3", endDate);*/
		
		/*Query query = sessionFactory.getCurrentSession().getNamedQuery("callMetricsStoredProcedure")
		.setParameter("DeliveryDate1", month)
		.setParameter("DeliveryDate2", beginDate)
		.setParameter("DeliveryDate3", endDate);
*/

	}

}
