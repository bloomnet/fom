/**
 * 
 */
package com.bloomnet.bom.mvc.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.BomorderviewV2Id;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Useractivity;
import com.bloomnet.bom.common.entity.Userrole;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.utils.HibernateHelper;

/**
 * Implementation of a DAO for {@link com.bloomnet.bom.common.dao.UserDAO}
 * 
 * @author Danil Svirchtchev
 */
@Transactional
@Repository("userDAO")
public class UserDAOImpl implements UserDAO {
	
	@Autowired private SessionFactory sessionFactory;
	@Autowired private Properties bomProperties;
	@Autowired private OrderDAO orderDAO;



	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#deleteUser(java.lang.Long)
	 */
	@Override
	public String deleteUser( User user ) throws HibernateException {
		final String result = user.getUserName();
		sessionFactory.getCurrentSession().delete( user );
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getAllUsers()
	 */
	@Override
	public List<User> getAllUsers() throws HibernateException {
        
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
        cq.from(User.class);

        List<User> results = session.createQuery(cq).getResultList();
		
		return results;
	}
	
	@Override
	public List<Role> getAllRoles() throws HibernateException {
        
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Role> cq = cb.createQuery(Role.class);
        cq.from(Role.class);

        List<Role> results = session.createQuery(cq).getResultList();
		
		return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getCurrentOrderBeingWorked(java.lang.Long)
	 */
	@Override
	public Bomorder getCurrentOrderBeingWorked(Long userId) throws HibernateException {
		throw new HibernateException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getTotalOrdersTouchedToday(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getTotalOrdersTouchedToday(Long userId) throws HibernateException {

		List<BomorderviewV2> results = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		Set<BomorderviewV2> hashSet = new LinkedHashSet<BomorderviewV2>();
		
		final User m_user = getUserById(userId);

		List<Orderactivity> m_orderActivities = getUsersOrderActivityForToday(m_user);

		for(Orderactivity orderActivity: m_orderActivities){
			Long bomorderId = orderActivity.getBomorder().getBomorderId();


			String str = "from BomorderviewV2 where parentBomid = :bomorderId "; 

			Query<BomorderviewV2> query = sessionFactory.getCurrentSession().createQuery(str); 
			query.setParameter("bomorderId", bomorderId);
			orders = query.list();

			for(BomorderviewV2 order: orders){
				if (order!=null){

					hashSet.add(order);

				}
			}
		}


		if ( ! hashSet.isEmpty() ) {

			Iterator<BomorderviewV2> itr = hashSet.iterator();

			while(itr.hasNext()){
				BomorderviewV2 order = itr.next();
				results.add(order);
			}

		}

		return results;


	}
	
	public List<Orderactivity> getUsersOrderActivityForToday(User user){
		
		List<Orderactivity> results = new ArrayList<Orderactivity>();
		
		Date todayMidnight = new DateTime().toDateMidnight().toDate();
		Date nextMidnight  = new DateTime(todayMidnight).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"),user),cb.between(root.get("createdDate"),todayMidnight,nextMidnight)));
        results = session.createQuery(cq).getResultList();
		
		return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getTotalOrdersWorkedToday(java.lang.Long)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<BomorderviewV2> getTotalOrdersWorkedToday(Long userId) throws HibernateException {
		
		List<BomorderviewV2> results = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		Set<BomorderviewV2> hashSet = new LinkedHashSet<BomorderviewV2>();


		
		User m_user = getUserById(userId);

		List<Orderactivity> m_orderActivities = getUsersOrderActivityForToday(m_user);


		if ( ! m_orderActivities.isEmpty() ) {

			for(Orderactivity orderActivity: m_orderActivities){
				
				ActRouting m_actRouting = orderActivity.getActRouting();

				if ( m_actRouting != null && 
						m_actRouting.getStsRouting()!= null ) {
					
					Long bomorderId = orderActivity.getBomorder().getBomorderId();

					String str = "from BomorderviewV2 where parentBomid = :bomorderId "; 

					Query query = sessionFactory.getCurrentSession().createQuery(str); 
					query.setParameter("bomorderId", bomorderId);
					orders = query.list();

					for(BomorderviewV2 order: orders){
						if (order!=null){
							BomorderviewV2Id orderId = order.getId();
							String status = orderId.getStsRoutingId().toString();
							if ( status.equals(bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP)) || 
									status.equals(bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP)) || 
									status.equals(bomProperties.getProperty(BOMConstants.ORDER_COMPLETED_OUTSIDE_BOM)) || 
									status.equals(bomProperties.getProperty(BOMConstants.ORDER_COMPLETED))  ) {
								
								hashSet.add(order);

							}

						}
					}
				}
			}
		}
		
		if ( ! hashSet.isEmpty() ) {

			Iterator<BomorderviewV2> itr = hashSet.iterator();

			while(itr.hasNext()){
				BomorderviewV2 order = itr.next();
				results.add(order);
			}
			
			}

				return results;
		
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getTotalWorkTimeToday(java.lang.Long)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Long> getTotalWorkTimeToday(Long userId) throws HibernateException {
		
		
		Date result = new Date();
		Date lock = null;
		Date unlock = null;
		
		List<Long> orderTime = new ArrayList();
		
		//get orders touched today
		List<BomorderviewV2> touchedList = getTotalOrdersTouchedToday(userId);
		
		for ( BomorderviewV2 touched: touchedList){
			long bomid = touched.getId().getParentBomid();
			Bomorder bomorder = orderDAO.getOrderByOrderId(bomid);
			List<Orderactivity> orderActivities = orderDAO.getOrderActivities(bomorder);
			System.out.println(touched.getId().getParentOrderNumber());
			 lock = null;
			 unlock = null;
			
			if ( ! orderActivities.isEmpty() ) {
				
				Collections.sort ( orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse( orderActivities );
				
				for (Orderactivity m_oactivity:orderActivities ){
					
				
				if( m_oactivity != null ) {
					
					final ActRouting m_actRouting = m_oactivity.getActRouting();
					
					if ( m_actRouting != null && 
						 m_actRouting.getStsRouting()!= null ) {
							
						 result = m_oactivity.getCreatedDate();
						 String resultStr = DateUtil.toXmlNoTimeFormatString(result);
						 String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());

						 
						 String queue = m_actRouting.getVirtualQueue();
							String status = m_actRouting.getStatus();
							
							if (status.equals("Actively being worked")&&
									(queue.equals(BOMConstants.TLO_QUEUE))&&
									(resultStr.equals(todayStr))){
								lock = m_oactivity.getCreatedDate();
								break;
							}
							else if (!status.equals("Actively being worked")&&
									!status.equals("To be worked")&&
									(queue.equals(BOMConstants.TLO_QUEUE))&&
									(resultStr.equals(todayStr))){
								
								//get the difference between the time order is being worked to now
								// if time is greater than invalidSession time then unlock
								 unlock = m_oactivity.getCreatedDate();

							}	
						
					}
					
					
				}
				
				
				
			}
				long lockedDuration = 0;
				if (unlock!=null){
				 lockedDuration = lock.getTime() - unlock.getTime();
				}
				else{
					Date now = new Date();
					lockedDuration = lock.getTime() - now.getTime();
				}
				long time = lockedDuration * (-1)/ 1000;
				orderTime.add(time);
				
			}
		}
		
	
		return orderTime;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getTotalOrdersTouchedByDate(Long userId, Date date) throws HibernateException {

		List<BomorderviewV2> results = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		Set<BomorderviewV2> hashSet = new LinkedHashSet<BomorderviewV2>();
		
		final User m_user = getUserById(userId);

		List<Orderactivity> m_orderActivities = getUsersOrderActivityByDate(m_user, date);

		for(Orderactivity orderActivity: m_orderActivities){
			Long bomorderId = orderActivity.getBomorder().getBomorderId();


			String str = "from BomorderviewV2 where parentBomid = :bomorderId "; 

			Query<BomorderviewV2> query = sessionFactory.getCurrentSession().createQuery(str); 
			query.setParameter("bomorderId", bomorderId);
			orders = query.list();

			for(BomorderviewV2 order: orders){
				if (order!=null){

					hashSet.add(order);

				}
			}
		}


		if ( ! hashSet.isEmpty() ) {

			Iterator<BomorderviewV2> itr = hashSet.iterator();

			while(itr.hasNext()){
				BomorderviewV2 order = itr.next();
				results.add(order);
			}

		}

		return results;


	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getTotalOrdersTouchedByDate(Date date) throws HibernateException {

		List<BomorderviewV2> results = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		Set<BomorderviewV2> hashSet = new LinkedHashSet<BomorderviewV2>();
		

		List<Orderactivity> m_orderActivities = getAllUsersOrderActivityByDate( date);

		for(Orderactivity orderActivity: m_orderActivities){
			if ((orderActivity.getUser().getUserId()!=1)&&
			(orderActivity.getUser().getUserId()!=2)&&
			(orderActivity.getUser().getUserId()!=3)){
			
				Long bomorderId = orderActivity.getBomorder().getBomorderId();
			String fname = orderActivity.getUser().getFirstName();
			String lname = orderActivity.getUser().getLastName();


			String str = "from BomorderviewV2 where parentBomid = :bomorderId "; 

			Query<BomorderviewV2> query = sessionFactory.getCurrentSession().createQuery(str); 
			query.setParameter("bomorderId", bomorderId);
			orders = query.list();

			for(BomorderviewV2 order: orders){
				if (order!=null){
					order.getId().setUserName(fname + " " + lname);
					hashSet.add(order);

				}
			}
			}
		}


		if ( ! hashSet.isEmpty() ) {

			Iterator<BomorderviewV2> itr = hashSet.iterator();

			while(itr.hasNext()){
				BomorderviewV2 order = itr.next();
				results.add(order);
			}

		}

		return results;


	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<BomorderviewV2> getTotalOrdersWorkedByDate(Long userId, Date date) throws HibernateException {
		
		List<BomorderviewV2> results = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = new ArrayList<BomorderviewV2>();
		Set<BomorderviewV2> hashSet = new LinkedHashSet<BomorderviewV2>();


		
		User m_user = getUserById(userId);

		List<Orderactivity> m_orderActivities = getUsersOrderActivityByDate(m_user, date);


		if ( ! m_orderActivities.isEmpty() ) {

			for(Orderactivity orderActivity: m_orderActivities){
				
				ActRouting m_actRouting = orderActivity.getActRouting();

				if ( m_actRouting != null && 
						m_actRouting.getStsRouting()!= null && 
						m_actRouting.getStsRouting().getStsRoutingId() == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP ))
						) {
					
					Long bomorderId = orderActivity.getBomorder().getBomorderId();

					String str = "from BomorderviewV2 where parentBomid = :bomorderId "; 

					Query query = sessionFactory.getCurrentSession().createQuery(str); 
					query.setParameter("bomorderId", bomorderId);
					orders = query.list();

					for(BomorderviewV2 order: orders){
						hashSet.add(order);

					}
				}
			}
		}
		
		if ( ! hashSet.isEmpty() ) {

			Iterator<BomorderviewV2> itr = hashSet.iterator();

			while(itr.hasNext()){
				BomorderviewV2 order = itr.next();
				results.add(order);
			}
			
			}

				return results;
		
	}

	/**
	* Get orders touched by date and sorts results by date 
	* Iterate through list of orders and get order activities
	* get the actRouting activity 
	* set order lock time as earliest order activity with status Actively being worked
	* set order unlock time as the last order activity with status not Actively being worked or To be worked
	* The lock duration is the lock time minus the unlock time
	* each time for each order is then added up
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<Long> getTotalWorkTimeByDate(Long userId, Date date) throws HibernateException {


		Date lock = null;
		Date unlock = null;

		List<Long> orderTime = new ArrayList();

		//get orders touched today
		List<BomorderviewV2> touchedList = getTotalOrdersTouchedByDate(userId, date);

		for ( BomorderviewV2 touched: touchedList){
			long bomid = touched.getId().getParentBomid();
			Bomorder bomorder = orderDAO.getOrderByOrderId(bomid);
			List<Orderactivity> orderActivities = orderDAO.getOrderActivities(bomorder);
			System.out.println(touched.getId().getParentOrderNumber());
			lock = null;
			unlock = null;

			if ( ! orderActivities.isEmpty() ) {

				Collections.sort ( orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse( orderActivities );

				for (Orderactivity m_oactivity:orderActivities ){

					int eq = date.compareTo(m_oactivity.getCreatedDate());

					if( m_oactivity != null && eq<0 ) {

						final ActRouting m_actRouting = m_oactivity.getActRouting();
						
						if ( m_actRouting != null && 
								m_actRouting.getStsRouting()!= null
								 ) {

							String queue = m_actRouting.getVirtualQueue();
							String status = m_actRouting.getStatus();

							if (status.equals("Actively being worked")&&
									(queue.equals(BOMConstants.TLO_QUEUE))){
								lock = m_oactivity.getCreatedDate();
	
							}
							else if (!status.equals("Actively being worked")&&
									!status.equals("To be worked")&&
									(unlock==null)&&
									(queue.equals(BOMConstants.TLO_QUEUE))){

								//get the difference between the time order is being worked to now
								// if time is greater than invalidSession time then unlock
								unlock = m_oactivity.getCreatedDate();

							}	

						}


					}



				}
				
				long lockedDuration = 0;
				
				if (unlock!=null) {
					int eq = lock.compareTo(unlock);

					if  ( eq<0 ){	
						lockedDuration = lock.getTime() - unlock.getTime();
					
					/*}
			else{
				Date now = new Date();
				lockedDuration = lock.getTime() - now.getTime();
			}*/
					long time = lockedDuration * (-1)/ 1000;
					orderTime.add(time);
					String seconds = Integer.toString((int)(time % 60));   
					String minutes = Integer.toString((int)((time % 3600) / 60));   
					String hours = Integer.toString((int)(time / 3600));
					System.out.println("time: " + hours+":"+minutes+":"+seconds);
					}
				}
			}
		}


		return orderTime;
	}



	public List<Orderactivity> getUsersOrderActivityByDate(User user, Date date){
		
		List<Orderactivity> results = new ArrayList<Orderactivity>();
		
		Date nextMidnight  = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"),user),cb.between(root.get("createdDate"),date,nextMidnight)));
        results = session.createQuery(cq).getResultList();
		
		
		return results;
	}
	
	
	public List<Orderactivity> getAllUsersOrderActivityByDate( Date date){
		
		List<Orderactivity> results = new ArrayList<Orderactivity>();
		
		Date nextMidnight  = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.between(root.get("createdDate"),date,nextMidnight));
        results = session.createQuery(cq).getResultList();
		
		
		return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getUserByEmail(java.lang.String)
	 */
	@Override
	public User getUserByEmail(String email) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
        
        Root<User> root = cq.from(User.class);
        cq.select(root);
        cq.where(cb.equal(root.get("email"),email));
        User result = session.createQuery(cq).uniqueResult();
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getUserById(com.bloomnet.bom.common.entity.User)
	 */
	@Override
	public User getUserById(Long userId) throws HibernateException {
		return (User) sessionFactory.getCurrentSession().get( User.class, userId );
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getUserByUserName(java.lang.String)
	 */
	@Override
	public User getUserByUserName(String userName) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<User> cq = cb.createQuery(User.class);
        
        Root<User> root = cq.from(User.class);
        cq.select(root);
        cq.where(cb.equal(root.get("userName"),userName));
        User result = session.createQuery(cq).uniqueResult();
		
        return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getUsersByRole(java.lang.String)
	 */
	@Override
	public List<User> getUsersByRoleDescription( String roleDescription ) throws HibernateException {

		List<User> result = new ArrayList<User>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Role> cq = cb.createQuery(Role.class);
        
        Root<Role> root = cq.from(Role.class);
        cq.select(root);
        cq.where(cb.equal(root.get("description"),roleDescription));
        final Role m_role = session.createQuery(cq).uniqueResult();
		
		
		for( Userrole userrole : m_role.getUserroles() ) {
			
			final User m_user = userrole.getUserByUserId();
			
			if( ! result.contains(m_user)){
				
				result.add(m_user);
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#persistUser(com.bloomnet.bom.common.entity.User)
	 */
	@Override
	public String persistUser(User user) throws HibernateException {
		final String result = user.getUserName();
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#persistUserActivity(com.bloomnet.bom.common.entity.User, com.bloomnet.bom.common.entity.Uactivitytype)
	 */
	@Override
	public void persistUserActivity(User user, Uactivitytype activity) throws HibernateException {
		
		final Uactivitytype m_activity = getUserActivityType( activity.getDescription() );
		final User m_user = getUserById(user.getUserId());
		
		Useractivity m_user_activity = new Useractivity( m_user, m_activity );
		
		sessionFactory.getCurrentSession().saveOrUpdate(m_user_activity);
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.UserDAO#getUserActivity(java.lang.String)
	 */
	@Override
	public Uactivitytype getUserActivityType(String activityDescription) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Uactivitytype> cq = cb.createQuery(Uactivitytype.class);
        
        Root<Uactivitytype> root = cq.from(Uactivitytype.class);
        cq.select(root);
        cq.where(cb.equal(root.get("description"),activityDescription));
        final Uactivitytype activity = session.createQuery(cq).uniqueResult();
		
		return activity;
	}
	
	@Override
	public List<Useractivity> getLastUserActivity(User user, Uactivitytype uactivitytype) throws HibernateException {
			
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Useractivity> cq = cb.createQuery(Useractivity.class);
        
        Root<Useractivity> root = cq.from(Useractivity.class);
        cq.select(root);
        cq.where(cb.and(cb.equal(root.get("user"),user),cb.equal(root.get("uactivitytype"),uactivitytype)));
        cq.orderBy(cb.desc(root.get("createdDate")));
        List<Useractivity> activities = session.createQuery(cq).getResultList();

		return activities;
	}
	
	@Override
	public Date getCurrentTimeInOrder(User user) throws HibernateException{
		
		Date result = new Date();
		String stsOrderId="2";		
		final User m_user = user;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
        cq.where(cb.equal(root.get("user"),m_user));
        List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
		
		if ( ! m_orderActivities.isEmpty() ) {
			
			Collections.sort ( m_orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse( m_orderActivities );
			
			final Orderactivity m_oactivity = m_orderActivities.get(0);
			
			if( m_oactivity != null ) {
				
				final ActRouting m_actRouting = m_oactivity.getActRouting();
				final byte m_id = Byte.parseByte(stsOrderId);
				
				if ( m_actRouting != null && 
					 m_actRouting.getStsRouting()!= null && 
					 m_actRouting.getStsRouting().getStsRoutingId().equals(m_id) ) {
						
					 result = m_oactivity.getCreatedDate();
					 return result;
				}
			}
		}
		
		return result;
	}

	@Override
	public String persistUserrole(Userrole userrole) throws HibernateException {
		final String result = " ";
		sessionFactory.getCurrentSession().saveOrUpdate(userrole);
		return result;
	}

	@Override
	public Role getRoleById(Long roleId) throws HibernateException {
		return (Role) sessionFactory.getCurrentSession().get( Role.class, roleId );
	}
	
	@Override
	public List<Role> getUserroleByUser(User user) throws HibernateException {
		
		
		List<Role> roles = new ArrayList<Role>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Userrole> cq = cb.createQuery(Userrole.class);
        
        Root<Userrole> root = cq.from(Userrole.class);
        cq.select(root);
        cq.where(cb.equal(root.get("userByUserId"),user));
        List<Userrole> userroles = session.createQuery(cq).getResultList();
		
		for (Userrole userrole: userroles){
			HibernateHelper.initializeAndUnproxy(userrole);
			Role role = userrole.getRole();
			roles.add(role);
		}
		
		
		return roles;
		
	}

}
