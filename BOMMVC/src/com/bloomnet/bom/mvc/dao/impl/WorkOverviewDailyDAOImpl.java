package com.bloomnet.bom.mvc.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.WorkOverviewDailyDAO;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;

@Transactional
@Repository("workOverviewDailyDAO")
public class WorkOverviewDailyDAOImpl implements WorkOverviewDailyDAO {
	
	@Autowired private SessionFactory sessionFactory;

	@Override
	public WorkOverviewDaily getAutomatedWorkOverviewDaily() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.equal(root.get("sentViaId"),1));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();
		
		return workOverviewDaily.get(0);
	}
	
	@Override
	public WorkOverviewDaily getTLOWorkOverviewDaily() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.equal(root.get("sentViaId"),2));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();
		
		return workOverviewDaily.get(0);
	}

	@Override
	public List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDate(Date date)
			throws HibernateException {
		
		Date nextMidnight  = new DateTime(date).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.and(cb.between(root.get("dateSent"),date,nextMidnight),cb.equal(root.get("sentViaId"),1)));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();
		
		
		return workOverviewDaily;
		
	}
	
	
	
	@Override
	public List<WorkOverviewDaily> getTLOWorkOverviewDailyByDate(Date date)
	throws HibernateException {

		Date nextMidnight  = new DateTime(date).plusHours(24).minusSeconds(1).toDate();

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.and(cb.between(root.get("dateSent"),date,nextMidnight),cb.equal(root.get("sentViaId"),2)));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();
		

		return workOverviewDaily;

	}
	
	@Override
	public List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDateRange(Date date, Date toDate)
			throws HibernateException {
		
		Date nextMidnight  = new DateTime(toDate).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.and(cb.between(root.get("dateSent"),date,nextMidnight),cb.equal(root.get("sentViaId"),1)));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();
		
		
		return workOverviewDaily;
		
	}
	
	@Override
	public List<WorkOverviewDaily> getTLOWorkOverviewDailyByDateRange(Date date, Date toDate)
	throws HibernateException {

		Date nextMidnight  = new DateTime(toDate).plusHours(24).minusSeconds(1).toDate();

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WorkOverviewDaily> cq = cb.createQuery(WorkOverviewDaily.class);
        
        Root<WorkOverviewDaily> root = cq.from(WorkOverviewDaily.class);
        cq.select(root);
        cq.where(cb.and(cb.between(root.get("dateSent"),date,nextMidnight),cb.equal(root.get("sentViaId"),2)));
        List<WorkOverviewDaily> workOverviewDaily = session.createQuery(cq).getResultList();

		return workOverviewDaily;

	}

}
