package com.bloomnet.bom.mvc.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.PaymentDAO;
import com.bloomnet.bom.common.entity.ActPayment;
import com.bloomnet.bom.common.entity.PaymentInterface;
import com.bloomnet.bom.common.entity.StsPayment;

@Transactional
@Repository("paymentDAO")
public class PaymentDAOImpl implements PaymentDAO {

	
	@Autowired private SessionFactory sessionFactory;

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#persistPayment(com.bloomnet.bom.common.entity.ActPayment)
	 */
	@Override
	public void persistPayment(ActPayment payment) {
		sessionFactory.getCurrentSession().saveOrUpdate(payment);
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#persistPayment(com.bloomnet.bom.common.entity.PaymentInterface)
	 */
	@Override
	public void persistPayment( PaymentInterface actPayment ) throws HibernateException {

		final ActPayment entity = new ActPayment( actPayment.getStsPayment(), 
				                                  actPayment.getOrderactivity(), 
				                                  actPayment.getPaymenttype(), 
				                                  actPayment.getPaymentAmount(), 
				                                  actPayment.getPayTo() );
		
		persistPayment( entity );
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#getPayment(java.lang.String)
	 */
	@Override
	public ActPayment getPayment( String orderNumber ) {
		// TODO: Need more implementation details
		throw new HibernateException("Not implemented");
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#deletePayment(java.lang.String)
	 */
	@Override
	public void deletePayment( String orderNumber ) {
		// TODO: Need more implementation details
		throw new HibernateException("Not implemented");
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#getStatusById(byte)
	 */
	@Override
	public StsPayment getStatusById( Byte id ) throws HibernateException {
		return (StsPayment) sessionFactory.getCurrentSession().get(StsPayment.class, id);
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.PaymentDAO#getStatusById(java.lang.String)
	 */
	@Override
	public StsPayment getStatusById( String id ) throws HibernateException {
		final Byte m_id = Byte.valueOf(id);
		return getStatusById( m_id );
	}
}
