package com.bloomnet.bom.mvc.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.CallDispDAO;
import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.LogacallInterface;
import com.bloomnet.bom.common.entity.Shop;

/**
 * Implementation of a DAO for {@link com.bloomnet.bom.common.dao.CallDispDAO}
 * 
 * @author Danil Svirchtchev
 */
@Transactional
@Repository("callDispDAO")
public class CallDispDAOImpl implements CallDispDAO {
	
	
	@Autowired private SessionFactory sessionFactory;

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#getAllCallDisp()
	 */
	@Override
	public List<Calldisp> getAllCallDisp() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<Calldisp> cq = session.getCriteriaBuilder().createQuery(Calldisp.class);
        cq.from(Calldisp.class);
        
        List<Calldisp> disps = session.createQuery(cq).getResultList();
        
        
        
        return disps;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#persistCallDisp(com.bloomnet.bom.common.entity.Calldisp)
	 */
	@Override
	public void persistCallDisp( Calldisp callDisp ) throws HibernateException{
		sessionFactory.getCurrentSession().saveOrUpdate(callDisp);
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#deleteCallDisp(java.lang.String)
	 */
	@Override
	public void deleteCallDisp( String description ) throws HibernateException {
		final Calldisp cd = getCallDispByDesc(description);
		sessionFactory.getCurrentSession().delete( cd );
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#getCallDispByDesc(java.lang.String)
	 */
	@Override
	public Calldisp getCallDispByDesc( String description ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Calldisp> cq = cb.createQuery(Calldisp.class);
		Root<Calldisp> root = cq.from(Calldisp.class);
		cq.select(root);
		cq.where(cb.equal(root.get("description"), description));
        
		Calldisp callDisp = session.createQuery(cq).uniqueResult();
		
		
		
		return callDisp;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#getCallDispById(byte)
	 */
	@Override
	public Calldisp getCallDispById( byte id ) throws HibernateException {
		return (Calldisp) sessionFactory.getCurrentSession().get( Calldisp.class, id );
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#getCallDispById(java.lang.String)
	 */
	@Override
	public Calldisp getCallDispById( String id ) throws HibernateException {
		final byte m_id = Byte.parseByte(id);
		return getCallDispById( m_id );
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#persistCall(com.bloomnet.bom.common.entity.ActLogacall)
	 */
	@Override
	public void persistCall(ActLogacall call) throws HibernateException {
		sessionFactory.getCurrentSession().saveOrUpdate(call);
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#persistCall(com.bloomnet.bom.common.entity.LogacallInterface)
	 */
	@Override
	public ActLogacall persistCall( LogacallInterface call ) throws HibernateException {
		
		final ActLogacall entity = new ActLogacall( call.getOrderactivity(), 
													call.getCalldisp(), 
													call.getLogAcallText(), 
													call.getShopCalled() );
		
		persistCall( entity );
		
		return entity;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.CallDispDAO#getCallsByShop(com.bloomnet.bom.common.entity.Shop)
	 */
	@Override
	public List<ActLogacall> getCallsByShop( Shop shop ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<ActLogacall> cq = cb.createQuery(ActLogacall.class);
		Root<ActLogacall> root = cq.from(ActLogacall.class);
		cq.select(root);
		cq.where(cb.equal(root.get("shopCalled"), shop));
        
		List<ActLogacall> logACall = session.createQuery(cq).getResultList();
		
		
		
		return logACall;
	}
}
