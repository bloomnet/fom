package com.bloomnet.bom.mvc.validators;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

public class ScrubData {
	
	public String scrubData(String data){
		String pass1 = removeHTMLThreats(data);
		String pass2 = removeSQL(pass1);
		return pass2;
	}
	
	protected String removeHTMLThreats(String data){
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(data);
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == '<') {
	    	  result.append("%3C");
	      }
	      else if (character == '>') {
	    	  result.append("%3E");
	      }
	      else {
	        //the char is not a one that would
	    	//cause malicious HTML/js scripting
	        result.append(character);
	      }
	      character = iterator.next();
	    }
		
	    return result.toString();
	}
	
	protected String removeSQL(String data){
		
		List<String> badStrings = new ArrayList<String>();
		badStrings.add("insert into");
		badStrings.add("delete from");
		badStrings.add("bloomnetordermanagement");
		badStrings.add("drop database");
		badStrings.add("drop table");
		badStrings.add("alter table");
		badStrings.add("alter database");
		
		String tempData = data;
		
		tempData = tempData.replaceAll("\n", " ");
		tempData = tempData.replaceAll("\r", " ");
		tempData = removeExtraWhiteSpaces(tempData);
		
		boolean badDataFound = false;
		
		for(int ii=0; ii<badStrings.size(); ++ii){
			int index = tempData.toLowerCase().indexOf(badStrings.get(ii));
			if(index != -1){
				tempData = tempData.substring(0,index) + tempData.substring(index + badStrings.get(ii).length(), tempData.length());
				badDataFound = true;
			}else{
				badDataFound = false;
			}
			while(badDataFound){
				index = tempData.toLowerCase().indexOf(badStrings.get(ii));
				if(index != -1){
					tempData = tempData.substring(0,index) + tempData.substring(index + badStrings.get(ii).length(), tempData.length());
					badDataFound = true;
				}else{
					badDataFound = false;
				}
			}
		}
		
		return tempData;
	}
	
	public String allowNumbersOnly(String data){
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(data);
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == '0')
	    	  result.append(character);
	      else if (character == '1')
	    	  result.append(character);
	      else if (character == '2')
	    	  result.append(character);
	      else if (character == '3')
	    	  result.append(character);
	      else if (character == '4')
	    	  result.append(character);
	      else if (character == '5')
	    	  result.append(character);
	      else if (character == '6')
	    	  result.append(character);
	      else if (character == '7')
	    	  result.append(character);
	      else if (character == '8')
	    	  result.append(character);
	      else if (character == '9')
	    	  result.append(character);
	    	 
	      character = iterator.next();
	    }
		
	    return result.toString();
	}
	
	protected String removeExtraWhiteSpaces(String data){
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(data);
	    char character =  iterator.current();
	    char previousChar = '`';
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == ' ' && previousChar == ' ') {
	      }
	      else {
	        //not extra white space
	        result.append(character);
	      }
	      previousChar = character;
	      character = iterator.next();
	    }
		
	    return result.toString();
	}
}
