package com.bloomnet.bom.mvc.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;

/**
 * Validator for Shop Order
 * 
 * @author Danil Svirchtchev
 */
public class ShopValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(ShopValidator.class);

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppShop.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm(Object target, Errors errors) {
		
		String shopName;
		String shopAddress;
		String phone;
		String contact;
		String city;
		String zip;
		
		try{
			WebAppShop shop = (WebAppShop) target;
			
			shopName    = shop.getShopName();
			shopAddress = shop.getShopAddress1();
			phone       = shop.getShopPhone();
			contact     = shop.getShopContact();
			city        = shop.getCity().getName();
			zip         = shop.getZip().getZipCode();

		}catch(Exception ee){
			Shop shop = (Shop) target;
			
			shopName    = shop.getShopName();
			shopAddress = shop.getShopAddress1();
			phone       = shop.getShopPhone();
			contact     = shop.getShopContact();
			city        = shop.getCity().getName();
			zip         = shop.getZip().getZipCode();
		}

		if ((shopName == null) || shopName.equals("")) {
			errors.rejectValue("shopName", "error.shop.noname");
		}
		
		if ((shopAddress == null) || shopAddress.equals("")) {
			errors.rejectValue("shopAddress1", "error.shop.noaddress");
		}
		
		if ((phone == null) || phone.equals("")) {
			errors.rejectValue("shopPhone", "error.shop.nophone");
		}
		
		if ((contact == null) || contact.equals("")) {
			errors.rejectValue("shopContact", "error.shop.nocontact");
		}
		
		if ((city == null) || city.equals("")) {
			errors.rejectValue("city.name", "error.shop.city.noname");
		}
		
		if ((zip == null) || zip.equals("")) {
			errors.rejectValue("zip.zipCode", "error.shop.zip.noname");
		}
	}
}
