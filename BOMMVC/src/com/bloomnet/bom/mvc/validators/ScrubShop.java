package com.bloomnet.bom.mvc.validators;

import com.bloomnet.bom.mvc.businessobjects.WebAppShop;

public class ScrubShop extends ScrubData {
	
	public WebAppShop scrubShop(WebAppShop shop){
		
		final String shopName    = shop.getShopName();
		final String shopAddress = shop.getShopAddress1();
		final String phone       = shop.getShopPhone();
		final String contact     = shop.getShopContact();
		final String city        = shop.getCity().getName();
		final String zip         = shop.getZip().getZipCode();
		final String email       = shop.getShopEmail();
		final String fax         = shop.getShopFax();
		final String bmtShopCode = shop.getBmtShopCode();
		final String tfShopCode  = shop.getTfShopCode();
		
		String scrubbedName = "";
		String scrubbedAddress = "";
		String scrubbedPhone = "";
		String scrubbedContact = "";
		String scrubbedCity = "";
		String scrubbedZip = "";
		String scrubbedEmail = "";
		String scrubbedFax = "";
		String scrubbedBmtShopCode = "";
		String scrubbedTfShopCode = "";
		
		if(shopName != null) scrubbedName = scrubData(shopName);
		if(shopAddress != null) scrubbedAddress = scrubData(shopAddress);
		if(phone != null) scrubbedPhone = allowNumbersOnly(phone);
		if(contact != null) scrubbedContact = scrubData(contact);
		if(city != null) scrubbedCity = scrubData(city);
		if(zip != null) scrubbedZip = scrubData(zip);
		if(email != null) scrubbedEmail = scrubData(email);
		if(fax != null) scrubbedFax = allowNumbersOnly(fax);
		if(bmtShopCode != null) scrubbedBmtShopCode = scrubData(bmtShopCode);
		if(tfShopCode != null) scrubbedTfShopCode = scrubData(tfShopCode);
		
		shop.setShopName(scrubbedName);
		shop.setShopAddress1(scrubbedAddress);
		shop.setShopPhone(scrubbedPhone);
		shop.setShopContact(scrubbedContact);
		shop.getCity().setName(scrubbedCity);
		shop.getZip().setZipCode(scrubbedZip);
		shop.setShopFax(scrubbedFax);
		shop.setShopEmail(scrubbedEmail);
		shop.setBmtShopCode(scrubbedBmtShopCode);
		shop.setTfShopCode(scrubbedTfShopCode);
		
		return shop;
	}

}
