/**
 * 
 */
package com.bloomnet.bom.mvc.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

/**
 * Validator for order notes
 * 
 * @author Danil Svirchtchev
 *
 */
public class OrderNotesValidator extends AbstractValidator {
	
	// Define a static logger variable
	static Logger logger = Logger.getLogger(ActLogacallValidator.class);

	/* (non-Javadoc)
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppOrder.class);
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.validators.AbstractValidator#validateForm(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	protected void validateForm( Object target, Errors errors) throws Exception {
		
		WebAppOrder order = (WebAppOrder) target;
		
		final String text = order.getLogAcallText();

		if ((text == null) || text.equals("")) {
			errors.rejectValue("logAcallText", "error.logacall.notext");
		}
	}
}
