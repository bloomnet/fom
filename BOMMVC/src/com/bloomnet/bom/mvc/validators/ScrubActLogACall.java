package com.bloomnet.bom.mvc.validators;

import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.mvc.businessobjects.WebAppActLogacall;

public class ScrubActLogACall extends ScrubData{
	
	public WebAppActLogacall scrubActLogaCall(WebAppActLogacall logacall){
		
		final String reason = logacall.getCallDispDescription();
		final String text = logacall.getLogAcallText();
		
		String scrubbedReason = "";
		String scrubbedText = "";
		
		if(reason != null) scrubbedReason = scrubData(reason);
		if(text != null) scrubbedText = scrubData(text);
		
		logacall.setCallDispDescription(scrubbedReason);
		logacall.setLogAcallText(scrubbedText);
		
		return logacall;
	}
}
