package com.bloomnet.bom.mvc.validators;

import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

public class ScrubActPayment extends ScrubData{
	
	public WebAppOrder scrubActPayment(WebAppOrder order){
		
		final String payTo       = order.getPayTo();
		final String payAmmount  = order.getPaymentAmountStr();
		final String paymentType = order.getPaymentTypeStr();
		
		String scrubbedPayTo = "";
		String scrubbedPayAmount = "";
		String scrubbedPaymentType = "";
		
		if(payTo != null) scrubbedPayTo = scrubData(payTo);
		if(payAmmount != null) scrubbedPayAmount = scrubData(payAmmount);
		if(paymentType != null) scrubbedPaymentType = scrubData(paymentType);
		
		order.setPayTo(scrubbedPayTo);
		order.setPaymentAmountStr(scrubbedPayAmount);
		order.setPaymentTypeStr(scrubbedPaymentType);
		
		return order;
	}
}
