package com.bloomnet.bom.mvc.validators;

import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;

public class ScrubWebAppActMessage extends ScrubData{

	public WebAppActMessage scrubWebAppActMessage(WebAppActMessage message){
		
		final String text = message.getMessageText();
		final String signature = message.getSignature();
		
		String scrubbedText = "";
		String scrubbedSignature = "";
		
		if(text != null) scrubbedText = scrubData(text);
		if(signature != null) scrubbedSignature = scrubData(signature);
		
		message.setMessageText(scrubbedText);
		message.setSignature(scrubbedSignature);
		
		return message;
		
	}
}
