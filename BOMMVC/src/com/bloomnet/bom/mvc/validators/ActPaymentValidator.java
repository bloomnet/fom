package com.bloomnet.bom.mvc.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

/**
 * Validator for Master Order
 * 
 * @author Danil Svirchtchev
 */
public class ActPaymentValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(ActPaymentValidator.class);

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppOrder.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm(Object target, Errors errors) {

		WebAppOrder order = (WebAppOrder) target;

		final String payTo       = order.getPayTo();
		final String payAmmount  = order.getPaymentAmountStr();
		final String paymentType = order.getPaymentTypeStr();
		final String shopnetwork = order.getSelectedShopnetwork();
		
		String networkName = "";
		
    	if(paymentType.equals("3")){
    		
    		for ( Shopnetwork selectedNetwork : order.getContactFlorist().getShopnetworks() ) {
    			
	    		final String id = String.valueOf( selectedNetwork.getShopNetworkId().longValue() );
	    		
	    		if ( order.getSelectedShopnetwork().equals( id ) ) {
	    			
	    			networkName = String.valueOf( selectedNetwork.getNetwork().getName());
	    		}
    		}
    	}

		if ((paymentType == null) || paymentType.equals("")) {
			errors.rejectValue("paymentTypeStr", "error.paymenttype.selection");
		}
		else if ( paymentType.equals("1") && ((payTo == null) || payTo.equals("")) ) {
			errors.rejectValue("payTo", "error.paymenttype.payto");
		}

		else if ( paymentType.equals("1") && ((payAmmount == null) || payAmmount.equals("")) ) {
			errors.rejectValue("paymentAmountStr", "error.paymenttype.amount");
		}
		
		else if ( paymentType.equals("3") && ((shopnetwork == null) || shopnetwork.equals("")) ) {
			errors.rejectValue("selectedShopnetwork", "error.shopnetwork.selection");
		}
		
		else if ( paymentType.equals("3") && (networkName.equalsIgnoreCase("FTD") || networkName.equalsIgnoreCase("OTHER")) ) {
			errors.rejectValue("selectedShopnetwork", "error.shopnetwork.notallowed");
		}
		
		else {
			
			try {
				
				order.setPrice( Double.parseDouble( payAmmount ) );
			}
			catch(NumberFormatException e) {
				
				errors.rejectValue("paymentAmountStr", "error.paymenttype.amount");
			}
		}
	}
}
