package com.bloomnet.bom.mvc.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

/**
 * Validator for Master Order
 * 
 * @author Danil Svirchtchev
 */
public class ComMethodValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(ComMethodValidator.class);

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppOrder.class);
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.validators.AbstractValidator#validateForm(java.lang.Object, org.springframework.validation.Errors)
	 */
	public void validateForm(Object target, Errors errors) {

		WebAppOrder order = (WebAppOrder) target;

		final String comMethod = order.getMethodToSend().getDescription();
		final String faxNumber = order.getFaxConfirmation();

		if ((comMethod == null) || comMethod.equals("")) {
			errors.rejectValue("methodToSend.description", "error.sendmethod.none");
		}
		else if ( comMethod.indexOf("na") != -1 ) {
			
			errors.rejectValue("methodToSend.description", "error.sendmethod.notsupported");
		}
		else if ( comMethod.equalsIgnoreCase("fax") && (faxNumber == null || faxNumber.equals(""))) {
			
			errors.rejectValue("methodToSend.description", "error.sendmethod.fax.none");
		}
	}
}
