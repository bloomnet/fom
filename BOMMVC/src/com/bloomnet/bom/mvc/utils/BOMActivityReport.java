package com.bloomnet.bom.mvc.utils;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 


public class BOMActivityReport {
	
	SQLData mySQL = new SQLData();

    public File getBOMActivityReport(){
        mySQL.login();
        String statement = "USE bloomnetordermanagement;";
        mySQL.executeStatement(statement);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date());
        
        Calendar c = Calendar.getInstance(); 
        c.setTime(new Date()); 
        c.add(Calendar.DATE, 1);
        String tomorrow = sdf.format(c.getTime());
        
        statement = "SET @start := \""+today+"\", @end := \""+tomorrow+"\"";
        mySQL.executeStatement(statement);
        
        System.out.println("INITIALIZED AND DATABASE SELECTED");
        String query = "SELECT FirstName, LastName, IFNULL((SELECT SUM(Count) FROM "
        		+ "(SELECT FirstName, LastName, Description, Route, COUNT(Description) AS Count FROM "
        		+ "(SELECT user.FirstName,user.LastName,ifnull(sr.description,\"Order Notes\") AS Description"
        		+ ", bv.Route AS Route FROM orderactivity oa LEFT JOIN act_routing al ON oa.OrderActivity_ID ="
        		+ " al.OrderActivity_ID LEFT JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN "
        		+ "bomorderview_v2 bv ON bo.ParentOrder_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID = use"
        		+ "r.User_ID LEFT JOIN sts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_l"
        		+ "ogacall alc ON oa.OrderActivity_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2,3) A"
        		+ "ND oa.CreatedDate >= @start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_I"
        		+ "D AND (al.Sts_Routing_ID != 1 OR alc.CallDisposition_ID = 10) GROUP BY oa.BOMOrder_ID,sr.de"
        		+ "scription, LastName,FirstName, Route) AS temp GROUP BY Description,LastName,FirstName, Rout"
        		+ "e ORDER BY LastName,FirstName) as temp2  WHERE temp2.Route = \"BMTFSI\" AND temp3.LastName "
        		+ "= temp2.LastName AND temp3.FirstName = temp2.FirstName AND temp2.Description = \"Sent to Sh"
        		+ "op\"), 0) as FSIWired,   IFNULL((SELECT SUM(Count) FROM  (SELECT FirstName, LastName, Descr"
        		+ "iption, Route, COUNT(Description) AS Count FROM ( SELECT user.FirstName,user.LastName,ifnul"
        		+ "l(sr.description,\"Order Notes\") AS Description, bv.Route AS Route FROM orderactivity oa L"
        		+ "EFT JOIN act_routing al ON oa.OrderActivity_ID = al.OrderActivity_ID LEFT JOIN bomorder bo "
        		+ "ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview_v2 bv ON bo.ParentOrder_ID = b"
        		+ "v.ParentBOMid LEFT JOIN user ON oa.User_ID = user.User_ID LEFT JOIN sts_routing sr ON al.St"
        		+ "s_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc ON oa.OrderActivity_ID = alc.Or"
        		+ "derActivity_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.CreatedDate >= @start AND oa.CreatedD"
        		+ "ate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.Sts_Routing_ID != 1 OR alc.CallDi"
        		+ "sposition_ID = 10) GROUP BY oa.BOMOrder_ID,sr.description, LastName,FirstName, Route) AS te"
        		+ "mp GROUP BY Description,LastName,FirstName, Route ORDER BY LastName,FirstName) as temp2  WH"
        		+ "ERE temp2.Route = \"TFSI\" AND temp3.LastName = temp2.LastName AND temp3.FirstName = temp2."
        		+ "FirstName AND temp2.Description = \"Sent to Shop\"), 0) as TFSIWired,    IFNULL((SELECT SUM"
        		+ "(Count) FROM  (SELECT FirstName, LastName, Description, Route, COUNT(Description) AS Count "
        		+ "FROM ( SELECT user.FirstName,user.LastName,ifnull(sr.description,\"Order Notes\") AS Descri"
        		+ "ption, bv.Route AS Route FROM orderactivity oa LEFT JOIN act_routing al ON oa.OrderActivity"
        		+ "_ID = al.OrderActivity_ID LEFT JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT "
        		+ "JOIN bomorderview_v2 bv ON bo.ParentOrder_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID "
        		+ "= user.User_ID LEFT JOIN sts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN "
        		+ "act_logacall alc ON oa.OrderActivity_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2"
        		+ ",3) AND oa.CreatedDate >= @start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOr"
        		+ "der_ID AND (al.Sts_Routing_ID != 1 OR alc.CallDisposition_ID = 10) GROUP BY oa.BOMOrder_ID,"
        		+ "sr.description, LastName,FirstName, Route) AS temp GROUP BY Description,LastName,FirstName,"
        		+ " Route ORDER BY LastName,FirstName) as temp2  WHERE temp2.Route IN (\"TLO\",\"FAXoutbound\""
        		+ ") AND temp3.LastName = temp2.LastName AND temp3.FirstName = temp2.FirstName AND temp2.Descr"
        		+ "iption = \"Sent to Shop\"), 0) as SentTLO,     IFNULL((SELECT SUM(Count) FROM  (SELECT Firs"
        		+ "tName, LastName, Description, Route, COUNT(Description) AS Count FROM ( SELECT user.FirstNa"
        		+ "me,user.LastName,ifnull(sr.description,\"Order Notes\") AS Description, bv.Route AS Route F"
        		+ "ROM orderactivity oa LEFT JOIN act_routing al ON oa.OrderActivity_ID = al.OrderActivity_ID "
        		+ "LEFT JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview_v2 bv ON"
        		+ " bo.ParentOrder_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID = user.User_ID LEFT JOIN s"
        		+ "ts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc ON oa.Ord"
        		+ "erActivity_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.CreatedDate >= "
        		+ "@start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.Sts_Routing"
        		+ "_ID != 1 OR alc.CallDisposition_ID = 10) GROUP BY oa.BOMOrder_ID,sr.description, LastName,F"
        		+ "irstName, Route) AS temp GROUP BY Description,LastName,FirstName, Route ORDER BY LastName,F"
        		+ "irstName) as temp2  WHERE temp2.Route = \"TLO\" AND temp3.LastName = temp2.LastName AND tem"
        		+ "p3.FirstName = temp2.FirstName AND temp2.Description = \"Rejected by Bloomnet\"), 0) as Age"
        		+ "ntRejected,    IFNULL((SELECT SUM(Count) FROM  (SELECT FirstName, LastName, Description, Ro"
        		+ "ute, COUNT(Description) AS Count FROM ( SELECT user.FirstName,user.LastName,ifnull(sr.descr"
        		+ "iption,\"Order Notes\") AS Description, bv.Route AS Route FROM orderactivity oa LEFT JOIN a"
        		+ "ct_routing al ON oa.OrderActivity_ID = al.OrderActivity_ID LEFT JOIN bomorder bo ON oa.BOMO"
        		+ "rder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview_v2 bv ON bo.ParentOrder_ID = bv.ParentBO"
        		+ "Mid LEFT JOIN user ON oa.User_ID = user.User_ID LEFT JOIN sts_routing sr ON al.Sts_Routing_"
        		+ "ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc ON oa.OrderActivity_ID = alc.OrderActivit"
        		+ "y_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.CreatedDate >= @start AND oa.CreatedDate < @end"
        		+ " AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.Sts_Routing_ID != 1 OR alc.CallDisposition_"
        		+ "ID = 10) GROUP BY oa.BOMOrder_ID,sr.description, LastName,FirstName, Route) AS temp GROUP B"
        		+ "Y Description,LastName,FirstName, Route ORDER BY LastName,FirstName) as temp2  WHERE temp2."
        		+ "Route = \"TLO\" AND temp3.LastName = temp2.LastName AND temp3.FirstName = temp2.FirstName A"
        		+ "ND temp2.Description = \"Cancel Confirmed by Bloomnet\"), 0) as AgentCancelled,    IFNULL(("
        		+ "SELECT SUM(Count) FROM  (SELECT user.FirstName,user.LastName,IFNULL(sr. description,\"Order"
        		+ " Notes\")AS description,COUNT(IFNULL(sr. description,\"Order Notes\")) AS Count FROM ordera"
        		+ "ctivity oa LEFT JOIN act_routing al ON oa.OrderActivity_ID = al.OrderActivity_ID LEFT JOIN "
        		+ "bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview_v2 bv ON bo.Parent"
        		+ "Order_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID = user.User_ID LEFT JOIN sts_routing"
        		+ " sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc ON oa.OrderActivity"
        		+ "_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.CreatedDate >= @start AND"
        		+ " oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.Sts_Routing_ID != 1 O"
        		+ "R alc.CallDisposition_ID = 10) GROUP BY user.LastName,FirstName,sr.description ORDER BY Las"
        		+ "tName,FirstName) as temp2  WHERE  temp3.LastName = temp2.LastName AND temp3.FirstName = tem"
        		+ "p2.FirstName AND temp2.Description = \"Order Notes\"), 0) as AgentNotesCreated,   IFNULL((S"
        		+ "ELECT SUM(Count) FROM  (SELECT user.LastName, user.FirstName, COUNT(orderactivity.OrderActi"
        		+ "vity_ID) AS Count FROM act_message   LEFT JOIN orderactivity ON act_message.OrderActivity_I"
        		+ "D = orderactivity.OrderActivity_ID  LEFT JOIN user ON orderactivity.User_ID = user.User_ID "
        		+ " WHERE orderactivity.CreatedDate >=@start AND orderactivity.CreatedDate < @end  GROUP BY us"
        		+ "er.LastName, user.FirstName) AS temp2  WHERE temp3.LastName = temp2.LastName AND temp3.Firs"
        		+ "tName = temp2.FirstName),  0) as AgentMessagesSent,     IFNULL((SELECT SUM(Count) FROM  (SE"
        		+ "LECT FirstName, LastName, Description, Route, COUNT(Description) AS Count FROM ( SELECT use"
        		+ "r.FirstName,user.LastName,ifnull(sr.description,\"Order Notes\") AS Description, bv.Route A"
        		+ "S Route FROM orderactivity oa LEFT JOIN act_routing al ON oa.OrderActivity_ID = al.OrderAct"
        		+ "ivity_ID LEFT JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview"
        		+ "_v2 bv ON bo.ParentOrder_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID = user.User_ID LE"
        		+ "FT JOIN sts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc "
        		+ "ON oa.OrderActivity_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.Create"
        		+ "dDate >= @start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.St"
        		+ "s_Routing_ID != 1 OR alc.CallDisposition_ID = 10) GROUP BY oa.BOMOrder_ID,sr.description, L"
        		+ "astName,FirstName, Route) AS temp GROUP BY Description,LastName,FirstName, Route ORDER BY L"
        		+ "astName,FirstName) as temp2  WHERE temp3.LastName = temp2.LastName AND temp3.FirstName = te"
        		+ "mp2.FirstName AND temp2.Description = \"Actively being Worked\"), 0) as Touched    FROM (SE"
        		+ "LECT FirstName, LastName, Description, Route, COUNT(Description) AS Count FROM ( SELECT use"
        		+ "r.FirstName,user.LastName,ifnull(sr.description,\"Order Notes\") AS Description, bv.Route A"
        		+ "S Route FROM orderactivity oa LEFT JOIN act_routing al ON oa.OrderActivity_ID = al.OrderAct"
        		+ "ivity_ID LEFT JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID LEFT JOIN bomorderview"
        		+ "_v2 bv ON bo.ParentOrder_ID = bv.ParentBOMid LEFT JOIN user ON oa.User_ID = user.User_ID LE"
        		+ "FT JOIN sts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID LEFT JOIN act_logacall alc "
        		+ "ON oa.OrderActivity_ID = alc.OrderActivity_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.Create"
        		+ "dDate >= @start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND (al.St"
        		+ "s_Routing_ID != 1 OR alc.CallDisposition_ID = 10) GROUP BY oa.BOMOrder_ID,sr.description, L"
        		+ "astName,FirstName, Route) AS temp GROUP BY Description,LastName,FirstName, Route ORDER BY L"
        		+ "astName,FirstName) as temp3 GROUP BY LastName, FirstName;";
        
        File file = new File("/var/lib/tomcat/webapps/BOMMVC/images/ActivitySummary.xlsx");        
        String filePath = file.getPath();
        
        createWorkbook("XLSX written for Activity Summary", filePath, query, "ActivitySummary.xlsx","FOM Activity Summary Report","Activity Report Attached");
        return file;
    }
    
    private void createWorkbook(String verbiage, String filePath, String query, String fileName, String subject, String body){
    	try
        {
    		ResultSet queryResults = mySQL.executeQuery(query);
            XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("Sheet1");
            
            XSSFRow row = sheet.createRow(0);
            
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("First Name");
            cell = row.createCell(1);
            cell.setCellValue("Last Name");
            cell = row.createCell(2);
            cell.setCellValue("Unique Orders Wired FSI");
            cell = row.createCell(3);
            cell.setCellValue("Unique Orders Wired TFSI");
            cell = row.createCell(4);
            cell.setCellValue("Unique Orders Sent TLO");
            cell = row.createCell(5);
            cell.setCellValue("Unique Orders Rejected");
            cell = row.createCell(6);
            cell.setCellValue("Unique Orders Cancelled");
            cell = row.createCell(7);
            cell.setCellValue("Total Notes Created");
            cell = row.createCell(8);
            cell.setCellValue("Total Messages Sent");
            cell = row.createCell(9);
            cell.setCellValue("Unique Orders Touched");

    		int ii = 1;
    				
            while(queryResults.next())
            {
            	row = sheet.createRow(ii);
            	
                String firstName = queryResults.getString("FirstName");
                String lastName = queryResults.getString("LastName");
                String fsiWired = queryResults.getString("FSIWired");
                String tfsiWired = queryResults.getString("TFSIWired");
                String sentTLO = queryResults.getString("sentTLO");
                String agentRejected = queryResults.getString("AgentRejected");
                String agentCancelled = queryResults.getString("AgentCancelled");
                String notes = queryResults.getString("AgentNotesCreated");
                String messages = queryResults.getString("AgentMessagesSent");
                String touched = queryResults.getString("Touched");
                
                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(firstName).toString());
                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(lastName).toString());
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(fsiWired).toString());
                cell = row.createCell(3);
                cell.setCellValue(String.valueOf(tfsiWired).toString());
                cell = row.createCell(4);
                cell.setCellValue(sentTLO);
                cell = row.createCell(5);
                cell.setCellValue(agentRejected);
                cell = row.createCell(6);
                cell.setCellValue(String.valueOf(agentCancelled).toString());
                cell = row.createCell(7);
                cell.setCellValue(String.valueOf(notes).toString());
                cell = row.createCell(8);
                cell.setCellValue(String.valueOf(messages).toString());
                cell = row.createCell(9);
                cell.setCellValue(String.valueOf(touched).toString());
               
                ii++;
            }
            
            FileOutputStream fileOut = new FileOutputStream(filePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
            System.out.println(verbiage);
        }
        catch(IOException e){
            e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
