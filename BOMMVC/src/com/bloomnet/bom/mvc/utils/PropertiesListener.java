package com.bloomnet.bom.mvc.utils;

import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.DefaultFileMonitor;
import org.springframework.beans.factory.annotation.Autowired;

public class PropertiesListener implements FileListener {
	
	@Autowired private RefreshProperties refreshProperties;
	
	public PropertiesListener(){
		
		 FileSystemManager fsManager;
		 FileObject listendir = null;
		try {
			fsManager = VFS.getManager();
			listendir = fsManager.resolveFile("/var/bloomnet/cfg/");
		} catch (FileSystemException e1) {
			try {
				fsManager = VFS.getManager();
				listendir = fsManager.resolveFile("c:/var/bloomnet/cfg/");
			} catch (FileSystemException e) {
				e.printStackTrace();
			}
		}

		 DefaultFileMonitor fm = new DefaultFileMonitor(this);
		 fm.setRecursive(true);
		 fm.addFile(listendir);
		 fm.start();

	}

	@Override
	public void fileChanged(FileChangeEvent arg0) throws Exception {
		refreshProperties.refreshProperties();
		
	}

	@Override
	public void fileCreated(FileChangeEvent arg0) throws Exception {
		//Do Nothing
		
	}

	@Override
	public void fileDeleted(FileChangeEvent arg0) throws Exception {
		//Do Nothing
		
	}
	
}
