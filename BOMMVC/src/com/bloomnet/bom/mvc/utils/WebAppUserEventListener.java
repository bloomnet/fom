package com.bloomnet.bom.mvc.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class WebAppUserEventListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
		// DO NOTHING
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

	    List<?> l = Collections.synchronizedList( new ArrayList<Object>() );

	    //add to ServletContext
	    event.getServletContext().setAttribute( "logins", l );
	}
}
