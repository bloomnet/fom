/**
 * 
 */
package com.bloomnet.bom.mvc.exceptions;

/**
 * @author Danil svirchtchev
 *
 */
public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public UserNotFoundException(String userName) {
		super("Invalid user name or password.  Please try again");
	}
}
