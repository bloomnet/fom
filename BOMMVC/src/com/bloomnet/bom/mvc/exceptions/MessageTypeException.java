/**
 * 
 */
package com.bloomnet.bom.mvc.exceptions;

/**
 * @author Danil svirchtchev
 *
 */
public class MessageTypeException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public MessageTypeException( String messageType ) {
		super( "Message type ["+messageType+"] is not supported" );
	}

	/**
	 * @param message
	 * @param cause
	 */
	public MessageTypeException( String message, Throwable cause ) {
		super(message, cause);
	}
}
