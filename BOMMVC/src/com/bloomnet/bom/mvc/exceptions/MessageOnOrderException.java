package com.bloomnet.bom.mvc.exceptions;

import java.util.List;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.ActLogacall;

/**
 * @author Danil svirchtchev
 *
 */
public class MessageOnOrderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public MessageOnOrderException() {
		// TODO Auto-generated constructor stub
	}

	public MessageOnOrderException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MessageOnOrderException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MessageOnOrderException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	
	public MessageOnOrderException( MessageOnOrderBean orderBean, List<ActLogacall> dispositions ) {
		super( formatMessage(orderBean, dispositions) );
	}
	
	
	private static String formatMessage( MessageOnOrderBean orderBean, List<ActLogacall> dispositions ) {
		
		StringBuffer msg = new StringBuffer();
		
		msg.append( "Order ["+orderBean.getOrderNumber()+"] can't be loaded:\n" );
		
		for( ActLogacall disp : dispositions ) {
			
			msg.append( disp.getLogAcallText()+" ["+disp.getOrderactivity().getCreatedDate()+"]\n" );
		}
		
		return msg.toString();
	}
}
