package com.bloomnet.bom.mvc.exceptions;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;

public class RecommendedFloristsException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * @param e
	 */
	public RecommendedFloristsException( Throwable e ) {
		super("Could not load list of recommended florists: ", e);
	}
}
