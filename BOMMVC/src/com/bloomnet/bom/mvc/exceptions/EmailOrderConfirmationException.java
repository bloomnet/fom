package com.bloomnet.bom.mvc.exceptions;

import org.springframework.mail.MailException;

import com.bloomnet.bom.common.entity.Shop;

/**
 * @author Danil svirchtchev
 *
 */
public class EmailOrderConfirmationException extends MailException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public EmailOrderConfirmationException( String msg ) {
		super( msg );
	}
	
	
	public EmailOrderConfirmationException( Shop shop ) {		
		this( formatMessage( shop.getShopEmail(), shop.getShopName() ) );
	}
	
	
	public EmailOrderConfirmationException( String msg, Throwable cause ) {
		super( msg, cause );
	}

	
	public EmailOrderConfirmationException( String toEmail, String shopName  ) {
		this( formatMessage( toEmail, shopName ) );
	}
	
	
	private static String formatMessage( String toEmail, String shopName ) {
		
		StringBuffer msg = new StringBuffer();
		
		msg.append( "Unable to send email to shop: " + shopName + ", email address: " + toEmail );
		
		return msg.toString();
	}
}
