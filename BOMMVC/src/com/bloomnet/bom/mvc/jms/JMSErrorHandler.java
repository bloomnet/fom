package com.bloomnet.bom.mvc.jms;

import org.apache.log4j.Logger;
import org.springframework.util.ErrorHandler;

public class JMSErrorHandler implements ErrorHandler {
	
	static final Logger logger = Logger.getLogger(JMSErrorHandler.class);


	@Override
	public void handleError(Throwable arg0) {
		logger.error("***************JMSErrorHandler***************\n"+ "cause " + arg0.getCause() + "\nmessage " + arg0.getMessage() + "\nstack trace " +arg0.getStackTrace() );				
	}

}
