package com.bloomnet.bom.mvc.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.mvc.service.FaxService;

public class FaxMessageListener implements SessionAwareMessageListener<Message> {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger(FaxMessageListener.class);

	// Injected dependency
	private FaxService faxServiceImpl;
	
	


	ShopDAO shopDao;


	public void setShopDao(ShopDAO shopDao) {
		this.shopDao = shopDao;
	}

	@Override
	public void onMessage(Message message, Session session) throws JMSException {

		String order = "";

		TextMessage msg = (TextMessage) message;
		try {
			if (logger.isDebugEnabled()) {

				logger.debug("***************FaxMessageListener onMessage***************");
				logger.debug("received: " + msg.getText());
			}
			order = msg.getText();
			String orderId = msg.getStringProperty("ORDERNUMBER");
			
			String recievingshop = "";
			recievingshop = msg.getStringProperty("RECEIVINGSHOP");
			
			String fax = "";
			fax = msg.getStringProperty("CONTACTINFO");
			
			
			
			
			faxServiceImpl.SendFax(order, recievingshop, fax);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JMSException(e.getMessage());
		}
	}

	public void setFaxService(FaxService faxServiceImpl) {
		this.faxServiceImpl = faxServiceImpl;
	}
	


	
}
