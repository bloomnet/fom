package com.bloomnet.bom.mvc.jms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.JmsUtils;
import org.springframework.stereotype.Service;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.service.impl.AgentServiceImpl;

@Service("tloConsumer")
public class TLOConsumer {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( AgentServiceImpl.class );

    // Injected dependency
	private JmsTemplate bmttelConsumer;
	

	
	private String messageXml;
		

	
	public List<String> consumeMessage(  ) throws Exception {

		TextMessage receivedMessage = null;
		
		List<String> timezones = new ArrayList<String>();
		List<String> ocassions = new ArrayList<String>();
		
		
		/*timezones.add("12");
		timezones.add("11");
		timezones.add("10");
		timezones.add("9");
		timezones.add("-3.5");
		timezones.add("-4");*/
		timezones.add("-5");
		timezones.add("-6");
		timezones.add("-7");
		timezones.add("-8");
		timezones.add("-9");
		timezones.add("-10");
		//timezones.add("-11");
		
		ocassions.add("1");
		ocassions.add("7");
		ocassions.add("3");

		String skillsetCriteria = "";
		
		try {
			//First get past due orders
			receivedMessage = getPastDueOrders(timezones, ocassions);
			if (receivedMessage!=null){

				return returnMessage(receivedMessage);

			}
			else {


				//try to get todays orders			
				receivedMessage = getTodaysOrders(timezones, ocassions);

				if (receivedMessage!=null){

					return returnMessage(receivedMessage);

				}
				else {
					//Try to get tomorrows orders
					receivedMessage = getTomorrowsOrders(timezones, ocassions);

					if (receivedMessage!=null){

						return returnMessage(receivedMessage);

					}
					// get order regardless of date
					else{

						receivedMessage = getOrder(skillsetCriteria);

						return returnMessage(receivedMessage);

					}


				}
			}
			
			
		} catch (JMSException jmsException) {
			logger.error("Unable to get message from TLO destination \n Received exception: " + jmsException.getMessage());
			throw JmsUtils.convertJmsAccessException(jmsException);
		}
		
	}

	public List<String> consumeMessageTZ(  ) throws Exception {

		TextMessage receivedMessage = null;
		
		List<String> ocassions = new ArrayList<String>();
		
		
		ocassions.add("1");
		ocassions.add("2");
		ocassions.add("3");

		// BUILD OPEN TIMEZONES BASED ON HOUR ON SERVER BEING KEY, AND list of timezones BEING VALUE
		// example: est begins at 7 and ends at 6
		HashMap<String,String> myTZMap = new HashMap<String,String>();
		myTZMap.put("00","12,11,10,9,8,7,6,5,4,3,2");
		myTZMap.put("01","11,10,9,8,7,6,5,4,3,2,1");
		myTZMap.put("02","10,9,8,7,6,5,4,3,2,1,0");
		myTZMap.put("03","9,8,7,6,5,4,3,2,1,0,-1");
		myTZMap.put("04","8,7,6,5,4,3,2,1,0,-1,-2");
		myTZMap.put("05","7,6,5,4,3,2,1,0,-1,-2,-3");
		myTZMap.put("06","6,5,4,3,2,1,0,-1,-2,-3,-4");
		myTZMap.put("07","5,4,3,2,1,0,-1,-2,-3,-4,-5");
		myTZMap.put("08","4,3,2,1,0,-1,-2,-3,-4,-5,-6");
		myTZMap.put("09","3,2,1,0,-1,-2,-3,-4,-5,-6,-7");
		myTZMap.put("10","2,1,0,-1,-2,-3,-4,-5,-6,-7,-8");
		myTZMap.put("11","1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9");
		myTZMap.put("12","0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10");
		myTZMap.put("13","-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11");
		myTZMap.put("14","-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,12");
		myTZMap.put("15","-3,-4,-5,-6,-7,-8,-9,-10,-11,12,11");
		myTZMap.put("16","-4,-5,-6,-7,-8,-9,-10,-11,12,11,10");
		myTZMap.put("17","-5,-6,-7,-8,-9,-10,-11,12,11,10,9");
		myTZMap.put("18","-6,-7,-8,-9,-10,-11,12,11,10,9,8");
		myTZMap.put("19","-7,-8,-9,-10,-11,12,11,10,9,8,7");
		myTZMap.put("20","-8,-9,-10,-11,12,11,10,9,8,7,6");
		myTZMap.put("21","-9,-10,-11,12,11,10,9,8,7,6,5");
		myTZMap.put("22","-10,-11,12,11,10,9,8,7,6,5,4");
		myTZMap.put("23","-11,12,11,10,9,8,7,6,5,4,3");

		
		String[] myZoneValues;
		//GET CURRENT HOUR and then Zone range
		String myCurrentHour = DateUtil.toHourFormatString(new Date());
		System.out.println("*******myCurrentHour******* " + myCurrentHour);
		logger.debug("*******myCurrentHour******* " + myCurrentHour);

		if(!myCurrentHour.equals("")){
			 myZoneValues = myTZMap.get(myCurrentHour).split(",");
		}else{
			logger.error("unable to get myCurrentHour");
			System.out.println("unable to get myCurrentHour");
			myZoneValues = myTZMap.get("12").split(",");
		}

		String skillsetCriteria = "";
		
		try {
			//First get past due orders
			receivedMessage = getPastDueOrdersTZ(myZoneValues, ocassions);
			if (receivedMessage!=null){

				return returnMessage(receivedMessage);

			}
			else {


				//try to get todays orders			
				receivedMessage = getTodaysOrdersTZ(myZoneValues, ocassions);

				if (receivedMessage!=null){

					return returnMessage(receivedMessage);

				}
				else {
					//Try to get tomorrows orders
					receivedMessage = getTomorrowsOrdersTZ(myZoneValues, ocassions);

					if (receivedMessage!=null){

						return returnMessage(receivedMessage);

					}
					// get order regardless of date
					else{

						receivedMessage = getOrder(skillsetCriteria);

						return returnMessage(receivedMessage);

					}


				}
			}
			
			
		} catch (JMSException jmsException) {
			logger.error("Unable to get message from TLO destination \n Received exception: " + jmsException.getMessage());
			throw JmsUtils.convertJmsAccessException(jmsException);
		}
		
	}

	/**
	 * Consumes orders and messagesOnOrder by skillset provided
	 * @param skillsetCriteria
	 * @return
	 */
	private TextMessage getOrder(String skillsetCriteria) {
		
		System.out.println("********** messageSelector for order **********");
		TextMessage receivedMessage = (TextMessage) bmttelConsumer.receive(BOMConstants.TLO_QUEUE);
		

		return receivedMessage;
	}

	/**
	 * return order/messages from queue
	 * @param receivedMessage
	 * @return
	 * @throws JMSException
	 */
	private List<String> returnMessage(TextMessage receivedMessage) throws JMSException {
		
		ArrayList<String> messageProperties = new ArrayList<String>();

		messageXml = receivedMessage.getText();

		String messageType = "";

		String sendingshop = "";
		sendingshop = receivedMessage.getStringProperty("SENDINGSHOP");

		messageType =  receivedMessage.getStringProperty("MESSAGETYPE");

		String orderNumber  = "";
		orderNumber =  receivedMessage.getStringProperty("ORDERNUMBER");

		String skillSet  = "";
		skillSet =  receivedMessage.getStringProperty("SKILLSET");
		
		String orderType  = "";
		orderType =  receivedMessage.getStringProperty("ORDERTYPE");
		
		String timeZone = "";
		timeZone = receivedMessage.getStringProperty("TIMEZONE");

		messageProperties = new ArrayList<String>();

		messageProperties.add(0, skillSet);
		messageProperties.add(1, messageXml);
		messageProperties.add(2, messageType);
		messageProperties.add(3, sendingshop);
		messageProperties.add(4, orderNumber);
		messageProperties.add(5, orderType);
		messageProperties.add(6, timeZone);
		
		return messageProperties;
	}

/**
 * get orders/messages with delivery date of today
 * @param skillsetCriteria
 * @return
 */
	private TextMessage getTodaysOrders(List<String> timezones, List<String> ocassions) {
		

		TextMessage receivedMessage = null;
		
		
		String currentDate = DateUtil.toString(new Date());
		
		String dateCriteria = "DELIVERYDATE = '" + currentDate + "'";
		
		String messageSelector =  dateCriteria;
		
		System.out.println("********** messageSelector for Today orders **********");
		System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));

		for (int i=0;i<timezones.size();i++){

			for (int j=0;j<ocassions.size();j++){

				String newMessageSelector = messageSelector + " AND TIMEZONE = '" + timezones.get(i)+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
				System.out.println( newMessageSelector);

				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
				if (receivedMessage==null) {
					continue;
				}
				else{
					
					return receivedMessage;

				}

			}


		}
		if (receivedMessage==null) {
			
			System.out.println(messageSelector);
			receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

		}
		System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));

		return receivedMessage;
	}
	
	/**
	 * get orders/messages with delivery date of today
	 * @param skillsetCriteria
	 * @return
	 */
		private TextMessage getTodaysOrdersTZ(String[] myTZRange, List<String> ocassions) {
			

			TextMessage receivedMessage = null;
			
			
			String currentDate = DateUtil.toString(new Date());
			
			String dateCriteria = "DELIVERYDATE = '" + currentDate + "'";
			
			String messageSelector =  dateCriteria;
			
			System.out.println("********** messageSelector for Today orders **********");
			System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));

			for (int i=0;i<myTZRange.length;i++){
				for (int j=0;j<ocassions.size();j++){

					String newMessageSelector = messageSelector + " AND TIMEZONE = '" + myTZRange[i]+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
					System.out.println( newMessageSelector);

					receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
					if (receivedMessage==null) {
						continue;
					}
					else{
						
						return receivedMessage;

					}

				}

			}
			if (receivedMessage==null) {
				
				System.out.println(messageSelector);
				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

			}
			System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));

			return receivedMessage;
		}
		
	
	
	/**
	 * get orders/messages with delivery date of tomorrow
	 * @param skillsetCriteria
	 * @return
	 */
	private TextMessage getTomorrowsOrders(List<String> timezones, List<String> ocassions) {
		

		TextMessage receivedMessage = null;
		
		
		String dateCriteria = " DELIVERYDATE = '" + DateUtil.tomorrowsDate() + "'";
		
		String messageSelector =  dateCriteria;
		System.out.println("********** messageSelector for Tomorrow orders **********");
		System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));


		
		for (int i=0;i<timezones.size();i++){

			for (int j=0;j<ocassions.size();j++){

				String newMessageSelector = messageSelector + " AND TIMEZONE = '" + timezones.get(i)+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
				System.out.println(newMessageSelector);
				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
				if (receivedMessage==null) {
					continue;
				}
				else{
					return receivedMessage;

				}

			}


		}
		if (receivedMessage==null) {
			
			System.out.println(messageSelector);
			receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

		}
		System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));
		return receivedMessage;
	}
	
	/**
	 * get orders/messages with delivery date of tomorrow
	 * @param skillsetCriteria
	 * @return
	 */
	private TextMessage getTomorrowsOrdersTZ(String[] myTZRange, List<String> ocassions) {
		

		TextMessage receivedMessage = null;
		
		
		String dateCriteria = " DELIVERYDATE = '" + DateUtil.tomorrowsDate() + "'";
		
		String messageSelector =  dateCriteria;
		System.out.println("********** messageSelector for Tomorrow orders **********");
		System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));


		
		for (int i=0;i<myTZRange.length;i++){

			for (int j=0;j<ocassions.size();j++){

				String newMessageSelector = messageSelector + " AND TIMEZONE = '" + myTZRange[i]+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
				System.out.println(newMessageSelector);
				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
				if (receivedMessage==null) {
					continue;
				}
				else{
					return receivedMessage;

				}

			}

		}
		if (receivedMessage==null) {
			
			System.out.println(messageSelector);
			receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

		}
		System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));
		return receivedMessage;
	}
	
	/**
	 * Checks for orders in queue with delivery date from the last 5 days
	 * @param skillsetCriteria
	 * @return
	 */
	private TextMessage getPastDueOrders(List<String> timezones, List<String> ocassions) {
		

		TextMessage receivedMessage = null;

		List<String> lastFiveDays = DateUtil.lastFiveDays();
		
		String OR = "";
		OR = "DELIVERYDATE = '" + lastFiveDays.get(0) + "'";
		for (int i = 1; i < lastFiveDays.size(); i++) {
			OR = OR + " OR DELIVERYDATE = '" + lastFiveDays.get(i) + "'";
		}
		
		String dateCriteria= OR;
		
		String messageSelector =  dateCriteria;
		
		System.out.println("********** messageSelector for Past due orders **********");
		System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));


		for (int i=0;i<timezones.size();i++){

			for (int j=0;j<ocassions.size();j++){

				String newMessageSelector = messageSelector + " AND TIMEZONE = '" + timezones.get(i)+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
				System.out.println(newMessageSelector);
				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
				if (receivedMessage==null) {
					continue;
				}
				else{
	
					return receivedMessage;

				}

			}


		}
		if (receivedMessage==null) {
			
			System.out.println(messageSelector);
			receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

		}
		System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));

		return receivedMessage;
	}
	
	/**
	 * Checks for orders in queue with delivery date from the last 5 days
	 * @param skillsetCriteria
	 * @return
	 */
	private TextMessage getPastDueOrdersTZ(String[] myTZRange, List<String> ocassions) {
		

		TextMessage receivedMessage = null;

		List<String> lastFiveDays = DateUtil.lastFiveDays();
		
		String OR = "";
		OR = "DELIVERYDATE = '" + lastFiveDays.get(0) + "'";
		for (int i = 1; i < lastFiveDays.size(); i++) {
			OR = OR + " OR DELIVERYDATE = '" + lastFiveDays.get(i) + "'";
		}
		
		String dateCriteria= OR;
		
		String messageSelector =  dateCriteria;
		
		System.out.println("********** messageSelector for Past due orders **********");
		System.out.println("begin: " + DateUtil.toTimeFormatString(new Date()));


		for (int i=0;i<myTZRange.length;i++){
			for (int j=0;j<ocassions.size();j++){

				String newMessageSelector = messageSelector + " AND TIMEZONE = '" + myTZRange[i]+ "' AND OCCASSIONCODE = '" + ocassions.get(j)+ "'";
				System.out.println(newMessageSelector);
				receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(newMessageSelector);
				if (receivedMessage==null) {
					continue;
				}
				else{
	
					return receivedMessage;

				}


			}
		}
		if (receivedMessage==null) {
			
			System.out.println(messageSelector);
			receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

		}
		System.out.println("end: " + DateUtil.toTimeFormatString(new Date()));

		return receivedMessage;
	}
	

	/**
	 * Returns messageOnOrder from queue
	 */
	public List<String> browseQueueForMessagesOnOrder(String orderNumber) {

		String message = new String();
		List<String> messages = new ArrayList<String>();

		while (message != "") {

			try {

				String messageSelector = new String();
				messageSelector = "ORDERNUMBER = '" + orderNumber + "'";


				TextMessage receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

				if (receivedMessage == null) {
					logger.debug("There are no message related to order number " + orderNumber + " on TLO queue");
					message = "";
				} else {
					logger.debug("received message: " +receivedMessage.getText() + " on order "+ orderNumber);
					message = receivedMessage.getText();
					messages.add(message);

				}

			} catch (JMSException jmsException) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + jmsException.getMessage());
				throw JmsUtils.convertJmsAccessException(jmsException);
			}
			catch (Exception e) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + e.getMessage());

			}
		}
		return messages;
	}

	/**
	 * Returns messageOnOrder from queue by zip and city
	 * TODO add also by delivery date
	 */
	public List<String> browseQueueForRelatedOrders(String zip, String city, String orderNumber) {

		String message = new String();
		List<String> messages = new ArrayList<String>();

		while (message != "") {

			try {

				String messageSelector = new String();
				messageSelector = "ZIP = '" + zip + "' AND CITY = '" + city + "'";

				TextMessage receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

				if (receivedMessage == null) {
					logger.debug("There is no order related to " + orderNumber + " on TLO queue");
					message = "";
				} else {
					logger.debug("Order : " +receivedMessage.getText()+ "is related to order number" + orderNumber);
					message = receivedMessage.getText();
					messages.add(message);

				}

			} catch (JMSException jmsException) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + jmsException.getMessage());
				throw JmsUtils.convertJmsAccessException(jmsException);
			}
			catch (Exception e) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + e.getMessage());

			}
		}
		return messages;
	}
	
	
	public boolean browseQueueForDuplicateOrders(String orderNumber, String messageType) {

		String message = new String();
		List<String> messages = new ArrayList<String>();

		while (message != "") {

			try {

				String messageSelector = new String();
				messageSelector = "ORDERNUMBER = '" + orderNumber + "' AND MESSAGETYPE = '" + messageType + "'";

				TextMessage receivedMessage = (TextMessage) bmttelConsumer.receiveSelected(messageSelector);

				if (receivedMessage == null) {
					logger.debug("There are no duplicate orders for order " + orderNumber + " on TLO queue");
					message = "";
				} else {
					logger.debug("Duplicate order found for order " + orderNumber + " on TLO queue" );
					message = receivedMessage.getText();
					messages.add(message);

				}

			} catch (JMSException jmsException) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + jmsException.getMessage());
				throw JmsUtils.convertJmsAccessException(jmsException);
			}
			catch (Exception e) {
				logger.error("Unable to get message from TLO destination \n Received exception: " + e.getMessage());

			}
		}
		
		if (!messages.isEmpty()){
		return true;
		}
		else {
			return false;
		}
	}

	public void setBmttelConsumer(JmsTemplate bmttelConsumer) {
		this.bmttelConsumer = bmttelConsumer;
	}
	public JmsTemplate getBmttelConsumer() {
		return bmttelConsumer;
	}
}
