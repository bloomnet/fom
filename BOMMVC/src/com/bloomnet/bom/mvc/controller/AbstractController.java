package com.bloomnet.bom.mvc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.exceptions.RecommendedFloristsException;
import com.bloomnet.bom.mvc.service.ShopDataService;


/**
 * An abstract controller user as a parent to BOM controller
 * shared functionality for shared functionality.
 * 
 * @author Danil Svirchtchev
 *
 */
public abstract class AbstractController {
	
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( AbstractController.class );
    
    
    // abstract methods:
    protected abstract ShopDataService getShopDataService() throws Exception;
    

	/**
	 * Get a list of the recommended florists based on the orders zip code.
	 * 
	 * @param orderBean
	 * @return
	 * @throws RecommendedFloristsException
	 */
	protected List<Shop> getRecommendedFlorists ( MessageOnOrderBean orderBean ) throws RecommendedFloristsException  {
		
		String zipCode = orderBean.getRecipientZipCode();
		String orderNumber = orderBean.getOrderNumber();
		
		String country  = orderBean.getRecipientCountryCode();
		
		List<Shop> result = new ArrayList<Shop>();
		
		try {
			List<Shop> temp = new ArrayList<Shop>();
			if ((country.equals("CA")||country.equals("CAN")))
			{
				temp.addAll( getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_PHONE ) );
				temp.addAll( getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_API ) );

				if (temp.isEmpty()){
					zipCode = zipCode.substring(0, 3);
					temp.addAll( getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_PHONE ) );
					temp.addAll( getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_API ) );

				}
			}

			else {
				temp.addAll(getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_PHONE ));
				temp.addAll( getShopDataService().getShops( orderNumber, zipCode, BOMConstants.COMMMETHOD_API ) );

			}
			
			if(!temp.isEmpty()){
				List<String> shopsSeen = new ArrayList<String>();
				List<Shop> bloomnetShops = new ArrayList<Shop>();
				List<Shop> telefloraShops = new ArrayList<Shop>();
				List<Shop> otherShops = new ArrayList<Shop>();
				for(Shop s : temp){
					if(!shopsSeen.contains(String.valueOf(s.getShopId()))){
						Iterator<Shopnetwork> shopNetworks = s.getShopnetworks().iterator();
						while(shopNetworks.hasNext()){
							Shopnetwork shopNetwork = shopNetworks.next();
							if(shopNetwork.getNetwork().getNetworkId() == 1 && shopNetwork.getShopnetworkstatus().getShopNetworkStatusId() == 1 ){
								bloomnetShops.add(s);
								break;
							}
						}
						shopNetworks = s.getShopnetworks().iterator();
						if(!bloomnetShops.contains(s)){
							while(shopNetworks.hasNext()){
								Shopnetwork shopNetwork = shopNetworks.next();
								if(shopNetwork.getNetwork().getNetworkId() == 2 && shopNetwork.getShopnetworkstatus().getShopNetworkStatusId() == 1 ){
									telefloraShops.add(s);
									break;
								}
							}
						}
						shopNetworks = s.getShopnetworks().iterator();
						if(!bloomnetShops.contains(s) && !telefloraShops.contains(s)){
							while(shopNetworks.hasNext()){
								Shopnetwork shopNetwork = shopNetworks.next();
								if(shopNetwork.getNetwork().getNetworkId() != 1 && shopNetwork.getNetwork().getNetworkId() != 2 ){
									if(shopNetwork.getShopnetworkstatus().getShopNetworkStatusId() != 3){
										otherShops.add(s);
										break;
									}
								}
							}
						}
						shopsSeen.add(String.valueOf(s.getShopId()));
					
					}
				}
				result.addAll(bloomnetShops);
				result.addAll(telefloraShops);
				result.addAll(otherShops);
			}
			
		}catch (Exception e) {
				e.printStackTrace();
				throw new RecommendedFloristsException(e);
			}
		
		return result;
	}
	
	
	/**
	 * Load a shop code into a map.
	 * 
	 * @param key
	 * @param order
	 * @param shopId
	 */
	protected void loadShopCode( final String key, WebAppOrder order, final Long shopId ) {

		try {

		order.getShopCode().putAll( getShopDataService().getShopnetwork( key, shopId ) );
			
		} catch (Exception e) {
			
			order.getMessages().add("Could not load shop codes");
			logger.error(e);
			
			order.setShopCode( new HashMap<String,Shopnetwork>() );
		}	
	}
	
	
	/**
	 * Pupulate the list of the recommended florists
	 * 
	 * @param wao - The BOM order adapter
	 */
	protected void getRecommendedFlorists( WebAppOrder order ) {
		
		String     errorMessage        = null;
		List<Shop> recommendedFlorists = null;
		
		if ( order.getBean() != null ) {
		
			try {
	
				order.getBean().setOrderNumber( order.getOrderNumber() );
				recommendedFlorists = getRecommendedFlorists( order.getBean() );
				order.getMessages().clear();
			} 
			catch ( RecommendedFloristsException e ) {
				
				errorMessage = e.getMessage();
				logger.error(e);
			}
			finally {
	
				if ( errorMessage == null ) {
					
					order.getMessages().add("Order ["+order.getBean().getOrderNumber()+"] has been loaded");
					loadShopCode( "sending", order, order.getSendingShop().getShopId() );
					order.setRecommendedFlorists( recommendedFlorists );
				}
				else {
					
					order.getMessages().add( errorMessage );
				}
			}
		}
	}
}
