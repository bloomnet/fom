package com.bloomnet.bom.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.validators.OrderNotesValidator;

/**
 * Controller for the order notes.
 *
 * @author Danil Svirchtchev
 */
@Controller
public class OrderNotesController {
	
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( OrderNotesController.class );
    
    
    @Autowired private SessionManager      sessionManager;
    @Autowired private OrderNotesValidator notesValidator;
    @Autowired private ActivitiesService   activitiesService;

    
	/**
	 * Process GET method for URL orderNotes.htm
	 * 
	 */
/*	@RequestMapping( value = "orderNotes.htm", method = RequestMethod.GET )
	public String processSearchOrder ( HttpServletRequest request, 
									   SearchCriteria query, 
									   Model model ) {
		
		final String nextView = "orderNotes";
		
		model.addAttribute( "formOrder", new WebAppOrder() );
		
		return nextView;
	}
	*/
/*
	@RequestMapping( value = "orderNotes.htm", method = RequestMethod.POST )
	public String processSearchOrder ( HttpServletRequest request,
									   @ModelAttribute("formOrder") WebAppOrder formOrder, 
						               BindingResult result, 
						               SessionStatus status,
						               Model model ) {
		
		String nextView = "orderNotes";
		
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		notesValidator.validate( formOrder, result );

        if ( result.hasErrors() ) {
        	// do nothing
        } else {

        	try {
        		
        		order.getMessages().clear();
        		order.setLogAcallText( formOrder.getLogAcallText() );
        		
				// write audit record to the db
				activitiesService.persistActivityOrderNote( order, user );
				
				sessionManager.setMasterOrder(request, activitiesService.lockOrder( order, user ) );

	        	nextView = "redirect:AgentViewCloseModalAndRedirect.htm?query=AgentView.htm";
				
			} catch ( Exception e ) {

				order.getMessages().add( e.getMessage() );
				result.reject( "java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
	}
	*/
}
