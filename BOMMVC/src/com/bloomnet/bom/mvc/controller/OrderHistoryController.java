package com.bloomnet.bom.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.HistoryDataService;
import com.bloomnet.bom.mvc.service.SessionManager;

@Controller
public class OrderHistoryController {
	
    // Define a static logger variable
    private static Logger logger = Logger.getLogger( OrderHistoryController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private HistoryDataService historyDataService;

	/**
	 * Default action, displays the "Order History" page.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping(value = "orderHistory.htm", method = RequestMethod.GET)
	public String index( HttpServletRequest request, 
						 SearchCriteria searchCriteria, 
						 BindingResult bindResult, 
						 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		String nextView = "orderHistory";

		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			final String orderNumber = order.getBean().getBmtOrderNumber();
			Map<Bomorder, List<Orderactivity>> history = new HashMap<Bomorder, List<Orderactivity>>();
			Map<Bomorder, List<Orderactivity>> allMessages = new HashMap<Bomorder, List<Orderactivity>>();
			
			try {
				
				//if ( "masterorder".equals( searchCriteria.getQuery() ) ) {
					
					// get the parent order history
					history = historyDataService.getMasterOrderAndActivities( orderNumber );
					allMessages =historyDataService.getMasterOrderAndMessages( orderNumber );
				//}
				
				if ( "suborder".equals( searchCriteria.getQuery() ) ) {
					
					// populate view model with suborder activities
					history = extractSuborder( history );
				}
				
				model.addAttribute( "history", history );
				model.addAttribute( "allMessages", allMessages );
			} 
			catch (Exception e) {
				
				logger.error(e);
				order.getMessages().clear();
				order.getMessages().add( e.getMessage() );
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return nextView;
	}

	
	private Map<Bomorder, List<Orderactivity>> extractSuborder( Map<Bomorder, List<Orderactivity>> history ) throws Exception {

		Map<Bomorder, List<Orderactivity>> result = new HashMap<Bomorder, List<Orderactivity>>();
		
		if ( ! history.isEmpty() ) {
			
			for ( Map.Entry< Bomorder, List<Orderactivity> > entry : history.entrySet() ) {
				
				final Bomorder parentOrder = entry.getKey();
				final List<Bomorder> childOrders = parentOrder.getBomorders();
				
				for( Bomorder childOrder : childOrders ) {
					
					if ( ! childOrder.isParent() ) {
						
						final List<Orderactivity> m_activities = new ArrayList<Orderactivity>( childOrder.getOrderactivities() );
						
						Collections.sort( m_activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
						Collections.reverse(m_activities);
						
						result.put( childOrder, m_activities );
					}
				}
			}
		}
		
		return result;
	}
}
