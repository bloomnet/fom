package com.bloomnet.bom.mvc.controller;


import java.io.File;
import java.io.FileInputStream;

import java.util.Date;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.SessionManager;

@Controller
public class FilesController  {
		
    @Autowired private SessionManager sessionManager;

    /**
     * download
     */
	@RequestMapping(value = "download.htm", method = RequestMethod.GET)
    public String download(HttpServletRequest request,
        HttpServletResponse response) throws Exception {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	     
      
        String selectedDate = (String)sessionManager.getSelectedDate(request);
        
        if ((selectedDate == null) || selectedDate.equals("")) {
		    
			throw new Exception("Please search by date using MM/dd/yyyy format");
		}
        
        Date date  = DateUtil.toDateFormat(selectedDate);
        String dateStr = DateUtil.todashFormatString(date);
       
        String filename= "\\var\\bloomnet\\rpt\\BOM_Metrics_" + dateStr +".xls";

        File file = new File(filename);
        FileInputStream fileIn = new FileInputStream(file);

		response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition","attachment; filename=\"" + "BOM_Metrics_" + dateStr +".xls" +"\"");

        ServletOutputStream out = response.getOutputStream();
        
        byte[] outputByte = new byte[4096];
        //copy binary contect to output stream
        while(fileIn.read(outputByte, 0, 4096) != -1)
        {
        	out.write(outputByte, 0, 4096);
        }
        fileIn.close();
        out.flush();
        out.close();

        

        return null;

    }

    

}
