package com.bloomnet.bom.mvc.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Userrole;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.PasswordHasher;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.dao.impl.GenerateBOMBillingReport;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.utils.ActivityDetailReport;
import com.bloomnet.bom.mvc.utils.BOMActivityReport;
import com.bloomnet.bom.mvc.validators.ScrubData;

@Controller
public class BOMAdminController {
	
    @Autowired private SessionManager sessionManager;
    @Autowired private BomService bomService;
    @Autowired private MessagingService messagingService;
	@Autowired private OrderDAO orderDAO;
	@Autowired private UserDAO userDAO;
	@Autowired private ShopDAO shopDAO;


    
    ScrubData scrubData = new ScrubData();

    PasswordHasher passwordHasher = new PasswordHasher();

    @RequestMapping(value = "BOMAdminView.htm", method = RequestMethod.GET)
    public String index(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}

    	WebAppOrder  order = new WebAppOrder();
    	model.addAttribute("order", order );
    	String nextView = "BOMAdminView";

    	return nextView;
    }
    
    @RequestMapping(value = "BOMAdminSendOrder.htm", method = RequestMethod.GET)
    public String processSendOrderToQueue(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("send");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
		model.addAttribute("destinations", orderDAO.getAllQueues());

    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminSendOrder.htm", method = RequestMethod.POST )
    public String processSendOrderToQueue( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	

    	String  nextView  = "BOMAdminView";
    	String orderNumber = scrubData.scrubData(order.getBloomnetOrderNumber());
    	String destination = scrubData.scrubData(order.getQueues());

    	try {
    		bomService.sendOrderToQueue(orderNumber,destination);
    		order.getMessages().clear();
    		order.getMessages().add("order " + orderNumber + " sent to "+ destination);
    		request.setAttribute("message", "order " + orderNumber + " sent to "+ destination);    	}
    	
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		request.setAttribute("message", "java.exception.message" + e.getMessage());
    		e.printStackTrace();
    		
    		System.err.println(e.getMessage());
    	}
    	model.addAttribute("destinations", orderDAO.getAllQueues());
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminUnlockOrder.htm", method = RequestMethod.GET)
    public String processUnlockOrder(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("unlock");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminUnlockOrder.htm", method = RequestMethod.POST )
    public String processUnlockOrder( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {

    	String  nextView  = "BOMAdminView";
    	String orderNumber = scrubData.scrubData(order.getBloomnetOrderNumber());

    	try {
    		bomService.removeUserFromOrder(order);
    		order.getMessages().clear();
    		request.setAttribute("message", "order " + orderNumber + " unlocked and sent to TLO queue ");
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());
    		request.setAttribute("message", "java.exception.message: " + e.getMessage());
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    
    @RequestMapping(value = "BOMAdminUpdateOrder.htm", method = RequestMethod.GET)
    public String processUpdateOrder(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("updateOrder");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminUpdateOrder.htm", method = RequestMethod.POST )
    public String processUpdateOrder( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {

    	String  nextView  = "BOMAdminView";
    	String orderNumber = order.getBloomnetOrderNumber();
    	String destination = order.getQueues();

    	try {
    		bomService.updateOrder(order);
    		order.getMessages().clear();
    		order.getMessages().add("order " + orderNumber + " sent to "+ destination);
    		sessionManager.removeMasterOrder(request);
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminUpdateOrderStatus.htm", method = RequestMethod.GET)
    public String processUpdateOrderStatus(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("updateOrderStatus");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
		model.addAttribute("destinations", orderDAO.getAllQueues());
		model.addAttribute("statuses", orderDAO.getAllStatuses());

    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminUpdateOrderStatus.htm", method = RequestMethod.POST )
    public String processUpdateOrderStatus( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {

    	String  nextView  = "BOMAdminView";
    	String orderNumber = order.getBloomnetOrderNumber();
    	String destination = order.getQueues();
    	String order_status = order.getStatus();
		String userName          = UserDAO.INTERNAL_USER;
		long childId = 0;

		
		try {
		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

		if (!(destination.equals(BOMConstants.FSI_QUEUE))
				&&!(destination.equals(BOMConstants.TFSI_QUEUE))
				&&!(destination.equals(BOMConstants.TLO_QUEUE)))
		{
			throw new Exception("Queue: '" + destination + "' is not a valid destination");
		}else{

    	if (bomorder!=null){
    		
    		orderDAO.persistOrderactivityByUser(userName, 
    				BOMConstants.ACT_ROUTING, 
    				order_status, 
    				orderNumber, 
    				destination);
    		
    		childId = orderDAO.getCurrentFulfillingOrder(bomorder.getOrderNumber()).getBomorderId();
    		
    		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination );
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomorder.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(order_status));
			bomorderSts.setVirtualqueueId(virtualqueueId);
			
			orderDAO.updateBomorderSts(bomorderSts, childId);
    		
			order.getMessages().clear();
    		request.setAttribute("message", "order " + orderNumber + "  upated to status "+ order_status+ ", queue " + destination);
    	}
		
		else{
			throw new Exception("order number: " + orderNumber + " not found");
		}
    		
		}
		}catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		model.addAttribute("destinations", orderDAO.getAllQueues());
    		model.addAttribute("statuses", orderDAO.getAllStatuses());
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
		model.addAttribute("destinations", orderDAO.getAllQueues());
		model.addAttribute("statuses", orderDAO.getAllStatuses());
    	return nextView; 
		

    }
    
    @RequestMapping(value = "BOMAdminAddOrder.htm", method = RequestMethod.GET)
    public String processAddOrder(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("addOrder");
    	model.addAttribute("order", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping(value = "BOMAdminEditOrderStatus.htm", method = RequestMethod.GET)
    public String editOrderStatus(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("editOrderSts");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminEditOrderStatus.htm", method = RequestMethod.POST )
    public String editOrderStatus( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {

    	String  nextView  = "BOMAdminView";
    	String orderNumber = order.getBloomnetOrderNumber();
    	String destination = order.getQueues();
		String userName          = UserDAO.INTERNAL_USER;

    	 byte orderSts = order.getCurrentOrderStatus();

    	try {

			orderDAO.persistOrderactivityByUser( userName, BOMConstants.ACT_ROUTING, Byte.toString(orderSts), orderNumber, destination );
    		sessionManager.removeMasterOrder(request);
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminEditUser.htm", method = RequestMethod.GET)
    public String processEditUser(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}
    	
    	WebAppOrder  order = new WebAppOrder();
    	WebAppUser  user = new WebAppUser();
		Map<Long,String> results = new LinkedHashMap<Long,String>();

		List<User> userList = userDAO.getAllUsers();
    	for (User aUser: userList){
    		if (aUser.getUserId()!=1 &&
    				aUser.getUserId()!=2 &&
    				aUser.getUserId()!=3){
    			
    			results.put(aUser.getUserId(),aUser.getFirstName()+ " " + aUser.getLastName());

    		}
    	}
    	
    	
    	order.setActionStr("editUser");
    	model.addAttribute("order", order );
    	model.addAttribute("command", user );
		model.addAttribute("users", results);
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);


    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminEditUser.htm", method = RequestMethod.POST )
    public String processEditUser( HttpServletRequest request,
    		@ModelAttribute("command") User user, 
    		@ModelAttribute("order") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {

    	String  nextView  = "BOMAdminView";

    	try {
    		User adminUser = (User) sessionManager.getApplicationUser(request);
    		User originalUser = userDAO.getUserByUserName(user.getUserName());
    		String originalPassword = originalUser.getPassword();
    		
    		if(!user.getPassword().equals(originalPassword))
    			user.setPassword(passwordHasher.hash(user.getPassword()));

    		bomService.updateUser(user,adminUser);
    		order.getMessages().clear();
    		order.getMessages().add("user " + user.getUserName() + " updated ");
    		sessionManager.removeMasterOrder(request);
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminAddUser.htm", method = RequestMethod.GET)
    public String processAddUser(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}
    	
    	WebAppOrder  order = new WebAppOrder();
    	WebAppUser  user = new WebAppUser();
    	Userrole role = new Userrole();
    	order.setActionStr("addUser");
    	model.addAttribute("order", order );
    	model.addAttribute("command", user );
    	model.addAttribute("role", role );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping( value = "BOMAdminAddUser.htm", method = RequestMethod.POST )
    public String processAddUser( HttpServletRequest request,
    		@ModelAttribute("command") User user, 
    		@ModelAttribute("order") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	
    	String  nextView  = "BOMAdminView";
  

    	try {
    		User userByCreatedUserId;
			userByCreatedUserId = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
    		user.setUserByCreatedUserId(userByCreatedUserId);
    		user.setCreatedDate(DateUtil.toXmlDashFormatString(new Date()));
    		user.setActive("Y");
    		String password = user.getPassword();
    		password = passwordHasher.hash(password);
    		user.setPassword(password);
    		bomService.updateUser(user);
    	
			
			for (int i=1;i<6;i++){
				String longStr = Integer.toString(i);
				
				
				Role role = userDAO.getRoleById(Long.parseLong(longStr));
				Userrole userrole = new Userrole(userByCreatedUserId, 
					user, 
    				role, 
    				new Date(), 
    				true);
    		
				bomService.addUserrole(userrole);
			}
			
    		order.getMessages().clear();
    		order.getMessages().add("user " + user.getUserId() + " was created");
    		sessionManager.removeMasterOrder(request);
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminTLOOrders.htm", method = RequestMethod.GET)
    public String viewTLOOrdersBeingWorking(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		String nextView  = "BOMAdminView";

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("vieworders");
    	List<WebAppUser> users= new ArrayList<WebAppUser>();
    	try {
    		users = bomService.getTLOAgentsWorking();
    		
    		
    		//order.setBomorders(bomorders);
    		model.addAttribute("users", users );
    	
    	} catch (Exception e) {
    		nextView  = "BOMAdminView";
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());
    		bindResult.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
		}
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping(value = "frozenOrders.htm", method = RequestMethod.GET)
    public String viewFrozenOrders(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		String nextView  = "BOMAdminView";

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("frozen");
    	List<BomorderviewV2> orders;
    	try {
    		orders = bomService.getFrozenAutomatedOrders();
    		
    		
    		//order.setBomorders(bomorders);
    		model.addAttribute("orders", orders );
    	
    	} catch (Exception e) {
    		nextView  = "BOMAdminView";
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());
    		bindResult.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
		}
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping(value = "BOMAdminOrderHistory.htm", method = RequestMethod.GET)
    public String viewOrderHistory(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("history");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminOrderHistory.htm", method = RequestMethod.POST )
    public String viewOrderHistory( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	

    	String  nextView  = "BOMAdminView";
    	String orderNumber = order.getBloomnetOrderNumber();
    	WebAppOrder wao = new WebAppOrder();

    	
    	
    	try {
        	wao = bomService.viewOrderHistory(orderNumber);
        	wao.setActionStr("vieworderhistory");
        	sessionManager.setMasterOrder(request, wao);

    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminMessages.htm", method = RequestMethod.GET)
    public String viewMessages(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	order.setActionStr("messages");
    	model.addAttribute("command", order );
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminMessages.htm", method = RequestMethod.POST )
    public String viewMessages( HttpServletRequest request,
    		@ModelAttribute("command") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	
    	
    	
    	WebAppUser user = new WebAppUser();

    	String  nextView  = "BOMAdminView";
    	String orderNumber = order.getBloomnetOrderNumber();
    	WebAppOrder wao = new WebAppOrder();
    	
    	
    	try {
        	wao = bomService.viewOrderHistory(orderNumber);

    		
    		List<ActMessage> messages = messagingService.getMessagesOnOrder(user, wao);
    		wao.setOrderMessages(messages);
        	wao.setActionStr("viewmessages");
        	model.addAttribute("orderMessages", messages);
        	sessionManager.setMasterOrder(request, wao);

    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
	 
    
    @RequestMapping(value = "user.json", method = RequestMethod.GET)
	public String createUserOrder( HttpServletRequest request, 
								   HttpServletResponse response,
					               SearchCriteria searchCriteria, 
					               BindingResult bindResult, 
					               Model model ) {
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "user";
		
		try {

			Long selectedUserId = null;
			
			if ( ( searchCriteria.getQuery() != null ) && !searchCriteria.getQuery().equals("") ) {
				
				selectedUserId = Long.valueOf( searchCriteria.getQuery() );
			}
			
				
					
					User user = userDAO.getUserById(selectedUserId);
					
					WebAppUser wau = new WebAppUser(user);
			
					
					model.addAttribute( "user", wau);
				
			
		}
		catch ( Exception e ) {
			
			model.addAttribute( "error", e.getMessage() );
			e.printStackTrace();
		}
		
		return nextView;
	}
    
    @RequestMapping(value = "role.json", method = RequestMethod.GET)
	public String createRoleOrder( HttpServletRequest request, 
								   HttpServletResponse response,
					               SearchCriteria searchCriteria, 
					               BindingResult bindResult, 
					               Model model ) {
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "role";
		
		try {

			Long selectedUserId = null;
			
			if ( ( searchCriteria.getQuery() != null ) && !searchCriteria.getQuery().equals("") ) {
				
				selectedUserId = Long.valueOf( searchCriteria.getQuery() );
			}
			
					User user = userDAO.getUserById(selectedUserId);

					List<Role> roles = bomService.getRoles(user);
					
					model.addAttribute( "roles", roles);
				
			
		}
		catch ( Exception e ) {
			
			model.addAttribute( "error", e.getMessage() );
			e.printStackTrace();
		}
		
		return nextView;
	}
    
    @RequestMapping(value = "BOMAdminAddRole.htm", method = RequestMethod.GET)
    public String processAddRole(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}
    	
    	Map<Long,String> results = new LinkedHashMap<Long,String>();
    	Map<Long,String> roles = new LinkedHashMap<Long,String>();


		List<User> userList = userDAO.getAllUsers();
    	for (User aUser: userList){
    		if(aUser.getUserId()!=1 &&
    				aUser.getUserId()!=2 &&
    				aUser.getUserId()!=3 &&
    				(aUser.getActive() == null || !aUser.getActive().equals("N"))){
    			results.put(aUser.getUserId(),aUser.getFirstName()+ " " + aUser.getLastName());
    		}
    	}
    	
    	List<Role> rolesList = userDAO.getAllRoles();
    	for (Role aRole: rolesList){
    		roles.put(aRole.getRoleId(),aRole.getDescription());
    	}
    	
    	WebAppOrder  order = new WebAppOrder();
    	WebAppUser  user = new WebAppUser();
    	Userrole role = new Userrole();
    	order.setActionStr("addRole");
    	model.addAttribute("order", order );
    	model.addAttribute("command", user );
    	model.addAttribute("role", role );
    	model.addAttribute("roles", roles );
		model.addAttribute("users", results);
    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);
    
    	return nextView;
    }
    
    
    @RequestMapping( value = "BOMAdminAddRole.htm", method = RequestMethod.POST )
    public String processAddRole( HttpServletRequest request,
    		@ModelAttribute("command") WebAppUser user, 
    		@ModelAttribute("order") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	
    	String  nextView  = "BOMAdminView";


    	try {
    		User userByCreatedUserId;
    		userByCreatedUserId = userDAO.getUserByUserName(UserDAO.INTERNAL_USER);
    		user.setUserByCreatedUserId(userByCreatedUserId);
    		user.setCreatedDate(DateUtil.toXmlDashFormatString(new Date()));

    		String roleIdStr = user.getSelectedRoleId();


    		Role role = userDAO.getRoleById(Long.parseLong(roleIdStr));
    		Userrole userrole = new Userrole(userByCreatedUserId, 
    				user, 
    				role, 
    				new Date(), 
    				true);

    		bomService.addUserrole(userrole);


    		order.getMessages().clear();
    		order.getMessages().add("user " + user.getUserId() + " was assigned role " + roleIdStr);
    		sessionManager.removeMasterOrder(request);
    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";

    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMAdminSearchShop.htm", method = RequestMethod.GET)
    public String searchShop(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	

    	WebAppOrder  order = new WebAppOrder();
    	WebAppShop  shop = new WebAppShop();

    	order.setActionStr("search");
    	model.addAttribute("order", order );
    	model.addAttribute("command", shop );

    	String nextView = "BOMAdminView";
    	
    	sessionManager.setMasterOrder(request, order);
        request.getSession(false).setAttribute( "SHOP_RESULTS" , shop );


    	return nextView;
    }
	 
    @RequestMapping( value = "BOMAdminSearchShop.htm", method = RequestMethod.POST )
    public String searchShop( HttpServletRequest request,
    		@ModelAttribute("command") WebAppShop shop, 
    		@ModelAttribute("order") WebAppOrder order, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	

    	String  nextView  = "BOMAdminView";
    	//String orderNumber = order.getBloomnetOrderNumber();
    	WebAppShop was = new WebAppShop();
		List<WebAppShop> wasList = new ArrayList();

    	Enumeration params = request.getParameterNames();
    	HashMap map = (HashMap) request.getParameterMap();
    	
    	while(params.hasMoreElements()){
    		String param = (String)params.nextElement();
    		System.out.println("param"+ param);
    	}
    	
    	

    	
    	
    	try {
        	//wao = bomService.searchShop(shop);
    		WebAppOrder wao  = new WebAppOrder();
    		
    		boolean hasShopCode = map.containsKey("shopCode");
    		wasList = orderDAO.searchShops(map);
    		
    		//
        	wao.setActionStr("searchresults");
        	sessionManager.setMasterOrder(request, wao);

            String SHOP_RESULTS="SHOP_RESULTS";
            Collections.sort ( wasList, shopDAO.SHOP_BY_NAME );
            
			request.getSession(false).setAttribute( SHOP_RESULTS , wasList );
			
			model.addAttribute( "ordersCount", wasList.size());


    	}
    	catch( Exception e  ){

    		nextView  = "BOMAdminView";
    		
    	//	order.getMessages().clear();
    	//	order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 

    }
    
    @RequestMapping(value = "BOMActivityReports.htm", method = RequestMethod.GET)
	 public String createActivityReports(HttpServletRequest request, 
	    		SearchCriteria searchCriteria, 
	    		BindingResult bindResult, 
	    		Model model){
    	
	    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
			if(usera == null) {
				
				return "redirect:signin.htm";
			}
		 
		 	WebAppOrder  order = new WebAppOrder();
	    	WebAppUser  user = new WebAppUser();
	    	Userrole role = new Userrole();
	    	order.setActionStr("activityReport");
	    	model.addAttribute("order", order );
	    	model.addAttribute("command", user );
	    	model.addAttribute("role", role );
	    	String nextView = "BOMAdminView";
	    	
	    	sessionManager.setMasterOrder(request, order);
			
			return nextView;
		 
	 }
	 
	 @RequestMapping(value = "BOMActivityReports.htm", method = RequestMethod.POST)
	    public String getActivityReports(HttpServletRequest request,
	    		@ModelAttribute("command") WebAppUser user,
	    		ModelMap modeled,
	    		SearchCriteria searchCriteria, 
	    		BindingResult bindResult, 
	    		Model model){
	    	
			String nextView  = "BOMAdminView";
			
			BOMActivityReport report = new BOMActivityReport();
			ActivityDetailReport detailReport = new ActivityDetailReport();
			File downloadFile = report.getBOMActivityReport();
			File downloadFile2 = detailReport.getActivityDetailReport();
			request.setAttribute("downloadFile", downloadFile.getName());
			request.setAttribute("downloadFile2", downloadFile2.getName());

	    	return nextView;
	    }
    
    @RequestMapping(value = "BOMAdminBillingReport.htm", method = RequestMethod.GET)
	 public String createBOMBillingReport(HttpServletRequest request, 
	    		SearchCriteria searchCriteria, 
	    		BindingResult bindResult, 
	    		Model model){
    	
	    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
			if(usera == null) {
				
				return "redirect:signin.htm";
			}
		 
		 	WebAppOrder  order = new WebAppOrder();
	    	WebAppUser  user = new WebAppUser();
	    	Userrole role = new Userrole();
	    	order.setActionStr("billingReport");
	    	model.addAttribute("order", order );
	    	model.addAttribute("command", user );
	    	model.addAttribute("role", role );
	    	String nextView = "BOMAdminView";
	    	
	    	sessionManager.setMasterOrder(request, order);
			
			return nextView;
		 
	 }
	 
	 @RequestMapping(value = "BOMAdminBillingReport.htm", method = RequestMethod.POST)
	    public String viewDailyMetrics(HttpServletRequest request,
	    		@ModelAttribute("command") WebAppUser user,
	    		ModelMap modeled,
	    		SearchCriteria searchCriteria, 
	    		BindingResult bindResult, 
	    		Model model){
	    	
			String nextView  = "BOMAdminView";

	    	String dateStr = user.getSelectedDate();
	    	String endDateStr = user.getEndDate();
	    	
			Date date  = DateUtil.toDateFormat(dateStr);
			Date endDateTemp  = DateUtil.toDateFormat(endDateStr);
			
			String startDate = DateUtil.toXmlDashFormatString(date);
			String endDate = DateUtil.toXmlDashFormatString(endDateTemp);


			if ((dateStr == null) || dateStr.equals("")) {
			    
				try {
					throw new Exception("Please enter date using MM/dd/yyyy format");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			File downloadFile = new GenerateBOMBillingReport().generateBOMBillingReport("", "", startDate, endDate);
			request.setAttribute("downloadFile", downloadFile.getName());
	    	
	    	sessionManager.setSelectedDate(request, dateStr);
	    	sessionManager.setSelectedEndDate(request, endDateStr);
	    	

	    	return nextView;
	    }
   
	 
}
