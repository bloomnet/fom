/**
 * 
 */
package com.bloomnet.bom.mvc.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.AuthUserDetailsService;
import com.bloomnet.bom.mvc.service.SessionManager;



/**
 * Controller for "signing out" of the application.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/signout.htm")
public class SignOutController {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignOutController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private AuthUserDetailsService authService;
    @Autowired private Uactivitytype signOutActivity;
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
								 ModelMap model ) {
		
        try {
            
            WebAppUser user = (WebAppUser) sessionManager.getApplicationUser(request);
    	
            if( user != null ) {
            	
            	final Date now = new Date();
            	
            	// Store user activity in the db when user is out.
            	user.setSignOutTime( now );          	
                authService.updateUserDetails(user,signOutActivity);
            }
        } 
        catch (HibernateException ex) {
            logger.error( ex );
        }
        finally {
        	
            sessionManager.deleteAllApplicationObjectsFromSession(request);
            request.getSession().invalidate();
        }
		
		return "redirect:signin.htm";
	}
}
