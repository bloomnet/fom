package com.bloomnet.bom.mvc.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.common.bean.FsiSecurityElementInterface;
import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.UserNotFoundException;
import com.bloomnet.bom.mvc.exceptions.UserWrongPasswordException;
import com.bloomnet.bom.mvc.exceptions.WorkingShopNotFoundException;
import com.bloomnet.bom.mvc.service.AuthUserDetailsService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.validators.SignInValidator;

/**
 * Controller for the Sign In screen.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/signin.htm")
public class SignInController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignInController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private SignInValidator signInValidator;
    @Autowired private AuthUserDetailsService authService;
    @Autowired private Uactivitytype signInActivity;
    @Autowired private FsiSecurityElementInterface securityElement;

    @RequestMapping(method = RequestMethod.POST)
    public String processSubmit( HttpServletRequest request,
    							 @ModelAttribute("command") WebAppUser webAppUser, 
    		                     BindingResult result, 
    		                     SessionStatus status ) {
    	
    	String nextView = "signin";
    	
    	signInValidator.validateForm(webAppUser, result);
        
        if (result.hasErrors()) {
        	// do nothing
        } else {

        	try {
        		
        		final Date now = new Date();
        		
				WebAppUser authenticatedUser = authService.authenticate(webAppUser);
				
				if( authenticatedUser.getHighestRole() == null ){
					result.reject("error.login.no.roles", new Object[] { authenticatedUser.getUserName() }, null );
					return nextView;
				}
				
				authenticatedUser.setSignInTime( now );
				authenticatedUser.setSecurityElement( securityElement );
				authService.updateUserDetails( authenticatedUser, signInActivity );//TODO persist ip address
				sessionManager.setApplicationUser( request, authenticatedUser );        		
				nextView = "redirect:default.htm";
				
			} catch (HibernateException e) {
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
				e.printStackTrace();
			} catch (UserNotFoundException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
				logger.info(e);
			} catch (UserWrongPasswordException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
			} catch (WorkingShopNotFoundException e) {
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
				e.printStackTrace();
	        }
        }
        
        return nextView;
    }
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
			                     ModelMap model ) {
		
		String nextView = "signin";
		
		if( sessionManager.getApplicationUser(request) != null ) {
		    
			nextView = "redirect:default.htm";			
		} 
		else {
		    
			//WebAppUser user = WebAppUser.getInstance();		
			model.addAttribute( "command", new WebAppUser() );
		}
	
		return nextView;
	}
	
	private String getClientIpAddr(HttpServletRequest request) {   
        String ip = request.getHeader("X-Forwarded-For");   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("Proxy-Client-IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("WL-Proxy-Client-IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("HTTP_CLIENT_IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getRemoteAddr();   
        }   
        return ip;   
    }  

	
}
