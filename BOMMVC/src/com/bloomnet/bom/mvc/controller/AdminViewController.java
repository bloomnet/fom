package com.bloomnet.bom.mvc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.AdminService;
import com.bloomnet.bom.mvc.service.SessionManager;

/**
 * Controller for the "AdminView" web application screen.
 *
 * @author Danil Svirchtchev
 */
@Controller
public class AdminViewController {
	
	@Autowired private SessionManager sessionManager;

    // Define a static logger variable
	private static Logger logger = Logger.getLogger( AdminViewController.class );
	
	private static TreeMap<Integer, Page> PAGES_MAP = new TreeMap<Integer, Page>();
    
	// Injected service implementation
    @Autowired private AdminService adminService;
    
    // Injected DAO implementation
    @Autowired private OrderDAO orderpDAO;
    
    
    /**
     * Simple inner class to represent a page
     * 
     * @author Administrator
     *
     */
    protected class Page {
    	
    	public int startIndex, pageSize;
    	
		public Page( int startIndex, int pageSize ) {

			this.startIndex = startIndex;
			this.pageSize = pageSize;
		}

		public int getStartIndex() {
			return startIndex;
		}
		public void setStartIndex(int startIndex) {
			this.startIndex = startIndex;
		}

		public int getPageSize() {
			return pageSize;
		}
		public void setPageSize(int pageSize) {
			this.pageSize = pageSize;
		}
    }
    
    /**
     * Default action, displays the "BOM admin" page.
     * 
     * @param searchCriteria The criteria to search for
     */
    @RequestMapping(value = "ManagerView.htm", method = RequestMethod.GET)
    public String index( HttpServletRequest request, 
                         SearchCriteria searchCriteria, 
                         BindingResult bindResult, 
                         Model model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
        
        String nextView = "ManagerView";
        
        try {
        	
        	List<Bomorder> ordersToShow = new ArrayList<Bomorder>();
        	List<Bomorder> ordersForToday = orderpDAO.getOrdersForToday();
        	List<Bomorder> ordersForTomorrow = orderpDAO.getOrdersForTommorrow();
        	/*for(int ii=0; ii < ordersForToday.size(); ++ii){
        		ordersToShow.add(ordersForToday.get(ii));
        	}
        	for(int ii=0; ii < ordersForTomorrow.size(); ++ii){
        		ordersToShow.add(ordersForTomorrow.get(ii));
        	}*/
        	Long ordersCount = Long.valueOf(ordersToShow.size());
        	
        	populatePagesMap( ordersCount );
        	
        	model.addAttribute( "ordersCount", ordersCount );
        	model.addAttribute( "paginationLinks", createPaginationLinks( nextView ) );
        	model.addAttribute( "ordersTodayCount", ordersForToday.size() );
        	model.addAttribute( "ordersTomorrowCount", ordersForTomorrow.size() );
            
        	// Populate model with orders per page.
        	// Page sige is defined in {@link com.bloomnet.bom.mvc.service.AdminService.DEFAULT_RAWS_NUM}
            model.addAttribute( "orders", getOrders( searchCriteria.getQuery(), ordersToShow ) );

        } catch (Exception e) {
            
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 

		return nextView;
	}
    

	private void populatePagesMap( Long ordersCount ) {

		int numberOfPages = (int) ( ordersCount / AdminService.DEFAULT_RAWS_NUM );
		int offset = (int) ( ordersCount % AdminService.DEFAULT_RAWS_NUM );
		
		if(offset != 0){
			for ( int i = 0; i <= numberOfPages; i++ ) {
				
				Page page = new Page( i*AdminService.DEFAULT_RAWS_NUM, AdminService.DEFAULT_RAWS_NUM );
				PAGES_MAP.put( new Integer( i ), page );
			}
		}else{
			for ( int i = 0; i < numberOfPages; i++ ) {
				
				Page page = new Page( i*AdminService.DEFAULT_RAWS_NUM, AdminService.DEFAULT_RAWS_NUM );
				PAGES_MAP.put( new Integer( i ), page );
			}
		}
		
		if ( numberOfPages == 0 ) {
			
			Page page = new Page( numberOfPages, offset );
			PAGES_MAP.put( new Integer( numberOfPages+1 ), page );
		}
	}

	
	private List<WebAppOrder> getOrders( String query, List<Bomorder> ordersToShow ) throws Exception {
		
		List<WebAppOrder> result = null;
		Page page = null;
		
		try {
			
			Integer pageNum = Integer.valueOf( query );
			page = PAGES_MAP.get( (pageNum-1) );
		}
		catch( NumberFormatException e ) {
			
			page = PAGES_MAP.firstEntry().getValue();
		}
		finally {
			
			if ( page != null ) {
				
				result = adminService.getOrders( ordersToShow, ((Integer.valueOf( query ) -1)  * AdminService.DEFAULT_RAWS_NUM), (Integer.valueOf( query ) *  AdminService.DEFAULT_RAWS_NUM));
			}
		}
				
		return result;
	}

	
	private List<String> createPaginationLinks(String nextView) throws Exception {
		
		List<String> result = new ArrayList<String>();
		
		for ( Iterator<Integer> it = PAGES_MAP.keySet().iterator(); it.hasNext(); ) {
			
		    Integer key = (Integer)it.next();
		    result.add( nextView+".htm?query="+(key+1) );
		}

		return result;
	}
}
