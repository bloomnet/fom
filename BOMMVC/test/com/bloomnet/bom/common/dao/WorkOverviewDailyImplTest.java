package com.bloomnet.bom.common.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.entity.WorkOverviewDaily;
import com.bloomnet.bom.common.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class WorkOverviewDailyImplTest {
	
	@Autowired protected WorkOverviewDailyDAO workOverviewDailyDAO;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAutomatedGetWorkOverviewDaily() {
		WorkOverviewDaily workOverviewDaily = workOverviewDailyDAO.getAutomatedWorkOverviewDaily();
		System.out.println("testAutomatedGetWorkOverviewDaily : " + workOverviewDaily.getOrders());
	}
	
	@Test
	public void testTLOGetWorkOverviewDaily() {
		WorkOverviewDaily workOverviewDaily = workOverviewDailyDAO.getTLOWorkOverviewDaily();
		System.out.println("testTLOGetWorkOverviewDaily : " + workOverviewDaily.getOrders());

	}

	@Test
	public void testGetWorkOverviewDailyByDate() {
		fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void tesGetAutomatedWorkOverviewDailyByDateRange(){
		
		String dateStr="06/27/2012";
		String dateToStr="07/08/2012";
		
		Date date  = DateUtil.toDateFormat(dateStr);
		Date toDate  = DateUtil.toDateFormat(dateToStr);
		
		
		
		List<WorkOverviewDaily> results = workOverviewDailyDAO.getAutomatedWorkOverviewDailyByDateRange(date, toDate);
		
		for (WorkOverviewDaily workOverviewDaily:results){
			System.out.println("date: " +workOverviewDaily.getDateSent());
		}
		
		
	}

}
