package com.bloomnet.bom.mvc.dao;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.BusinessLogicDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Businesslogic;
import com.bloomnet.bom.common.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class BusinessLogicDAOImplTest {
	
	String route;
	@Autowired
    protected BusinessLogicDAO businessLogicDAO;
	
	@Autowired
    protected UserDAO usercDAO;


	@Before
	public void setUp() throws Exception {
		route = "FSI,TLO,TFSI";
		

		
	}

	@Test
	public void testGetCurrentQueueRoute() {
		 Businesslogic currentRoute = businessLogicDAO.getCurrentQueueRoute();
		System.out.println("current route: " + currentRoute.getFlowInUse());
	}

	@Test
	public void testSetCurrentQueueRoute() {
		String flowInUse = new String();
		byte[] status = new byte[]{1};

		
		Businesslogic businessLogic = new Businesslogic();
		businessLogic.setFlowInUse(route);
		businessLogic.setStatus(status);
		long userId=3;
		User user = usercDAO.getUserById(userId);
		businessLogic.setUserByCreatedUserId(user );
		businessLogic.setCreatedDate(new Date());
		businessLogicDAO.setCurrentQueueRoute(businessLogic);		
	}

	@Test
	public void testIsDefaultRoute() {
		System.out.println("is default route: " +  businessLogicDAO.isDefaultRoute());
	}
	@Test
	public void testSetRouteToDefault(){
			/*long id=0;
			id  = businessLogicDAO.setRouteToDefault();*/
		System.out.println(businessLogicDAO.setRouteToDefault());
		
	}
	

}
