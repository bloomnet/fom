package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.BomorderviewDAO;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class BomorderviewDAOImplTest {
	
	@Autowired private BomorderviewDAO bomorderviewDAO;

	

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAllOrders() {
		bomorderviewDAO.getAllOrders();
	}

	@Test
	public void testGetOrdersForToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetOrdersForTommorrow() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetOutstandingOrdersForToday() {
		bomorderviewDAO.getOutstandingOrdersForToday();
	}
	
	@Test
	public void testGetOutstandingOrders(){
		bomorderviewDAO.getOutstandingOrders();
		
	}

	@Test
	public void testGetOrdersPastDue() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetFutureOrders() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetOutstandingOrdersPriorToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShopOrdersForToday() {
		fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void testGetOrderNoteView(){
		
		String parentBomid = "25556142";
		Bomlatestordernoteview orderNote = bomorderviewDAO.getOrderNoteView(parentBomid);
		
		System.out.println(orderNote.getId().getNote());
		System.out.println(orderNote.getId().getTimeLogged());
		
		
	}

}
