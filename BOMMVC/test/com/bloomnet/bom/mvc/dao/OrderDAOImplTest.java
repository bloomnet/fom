package com.bloomnet.bom.mvc.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class OrderDAOImplTest {
	
	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected property
	@Autowired private Properties fsiProperties;
	
	@Autowired protected OrderDAO orderDAO;
    @Autowired private ShopDAO shopDAO;
  
	String orderXML;

	@Before
	public void setUp() throws Exception {
		
		orderXML="<foreignSystemInterface>" +
					"<security>" +
						"<username>M177</username>" +
						"<password></password>" +
						"<shopCode>M1770000</shopCode>" +
					"</security>" +
					"<messagesOnOrder>" +
						"<messageCount>1</messageCount>" +
						"<messageOrder>" +
							"<messageType>0</messageType>" +
							"<sendingShopCode>M1770000</sendingShopCode>" +
							"<receivingShopCode>Q8830000</receivingShopCode>" +
							"<fulfillingShopCode>Q8830000</fulfillingShopCode>" +
							"<systemType>GENERAL</systemType>" +
							"<identifiers>" +
								"<generalIdentifiers>" +
									"<externalShopOrderNumber>10001</externalShopOrderNumber>" +
									"</generalIdentifiers>" +
							"</identifiers>" +
							"<messageCreateTimestamp>20110926062836</messageCreateTimestamp>" +
							"<orderDetails>" +
								"<occasionCode>1</occasionCode>" +
								"<totalCostOfMerchandise>5.99</totalCostOfMerchandise>" +
								"<orderProductsInfo><orderProductInfoDetails><" +
								"units>1</units>" +
								"<costOfSingleProduct>5.99</costOfSingleProduct>" +
								"<productDescription>10 flowers</productDescription>" +
								"</orderProductInfoDetails></orderProductsInfo>" +
								"<orderCardMessage>good luck!</orderCardMessage>" +
								"<orderType>1</orderType>" +
							"</orderDetails>" +
							"<orderCaptureDate>20110926062836</orderCaptureDate>" +
							"<deliveryDetails>" +
								"<deliveryDate>12/10/2011</deliveryDate>" +
								"<specialInstruction>please leave at door</specialInstruction>" +
							"</deliveryDetails>" +
							"<recipient>" +
								"<recipientFirstName>Mark</recipientFirstName>" +
								"<recipientLastName>Slvr</recipientLastName>" +
								"<recipientAddress1>2 old country rd</recipientAddress1>" +
								"<recipientCity>carle place</recipientCity>" +
								"<recipientState>NY</recipientState>" +
								"<recipientZipCode>11550</recipientZipCode>" +
								"<recipientCountryCode>USA</recipientCountryCode>" +
								"<recipientPhoneNumber>516237708</recipientPhoneNumber>" +
							"</recipient>" +
							"<wireServiceCode>BMT</wireServiceCode>" +
							"<shippingDetails>" +
								"<trackingNumber></trackingNumber>" +
								"<shipperCode></shipperCode>" +
								"<shippingMethod></shippingMethod>" +
								"<shippingDate></shippingDate>" +
							"</shippingDetails>" +
						"</messageOrder>" +
					"</messagesOnOrder>" +
				"</foreignSystemInterface>";
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getOrdersByAgent() {
		
		final String status = bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED);
		
		//List<Bomorder> orders = orderDAO.getOrdersByAgent(orderDAO.INTERNAL_USER, null);
		
		List<Bomorder> orders = orderDAO.getOrdersByAgent(UserDAO.INTERNAL_USER, status);

		System.out.println("Complete...");
	}

	@Test
	public void persistParentOrder() throws Exception {
		
		Bomorder order = new Bomorder();
		City city = new City();
		Commmethod commmethod1 = new Commmethod();
		byte commid = 1;
		commmethod1= orderDAO.getCommmethodById(commid);

		city = orderDAO.getCityByNameAndState("Carle Place", "New York");
		order.setCity(city);
		order.setCommmethod(commmethod1);
		order.setCreatedDate(new Date());
		order.setDeliveryDate(new Date());
		//order.setOrderactivities(orderactivities);
		order.setOrderNumber("12345");
		//order.setOrderType(orderType);
		order.setOrderXml(orderXML);
		order.setPrice(Double.parseDouble("15.99"));
		long pid = 1346;
		order.setParentorderId(pid);
		long shopID1 = 70;
		long shopID2 = 71;

		Shop receivingShop  = shopDAO.getShopByShopId(shopID1);
		//Shop receivingShop  = shopDAO.getShopByCode("Q8830000");
		order.setShopByReceivingShopId(receivingShop);
		//Shop sendingShop  = shopDAO.getShopByCode("M1770000");
		Shop sendingShop  = shopDAO.getShopByShopId(shopID2);

		order.setShopBySendingShopId(sendingShop);
		Zip zip = orderDAO.getZipByCode("11550");
		order.setZip(zip);

		Bomorder bomorder = orderDAO.persistParentOrder(order);
		 final String orderNumber = bomorder.getOrderNumber();
		
		System.out.println("Complete: "+orderNumber);
	}
	
	@Test
	public void deleteOrders(){
		
		for ( int i = 1; i < 9; i++) {
			
			Bomorder order = orderDAO.getOrderByOrderId(new Long(i));
			
			if(order != null)
				orderDAO.deleteOrder(order);
		}
	}
	
	@Test
	public void getCurrentOrderActivity(){
		
		Bomorder order = orderDAO.getOrderByOrderId(new Long(165));
		
		if(order != null){
			orderDAO.getCurrentOrderStatus(order);
	}
	}
		
		@Test
		public void TestGetOrdersByDate(){
			
			/*SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			GregorianCalendar calendar = new GregorianCalendar();

			//Display the date now:
			Date now = calendar.getTime();
			String formattedDate = format.format(now);
			//System.out.println("todays date: " + formattedDate);

			//Advance the calendar one day:
			calendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
			Date tomorrow = calendar.getTime();
			
			List<Bomorder> orders = orderDAO.getOrdersForToday( new Date(), tomorrow);*/
			
			List<Bomorder> orders = orderDAO.getOrdersByToday();
			
			System.out.println("Number of orders for " + new Date() + ": "+ orders.size());
			for (Bomorder order: orders){
				byte status = orderDAO.getCurrentOrderStatus(order);
				System.out.println("Order number: " +order.getOrderNumber() + " status "+ status);
			}
		
	}
		
		@Test
		public void TestGetAllParentOrders(){
			
			List<Bomorder> orders = orderDAO.getAllParentOrders();
			
			System.out.println("Number of orders for " + new Date() + ": "+ orders.size());
			for (Bomorder order: orders){
				byte status = orderDAO.getCurrentOrderStatus(order);
				System.out.println("Order number: " +order.getOrderNumber() + " status "+ status);
				
			}
	}
		
		@Test
		public void TestGetOrdersForToday(){
			
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			GregorianCalendar calendar = new GregorianCalendar();

			//Display the date now:
			Date now = calendar.getTime();
			String formattedDate = format.format(now);
			//System.out.println("todays date: " + formattedDate);

			//Advance the calendar one day:
			calendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
			Date tomorrow = calendar.getTime();
			
			List<Bomorder> orders = orderDAO.getOrdersForToday( );
			
			
			System.out.println("Number of orders for " + new Date() + ": "+ orders.size());
			for (Bomorder order: orders){
				byte status = orderDAO.getCurrentOrderStatus(order);
				System.out.println("Order number: " +order.getOrderNumber() + " status "+ status);
			}
		
	}
		@Test
		public void  TestGetOpenTimeZone() {
			
			int localTimeZone = -5;
			
			String localTimeStr = DateUtil.getCurrentTimeString(new Date());
			Date localTime = DateUtil.toDateTime(localTimeStr);
			
			String openTimeStr="080000";
			String closeTimeStr="190000";
			
			Date openTime = DateUtil.toDateTime(openTimeStr);
			Date closeTime = DateUtil.toDateTime(closeTimeStr);
			
			long diff1 =   openTime.getTime() - localTime.getTime() ;
			long time = (diff1 * (-1)/ 100);
			
			System.out.println(time);
			
			if (time>125959){
				time = time - 120000;
			}
			
			String timeStr = "0" + Long.toString(time);
			
			Date timedt = DateUtil.toDateTime(timeStr);


			
		/*	if ((localTime>openTime)&&(localTime<closeTime)){
				
			}
			*/
			
		}
		
		@Test
		public void TestGetTLOOrdersBeingWorked(){
			List<Bomorder> orders = orderDAO.getTLOOrdersBeingWorked();
			System.out.println("orders being worked: " + orders.size());
		}
		
		@Test 
		public void TestGetManualMessageCounts(){
		    Map<String,Integer> results = new HashMap<String,Integer>();

			String lastReportDateStr = "05-01-2012";
			Date lastReportDate = DateUtil.toDateDashFormat(lastReportDateStr);
			results = orderDAO.getManualMessageCounts(lastReportDate);
			
			System.out.println(results.size());
		}
		
		@Test 
		public void TestGetAutomatedMessageCounts(){
		    Map<String,Integer> results = new HashMap<String,Integer>();

			String lastReportDateStr = "05-01-2012";
			Date lastReportDate = DateUtil.toDateDashFormat(lastReportDateStr);
			results = orderDAO.getAutomatedMessageCounts(lastReportDate);
			
			System.out.println(results.size());
		}
		
		@Test
		public void TestGetLastOrderAndSequenceNumberTest(){
			Map<String,String> results = orderDAO.getLastOrderAndSequenceNumberTest();
			String ordernumber = new String(); 
		    String seqnumber = new String(); 
			
			for (Map.Entry<String, String> entry : results.entrySet()) { 
				ordernumber = entry.getKey(); 
				seqnumber = entry.getValue(); 
			} 
			
			System.out.println("ordernumber: " + ordernumber);
			System.out.println("seqnumber: " + seqnumber);


		}

}
