package com.bloomnet.bom.mvc.dao;


import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class RoutingDAOImplTest {
	
	@Autowired
    protected RoutingDAO routingDAO;
	
	@Autowired
    protected OrderDAO orderDAO;
	
	@Autowired
    protected UserDAO userDAO;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetOrderStatus() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetVirtualQueue() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistRouting() {
		
		String destination =  "BMTFSI";
		byte status = 1;


		ActRouting route = new ActRouting();
		StsRouting stsRouting = new StsRouting();
		Oactivitytype oactivitytype = new Oactivitytype();
		 
		oactivitytype = orderDAO.getActivityTypeByDesc("Act_Routing");
		stsRouting = routingDAO.getStatusById(status);
		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		long userId  =3 ;
		User user = userDAO.getUserById(userId);
		orderactivity.setUser(user);
		orderactivity.setOactivitytype(oactivitytype );
		/*Bomorder order = Bomorder.getInstance();
		String orderNumber=null;
		bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
		orderactivity.setBomorder(order);*/
		route.setOrderactivity(orderactivity);
		route.setStsRouting(stsRouting);
		route.setVirtualQueue(destination);
		
		routingDAO.persistRouting(route );

	}

	@Test
	public void testGetStatusById() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteRouting() {
		fail("Not yet implemented");
	}

}
