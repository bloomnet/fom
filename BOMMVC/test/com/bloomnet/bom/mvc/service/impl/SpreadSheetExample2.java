package com.bloomnet.bom.mvc.service.impl;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.examples.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;


public class SpreadSheetExample2 {
	
	 private static final String[] titles = {
         "Delivery Date", "Fulfillment %", "Total Orders", "Unassigned Orders", "Assigned Active Orders", "Rejected Orders", "Canceled Orders", "Delivery Confirmations"};


   @SuppressWarnings("deprecation")
public static void main(String[] args){
	   
	   try {
		   
		   Workbook wb = new HSSFWorkbook();
		    Sheet sheet = wb.createSheet("new sheet");
		    
            CellStyle style = wb.createCellStyle();
            style.setWrapText(true);


		    //title row
		    Row titleRow = sheet.createRow((short) 1);
		    Cell titleCell = titleRow.createCell((short) 0);
		    titleCell.setCellValue("Delivery on Wed 07/18/2012");

		    //Merge title row
		    sheet.addMergedRegion(new CellRangeAddress(
		            1, //first row (0-based)
		            1, //last row  (0-based)
		            0, //first column (0-based)
		            8 //last column  (0-based)
		    ));
		    Row subTitleRow = sheet.createRow((short) 2);
		    for (int i = 0; i < titles.length; i++) {
	            Cell cell = subTitleRow.createCell(i);
	            cell.setCellValue(titles[i]);
				cell.setCellStyle(style);

	        }

		   
		   //file name
		    FileOutputStream fileOut = new FileOutputStream("\\var\\bloomnet\\rpt\\workbook.xls");
		   
		   //write file
		   wb.write(fileOut);
		   fileOut.close();
		   
		   System.out.println("completed");

	   } catch (FileNotFoundException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   } catch (IOException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }
	   
   }
	
}
