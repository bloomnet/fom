package com.bloomnet.bom.mvc.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.mvc.service.TaskExecutorExample;
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class TaskExecutorExampleImplTest {
	
	@Autowired TaskExecutorExample taskExecutorExample;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testPrintMessages() {
		taskExecutorExample.printMessages();
	}

}
