package com.bloomnet.bom.mvc.service.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.dao.WorkOverviewDailyDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.service.ReportingService;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class ReportingServiceImplTest {
	
	@Autowired private ReportingService reportingService;
	
	@Autowired protected FulfillmentSummaryDAO fulfillmentSummaryDAO;
	@Autowired protected WorkOverviewDailyDAO workOverviewDailyDAO;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetFSIOrdersForToday() {
		try {
			List<BomorderviewV2> bmtfsiList = reportingService.getFSIOrdersForToday();
			
			for (BomorderviewV2 order: bmtfsiList){
				System.out.println(order.getId().getParentOrderNumber());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetTFSIOrdersForToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetTLOOrdersForToday() {
		try {
			List<BomorderviewV2> tloList = reportingService.getTLOOrdersForToday();
			
			for (BomorderviewV2 order: tloList){
				System.out.println(order.getId().getParentOrderNumber());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOutstandingOrdersForToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetOutstandingOrdersPriorToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShopOrdersForToday() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetStdOrders() {
		try {
			reportingService.getStdOrders();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}

	@Test
	public void testGetDirOnlineOrders() {
		try {
			List<Bomorder> orders = reportingService.getDirOnlineOrders();

			System.out.println("***************DIR Online:" + orders.size() +" ************");
			
			for(Bomorder order : orders){
				System.out.println(order.getOrderNumber());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSendSummaryReport() {
		try {
			reportingService.sendSummaryReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendMessageCountReport() {
		try {
			reportingService.sendMessageCountReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendOutstandingReport(){
		try {
			reportingService.sendOutstandingReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetShopOrdersForTodayAccepted(){
		long shop = 15953;

		try {
			List<BomorderviewV2> orders =reportingService.getShopOrdersForTodayAccepted(shop);
			
			System.out.println("*******FROM_YOU_FLOWERS accepted for today: " + orders.size() +" ************");

			
			for (BomorderviewV2 order: orders){
				System.out.println(order.getId().getParentOrderNumber());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetFulfillmentSummaryDailyByDate() {

		String dateStr = "07/11/2012";
		Date date  = DateUtil.toDateFormat(dateStr);
		
		 List<FulfillmentSummaryDaily> fulfillmentSummary = reportingService.getFulfillmentSummaryDailyByDate(date);

		 System.out.println("total orders:" + fulfillmentSummary.get(0).getTotalOrders());
		 
	}

	@Test
	public void testGetFulfillmentSummaryMtdByDate() {

		String dateStr = "07/11/2012";
		Date date  = DateUtil.toDateFormat(dateStr);
		
		 List<FulfillmentSummaryMtd> fulfillmentSummaryMtd = reportingService.getFulfillmentSummaryMtdByDate(date);
		
	}

	@Test
	public void testGetWorkOverviewDailyByDate() {
		// TODO Auto-generated method stub
	}
	
	
	@Test
	public void testGenerateSummaryReportSpreadSheet(){
		
		//get data
		String dateStr = DateUtil.todashFormatString(new Date());

		   FulfillmentSummaryDaily summaryDaily = fulfillmentSummaryDAO.getFulfillmentSummaryDaily();
		   FulfillmentSummaryMtd summaryMtd = fulfillmentSummaryDAO.getFulfillmentSummaryMtd();
		   WorkOverviewDaily workOverviewDailyMan = workOverviewDailyDAO.getTLOWorkOverviewDaily();
		   WorkOverviewDaily workOverviewDailyAuto = workOverviewDailyDAO.getAutomatedWorkOverviewDaily();
		   
		   
		try {
			reportingService.generateMetricsSpreadSheet(summaryDaily,summaryMtd,workOverviewDailyMan, 
					workOverviewDailyAuto,dateStr );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
