package com.bloomnet.bom.mvc.service;

import static org.junit.Assert.fail;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.ActPayment;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Paymenttype;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.jms.MessageProducer;

/**
 * @author Danil Svirchtchev
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
@Transactional
public class AgentServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
	
    @Autowired protected UserDAO userDAO;
    @Autowired protected ShopDAO shopDAO;
    @Autowired protected OrderDAO orderDAO;
    @Autowired protected AgentService agentService;
    @Autowired protected MessageProducer producer;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetNextWorkItem() {
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final List<Role> skillSets = user.getRoles();
		
		System.out.println("-----> Will get a next order for role: ");
		
		try {
			
			MessageOnOrderBean orderBean = agentService.getNextWorkItem( skillSets );
			
			//Map<String, String> shopCodes = agentService.getShopCodes(orderBean);
			
			System.out.println(orderBean);
			
			//System.out.println(shopCodes);
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSendOrderToShop() {

		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");
		
		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			
			MessageOnOrderBean orderBean = order.getBean();
			
			Shop fulfillingshop = shopDAO.getShopByCode("D7140000");
			
			orderBean.setReceivingShop(fulfillingshop);
			orderBean.setFulfillingShop(fulfillingshop);
			
			User orderUser = userDAO.getUserByUserName("jpursoo");
			
			orderBean.setOrderType(BOMConstants.ORDER_TYPE_BMT);
			orderBean.setCommMethod(BOMConstants.COMMMETHOD_PHONE_ID);
			
			ActPayment payment = new ActPayment();
			Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc("Act_Payment");
			Orderactivity orderactivity = new Orderactivity(oactivitytype);
		
			orderactivity.setActPayment(payment);
			orderactivity.setUser(order.getUser());
			List<Bomorder> orders = orderDAO.getOrdersByOrderNumber(orderBean.getOrderNumber());
			Long bomorderId = orders.get(0).getBomorderId();
			
			Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
			orderactivity.setBomorder(bomorder);
			orderactivity.setCreatedDate(new Date());
			
			StsPayment stsPayment = new StsPayment();
			byte id =1;
			stsPayment = orderDAO.getPaymentStatusById(id);
			payment.setStsPayment(stsPayment);
			Paymenttype paymenttype = orderDAO.getPaymentTypeById(id);
			payment.setPaymenttype(paymenttype);
			double paymentAmount = 10;
			payment.setPaymentAmount(paymentAmount );
			payment.setPayTo("bob");
			
			payment.setOrderactivity(orderactivity);

			//orderBean.setPayment(payment );

			
			System.out.println(orderBean);
			
			order.setBean(orderBean);
			
			agentService.sendOrderToShop(order, user);
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");
	}

	@Test
	public void testSendOrderToNextQueue() {
		fail("Not yet implemented");
	}

	@Test
	public void testViewOrderHistory() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessagesOnOrderFromDB() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessagesOnOrderFromQueue() {
		
		int numOfMessages =3;
	
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will get a next order for role: ");
		
		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();
			
			System.out.println(orderBean);

			for (int i =0;i<numOfMessages;i++){
			  sendTestMessagesToTLO(orderBean);
			}
			
			
			
			MessageOnOrderBean messages = agentService.getMessagesOnOrderFromQueue(orderBean);
			
			System.out.println("Messages on Queue: " +messages);
			
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");

	}

	@Test
	public void testLogCall() {
		
		
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");
		
		try {
			
			
			
			ActLogacall call = new ActLogacall();

			final Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_LOGACALL);
			final Orderactivity orderactivity = new Orderactivity(oactivitytype);
			Byte callDispDesc = 1;
			Calldisp calldisp;
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();

			User orderUser = userDAO.getUserByUserName("jpursoo");

			
			//calldisp = orderDAO.getCallDispById(callDispDesc );
			
			
			//call.setCalldisp(calldisp);
			String callText = "Cannot create order";
			call.setLogAcallText(callText );
			//call.setCalldisp(calldisp);
			call.setOrderactivity(orderactivity);

			
			//agentService.logCall(orderBean);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		
		
	}

	@Test
	public void testAddPayment() {
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");
		
		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();
			
			Shop fulfillingshop = shopDAO.getShopByCode("D7140000");
			
			orderBean.setReceivingShop(fulfillingshop);
			orderBean.setFulfillingShop(fulfillingshop);
			
//			User orderUser = userDAO.getUserByUserName("jpursoo");
//			orderBean.setUser(orderUser);
			
			orderBean.setOrderType(BOMConstants.ORDER_TYPE_BMT);
			orderBean.setCommMethod(BOMConstants.COMMMETHOD_FAX_ID);

			
			double amount=10;
//			orderBean.setAmount(amount);
//			orderBean.setPaymentType("Credit Card");
//			orderBean.setPaymentTo("Bob");
//			
//			System.out.println(orderBean);
//			
//			agentService.addPayment(orderBean);
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");
			
		
	}

	@Test
	public void testGetCallDisposition() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendNewMessageToShop() {
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");

		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();
			
			Shop fulfillingshop = shopDAO.getShopByCode("D7140000");
			
			orderBean.setReceivingShop(fulfillingshop);
			orderBean.setFulfillingShop(fulfillingshop);
			
			User orderUser = userDAO.getUserByUserName("jpursoo");
			order.setUser(orderUser);
			
			orderBean.setOrderType(BOMConstants.ORDER_TYPE_BMT);
			orderBean.setCommMethod(BOMConstants.COMMMETHOD_API_ID);
			
			ActPayment payment = new ActPayment();
			Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc("Act_Payment");
			Orderactivity orderactivity = new Orderactivity(oactivitytype);
		
			orderactivity.setActPayment(payment);
			orderactivity.setUser(order.getUser());
			List<Bomorder> orders = orderDAO.getOrdersByOrderNumber(orderBean.getOrderNumber());
			Long bomorderId = orders.get(0).getBomorderId();
			
			Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
			orderactivity.setBomorder(bomorder);
			orderactivity.setCreatedDate(new Date());
			
			StsPayment stsPayment = new StsPayment();
			byte id =1;
			stsPayment = orderDAO.getPaymentStatusById(id);
			payment.setStsPayment(stsPayment);
			Paymenttype paymenttype = orderDAO.getPaymentTypeById(id);
			payment.setPaymenttype(paymenttype);
			double paymentAmount = 10;
			payment.setPaymentAmount(paymentAmount );
			payment.setPayTo("bob");
			
			payment.setOrderactivity(orderactivity);

			//orderBean.setPayment(payment );
			
			System.out.println(orderBean);
			
			order.setBean(orderBean);
			
			String orderNum = agentService.sendOrderToShop( order, user );
			
			MessageOnOrderBean newOrderBean = orderBean;
			
			Bomorder childOrder = orderDAO.getOrderByOrderNumber(orderNum);
			
			String bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
			newOrderBean.setMessageType(1);
			newOrderBean.setMessageText("how can I buy flowers?");
			newOrderBean.setOrderNumber(orderNum);
			newOrderBean.setBmtSeqNumberOfOrder(Long.parseLong(bmtSeqNumberOfOrder));
			newOrderBean.setBmtOrderNumber(orderNum);

			
			List<MessageOnOrderBean> newMessages = new ArrayList();
			newMessages.add(newOrderBean);
			//orderBean.setOutgoingMessages(newMessages );
			
			//agentService.sendNewMessageToShop(orderBean);
			
			} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");
		
	}

	@Test
	public void testGetLostSession() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalOrdersTouchedToday() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalOrdersWorkedToday() {
		User user = userDAO.getUserByUserName("jpursoo");
		try {
			agentService.getTotalOrdersWorkedToday(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetTotalWorkTimeToday() {
		fail("Not yet implemented");
	}



	@Test
	public void testGetAvailableTFSIShops() {
		
		final String zip = "01011";

		//List<?> m_shops = agentService.getAvailableTFSIShops(zip);
		
		//System.out.println("-----> Got Available TFSI Shops :: "+m_shops.size());
	}

	@Test
	public void testSendEmailConfirmation() {
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");
		
		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();
			
			Shop fulfillingshop = shopDAO.getShopByCode("D7140000");
			
			String shopEmail = "jpursoo@gmail.com";
			fulfillingshop.setShopEmail(shopEmail );

			orderBean.setReceivingShop(fulfillingshop);
			orderBean.setFulfillingShop(fulfillingshop);
			
//			User orderUser = userDAO.getUserByUserName("jpursoo");
//			orderBean.setUser(orderUser);
			
			orderBean.setOrderType(BOMConstants.ORDER_TYPE_BMT);
			orderBean.setCommMethod(BOMConstants.COMMMETHOD_FAX_ID);
			

			
			System.out.println(orderBean);
			
			//agentService.sendEmailConfirmation(orderBean.getBmtOrderNumber(),orderBean);
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");

		
	}

	@Test
	public void testFindRelatedOrdersFromDB() {
		fail("Not yet implemented");
	}

	@Test
	public void testFindRelatedOrdersFromQueue() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistShop() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPreviousAttemptedShops() {
		fail("Not yet implemented");
	}
	
	@Test
	public void testSendOrderToFax() {
		
	WebAppUser user = new WebAppUser();
	user.initialize( userDAO.getUserByUserName("jpursoo") );

	final List<Role> skillSets = user.getRoles();

	
	System.out.println("-----> Will send an order to a shop: ");
	
	try {
		
		WebAppOrder order = agentService.getNextWorkItem( user );
		MessageOnOrderBean orderBean = order.getBean();
		
		Shop fulfillingshop = shopDAO.getShopByCode("D7140000");
		fulfillingshop.setShopFax("+15162374528");
		
		orderBean.setReceivingShop(fulfillingshop);
		orderBean.setFulfillingShop(fulfillingshop);
		
//		User orderUser = userDAO.getUserByUserName("jpursoo");
//		orderBean.setUser(orderUser);
		
		orderBean.setOrderType(BOMConstants.ORDER_TYPE_BMT);
		orderBean.setCommMethod(BOMConstants.COMMMETHOD_FAX_ID);
		
		ActPayment payment = new ActPayment();
		payment.setPayTo(orderBean.getFulfillingShop().getShopName());
		payment.setPaymentAmount(65);
		byte payid = 1;
		Paymenttype paymenttype = orderDAO.getPaymentTypeById(payid);
		payment.setPaymenttype(paymenttype );
		
		
		//orderBean.setPayment(payment );

		
		System.out.println(orderBean);
		
		agentService.sendOrderToShop( order, user );
		
	} catch (Exception e) {
		
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	System.out.println("\n\n\n-----> FINISHED...\n\n\n");
	}
	
	@Test
	public void testExitAssignment(){
		
		WebAppUser user = new WebAppUser();
		user.initialize( userDAO.getUserByUserName("jpursoo") );

		final String roleId =  user.getHighestRole().getRoleId().toString();
		
		System.out.println("-----> Will send an order to a shop: ");
		
		try {
			
			WebAppOrder order = agentService.getNextWorkItem( user );
			MessageOnOrderBean orderBean = order.getBean();
			
			User orderUser = userDAO.getUserByUserName("jpursoo");
			
			orderBean.setMessageType(0);
			orderBean.setSkillset("1");
			order.setUser(orderUser);
			System.out.println(orderBean);
			
			agentService.exitAssignment( order );
			
		} catch (Exception e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\n\n\n-----> FINISHED...\n\n\n");
		
	}
	
	private void sendTestMessagesToTLO(MessageOnOrderBean orderBean){
		
		String messageRjct="<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3417555</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
		String orderNumber=new String();
		String destination=new String();
		
		orderNumber=orderBean.getOrderNumber();
		destination="TLO";
		String skillset = "1";
		String sendingShopCode = "Z999";
		String deliveryDate = orderBean.getDeliveryDate();
		String occasionCode = orderBean.getOccasionId();
		String city = orderBean.getRecipientCity();
		String zip = orderBean.getRecipientZipCode();
		//producer.produceMessage(destination, message, orderNumber,sendingShopCode,deliveryDate,city,zip, occasionCode , skillset,BOMConstants.INQR_MESSAGE_DESC);
		//TODO add extra paramters
		//  producer.produceMessage( destination,  messageRjct,  orderNumber, sendingShopCode, BOMConstants.RJCT_MESSAGE_DESC, skillset);

		
	}
	
	@Test
	public void setupMessagesToTLO(){
		
		String messageRjct="<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3418127</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
		String orderNumber=new String();
		String destination=new String();
		
		orderNumber="3418654";
		destination="TLO";
		String skillset = "1";
		String sendingShopCode = "Z999";
		//producer.produceMessage(destination, message, orderNumber,sendingShopCode,deliveryDate,city,zip, occasionCode , skillset,BOMConstants.INQR_MESSAGE_DESC);
		  //TODO add extra parameters
		//producer.produceMessage( destination,  messageRjct,  orderNumber, sendingShopCode, BOMConstants.RJCT_MESSAGE_DESC, skillset);

		
	}
	
	@Test
	public void encodeBean() throws Exception{
		
		TransformService transform = new Transform();

		
		Long orderID = (long) 39;
		Bomorder order = orderDAO.getOrderByOrderId(orderID);
		String messageXml = order.getOrderXml();
		
		ForeignSystemInterface fsi = transform.convertToJavaFSI( messageXml.toString() );
		MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();
		
		// TODO: Add shops not found exceptions
		final Shop receivingShop  = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
		final Shop sendingShop    = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
		final Shop fulfillingShop = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());
		
		MessageOnOrderBean orderBean = new MessageOnOrderBean();

		orderBean = transform.createBeanFromFSI(fsi,receivingShop,sendingShop,fulfillingShop);
		
		String specialInstructions = orderBean.getSpecialInstruction();
		
		System.out.println("messageXml: " + messageXml);

		System.out.println("after unmarshalled: " + specialInstructions);
		

		String fsiXml = transform.convertToFSIXML(fsi);
		
		System.out.println("after marshalled: " + fsiXml);

		
		String decoded = URLDecoder.decode(specialInstructions, "UTF-8");
		
		System.out.println("decoded: " + decoded);

		
		String encoded =URLEncoder.encode(specialInstructions, "UTF-8");
		
		System.out.println("encoded: " + encoded);

	}
	
	
	
//	@Test
//	public void createBeanFromTFSI(){
//		String tfsiXML = "<?xml version=\"1.0\"?><TFETransactionHeader><ReceiverVSC>43300100</ReceiverVSC><SenderPSC>43300100</SenderPSC><SenderVSC>43300100</SenderVSC><SenderSequenceNumber>162</SenderSequenceNumber></TFETransactionHeader><?xml version=\"1.0\"?><Order xmlns:Dove=\"http://www.teleflora.com/Dove\" Confirmation=\"Y\" AutoForward=\"Z\" Operator=\"BloomNet\" OccassionCode=\"1\"><SendingFlorist><Name>212 FLORAL</Name><City>NEW YORK</City><State>NY</State><Phones><Phone Preference=\"1\">2128821020</Phone></Phones></SendingFlorist><Recipient><FirstName>Mark</FirstName><LastName>Slvr</LastName><AddressLines><AddressLine Line=\"1\">2 old country rd</AddressLine></AddressLines><City>beverly hills</City><State>CA</State><PostalCode>90210</PostalCode><Email/><Phones><Phone Preference=\"1\">5162377080</Phone></Phones></Recipient><CardMessageLines><CardMessage Line=\"1\">good luck!</CardMessage></CardMessageLines><Products><Product ProductCode=\"\" Index=\"1\"><DescLine><DescLine Sort=\"1\">1 order(s) of prod1</DescLine></DescLine></Product></Products><Instructions><Instruction Line=\"1\">please leave at door</Instruction></Instructions><Price>5.99</Price><DeliveryDate>20111205</DeliveryDate><OrderDate>20111207</OrderDate></Order>";
//		try {
//			MessageOnOrderBean moob = agentService.createBeanFromTFSI(tfsiXML);
//		} catch (JDOMException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//	}

}
