<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
    <body>
        <form:form method="post" commandName="formShop">
        <div id="container">
            <div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
            </div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
        <c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
        </c:import>
        </div>
        <div class="sidebar">
        <div class="headline">You Are Adding a florist</div>
        <!-- Close DIV headline -->
        <div class="sidebar-content">
            <div class="florist-list2" align="right">
                Name*: <form:input path="shopName" /><br /><br />
                Address*: <form:input path="shopAddress1" /><br /><br />
                City*: <form:input path="city.name"  value="${ MASTERORDER.bean.recipientCity }" /><br /><br />
                State*: <form:select path="selectedState" name="state">
                        <c:forEach items="${ states }" var="st" >
                        <c:choose>
                        <c:when test="${ st.shortName eq MASTERORDER.bean.recipientState }">
                            <form:option selected="selected" value="${ st.stateId }"><c:out value="${ st.shortName }" /></form:option>
                        </c:when>
                        <c:otherwise>
                            <form:option value="${ st.stateId }"><c:out value="${ st.shortName }" /></form:option>
                        </c:otherwise>
                        </c:choose>
                        </c:forEach>
                       </form:select><br /><br />
                Zip Code*: <form:input path="zip.zipCode" value="${ MASTERORDER.bean.recipientZipCode }" /><br /><br />
                Phone*: <form:input path="shopPhone" /><br /><br />
                BMT Code: <form:input path="bmtShopCode" /><br /><br />
                TF Code: <form:input path="tfShopCode" /><br /><br />
                Fax: <form:input path="shopFax" /><br /><br />
                Email: <form:input path="shopEmail" /><br /><br />
                Contact*: <form:input path="shopContact" /><br /><br />
                Open Sunday*: <form:select path="openSunday" name="openSunday">
                            <form:option value="1">Yes</form:option>
                             <form:option value="0">No</form:option>
                       </form:select><br /><br />
                Has Wire Service*: <form:select path="hasWireService" name="hasWireService">
                            <form:option value="1">Yes</form:option>
                             <form:option value="0">No</form:option>
                       </form:select><br /><br />
                <br />
                <!-- Close DIV send-order -->
            </div>
        </div>
        <!-- Close DIV sidebar-content -->
        <div class="sidebar-footer">
            <form:input type="hidden" path="coveredZip" value="${ MASTERORDER.bean.recipientZipCode }" />
            <input type="image" src="images/add-florist-small.png" alt="Add Florist" class="bottom-buttons" onclick="this.disabled=true,this.form.submit();" /></a>
        </div>
        <!-- Close DIV sidebar-footer -->
        <center>
        <font face="Arial" size="2" color="red">
          <spring:bind path="command.*">
            <c:if test="${status.errors.errorCount > 0}">
              <c:forEach var="error" items="${status.errors.allErrors}">
                <spring:message message="${error}"></spring:message><br />
              </c:forEach>
            </c:if>
          </spring:bind>
        </font>
        </center>
        </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>