<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>FOM :: Log In</title>
		<meta name="description" content="" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
	<style>
		.error {
			color: red;
		}
	</style>
 		<script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/JavaScript" src="js/curvycorners.js"></script>
        <script>
		  curvyCorners.addEvent(window, 'load', initCorners);
		  
		  function initCorners() {
		  var settings = {
		  tl: { radius: 8 },
		  tr: { radius: 8 },
		  bl: { radius: 8 },
		  br: { radius: 8 },
		  antiAlias: true
		  }
		  curvyCorners(settings, "#container");
		  curvyCorners(settings, ".small-box");
		  }
		  var settings2 = {
		  tl: { radius: 8 },
		  tr: { radius: 8 },
		  antiAlias: true
		  }
		  curvyCorners(settings2, ".sub-headline");
		</script>
	</head>
   <body>
	<div id="login">
    <form:form method="post" commandName="webAppUser">
        <ul>
          <li><img src="images/logo-login.gif" width="229" height="114" alt="Bloomnet" /></li>
          <li><form:input path="userName" type="text" name="username" value="username" class="field" onfocus="if (this.value=='username'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='username';this.style.color='#c7c6c6'}" /></li>
          <li><form:input path="password" type="password" name="password" value="password" class="field" onfocus="if (this.value=='password'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='password';this.style.color='#c7c6c6'}" /></li>
          <li class="btn"><input type="image" value="Submit" src="images/btn-login.png" alt="Login" /></li>
        </ul>
    </form:form>
	</div>
	<br />
	<br />
	<br />
	<br />
	<div align="center">
		<table align="center">
	            <tr> 
	              <td height="40" colspan="2" align="center">
	                <font face="Arial" size="2" color="red">
				<spring:bind path="command.*">
				  <c:if test="${status.errors.errorCount > 0}">
				    <c:forEach var="error" items="${status.errors.allErrors}">
				      <spring:message message="${error}"></spring:message><br>
				    </c:forEach>
				  </c:if>
				</spring:bind>
		      </font>
	              </td>
	            </tr> 
	          </table>
	</div>
  </body>
</html>