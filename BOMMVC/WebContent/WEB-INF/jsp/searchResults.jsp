<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
  <body>
	<div id="container">
		<div id="header">
	      <c:import url="/WEB-INF/jsp/includes/header.jsp" />				
		</div> <!-- Close DIV header -->
		<div class="content-container">
		    <div class="manager-area">
				<div class="headline">
					Search
				  <div class="clear"></div>
				</div> <!-- Close DIV headline -->
				<div class="manager-all-orders">
				  <div class="sub-headline">
						Your Results [<c:out value="${ ordersCount }"/>]
				  </div><!-- Close DIV sub-headline -->
				  <c:set var="containsHyperlinkaccess" value="false" scope="page" />
					<c:forEach var="item" items="${roles}">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(item.key,'moaccess') }">
								<c:set var="containsMultiOrderAccess" value="true" scope="page" />
							</c:when>
						</c:choose>
					</c:forEach>
		          <table border="0" cellspacing="0" cellpadding="0" class="search-table">
		            <tr>
			            <td class="col1 header">Order#</td>
			            <td class="col2 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recipient</td>
			            <td class="col3 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sending Shop</td>
			            <td class="col4 header" style="margin-left: -20px;">Delivery</td>
			            <td class="col5 header">City</td>
			            <td class="col6 header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State</td>
			            <td class="col6 header">Zip</td>
			            <td class="col8 header">&nbsp;</td>
		            </tr>
		          </table>
				  <div class="search-table-container">
			        <table border="0" cellspacing="0" cellpadding="0" class="search-table" id="searchResultsTable">
				  	  <c:forEach var="order" items="${ orders }" varStatus="status" >
				          <tr>
				          <c:choose>
							<c:when test='${containsMultiOrderAccess == "true"}'>
								<%if(request.getParameter("stateName") != null){ %>
									<td><input type="checkbox" value="<c:out value="${ order.bomorderId }"/>" class="selectedOrders"/></td>
								<%} %>
							</c:when>
						  </c:choose>
	          <c:choose>
	          <c:when test="${ fn:length(order.orderNumber) > 8 }">
	            <td class="orders-content-col1" title="<c:out value="${ order.orderNumber }"/>"  ><c:out value="${ fn:substring(order.orderNumber,0,7)}"/>...</td>
	          </c:when>
	          <c:otherwise>
	            <td class="orders-content-col1"><c:out value="${ order.orderNumber }"/></td>
	          </c:otherwise>
	          </c:choose>
	          				<c:set var="index1" value="${fn:indexOf(order.orderXml, '<recipientLastName>')}" />
	          				<c:set var="index2" value="${fn:indexOf(order.orderXml, '</recipientLastName>')}" />
	          				
				            <td class="search-content-col2"><c:out value="${fn:substring(order.orderXml, index1+19, index2)}"/></td>
				            <td class="search-content-col3"><c:out value="${ order.shopBySendingShopId.shopName }"/><%-- &nbsp;( Shopcode should be here ) --%></td>
				            <td class="search-content-col5"><fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" /></td>
				            <td class="search-content-col6"><c:out value="${ order.city.name }"/></td>
				            <td class="search-content-col6"><c:out value="${ order.city.state.shortName }"/></td>
				            <td class="search-content-col6"><c:out value="${ order.zip.zipCode }"/></td>
				         <!--  <td class="search-content-col5"><fmt:formatNumber value="${ bean.totalCost }" type="CURRENCY" /></td>-->
				            <td class="search-content-col7"><a href='#' onclick='showOrderDetails( <c:out value="${ order.bomorderId }"/> )'>Details</a></td>
				          </tr>
					  </c:forEach>
					</table>
				  </div> <!-- Close DIV order-table-container -->
				</div> <!-- Close DIV manager-all-orders -->
		        <div class="clear"></div> <!-- Clear Floats -->
		    </div><!-- Close DIV manager-area -->
	        <div class="search-sidebar">
		        <div class="headline">
					Order Information
		        </div> <!-- Close DIV headline -->
	        <div class="order-search">
	          <ul class="search-oi">
	            <li class="l"><strong>Order #:</strong></li>
	            <li class="c"><strong>Delivery Date:</strong></li>
	            <li class="r"><strong>Status:</strong></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l">
		            <div id="orderNumber">&nbsp;</div>
	              	<div id="viewHistory" style="display: none;">
		        		<a href="#" onclick="openMyModal('orderHistory.htm?query=masterorder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;">View History</a>

		        	</div>
	            </li>
	            <li class="c"><div id="date">&nbsp;</div></li>
	            <li class="r"><div style="word-wrap: break-word; width: 60px" id="status">&nbsp;</div></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l"><strong>Current Child:</strong></li>
	            <li class="c">&nbsp;</li>
	            <li class="r"><strong>Queue:</strong></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l"><div id="childOrderNumber">&nbsp;</div></li>
	            <li class="c">&nbsp;</li>
	            <li class="r"><div id="virtualQueue">&nbsp;</div></li>
	          </ul>
	          <div class="clear hr"></div> <!-- Clear Floats -->
	          <ul class="search-oi-2">
	            <li><strong>Recipient:</strong></li>
	        <li><strong>Occasion:</strong></li>

	          </ul>
	          <ul class="search-oi-2">
	            <li><div style="height: 72px" id="recipientPane" /></li>
	        <li><div id="occasion"/></li>

	          </ul>
	          <div class="clear hr"></div> <!-- Clear Floats -->
	          <ul class="search-oi-2">
	            <li><strong>Sending Shop:</strong></li>
	            <li><strong>Fulfilling Shop:</strong></li>
	          </ul>
	          <ul class="search-oi-2">
	            <li>
	              <div style="height: 93px" id="sendingShopPane" />
	            </li>
	            <li>
	              <div style="height: 93px" id="receivingShopPane" />
				</li>
	          </ul>
	          <div class="clear"></div> 
	          <!-- Clear Floats -->
	        </div>
	        <div class="headline">Order Details</div> 
	        <div class="agents-logged-in">
        		<div class="odetails-b" id="productsPane" />
	        </div><!-- Close DIV agents-logged-in -->
	        <div class="search-sidebar-footer">
	          <a href="#" id="workOrder"><img src="images/work-order.png" alt="Work Order" class="next-order" /></a>
	        </div> <!-- Close DIV search-sidebar-footer -->
	        </div> <!-- Close DIV search-sidebar -->
			<div class="clear" /> <!-- Clear Floats -->
		</div> <!-- Close DIV content-container -->
		<div class="content-footer">
		</div> <!-- Close DIV content-footer -->
		<div id="footer" ></div><!-- Close DIV footer -->
	</div> <!-- Close DIV container -->	
	<c:choose>
		<c:when test='${containsMultiOrderAccess == "true"}'>
			<%if(request.getParameter("stateName") != null){ %>
				<a href="#" onclick="workSelectedOrders();">Work Selected Orders</a>
			<%} %>
		</c:when>
	</c:choose>
  </body>
</html>