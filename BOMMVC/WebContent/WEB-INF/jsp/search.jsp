<%@ page language="java" contentType="text/xml" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<result>
<c:choose>
  <c:when test="${ not empty error }">
    <error><c:out value="${ error }" /></error>
  </c:when>
  <c:otherwise>
    <link>searchResults.htm</link>
    <count><c:out value="${ fn:length( orders ) }" /></count>
  </c:otherwise>
</c:choose>
</result>

