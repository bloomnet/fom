<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
		<div style='display: none' class="overlay-wrapper" id="overlay-1">
<!-- IMPORT OVERLAYS HERE AND MAKE VISIBLE BASED ON WHAT SELECTION IS MADE (CURRENTLY EDITORDER OR ORDERHISTORY -->
		</div>
        <form:form method="post" commandName="formOrder" id="changeStatus">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
        <div class="sidebar">
		<div class="headline">Change Order Status</div>
		<!-- Close DIV headline -->
		<div class="sidebar-content">
		    <div class="method-send">
		       
		             
		              <table border="0" cellpadding="10" cellspacing="10">
	
     <tr> 
      </tr>
			
			<tr>
 <td>Select status: 
 <form:select path="orderStatus" name="orderStatus">
 <br/>
						<c:forEach var="s" items="${ statuses }">

							<form:option value="${ s.value }">
								<c:out value="${ s.key }" />
							</form:option>

						</c:forEach>
					</form:select></td>
			</tr>
			
				

<tr>
			<td>
			<br/>
	 <a href="javascript:submitform()">Change</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

	 <a href="AgentViewCancelChangeStatus.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Cancel</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
</tr>
 

	
	</table>
	<br />
		    </div>
		    <!-- Close DIV method-send -->
		</div>
		<!-- Close DIV sidebar-content -->
		<div class="sidebar-footer">
				        
		</div>
		<!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="command.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>