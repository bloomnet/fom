<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
		<!-- Close DIV Overlay wrapper -->
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/headerManager.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
	    <div class="manager-area">
			<c:import url="/WEB-INF/jsp/includes/managerArea.jsp" >
			</c:import>
        </div>
	    <div class="manager-sidebar">
            <c:import url="/WEB-INF/jsp/includes/managerSidebar.jsp" >
            </c:import>
	    </div> <!-- Close DIV manager-sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
	        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
	        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
     <!--    
     	<c:if test="${not empty paginationLinks}">
			<c:forEach var="pageLink" items="${ paginationLinks }" varStatus="counter" >
				&nbsp;<a href="<c:out value='${ pageLink }' />"><c:out value='${ (counter.index+1) }' /></a>&nbsp;
			</c:forEach>
		</c:if>
		 -->
        </div> <!-- Close DIV footer -->
    </div> <!-- Close DIV container -->  
    <div id="dialog" title="Confirmation Required" /> 
	</body>
</html>