<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="sendingShop" scope="page" value="${MASTERORDER.sendingShop}" />
<c:choose>
<c:when test="${empty sendingShop.shopName}">
<div class="headline">
You have no assignments
</div> <!-- Close DIV headline -->
    <div class="sidebar-content">
    </div> <!-- Close DIV sidebar-content -->
	<div class="sidebar-footer">
	</div> <!-- Close DIV sidebar-footer -->
</c:when>
<c:otherwise>
<div class="headline">
Recommended Florists
</div> <!-- Close DIV headline -->
    <div class="sidebar-content">
      <div class="florist-list">
    <ol type="1">
    <c:forEach var="rf" items="${recommendedFlorists}"> 
     <li>
        <a href="AgentView.htm?query=<c:out value="${rf.shopId}" />&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><c:out value="${rf.shopName}" /></a><br />
         <c:out value="${rf.shopAddress1}" /><br /> 
         <c:if test='${ ( fn:trim(rf.shopAddress2) != null ) && ( fn:trim(rf.shopAddress2) != "" ) }'>
         	<c:out value="${rf.shopAddress2}" /><br />
         </c:if>
         <c:out value="${rf.city.name}" />, <c:out value="${rf.city.state.shortName}" /> <c:out value="${rf.zip.zipCode}" /><br />
         <c:out value="${rf.shopPhoneFormatted}" /><br />
         <a href="AgentView.htm?query=<c:out value="${rf.shopId}" />&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />" class="underlined">Contact Florist</a><br />
     </li>
    </c:forEach> 
    </ol> <!-- Close OL products -->
      </div> <!-- Close DIV florist-list -->
    </div> <!-- Close DIV sidebar-content -->
	<div class="sidebar-footer">
		<a href="AgentViewAddFlorist.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/add-florist-small.png" alt="Add Florist" class="bottom-buttons" /></a>
	</div> <!-- Close DIV sidebar-footer -->
</c:otherwise>
</c:choose>