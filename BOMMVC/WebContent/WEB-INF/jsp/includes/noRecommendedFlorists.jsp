<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="headline">Choosing a Florist</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="choosing-a-florist">
		<strong> No recommended florists<br /> are available <br />
		<br />
		<br /> 
		</strong>
	</div>
	<!-- Close DIV choose-florist -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer">
  	<div id="addFloristButton"><a href="#"><img src="images/add-florist-small.png" alt="Add a Florist" class="bottom-buttons" /></a></div>
		
</div>
<!-- Close DIV sidebar-footer -->

