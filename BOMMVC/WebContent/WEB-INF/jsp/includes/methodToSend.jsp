<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="headline">Method to Send</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="method-send">
		<ul>
			<li><input name="" type="radio" /><label>Electronically</label></li>
			<li><input name="" type="radio" checked="checked"/><label>Read Over Phone</label></li>
			<li><input name="" type="radio" /><label>Fax</label></li>
		</ul>
		<a href="AgentViewSelectPaymentType.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/next-btn.png" alt="Next" /></a>
	</div>
	<!-- Close DIV method-send -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer"></div>
<!-- Close DIV sidebar-footer -->
