<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="headline">Order Search</div>
<!-- Close DIV headline -->
<div class="order-search">
	<ul>
		<li><strong>Search By</strong>
		</li>
		<li>
		<input type="text" id="orderNumber"
			class="field large"
			onfocus="if (this.value==$orderNumberDefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$orderNumberDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
		<input type="text" id="occassion" 
			class="field large"
			onfocus="if (this.value==$occassionDefaultValue) {this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$occassionDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li><input type="text" id="sendingShop" 
			class="field large"
			onfocus="if (this.value==$sendingShopDefaultValue ){this.value=''};this.style.color='#000'"
			onblur="if (this.value==''){this.value=$sendingShopDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
		<input type="text" id="fulfillingShop" 
		    class="field large"
			onfocus="if (this.value==$fulfillingShopDefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$fulfillingShopDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
		<input type="text" id="zipCode" 
			class="field"
			onfocus="if (this.value==$zipCodeDefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$zipCodeDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
		<input type="text" id="date" 
		    class="field"
			onfocus="if (this.value==$dateDefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$dateDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
			<input type="text" id="date2" 
		    class="field"
			onfocus="if (this.value==$date2DefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$date2DefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
		<input type="text" id="status" 
			class="field"
			onfocus="if (this.value==$statusDefaultValue ){this.value='';this.style.color='#000'}"
			onblur="if (this.value==''){this.value=$statusDefaultValue;this.style.color='#c7c6c6'}" />
		</li>
		<li>
			<a href="#" id="adminSearchButton">
				<img src="images/search-btn.png" alt="Search" />
			</a>
		</li>
	</ul>
</div>

