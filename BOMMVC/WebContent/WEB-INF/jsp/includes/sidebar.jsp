<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="recommendedFlorists" scope="session" value="${MASTERORDER.recommendedFlorists}" />
<c:set var="orderMessages"       scope="session" value="${MASTERORDER.orderMessages}" />

<c:choose>

	<c:when test="${fn:length(MASTERORDER.outboundOrderNumber) > 0 && empty orderMessages}">
		<div class="headline">&nbsp;</div>
	    <!-- Close DIV headline -->
	    <div class="sidebar-content">
	        <div class="order-no-action">
	            <ul>
	                <li><strong>This order does not currently require any further action</strong></li>
	                <br /> <br />
	                <li><a href="AgentViewRejectPhoneOrder.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Order Rejected By Phone</a></li>
	            </ul>
	        </div>
	        <!-- Close DIV order-sent -->
	    </div>
	</c:when>

	<c:when test="${not empty recommendedFlorists && empty orderMessages && !MASTERORDER.complete}">
		<c:import url="/WEB-INF/jsp/includes/recommendedFlorists.jsp">
		</c:import>
	</c:when>
	
    <c:when test="${  not empty orderMessages  }">
        <c:import url="/WEB-INF/jsp/includes/messagesView.jsp">
        </c:import>
    </c:when>

	<c:when test="${MASTERORDER.complete}">
	    <div class="headline">&nbsp;</div>
	    <!-- Close DIV headline -->
	    <div class="sidebar-content">
	        <div class="order-sent">
	            <ul>
	                <li><strong>This Order Has Been Sent</strong></li>
	                <li>Commitment to Coverage</li>
	                <li>Order: #<c:out value="${MASTERORDER.outboundOrderNumber}" /></li>
	                <c:if test="${MASTERORDER.bean.commMethod != 2}">
	                <li>BloomNet Order: #<c:out value="${MASTERORDER.bloomnetOrderNumber}" /></li>
	                </c:if>
	            </ul>
	        </div>
	        <!-- Close DIV order-sent -->
	    </div>
	    <!-- Close DIV sidebar-content -->
	    <div class="sidebar-footer">
	        <a href="default.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">
	          <img src="images/exit-order.png" alt="Exit Order" class="exit-order" />
	        </a>
	    </div>
	</c:when>
	
	<c:otherwise>
		<c:import url="/WEB-INF/jsp/includes/noRecommendedFlorists.jsp">
		</c:import>
	</c:otherwise>

</c:choose>
