<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<c:set var="orderMessages"       scope="session" value="${MASTERORDER.orderMessages}" />
</head>

<body>
<br />

	<table border="0">
		<tr>
			<th>Messages</th>
		</tr>
		<tr>
		<td>

			<%if(request.getAttribute("message") != null){ %>
				<%= request.getAttribute("message") %>
			<%} %>

		
		</td>
	 </tr>
		</table><br />
 	<div class="messages-section">
	  <c:forEach var="m" items="${orderMessages}" varStatus="status" >
		<ul>
			<li>
				<strong><c:out value="${ m.messagetype.description }" /></strong>
				<br /> <br />
				
				<fmt:formatDate value="${m.orderactivity.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${m.orderactivity.createdDate}" pattern="MM/dd/yyyy" />
				<br /><br />
				
				
				<c:out value='${ fn:replace(fn:replace(m.messageText, "%0A", ""), "%0D", "") }' />
				<br /> <br />
				    <a href="AgentViewMessageReply.htm?query=<c:out value="${ m.orderActivityId }" />&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Reply</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    
					 <a href="#" onclick="readMessage( <c:out value="${ m.orderActivityId }" />)">Mark as read</a><br />
					 		
				<br />
			</li>
		</ul>
	  </c:forEach>	
	  </div>

	
</body>

</body>
</html>