<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<div class="headline">You Are Contacting...</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="florist-info">
	    <a href="#"><c:out value="${contactFlorist.shopName}" /></a><br />
	    <c:out value="${contactFlorist.shopAddress1}" /><br /> 
	    <c:out value="${contactFlorist.city.name}" />, <c:out value="${contactFlorist.city.state.name}" /> <c:out value="${contactFlorist.zip.zipCode}" /><br />
	    <c:out value="${contactFlorist.shopPhone}" /><br /><br />
	    <br />
        <a href="AgentViewEditFlorist.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />" >Edit Florist</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onclick='confirmCoverageRemoval(<c:out value="${MASTERORDER.bean.recipientZipCode}" />,"<c:out value="${contactFlorist.shopName}" />")' >Remove Coverage</a>
 	<div class="send-order">
		<a href="AgentViewSelectSendMethod.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/send-order.png" alt="Send Order" /></a>
	</div>
		<!-- Close DIV send-order -->
	</div>
	<!-- Close DIV florist-info -->
	<div class="choose-florist">
		<form>
			<h3>Choose another florist</h3>
			<br />
			<ul>
				<li><select name="Reason" class="field-width">
					<option></option>
					<option>Out of Delivery Area</option>
					<option>Cannot Create Arrangement</option>
					<option>Order Value Too Low</option>
					<option>Too Late for Today</option>
					<option>No Wire Service</option>
				</select>
				</li>
				<li><input name="text" type="text" class="field-width" /></li>
				<li><input name="" type="checkbox" value="" /><label>Do Not Contact</label></li>
			</ul>
			<br /> 
			<br /> 
            <input type="image" src="images/more-florists.png" alt="More Florists" name="MoreFlorists" value="more florists" />
            <input type="image" src="images/add-florist-small.png" alt="Add Florist" name="AddFlorists" value="add florists" />
		</form>
		<div id="shopNotes" >
			<c:forEach items="${ contactFlorist.shopNotes }" var="sn" >
			   <c:out value="${sn.shopNote}" /><br /> 
			</c:forEach>
		</div>
	</div>
	<!-- Close DIV choose-florist -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer"></div>
<!-- Close DIV sidebar-footer -->
