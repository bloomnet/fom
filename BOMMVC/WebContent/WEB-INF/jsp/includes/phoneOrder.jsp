<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="headline">
Phone Order
</div> <!-- Close DIV headline -->
    <div class="sidebar-content">
      <div class="phone-order">
          <ul>
            <li><input name="" type="radio" checked="checked"/><label>BloomNet Clearinghouse</label></li>
            <li class="no-border"><input name="" type="radio" /><label>Credit Card</label></li>
            <li class="no-border">
            <strong>Credit Card Type</strong><br />
              Visa
            </li>
            <li class="no-border">
              <strong>Credit Card Number</strong><br />
              4222-2222-2222-2222
            </li>
            <li class="no-border floatLeft">
              <strong>Exp Date</strong><br />
              09/2012
            </li>
            <li class="no-border floatLeft">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </li>
            <li class="no-border floatLeft">
              <strong>Security Code</strong><br />
              123
            </li>
            <li class="clear last"><input name="" type="radio" /><label>Check</label></li>
            <li class="no-border">
              <strong>Pay To:</strong><br />
              <input type="text" name="text" class="field-width" />
            </li>
            <li class="no-border no-padding">
              <strong>Amount:</strong><br />
              <input type="text" name="text" class="field-width-mid" />
            </li>
          </ul>
      </div><!-- Close DIV phone-order -->
    </div> <!-- Close DIV sidebar-content -->
<div class="sidebar-footer">
      <a href="AgentView.htm?query=emailConfirmation&timetamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/next-btn.png" alt="Next" /></a>
</div> <!-- Close DIV sidebar-footer -->
