<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title>FOM :: BloomNet Order Management</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/colorbox.css" />
	<link rel="stylesheet" type="text/css" href="css/bmt-theme/jquery-ui-1.8.23.custom.css" />
    <script type="text/javaScript" src="js/expand.js"></script>
    <script type="text/javaScript" src="js/bloomnet.js"></script>
    <script type="text/javaScript" src="js/curvycorners.js"></script>
	<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<c:set var="now" scope="session" value="<%=new java.util.Date()%>" />
</head>