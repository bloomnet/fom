<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="headline">&nbsp;</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="order-sent">
		<ul>
			<li><strong>Your Order Has Been Sent!</strong>
			</li>
			<li>Committment to Coverage</li>
			<li>Order: #010</li>
			<li>Bloomnet Order: #100234</li>
		</ul>
	</div>
	<!-- Close DIV order-sent -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer">
	<a href="#"><img src="images/exit-order.png" alt="Exit Order"
		class="exit-order" />
	</a>
</div>
<!-- Close DIV sidebar-footer -->
