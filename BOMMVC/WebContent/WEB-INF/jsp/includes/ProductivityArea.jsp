<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import ="java.util.Date" %>
<div class="headline" ><!-- Close DIV outbound -->
		&nbsp; Manager Console
  		<div class="clear"></div>
	</div> 
	<!-- Close DIV headline -->
<div class="productivity-all-orders">
	
	
		<table border="0">
			
			
			
	
	<tr>
	<td>
	
	<div class="sub-headline">
	<div style="float: left;">Last logged in&nbsp;</div>
	</div>
	

	
	<div class="productivity-table-container" id="orderStaticPane">
      <table border="0" cellspacing="0" cellpadding="0" class="orders-table">
      <tr>
	<br/>
	 <td class="productivity-content-col1" ><fmt:formatDate value="${ lastlogin }" pattern="MM-dd-yy hh:mm:ss" /></td>
	 </tr>
	 </table>
	 </div>
		<br />
		</td>

<td>

		 

<div class="productivity-table-container" id="orderStaticPane">
      <table border="0" cellspacing="0" cellpadding="0" class="orders-table">
      <tr>
	<br/>
	 </tr>
	 </table>
	 </div>
	 

		
		<br />
		</td>
		</tr>
		<tr>
		<td>
		<div class="sub-headline">
	<div style="float: left;">Orders Touched [<c:out value="${ touchedCount }"/>]</div>
		 </div>
	
	
	<div class="productivity-table-container2" id="orderStaticPane">
      <table border="0" cellspacing="0" cellpadding="0" class="orders-table">
      <tr>
	 <td class="productivity-content-col2" >
	 	 <div class="admin-list">
	 
	<ol type="1">
		<c:forEach var="touch" items="${touched}">
		 <li>
			<strong>Order Number:</strong> <c:out value="${touch.id.parentOrderNumber}" /><br />
			<strong>Child Order Number:</strong> <c:out value="${touch.id.childOrderNumber}" /><br />
			<strong>Delivery Date:</strong> <fmt:formatDate value="${ touch.id.deliveryDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			<strong>Status:</strong> <c:out value="${touch.id.status}" /><br />
			<strong>Time to Shop:</strong> <c:out value="${touch.id.timeToShop}" /><br />
			<strong>Last Updated:</strong> <fmt:formatDate value="${ touch.id.parentOrderDate }" pattern="MM-dd-yy hh:mm:ss" /><br /><br />
						<hr /><br/>
			
			</li>
			
		</c:forEach>
		</ol>
		</div>
	 </td>
	 </tr>
	 </table>
	 </div>
		
		
		
		<br />
		</td>
		<td>

<div class="sub-headline">
	<div style="float: left;">Orders Completed [<c:out value="${ ordersCount }"/>]</div>
	</div>
	
	<div class="productivity-table-container2" id="orderStaticPane">
      <table border="0" cellspacing="0" cellpadding="0" class="orders-table">
      <tr>
	
	 <td class="productivity-content-col2" >
	 <div class="admin-list">
    <ol type="1">
		<c:forEach var="order" items="${orders}">
		 <li>
			<strong>Order Number:</strong> <c:out value="${order.id.parentOrderNumber}" /><br />
			<strong>Child Order Number:</strong> <c:out value="${order.id.childOrderNumber}" /><br />
			<strong>Delivery Date:</strong> <fmt:formatDate value="${ order.id.deliveryDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			<strong>Status:</strong> <c:out value="${order.id.status}" /><br />
			<strong>Time to Shop:</strong> <c:out value="${order.id.timeToShop}" /><br />
			<strong>Last Updated:</strong> <fmt:formatDate value="${ order.id.parentOrderDate }" pattern="MM-dd-yy hh:mm:ss" /><br /><br />
						<hr /><br/>
			
			</li>
			
		</c:forEach>
		</ol>
		</div>
	 </td>
	 </tr>
	 </table>
	 </div>
		<br />
		</td>
		</tr>
		
		<tr>
		<td colspan="2">
		<div class="sub-headline2" style="display: none;">

		<br />
		 </td></tr>
		</table>
		
		
		</div>
		
						<br />
				<br />
	<center>
		<c:forEach var="messages" items="order">
			<font color="red" face="Arial" size="3"><c:out
					value="${order.messages}" />
			</font>
			<br />
		</c:forEach>
	</center>
				



