<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt" %>

{"results":[

  {  
  "firstName":"<c:out value="${user.firstName}" />",
  "lastName":"<c:out value="${user.lastName}" />",
  "email":"<c:out value="${user.email}" />",
  "password":"<c:out value="${user.password}" />",
  "userName":"<c:out value="${user.userName}" />",
  "active":"<c:out value="${user.active}" />"
 
}]}
