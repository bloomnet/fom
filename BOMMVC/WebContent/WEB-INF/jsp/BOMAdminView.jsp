<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>

<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
<body>
	<div id="container">
		<div id="header">
			<c:import url="/WEB-INF/jsp/includes/BOMAdminheader.jsp">
			</c:import>
		</div>
		<!-- Close DIV header -->
		<div class="content-container">
		<div class="admin-area">
		<c:import url="/WEB-INF/jsp/includes/adminArea.jsp" >
			</c:import>
        </div>
				
	    <div class="admin-sidebar">
            <c:import url="/WEB-INF/jsp/includes/BOMAdminsidebar.jsp" >
            </c:import>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
		<div class="content-footer">
			<c:import url="/WEB-INF/jsp/includes/footer.jsp">
			</c:import>
		</div>
		<!-- Close DIV content-footer -->
		<div id="footer"></div>
		<!-- Close DIV footer -->
	</div>
	</div>
</body>
</html>