<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
		<!-- Close DIV Overlay wrapper -->
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
			<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
			</c:import>
        </div>
	    <div class="sidebar">
            <c:import url="/WEB-INF/jsp/includes/sidebar.jsp" >
            </c:import>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
                <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
                </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
    </div> <!-- Close DIV container -->  
	</body>
</html>